package com.atlassian.plugin.cache.filecache.impl;

import com.atlassian.plugin.cache.filecache.Cache;

import java.io.IOException;
import java.io.OutputStream;

public class MockStreamProvider implements Cache.StreamProvider
{
    final String contents;
    final RuntimeException re;
    int callCount = 0;

    public MockStreamProvider(String contents)
    {
        this(contents, null);
    }

    public MockStreamProvider(String contents, RuntimeException re)
    {
        this.contents = contents;
        this.re = re;
    }

    @Override
    public void write(OutputStream out)
    {
        callCount++;
        try
        {
            out.write(contents.getBytes());
            if (re != null)
            {
                throw re;
            }
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}