package com.atlassian.plugin.cache.filecache.impl;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import static com.atlassian.plugin.webresource.TestUtils.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestFileCacheImpl
{
    File tmpDir;

    @Before
    public void setUp() throws Exception
    {
        tmpDir = File.createTempFile("TestCachedFile", "test");
        tmpDir.delete();
        tmpDir.mkdirs();
        setField(FileCacheImpl.class, "filenameCounter", new AtomicLong(0));
    }

    @Test
    public void normalOperationsForOneStream() throws Exception
    {
        FileCacheImpl cache = new FileCacheImpl(tmpDir, 100);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        MockStreamProvider in1 = new MockStreamProvider("contents1");
        cache.cache("bucket", "key1", out, in1);
        assertEquals(1, in1.callCount);
        assertEquals("contents1", new String(out.toByteArray()));
        assertCacheFile("contents1", 1);

        out.reset();
        cache.cache("bucket", "key1", out, in1);
        assertEquals(1, in1.callCount);
        assertEquals("contents1", new String(out.toByteArray()));
        assertCacheFile("contents1", 1);

        MockStreamProvider in2 = new MockStreamProvider("contents2");
        out.reset();
        cache.cache("bucket", "key2", out, in2);
        assertEquals(1, in2.callCount);
        assertEquals("contents2", new String(out.toByteArray()));
        assertCacheFile("contents1", 1);
        assertCacheFile("contents2", 2);
    }

    @Test
    public void normalOperationsForTwoStreams() throws Exception
    {
        FileCacheImpl cache = new FileCacheImpl(tmpDir, 100);
        final ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        final ByteArrayOutputStream out2 = new ByteArrayOutputStream();

        MockTwoStreamsProvider in1 = new MockTwoStreamsProvider("a", "b");
        cache.cacheTwo("bucket", "key1", out1, out2, in1);
        assertEquals(1, in1.callCount);
        assertEquals("a", new String(out1.toByteArray()));
        assertEquals("b", new String(out2.toByteArray()));
        assertCacheFile("a", 1);
        assertCacheFile("b", 2);

        out1.reset();
        out2.reset();
        cache.cacheTwo("bucket", "key1", out1, out2, in1);
        assertEquals(1, in1.callCount);
        assertEquals("a", new String(out1.toByteArray()));
        assertEquals("b", new String(out2.toByteArray()));
        assertCacheFile("a", 1);
        assertCacheFile("b", 2);

        MockTwoStreamsProvider in2 = new MockTwoStreamsProvider("c", "d");
        out1.reset();
        out2.reset();
        cache.cacheTwo("bucket", "key2", out1, out2, in2);
        assertEquals(1, in2.callCount);
        assertEquals("c", new String(out1.toByteArray()));
        assertEquals("d", new String(out2.toByteArray()));
        assertCacheFile("a", 1);
        assertCacheFile("b", 2);
        assertCacheFile("c", 3);
        assertCacheFile("d", 4);
    }

    @Test
    public void eviction() throws Exception
    {
        FileCacheImpl cache = new FileCacheImpl(tmpDir, 0); // zero tells map-maker to evict immediately

        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        MockStreamProvider in1 = new MockStreamProvider("contents1");
        cache.cache("bucket", "key1", out, in1);
        assertEquals(1, in1.callCount);
        assertEquals("contents1", new String(out.toByteArray()));
        assertCacheFileAbsent(1);

        out.reset();
        cache.cache("bucket", "key1", out, in1);
        assertEquals(2, in1.callCount);
        assertEquals("contents1", new String(out.toByteArray()));
        assertCacheFileAbsent(1);
    }

    @Test
    public void missingInitialDirectory() throws IOException
    {
        tmpDir.delete();
        assertFalse(tmpDir.exists());
        new FileCacheImpl(tmpDir, 100);
        assertTrue(tmpDir.exists());
        assertTrue(tmpDir.isDirectory());
    }

    @Test
    public void clearFilesInConstructor() throws IOException
    {
        File f1 = new File(tmpDir, "1" + FileCacheImpl.EXT);
        f1.createNewFile();
        File f2 = new File(tmpDir, "2" + FileCacheImpl.EXT);
        f2.createNewFile();

        assertTrue(f1.isFile());
        assertTrue(f2.isFile());
        new FileCacheImpl(tmpDir, 100);
        assertTrue(tmpDir.exists());
        assertTrue(tmpDir.isDirectory());
        assertFalse(f1.isFile());
        assertFalse(f2.isFile());
    }

    @Test
    public void clearCache() throws Exception
    {
        FileCacheImpl cache = new FileCacheImpl(tmpDir, 100);

        MockStreamProvider in1 = new MockStreamProvider("contents1");
        cache.cache("bucket", "key1", new ByteArrayOutputStream(), in1);
        assertCacheFile("contents1", 1);

        cache.clear();
        assertCacheFileAbsent(1);

        // recreate with same key -- should get a different file number
        cache.cache("bucket", "key1", new ByteArrayOutputStream(), in1);
        assertCacheFile("contents1", 2);
    }

    @Test
    public void sharedFilenameCounter() throws Exception
    {
        final FileCacheImpl cache1 = new FileCacheImpl(tmpDir, 100);
        final FileCacheImpl cache2 = new FileCacheImpl(tmpDir, 100);

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final MockStreamProvider in1 = new MockStreamProvider("contents1");
        final MockStreamProvider in2 = new MockStreamProvider("contents2");

        cache1.cache("bucket", "key1", out, in1);
        cache2.cache("bucket", "key2", out, in2);

        assertCacheFile("contents1", 1);
        assertCacheFile("contents2", 2);
    }

    private void assertCacheFile(String contents, int fcount) throws IOException
    {
        File f = new File(tmpDir, fcount + FileCacheImpl.EXT);
        assertTrue(f.exists());
        assertTrue(f.isFile());
        assertEquals(contents, FileUtils.readFileToString(f));
    }

    private void assertCacheFileAbsent(int fcount) throws IOException
    {
        File f = new File(tmpDir, fcount + FileCacheImpl.EXT);
        assertFalse(f.exists());
    }
}
