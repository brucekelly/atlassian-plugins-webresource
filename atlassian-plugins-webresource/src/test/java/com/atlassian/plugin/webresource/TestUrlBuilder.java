package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import org.junit.Test;

public class TestUrlBuilder
{
    @Test
    public void shouldAcceptNullAsHashName()
    {
        (new DefaultUrlBuilder()).addToHash("", null);
    }
}
