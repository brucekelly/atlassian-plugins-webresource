package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;

import java.util.HashMap;

public final class ConditionTestUtils
{
    public static DecoratingCondition decorate(UrlReadingCondition urlReadingCondition)
    {
        return new DecoratingUrlReadingCondition(urlReadingCondition, new HashMap<String, String>());
    }

    public static DecoratingCondition decorate(Condition condition)
    {
        return new DecoratingLegacyCondition(condition);
    }
}
