package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.plugin.webresource.impl.http.Router;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;

import static com.atlassian.plugin.webresource.TestUtils.createGlobals;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.joinWithSlashWithoutEmpty;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUrlPrefix
{
    @Test
    public void testUrlPrefixWithNullCdnStrategy()
    {
        assertEquals("/my-prefix/hash1-CDN/v1/download/blah/a.js", urlFor(null, "/blah/a.js", true, true, "hash1", "v1"));
    }

    @Test
    public void testStaticUrlWithNoCdnStrategy()
    {
        assertEquals("/my-prefix/hash1-CDN/v1/download/blah/a.js", urlFor(noCdn(), "/blah/a.js", true, true, "hash1", "v1"));
    }

    @Test
    public void testStaticUrlWithCdnStrategy()
    {
        assertEquals("https://my-cdn.com/cdn-prefix/my-prefix/hash1-CDN/v1/download/blah/a.js", urlFor(cdn(), "/blah/a.js", true, true, "hash1", "v1"));
    }

    @Test
    public void testTaintedStaticUrlWithCdnStrategy()
    {
        assertEquals("/my-prefix/hash1-T/v1/download/blah/a.js", urlFor(cdn(), "/blah/a.js", true, false, "hash1", "v1"));
    }

    @Test
    public void testNoCacheSupportedStaticUrlWithCdnStrategy()
    {
        assertEquals("/my-prefix/download/blah/a.js", urlFor(cdn(), "/blah/a.js", false, true, "hash1", "v1"));
    }

    private CDNStrategy noCdn()
    {
        return new CDNStrategy()
        {
            @Override
            public boolean supportsCdn()
            {
                return false;
            }

            @Override
            public String transformRelativeUrl(String url)
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    private CDNStrategy cdn()
    {
        return new CDNStrategy()
        {
            @Override
            public boolean supportsCdn()
            {
                return true;
            }

            @Override
            public String transformRelativeUrl(String url)
            {
                return "https://my-cdn.com/cdn-prefix" + url;
            }
        };
    }

    private String urlFor(CDNStrategy cdnStrategy, String relativeUrl, boolean isResourceSupportCache,
        boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        Router router = createRouter(cdnStrategy, "/my-prefix");
        return router.buildUrlWithPrefix(relativeUrl, new HashMap<String, String>(), isResourceSupportCache,
            isResourceSupportCdn, resourceHash, resourceVersion);
    }

    private Router createRouter(CDNStrategy cdnStrategy, final String prefix)
    {
        WebResourceIntegration integration = mock(WebResourceIntegration.class);
        when(integration.getCDNStrategy()).thenReturn(cdnStrategy);

        WebResourceUrlProvider urlProvider = mock(WebResourceUrlProvider.class);
        when(urlProvider.getStaticResourcePrefix(anyString(), anyString(), eq(UrlMode.RELATIVE))).thenAnswer(new Answer<Object>()
        {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                String hash = (String) invocationOnMock.getArguments()[0];
                String version = (String) invocationOnMock.getArguments()[1];
                return joinWithSlashWithoutEmpty(prefix, hash, version);
            }
        });
        when(urlProvider.getBaseUrl(UrlMode.RELATIVE)).thenReturn(prefix);

        return createGlobals(integration, urlProvider, Router.class).getRouter();
    }
}
