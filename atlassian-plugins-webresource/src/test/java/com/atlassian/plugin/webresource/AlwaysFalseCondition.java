package com.atlassian.plugin.webresource;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

public class AlwaysFalseCondition implements UrlReadingCondition
{
    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
        urlBuilder.addToQueryString("always-false", "true");
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        return !"true".equals(params.get("always-false"));
    }
}
