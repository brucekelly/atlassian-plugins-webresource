package com.atlassian.plugin.webresource.integration.stub;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HttpServletRequestStub
{
    public static HttpServletRequest build(String path, Map<String, String> params, Map<String, String> headers)
    {
        // Java uses string array as parameter value.
        Map<String, String[]> javaParams = new HashMap<String, String[]>();
        for (Map.Entry<String, String> param : params.entrySet())
        {
            javaParams.put(param.getKey(), new String[] { param.getValue() });
        }

        // Stubbing HTTP request and response.
        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getParameterMap()).thenReturn(javaParams);

        when(request.getRequestURI()).thenReturn(path);

        // Headers.
        for (Map.Entry<String, String> entry : headers.entrySet())
        {
            when(request.getHeader(entry.getKey())).thenReturn(entry.getValue());
            try
            {
                long value = Long.parseLong(entry.getValue());
                when(request.getDateHeader(entry.getKey())).thenReturn(value);
            }
            catch (NumberFormatException e)
            {
            }
        }

        // Forwarding.
        doAnswer(new Answer()
        {
            @Override
            public RequestDispatcher answer(InvocationOnMock invocation) throws Throwable
            {
                final String location = (String) invocation.getArguments()[0];
                return new RequestDispatcher()
                {
                    @Override
                    public void forward(ServletRequest request, ServletResponse response)
                            throws ServletException, IOException
                    {
                        ((HttpServletResponse) response).sendRedirect(location);
                    }

                    @Override
                    public void include(ServletRequest request, ServletResponse response)
                    {
                        throw new RuntimeException("not implemented");
                    }
                };
            }
        }).when(request).getRequestDispatcher(anyString());

        return request;
    }
}
