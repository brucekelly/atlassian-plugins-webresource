package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.createGlobals;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestBatchPluginResource
{
    private static final String JS_WRAP_ANNOTATION_PREFIX = ";\ntry";
    @Mock
    private WebResourceUrlProvider urlProvider;
    private Globals globals;
    private WebResource wr;

    @Before
    public void setUp() throws Exception
    {
        wr = new WebResourceImpl();

        MockitoAnnotations.initMocks(this);
        globals = createGlobals();
    }

    // Have no idea why, but it is the way the old implementation works.
    @Test
    public void shouldHaveDifferentSubBatchSortingThanContextBatch()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .condition(AlwaysFalseCondition.class)
                    .resource("a1.js")
                        .param("media", "print")
                    .resource("a2.js")
        .end();

        wr.requireResource("plugin:a");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:a", "js", buildMap("always-false", "true", "media", "print")),
            webResourceBatchUrl("plugin:a", "js", buildMap("always-false", "true"))
        ));
    }

    @Test
    public void testGetUrl()
    {
        wr.configure()
            .plugin("test.plugin")
                .webResource("webresources")
                    .resource("a.js")
        .end();

        wr.requireResource("test.plugin:webresources");

        assertThat(wr.paths(), matches(
            "/download/batch/test.plugin:webresources/test.plugin:webresources.js"
        ));
    }

    @Test
    public void testGetUrlWithParams()
    {
        wr.configure()
            .plugin("test.plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("webresources")
                    .condition(AlwaysTrueCondition.class)
                    .transformation("js", "AddLocation")
                    .resource("a.js")
                        .param("conditionalComment", "lt IE 9")
                        .param("foo", "bar")
        .end();

        wr.requireResource("test.plugin:webresources");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            "/download/batch/test.plugin:webresources/test.plugin:webresources.js?always-true=true&conditionalComment=lt+IE+9&location=true"
        ));

        assertThat(paths, not(matches(
            "foo"
        )));
    }

//    // disabling it, it is the same as previous.
//    @Test
//    public void testGetUrlWithParamsAndBatchableParams()
//    {
//        final Map<String, String> params = new TreeMap<String, String>();
//        params.put("conditionalComment", "lt IE 9");
//        params.put("foo", "bar");
//
//        final Map<String, String> batchableParams = new TreeMap<String, String>();
//        params.put("condition1", "true");
//        params.put("transform1", "123");
//
//        final BatchPluginResource resource = new BatchPluginResource(globals, "test.plugin:webresources", "js", UrlParameters.of(true, emptyHashes(), batchableParams), params,
//                new BatchedWebResourceDescriptor("2.0", "a.b.c.d:e"));
//        assertEquals("/download/batch/test.plugin:webresources/test.plugin:webresources.js?condition1=true&conditionalComment=lt+IE+9&foo=bar&transform1=123", resource.getUrl());
//    }

    @Test
    public void testGetUrlWithParamsAndBatchableParamsPreservesOrdering()
    {
        String url = globals.getRouter().contextBatchUrl("general", "js", ImmutableMap.of("cc", "1", "aa", "1"), false, false, "", "");
        assertTrue(url.endsWith("?aa=1&cc=1"));
    }

    @Test
    public void testIsCacheSupported() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                        .param("cache", "false")
                    .resource("a2.js")
        .end();

        wr.requireResource("plugin:a");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            webResourceBatchUrl("plugin:a", "js", buildMap("cache", "false")),
            webResourceBatchUrl("plugin:a", "js")
        ));
        assertThat(paths.get(0), not(containsString("/s/")));
        assertThat(paths.get(1), containsString("/s/"));
        assertThat(paths.get(1), not(containsString("cache")));
    }

//    Test outdated, removing it.
//    @Test
//    public void testEquals()
//    {
//        final String moduleKey = "test.plugin:webresources";
//        final String type = "js";
//
//        final Map<String, String> params1 = new TreeMap<String, String>();
//        params1.put("key", "value");
//        params1.put("foo", "bar");
//
//        final BatchPluginResource batch1 = new BatchPluginResource(globals, moduleKey, type, UrlParameters.of(true), params1,
//                new BatchedWebResourceDescriptor("2.0", "a.b.c.d:e"));
//        final Map<String, String> params2 = new TreeMap<String, String>();
//        params2.put("key", "value");
//        params2.put("foo", "bar");
//        final BatchPluginResource batch2 = new BatchPluginResource(globals, moduleKey, type, UrlParameters.of(true), params2,
//                new BatchedWebResourceDescriptor("2.0", "a.b.c.d:e"));
//
//        final Map<String, String> params3 = new TreeMap<String, String>();
//        params3.put("key", "value");
//        params3.put("foo", "bart");
//        final BatchPluginResource batch3 = new BatchPluginResource(globals, moduleKey, type, UrlParameters.of(true), params3,
//                new BatchedWebResourceDescriptor("2.0", "a.b.c.d:e"));
//
//        assertEquals(batch1, batch2);
//        assertNotSame(batch1, batch3);
//    }

    // old name testDefaultJavascriptConfigurationStreamingHttpResponse and testDefaultJavascriptConfigurationStreamingOutputStream
    @Test
    public void shouldServeJsBatchWithAnnotations()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("a.js")
                    .resource("b.js")
        .end();

        assertEquals(
            wrapWithJsAnnotations("content of a.js", "app:page", "a.js") +
            "\n" +
            wrapWithJsAnnotations("content of b.js", "app:page", "b.js"),
            wr.getContent(webResourceBatchUrl("app:page", "js"))
        );
    }

    @Test
    public void shouldWrapJsInTryCatchBlockIfEnabled()
    {
        WebResource wr = new WebResourceImpl();
        wr.configure()
            .enableJavaScriptTryCatchWrapping()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js", "var a = 0;")
        .end();
        String content = wr.getContent(webResourceBatchUrl("plugin:a", "js"));
        assertThat(content, startsWith(JS_WRAP_ANNOTATION_PREFIX));
        assertThat(content, matches("var a = 0;"));
        assertJsRunsWithoutErrors(content);
    }

    // old name testTryCatchJavascriptWrappingNotSet
    @Test
    public void shouldNotWrapWithTryCatchBlockIfNotEnabled()
    {
        WebResource wr = new WebResourceImpl();
        wr.configure()
            .disableJavaScriptTryCatchWrapping()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js", "var a = 0;")
        .end();
        String content = wr.getContent(webResourceBatchUrl("plugin:a", "js"));
        assertThat(content, not(startsWith(JS_WRAP_ANNOTATION_PREFIX)));
        assertThat(content, matches("var a = 0;"));
    }

    // old name testTryCatchJavascriptWrappingIgnoredForWrongType
    @Test
    public void shouldIgnoreTryCatchBlockForNonJsBatch()
    {
        WebResource wr = new WebResourceImpl();
        wr.configure()
            .enableJavaScriptTryCatchWrapping()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.css")
        .end();
        String content = wr.getContent(webResourceBatchUrl("plugin:a", "css"));
        assertThat(content, not(startsWith(JS_WRAP_ANNOTATION_PREFIX)));
        assertThat(content, matches("content of a1.css"));
    }

    private void assertJsRunsWithoutErrors(String js)
    {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");

        try
        {
            jsEngine.eval(js);
        }
        catch (ScriptException e)
        {
            throw new RuntimeException(e);
        }
    }

    private String wrapWithJsAnnotations(String content, String completeKey, String resourceName)
    {
        return
            ";\n/* module-key = '" + completeKey + "', location = '" + resourceName + "' */\n" +
            content +
            ";";
    }
}
