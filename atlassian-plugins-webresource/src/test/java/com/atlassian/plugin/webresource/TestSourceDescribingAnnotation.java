package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import org.junit.Test;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestSourceDescribingAnnotation extends TestCase
{
    @Test
    public void shouldAnnotateJavaScript()
    {
        assertAnnotates("js", "function myFunction() { alert(\"Hello World!\"); }");
    }

    @Test
    public void shouldAnnotateCssResource()
    {
        assertAnnotates("css", "body { font-family: arial;\n }");
    }

    @Test
    public void shouldCorrectlyAnnotateMultilineSource()
    {
        assertAnnotates("js", "function myFunction() { \n alert(\"Hello World!\"); \n }");
    }

    private void assertAnnotates(String extension, String source)
    {
        String annotated = "/* module-key = 'plugin:a', location = 'a1." + extension + "' */\n" + source;
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1." + extension, source)
        .end();
        String content = wr.getContent(webResourceBatchUrl("plugin:a", extension));
        assertThat(content, matches(annotated));
    }
}