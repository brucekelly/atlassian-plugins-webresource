package com.atlassian.plugin.webresource;

import java.io.ByteArrayOutputStream;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import junit.framework.TestCase;

public class TestTryCatchJsResourceContentAnnotator extends TestCase
{
    public void testGoodSyntax() throws Exception
    {
        ResourceContentAnnotator annotator = new TryCatchJsResourceContentAnnotator();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        annotator.before(null, stream);
        stream.write("6 * 2;\n".getBytes());
        annotator.after(null, stream);

        String tryCatchBlock = new String(stream.toByteArray());
        assertTrue(tryCatchBlock.startsWith("try"));

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine jsEngine = mgr.getEngineByName("JavaScript");
        try
        {
            Object eval = jsEngine.eval(tryCatchBlock);
            assertEquals(12.0, ((Number) eval).doubleValue());
        }
        catch (ScriptException ex)
        {
            fail("Exception running the created javascript: " + ex);
        }
    }
}
