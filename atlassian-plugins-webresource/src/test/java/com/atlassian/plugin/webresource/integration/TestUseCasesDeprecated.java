package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImplDeprecated;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

// Tests for base use cases, happy path, for deprecated implementation.
// It also includes cases from `TestUseCases` executed against deprecated Web Resource implementation.
public class TestUseCasesDeprecated extends TestUseCases {
    @Before
    public void before() throws Exception
    {
        wr = new WebResourceImplDeprecated();
    }

    public static class FalseCondition implements Condition
    {
        public void init(Map<String, String> params) {}

        public boolean shouldDisplay(Map<String, Object> stringObjectMap)
        {
            return false;
        }
    }

    @Test
    public void conditionShouldAffectDependencies()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a.js", "content of a")
                .webResource("b")
                    .dependency("plugin:a")
                    .condition(FalseCondition.class)
                    .resource("b.js", "content of b")
        .end();

        wr.requireResource("plugin:b");

        assertThat(wr.paths().size(), equalTo(0));
    }

    @Test
    public void shouldNotIncludeDependenciesFromNotSatisfiedConditionsComplexCase()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("general")
                    .dependency("plugin:b")
                    .dependency("plugin:c")
                    .resource("a1.js")
                .webResource("b")
                    .condition(AlwaysFalseCondition.class)
                    .dependency("plugin:d")
                    .dependency("plugin:e")
                    .resource("b1.js")
                .webResource("c")
                    .dependency("plugin:d")
                    .resource("c1.js")
                .webResource("d")
                    .resource("d1.js")
                .webResource("e")
                    .resource("e1.js")
        .end();

        wr.requireContext("general");

        assertThat(wr.paths(), matches(
            contextBatchUrl("general", "js")
        ));

        String content = wr.getContent();

        assertThat(content, matches(
            "content of d1",
            "content of c1",
            "content of a1"
        ));
        assertThat(content, not(matches(
            "content of b1"
        )));
        assertThat(content, not(matches("content of e1")));
    }

    @Test
    public void shouldIncludeSuperbatchResources()
    {
        wr.configure()
            .addToSuperbatch("plugin:a")
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .context("general")
                    .dependency("plugin:a")
                    .resource("b1.js")
            .end();

        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory().create().includeSuperbatchResources(true).build();
        webResourceAssembler.resources().requireContext("general");

        assertThat(toUrls(webResourceAssembler.assembled().drainIncludedResources().getResources()), matches(
            "_super",
            "general,-_super"
        ));
    }

    @Test
    public void shouldExcludeSuperbatchResources()
    {
        wr.configure()
            .addToSuperbatch("plugin:a")
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .context("general")
                    .dependency("plugin:a")
                    .resource("b1.js")
            .end();

        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory().create().includeSuperbatchResources(true).build();
        Set<String> excluded = new HashSet<String>();
        excluded.add("_super");
        webResourceAssembler.resources().exclude(new HashSet<String>(), excluded);
        webResourceAssembler.resources().requireContext("general");

        assertThat(toUrls(webResourceAssembler.assembled().drainIncludedResources().getResources()), matches(
            "general,-_super"
        ));
    }

    @Test
    public void shouldIncludeSuperbatchResourcesWithoutSuperbatch()
    {
        wr.configure()
            .addToSuperbatch("plugin:a")
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .context("general")
                    .dependency("plugin:a")
                    .resource("b1.js")
            .end();

        WebResourceAssembler webResourceAssembler = wr.getWebResourceAssemblerFactory().create().includeSuperbatchResources(false).build();
        webResourceAssembler.resources().requireContext("general");

        assertThat(toUrls(webResourceAssembler.assembled().drainIncludedResources().getResources()), matches(
            "general"
        ));
    }

    private List<String> toUrls(Iterable<WebResource> webResources)
    {
        List<String> urls = new ArrayList<String>();
        for (WebResource webResource : webResources)
        {
            PluginUrlResource pluginUrlResource = (PluginUrlResource) webResource;
            urls.add(pluginUrlResource.getStaticUrl(UrlMode.RELATIVE));
        }
        return urls;
    }
}
