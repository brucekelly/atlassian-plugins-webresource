package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.UrlReadingContentTransformer;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;

public abstract class ContentTransformerHelper implements ContentTransformerFactory
{
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
    {
        return new TransformerUrlBuilder()
        {
            public void addToUrl(UrlBuilder urlBuilder)
            {
            }
        };
    }

    public UrlReadingContentTransformer makeResourceTransformer(TransformerParameters parameters)
    {
        final ContentTransformerHelper _this = this;
        return new UrlReadingContentTransformer()
        {
            @Override
            public Content transform(Content content, ResourceLocation resourceLocation, QueryParams params,
                    String sourceUrl)
            {
                return _this.transform(content, resourceLocation, params, sourceUrl);
            }
        };
    }

    public abstract Content transform(Content content, ResourceLocation resourceLocation, QueryParams params, String sourceUrl);
}