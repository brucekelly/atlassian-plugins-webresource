package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;

import java.io.IOException;
import java.io.OutputStream;

public class AddLocationStatic implements StaticTransformers
{
    public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder)
    {
        urlBuilder.addToQueryString("location", "true");
    }

    @Override
    public Content transform(final Content content, final TransformerParameters tp,
            final ResourceLocation resourceLocation, final QueryParams params, String sourceUrl)
    {
        return new ContentImpl(content.getContentType(), true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                try
                {
                    if ("true".equals(params.get("location")))
                    {
                        out.write(("location=" + tp.getPluginKey() + ":" + tp.getModuleKey() + "/" + resourceLocation
                                .getName() + " (static)\n").getBytes());
                    }
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
                SourceMap sourceMap = content.writeTo(out, isSourceMapEnabled);
                if (isSourceMapEnabled && (sourceMap != null))
                {
                    sourceMap = Util.offset(sourceMap, 1);
                }
                return sourceMap;
            }
        };
    }
}