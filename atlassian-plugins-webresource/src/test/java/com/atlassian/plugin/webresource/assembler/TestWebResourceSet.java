package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;

import static com.atlassian.plugin.webresource.TestUtils.removeWebResourceLogs;
import static com.atlassian.plugin.webresource.TestUtils.stringDataProvider;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedResources;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.m;
import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;
import static org.junit.Assert.assertEquals;

public class TestWebResourceSet
{
    private AssemblerTestFixture f;

    @Before
    public void setUp() throws Exception
    {
        f = new AssemblerTestFixture();
    }

    @Test
    public void testFilterJs() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("test.atlassian:resource-css").res("css-1.css");
        f.mockPlugin("test.atlassian:resource-both").res("js-1.js", "css-1.css");

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        assembler.resources().requireWebResource("test.atlassian:resource-css");
        assembler.resources().requireWebResource("test.atlassian:resource-both");

        WebResourceSet resourceSet = assembler.assembled().drainIncludedResources();
        Iterable<PluginJsResource> resources = resourceSet.getResources(PluginJsResource.class);
        assertResolvedResources(resources, m("test.atlassian:resource-js"), m("test.atlassian:resource-both"));
    }

    @Test
    public void testFilterCss() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-js").res("js-1.js");
        f.mockPlugin("test.atlassian:resource-css").res( "css-1.css");
        f.mockPlugin("test.atlassian:resource-both").res("js-1.js", "css-1.css");

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        assembler.resources().requireWebResource("test.atlassian:resource-css");
        assembler.resources().requireWebResource("test.atlassian:resource-both");

        WebResourceSet resourceSet = assembler.assembled().drainIncludedResources();
        Iterable<PluginCssResource> resources = resourceSet.getResources(PluginCssResource.class);
        assertResolvedResources(resources, m("test.atlassian:resource-css", "css"), m("test.atlassian:resource-both", "css"));
    }

    @Test
    public void testWriteJs() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-js").res( "js-1.js");

        assembler.resources().requireWebResource("test.atlassian:resource-js");
        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals("<script type=\"text/javascript\" src=\"/download/batch/test.atlassian:resource-js/test.atlassian:resource-js.js\" ></script>", removeWebResourceLogs(writer.toString()));
    }

    @Test
    public void testWriteCss() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-css").res( "css-1.css");

        assembler.resources().requireWebResource("test.atlassian:resource-css");
        StringWriter writer = new StringWriter();
        assembler.assembled().drainIncludedResources().writeHtmlTags(writer, UrlMode.AUTO);

        assertEquals("<link type=\"text/css\" rel=\"stylesheet\" href=\"/download/batch/test.atlassian:resource-css/test.atlassian:resource-css.css\" media=\"all\">", removeWebResourceLogs(writer.toString()));
    }

    @Test
    public void testWriteData()
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:a").data("abc", stringDataProvider("123"));

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"test.atlassian:a.abc\"]=\"\\\"123\\\"\";\n");
    }
}
