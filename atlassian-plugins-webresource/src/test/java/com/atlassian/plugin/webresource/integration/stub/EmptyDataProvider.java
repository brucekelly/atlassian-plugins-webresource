package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import static com.atlassian.plugin.webresource.TestUtils.jsonableOf;

public class EmptyDataProvider implements WebResourceDataProvider
{

    @Override
    public Jsonable get()
    {
        return jsonableOf("nothing");
    }
}
