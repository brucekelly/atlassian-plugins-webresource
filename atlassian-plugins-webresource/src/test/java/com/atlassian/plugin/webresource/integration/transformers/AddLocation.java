package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

public class AddLocation implements WebResourceTransformerFactory
{
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
    {
        return new TransformerUrlBuilder()
        {
            public void addToUrl(UrlBuilder urlBuilder)
            {
                urlBuilder.addToQueryString("location", "true");
                urlBuilder.addToHash("location", "location");
            }
        };
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters)
    {
        return new UrlReadingWebResourceTransformer()
        {
            public DownloadableResource transform(final TransformableResource resource, final QueryParams params)
            {
                return new CharSequenceDownloadableResource(resource.nextResource())
                {
                    protected CharSequence transform(CharSequence originalContent)
                    {
                        if ("true".equals(params.get("location")))
                        {
                            return "location=" + parameters.getPluginKey() + ":" + parameters.getModuleKey() + "/" +
                                    resource.location().getName() + " (transformer)\n" + originalContent;
                        }
                        else
                        {
                            return originalContent;
                        }
                    }
                };
            }
        };
    }
}