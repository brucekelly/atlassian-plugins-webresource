package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class StringDownloadableResource implements DownloadableResource
{
    private final String content;
    private final DownloadableResource originalResource;

    public StringDownloadableResource(DownloadableResource originalResource, String content)
    {
        this.originalResource = originalResource;
        this.content = content;
    }

    public boolean isResourceModified(HttpServletRequest request, HttpServletResponse response)
    {
        return originalResource.isResourceModified(request, response);
    }

    public void serveResource(HttpServletRequest request, HttpServletResponse response) throws DownloadException
    {
        try
        {
            streamResource(response.getOutputStream());
        } catch (IOException e)
        {
            throw new DownloadException(e);
        }
    }

    public void streamResource(OutputStream out) throws DownloadException
    {
        try
        {
            out.write(content.getBytes());
        } catch (IOException e)
        {
            throw new DownloadException(e);
        }
    }

    public String getContentType()
    {
        return originalResource.getContentType();
    }
}