package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptorBuilder;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformers;
import com.atlassian.plugin.webresource.transformer.DefaultStaticTransformersSupplier;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import org.apache.commons.io.IOUtils;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.setField;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

// Web Resource stub using modern implementation.
public class WebResourceImpl extends WebResource
{
    protected PluginResourceLocator resourceLocator;
    protected PluginAccessor pluginAccessor;
    protected WebResourceIntegration integration;
    protected PageBuilderService pageBuilderService;
    protected ResourceBatchingConfiguration config;
    protected PluginResourceDownload pluginResourceDownload;
    protected WebResourceUrlProviderImpl urlProvider;
    protected StaticTransformers staticTransformers;
    protected Map<String, Object> requestCache;
    private DefaultWebResourceAssemblerFactory webResourceAssemblerFactory;

    @Override
    public void requireResource(String key)
    {
        pageBuilderService.assembler().resources().requireWebResource(key);
    }

    public PageBuilderService getPageBuilderService()
    {
        return pageBuilderService;
    }

    @Override
    public void requireContext(String key)
    {
        pageBuilderService.assembler().resources().requireContext(key);
    }

    @Override
    public void include(List<String> keys)
    {
        for (String key : keys)
        {
            if (key.contains(Config.CONTEXT_PREFIX))
            {
                requireContext(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            }
            else
            {
                requireResource(key);
            }
        }
    }

    @Override
    public void exclude(List<String> keys)
    {
        if (keys.isEmpty())
        {
            return;
        }
        LinkedHashSet<String> excludedWebResources = new LinkedHashSet<String>();
        LinkedHashSet<String> excludedContexts = new LinkedHashSet<String>();
        for (String key : keys)
        {
            if (key.contains(Config.CONTEXT_PREFIX))
            {
                excludedContexts.add(key.replaceAll(Config.CONTEXT_PREFIX + ":", ""));
            }
            else
            {
                excludedWebResources.add(key);
            }
        }
        getPageBuilderService().assembler().resources().exclude(excludedWebResources, excludedContexts);
    }

    @Override
    public void requireData(String key, String value)
    {
        pageBuilderService.assembler().data().requireData(key, value);
    }

    // Returns HTML that should be included in the head of the page to include all the required resources.
    @Override
    public String pathsAsHtml()
    {
        StringWriter out = new StringWriter();
        pageBuilderService.assembler().assembled().drainIncludedResources().writeHtmlTags(out, UrlMode.AUTO);
        return out.toString();
    }

    public WebResourceAssemblerFactory getWebResourceAssemblerFactory()
    {
        return webResourceAssemblerFactory;
    }

    @Override
    public void get(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            pluginResourceDownload.serveFile(request, response);
        }
        catch (DownloadException e)
        {
            throw new RuntimeException(e);
        }
    }

    // Configure web resources according to given XML configuration.
    protected void initialize(ConfigurationData configurationData)
    {
        // Mocking external parts.
        pluginAccessor = mock(PluginAccessor.class);

        PluginEventManager eventManager = mock(PluginEventManager.class);

        integration = mock(WebResourceIntegration.class);
        when(integration.getRequestCache()).thenAnswer(new Answer<Object>()
        {
            @Override
            public Object answer(final InvocationOnMock invocationOnMock) throws Throwable
            {
                return requestCache;
            }
        });
        when(integration.getPluginAccessor()).thenReturn(pluginAccessor);
        when(integration.getSystemBuildNumber()).thenReturn("system-build-number");
        when(integration.getSystemCounter()).thenReturn("system-build-counter");
        when(integration.getSuperBatchVersion()).thenReturn("super-batch-version");
        when(integration.getBaseUrl(com.atlassian.plugin.webresource.UrlMode.AUTO)).thenReturn("/app");
        when(integration.getStaticResourceLocale()).thenReturn("en");

        // Configuration.
        config = spy(new DefaultResourceBatchingConfiguration());
        when(config.isSuperBatchingEnabled()).thenReturn(configurationData.isSuperBatchEnabled);
        when(config.isContextBatchingEnabled()).thenReturn(configurationData.isBatchingEnabled);
        when(config.isPluginWebResourceBatchingEnabled()).thenReturn(configurationData.isBatchingEnabled);
        when(config.isJavaScriptTryCatchWrappingEnabled()).thenReturn(configurationData
                .isJavaScriptTryCatchWrappingEnabled);
        when(config.isBatchContentTrackingEnabled()).thenReturn(false);
        when(config.isSourceMapEnabled()).thenReturn(configurationData.isSourceMapEnabled);
        when(config.optimiseSourceMapsForDevelopment()).thenReturn(configurationData.optimiseSourceMapForDevelopment);

        // Resource locator.
        urlProvider = new WebResourceUrlProviderImpl(integration);
        if (configurationData.staticTransformers != null)
        {
            this.staticTransformers = configurationData.staticTransformers;
        }
        else
        {
            this.staticTransformers = new DefaultStaticTransformers(new DefaultStaticTransformersSupplier
                    (integration, urlProvider));
        }
        resourceLocator = new PluginResourceLocatorImpl(integration, null, urlProvider, config, eventManager,
                staticTransformers);

        // Globals.
        globals = resourceLocator.temporaryWayToGetGlobalsDoNotUseIt();

        // Support for HTTP.
        ContentTypeResolver contentTypeResolver = spy(new ContentTypeResolver()
        {
            @Override
            public String getContentType(String url)
            {
                return buildMap("css", "text/css", "js", "application/javascript", "png",
                        "image/png").get(Request.getType(url));
            }
        });
        pluginResourceDownload = new PluginResourceDownload(resourceLocator, contentTypeResolver, "UTF-8");

        reset();
    }

    @Override
    public void reset()
    {
        requestCache = new HashMap<String, Object>();
        // Page builder service.
        webResourceAssemblerFactory = new DefaultWebResourceAssemblerFactory(globals);
        pageBuilderService = new DefaultPageBuilderService(integration, webResourceAssemblerFactory);
    }

    @Override
    public void applyConfiguration(final ConfigurationData configurationData)
    {
        initialize(configurationData);

        List<ContentTransformerModuleDescriptor> contentTransformerDescriptor = new
                ArrayList<ContentTransformerModuleDescriptor>();
        List<UrlReadingWebResourceTransformerModuleDescriptor> transformerDescriptors = new
                ArrayList<UrlReadingWebResourceTransformerModuleDescriptor>();
        List<WebResourceTransformerModuleDescriptor> legacyTransformerDescriptors = new
                ArrayList<WebResourceTransformerModuleDescriptor>();

        // Annotators.
        if (configurationData.annotators != null)
        {
            Config config = spy(globals.getConfig());
            when(config.getContentAnnotators(any(String.class))).thenReturn(configurationData.annotators);
            setField(globals, "config", config);
        }

        // Configuring plugins.
        List<WebResourceModuleDescriptor> listOfWebResourceDescriptors = new ArrayList<WebResourceModuleDescriptor>();

        for (PluginData pluginData : configurationData.plugins)
        {
            // Searching for classes.
            List<Class> classes = new ArrayList<Class>();
            for (WebResourceData webResourceData : pluginData.webResources)
            {
                classes.addAll(webResourceData.conditions);
            }

            // Creating plugin.
            Plugin plugin = TestUtils.createTestPlugin(pluginData.key, "1", classes.toArray(new Class[classes.size()]));
            when(pluginAccessor.getPlugin(pluginData.key)).thenReturn(plugin);

            // Plugin resources.
            for (ResourceData resourceData : pluginData.resources)
            {
                when(plugin.getResourceLocation("download", resourceData.name)).thenReturn(new ResourceLocation
                        (resourceData.location, resourceData.name, ResourceUtils.getType(resourceData.name),
                                resourceData.contentType, resourceData.content, resourceData.params));
                final ResourceData finalResourceData = resourceData;
                // Callback should be used because otherwise resource input stream wouldn't be reset for the new usage.
                when(plugin.getResourceAsStream(resourceData.location + resourceData.filePath)).thenAnswer(new Answer<Object>()
                {
                    public Object answer(InvocationOnMock invocationOnMock)
                    {
                        return IOUtils.toInputStream(finalResourceData.content);
                    }
                });
                when(pluginAccessor.getEnabledPlugin(pluginData.key)).thenReturn(plugin);
            }

            // Creating web resources.
            for (WebResourceData webResourceData : pluginData.webResources)
            {
                // Conditions.
                WebResourceModuleDescriptorBuilder descriptorBuilder = new WebResourceModuleDescriptorBuilder(plugin,
                        webResourceData.key);
                for (Class klass : webResourceData.conditions)
                {
                    descriptorBuilder.addCondition(klass);
                }

                // Resources.
                for (final ResourceData resourceData : webResourceData.resources)
                {
                    descriptorBuilder.addDescriptor("download", resourceData.name, resourceData.location,
                            resourceData.contentType, resourceData.params);
                    // Callback should be used because otherwise resource input stream wouldn't be reset for the new
                    // usage.
                    when(plugin.getResourceAsStream(resourceData.location + resourceData.filePath)).thenAnswer(new Answer<Object>()
                    {
                        public Object answer(InvocationOnMock invocationOnMock)
                        {
                            return IOUtils.toInputStream(resourceData.content);
                        }
                    });
                }

                // Files.
                for (final FileData fileData : webResourceData.files)
                {
                    // Callback should be used because otherwise resource input stream wouldn't be reset for the new
                    // usage.
                    when(plugin.getResourceAsStream(fileData.name)).thenAnswer(new Answer<Object>()
                    {
                        public Object answer(InvocationOnMock invocationOnMock)
                        {
                            return IOUtils.toInputStream(fileData.content);
                        }
                    });
                }

                // Dependencies.
                for (String dependencyKey : webResourceData.dependencies)
                {
                    descriptorBuilder.addDependency(dependencyKey);
                }

                // Contexts.
                for (String contextKey : webResourceData.contexts)
                {
                    descriptorBuilder.addContext(contextKey);
                }

                // Tranformations.
                for (Map.Entry<String, List<String>> entry : webResourceData.transformations.entrySet())
                {
                    descriptorBuilder.addTransformers(entry.getKey(), entry.getValue());
                }

                // Data providers.
                for (Tuple<String, Class<?>> item : webResourceData.dataProviders)
                {
                    String key = item.getFirst();
                    Class<?> klass = item.getLast();
                    descriptorBuilder.addDataProvider(key, klass);
                    try
                    {
                        when(plugin.loadClass(Matchers.eq(klass.getName()), (Class<?>) any())).thenReturn((Class<Object>) klass);
                    }
                    catch (ClassNotFoundException e)
                    {
                        throw new RuntimeException(e);
                    }
                }

                ModuleDescriptor descriptor = descriptorBuilder.build();
                listOfWebResourceDescriptors.add((WebResourceModuleDescriptor) descriptor);
                when(pluginAccessor.getEnabledPluginModule(pluginData.key + ":" + webResourceData.key)).thenReturn(descriptor);
            }

            // Preparing transformers.
            for (Map.Entry<String, Tuple<String, Class>> entry : pluginData.transformers.entrySet())
            {
                String key = entry.getKey();
                String aliasKey = entry.getValue().getFirst();
                Class<?> klass = entry.getValue().getLast();
                if (ContentTransformerFactory.class.isAssignableFrom(klass))
                {
                    contentTransformerDescriptor.add(TestUtils.mockContentTransformer(key, klass));
                }
                else if (WebResourceTransformerFactory.class.isAssignableFrom(klass))
                {
                    transformerDescriptors.add(TestUtils.mockTransformer(key, aliasKey, klass));
                }
                else if (WebResourceTransformer.class.isAssignableFrom(klass))
                {
                    if (aliasKey != null)
                    {
                        throw new RuntimeException("legacy transformers can't have alias key!");
                    }
                    legacyTransformerDescriptors.add(TestUtils.mockLegacyTransformer(key, klass));
                }
                else
                {
                    throw new RuntimeException("invalid transformer class " + klass.getCanonicalName() + "!");
                }
            }
        }

        // Adding web resources.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
            .thenReturn(listOfWebResourceDescriptors);

        // Adding module keys that should be included in superbatch.
        if (configurationData.addEverythingToSuperbatch)
        {
            List<String> allWebResourceKeys = new ArrayList<String>();
            for (PluginData plugin : configurationData.plugins)
            {
                for (WebResourceData webResource : plugin.webResources)
                {
                    allWebResourceKeys.add(plugin.key + ":" + webResource.key);
                }
            }
            when(config.getSuperBatchModuleCompleteKeys()).thenReturn(allWebResourceKeys);
        }
        else
        {
            when(config.getSuperBatchModuleCompleteKeys()).thenReturn(configurationData.webResourcesToAddToSuperbatch);
        }

        // Adding transformers.
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(ContentTransformerModuleDescriptor.class)).thenReturn
                (contentTransformerDescriptor);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor
                .class)).thenReturn(transformerDescriptors);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceTransformerModuleDescriptor.class))
                .thenReturn(legacyTransformerDescriptors);
    }
}