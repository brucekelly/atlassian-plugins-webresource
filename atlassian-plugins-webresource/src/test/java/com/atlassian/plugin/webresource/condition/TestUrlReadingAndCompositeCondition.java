package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import org.junit.Test;

import static com.atlassian.plugin.webresource.TestUtils.emptyContext;
import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestUrlReadingAndCompositeCondition
{
    @Test
    public void testPasses()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);
        assertTrue(condition.shouldDisplay(QueryParams.of(urlBuilder.buildParams())));
    }

    @Test
    public void testFails()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);
        assertFalse(condition.shouldDisplay(QueryParams.of(urlBuilder.buildParams())));
    }

    @Test
    public void testImmediatePasses()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testImmediateFails()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testCombinationPass()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(QueryParams.of(urlBuilder.buildParams())));
    }

    @Test
    public void testCombinationFail1()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testCombinationFail2()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);
        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
        assertFalse(condition.shouldDisplay(QueryParams.of(urlBuilder.buildParams())));
    }

    @Test
    public void testCombinationFail3()
    {
        DecoratingCompositeCondition condition = new DecoratingAndCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }
}
