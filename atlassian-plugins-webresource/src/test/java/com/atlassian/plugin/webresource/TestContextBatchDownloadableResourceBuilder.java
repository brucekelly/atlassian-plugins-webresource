package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.integration.stub.UrlReadingConditionStub;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import org.junit.Test;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrlRelativeToContextBatch;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestContextBatchDownloadableResourceBuilder extends TestCase
{
    private void shouldServeBatchedContextFor(String type, String contentType)
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .context("general")
                    .resource("page." + type)
                    .resource("page.png")
                .webResource("networking")
                    .context("general")
                    .resource("networking." + type)
        .end();

        wr.requireContext("general");
        assertThat(wr.paths(), matches(
            contextBatchUrl("general", type)
        ));

        ResponseData response = wr.get(contextBatchUrl("general", type));
        assertThat(response.getContentType(), equalTo(contentType));
        assertThat(response.getContent(), matches(
            "content of page." + type,
            "content of networking." + type
        ));
        assertThat(response.getContent(), not(matches(
            "content of page.png"
        )));
    }

    @Test
    public void shouldServeBatchedContextForJs()
    {
        shouldServeBatchedContextFor("js", "application/javascript");
    }

    @Test
    public void shouldServeBatchedContextForCss()
    {
        shouldServeBatchedContextFor("css", "text/css");
    }

    @Test
    public void shouldServeMultipleContextsBatch()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .context("general")
                    .resource("page1.css")
                    .resource("page1.js")
                .webResource("widget")
                    .context("dashboard")
                    .resource("widget1.css")
        .end();

        String content = wr.getContent(contextBatchUrl("general,dashboard", "css"));

        assertThat(content, matches("content of page1.css"));
        assertThat(content, matches("content of widget1.css"));
        assertThat(content, not(matches("content of page1.js")));
    }

    @Test
    public void shouldServeBatchedContextWithParam()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .condition(UrlReadingConditionStub.class)
                    .context("general")
                    .resource("page1.css")
        .end();

        UrlReadingConditionStub.params = null;
        wr.get(contextBatchUrl("general", "css", buildMap("ieOnly", "true")));
        assertThat(UrlReadingConditionStub.params, equalTo(buildMap("ieOnly", "true")));
    }

    @Test
    public void shouldServeBatchedContextWithParams()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .condition(UrlReadingConditionStub.class)
                    .context("general")
                    .resource("page1.css")
        .end();

        UrlReadingConditionStub.params = null;
        wr.get(contextBatchUrl("general", "css", buildMap("ieOnly", "true", "debug", "false")));
        assertThat(UrlReadingConditionStub.params, equalTo(buildMap("ieOnly", "true", "debug", "false")));
    }

    @Test
    public void shouldServeResourceRelativeToContextBatch()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .context("general")
                    .resource("style.css")
                    .resource("image.png")
        .end();

        ResponseData contextResponse = wr.get(contextBatchUrl("general", "css"));
        assertThat(contextResponse.getContent(), matches("content of style.css"));
        assertThat(contextResponse.getContentType(), equalTo("text/css"));

        ResponseData imageResponse = wr.get(resourceUrlRelativeToContextBatch("general", "css", "image.png"));
        assertThat(imageResponse.getContent(), matches("content of image.png"));
        assertThat(imageResponse.getContentType(), equalTo("image/png"));
    }

    @Test
    public void shouldServeResourceRelativeToMultipleContextsBatch()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .context("general")
                    .resource("style.css")
                    .resource("image.png")
                .webResource("widget")
                    .context("dashboard")
                    .resource("dashboard.css")
        .end();

        ResponseData contextResponse = wr.get(contextBatchUrl("general,dashboard", "css"));
        assertThat(contextResponse.getContent(), matches("content of style.css"));
        assertThat(contextResponse.getContent(), matches("content of dashboard.css"));
        assertThat(contextResponse.getContentType(), equalTo("text/css"));

        ResponseData imageResponse = wr.get(resourceUrlRelativeToContextBatch("general,dashboard", "css", "image.png"));
        assertThat(imageResponse.getContent(), matches("content of image.png"));
        assertThat(imageResponse.getContentType(), equalTo("image/png"));
    }
}
