package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.ResourceContentAnnotator;

import java.io.IOException;
import java.io.OutputStream;

public class OffsetContentAnnotatorsWithSourceMap
{
    public static ResourceContentAnnotator[] build()
    {
        return new ResourceContentAnnotator[] { new ResourceContentAnnotator()
        {
            @Override
            public void before(Resource resource, OutputStream stream) throws IOException
            {
                stream.write("first-annotator\n".getBytes());
            }

            @Override
            public void after(Resource resource, OutputStream stream) throws IOException
            {
                stream.write("\nfirst-annotator".getBytes());
            }

            @Override
            public int beforeOffset()
            {
                return 1;
            }
        }, new ResourceContentAnnotator()
        {
            @Override
            public void before(Resource resource, OutputStream stream) throws IOException
            {
                stream.write("second-annotator\n".getBytes());
            }

            @Override
            public void after(Resource resource, OutputStream stream) throws IOException
            {
                stream.write("\nsecond-annotator".getBytes());
            }

            @Override
            public int beforeOffset()
            {
                return 1;
            }
        } };
    }
}
