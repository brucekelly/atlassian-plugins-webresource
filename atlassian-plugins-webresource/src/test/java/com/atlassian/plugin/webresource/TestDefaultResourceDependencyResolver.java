package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.condition.AlwaysTrueLegacyCondition;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import junit.framework.TestCase;

import java.util.List;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.printShortPaths;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class TestDefaultResourceDependencyResolver extends TestCase
{
    private WebResource wr;

    @Override
    protected void setUp() throws Exception
    {
        wr = new WebResourceImpl();
    }

    private void prepareSuperBatch(boolean enableSuperBatch)
    {
        WebResource.ConfigurationDsl config = wr.configure();
        config.addToSuperbatch("plugin.key:resource1", "plugin.key:resource2", "plugin.key:resource3");
        if (!enableSuperBatch)
        {
            config.disableSuperBatch();
        }
        config
            .plugin("plugin.key")
                .webResource("resource1")
                    .resource("a.js")
                .webResource("resource2")
                    .resource("b.js")
                .webResource("resource3")
                    .resource("c.js")
        .end();
    }

    public void testSuperBatchingNotEnabled()
    {
        prepareSuperBatch(false);
        assertThat(wr.paths().size(), equalTo(0));
    }

    public void testGetSuperBatchDependenciesInOrder()
    {
        prepareSuperBatch(true);

        assertThat(wr.paths(), matches(
            contextBatchUrl("_super", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of a",
            "content of b",
            "content of c"
        ));
    }

    public void testGetSuperBatchDependenciesWithCylicDependency()
    {
        wr.configure()
            .addToSuperbatch("plugin.key:resource1", "plugin.key:resource2")
            .plugin("plugin.key")
                .webResource("resource1")
                    .dependency("plugin.key:resource2")
                    .resource("a.js")
                .webResource("resource2")
                    .dependency("plugin.key:resource1")
                    .resource("b.js")
        .end();

        assertThat(wr.paths(), matches(
            contextBatchUrl("_super", "js")
        ));

        // Would be in reverse order.
        assertThat(wr.getContent(), matches(
            "content of b",
            "content of a"
        ));
    }

    public void testGetSuperBatchDependenciesWithDependencies()
    {
        wr.configure()
            .addToSuperbatch("plugin.key:super1", "plugin.key:super2")
            .plugin("plugin.key")
                .webResource("super1")
                    .dependency("plugin.key:a")
                    .dependency("plugin.key:b")
                    .resource("s1.js")
                .webResource("super2")
                    .dependency("plugin.key:c")
                    .dependency("plugin.key:d")
                    .resource("s2.js")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .resource("b1.js")
                .webResource("c")
                    .resource("c1.js")
                .webResource("d")
                    .resource("d1.js")
        .end();

        assertThat(wr.paths(), matches(
            contextBatchUrl("_super", "js")
        ));

        // Would be in reverse order.
        assertThat(wr.getContent(), matches(
            "content of a",
            "content of b",
            "content of s1",
            "content of c",
            "content of d",
            "content of s2"
        ));
    }

    public void testGetDependenciesExcludesSuperBatch()
    {
        wr.configure()
            .addToSuperbatch("plugin.key:super1", "plugin.key:super2")
            .plugin("plugin.key")
                .webResource("super1")
                    .resource("s1.js")
                .webResource("super2")
                    .resource("s2.js")
                .webResource("foo")
                    .dependency("plugin.key:super1")
                    .resource("foo.js")
        .end();

        wr.requireResource("plugin.key:foo");

        assertThat(wr.paths(), matches(
            contextBatchUrl("_super", "js"),
            webResourceBatchUrl("plugin.key:foo", "js")
        ));
    }


//    Removing, it's the same as testGetDependenciesExcludesSuperBatch.
//    public void testGetDependenciesIncludesSuperBatch() {}


    public void testGetDependenciesInContext()
    {
        wr.configure()
            .plugin("plugin.key")
                .webResource("foo")
                    .context("connie")
                    .resource("foo.js")
                .webResource("bar")
                    .context("jira")
                    .resource("bar.js")
        .end();

        wr.requireContext("jira");

        assertThat(wr.paths(), matches(
            contextBatchUrl("jira", "js")
        ));
    }

    public void testGetDependenciesInContextWithMultipleEntries()
    {
        wr.configure()
            .plugin("plugin.key")
                .webResource("foo")
                    .context("connie")
                    .resource("foo.js")
                .webResource("bar")
                    .context("connie")
                    .resource("bar.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("connie", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of foo",
            "content of bar"
        ));
    }

    public void testGetDependenciesInContextWithSharedDependenciesInDifferentContexts()
    {
        wr.configure()
            .plugin("plugin.key")
                .webResource("shared")
                    .resource("shared.js")
                .webResource("foo")
                    .context("connie")
                    .dependency("plugin.key:shared")
                    .resource("foo.js")
                .webResource("bar")
                    .context("jira")
                    .dependency("plugin.key:shared")
                    .resource("bar.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("connie", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of shared",
            "content of foo"
        ));
        assertThat(wr.getContent(), not(matches(
            "bar"
        )));
    }

    public void testGetDependenciesInContextWithSharedDependenciesInTheSameContext()
    {
        wr.configure()
            .plugin("plugin.key")
                .webResource("shared")
                    .resource("shared.js")
                .webResource("foo")
                    .context("connie")
                    .dependency("plugin.key:shared")
                    .resource("foo.js")
                .webResource("bar")
                    .context("connie")
                    .dependency("plugin.key:shared")
                    .resource("bar.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("connie", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of shared",
            "content of foo",
            "content of bar"
        ));
    }

    public void testGetDependenciesInContextWithDuplicateResources()
    {
        wr.configure()
            .plugin("plugin.key")
                .webResource("parent")
                    .context("connie")
                    .resource("parent.js")
                .webResource("foo")
                    .context("connie")
                    .dependency("plugin.key:parent")
                    .resource("foo.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("connie", "js")
        ));

        assertThat(wr.getContent(), matches(
            "content of parent",
            "content of foo"
        ));

        assertThat(wr.getContent(), not(matches(
            "content of parent",
            "content of parent"
        )));
    }

    public void testGetDependenciesInContextWithDependenciesInSuperbatch()
    {
        wr.configure()
            .addToSuperbatch("plugin.key:super1", "plugin.key:super2")
            .plugin("plugin.key")
                .webResource("super1")
                    .resource("s1.js")
                .webResource("super2")
                    .resource("s2.js")
                .webResource("parent")
                    .context("connie")
                    .resource("parent.js")
                .webResource("foo")
                    .context("connie")
                    .dependency("plugin.key:parent")
                    .dependency("plugin.key:super1")
                    .resource("foo.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("_super", "js"),
            contextBatchUrl("connie,-_super", "js")
        ));

        assertThat(wr.getContent(), not(matches(
            "content of super1",
            "content of super2",
            "content of parent",
            "content of foo"
        )));
    }

//    Removing, it's same as testGetDependenciesInContextWithDependenciesInSuperbatch, also it is wrong, it checks
//    that superbatch goes after the non-superbatch dependency.
//    public void testGetDependenciesInContextExcludingSuperbatchWithDependenciesInSuperbatch() {}

    public void testSkippedConditions() throws ClassNotFoundException
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("parent")
                    .context("connie")
                    .resource("parent.js")
                .webResource("skipped-parent")
                    .resource("skipped-parent.js")
                .webResource("skipped-resource")
                    .condition(AlwaysTrueLegacyCondition.class)
                    .dependency("test.atlassian:skipped-parent")
                    .context("connie")
                    .resource("skipped-resource.js")
                .webResource("foo")
                    .dependency("test.atlassian:parent")
                    .context("connie")
                    .resource("foo.js")
        .end();

        wr.requireContext("connie");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            contextBatchUrl("connie", "js"),
            webResourceBatchUrl("test.atlassian:skipped-parent", "js"),
            webResourceBatchUrl("test.atlassian:skipped-resource", "js")
        ));

        assertThat(wr.getContent(contextBatchUrl("connie", "js")), matches(
            "content of parent",
            "content of foo"
        ));
        assertThat(wr.getContent(contextBatchUrl("connie", "js")), not(matches("content of skipped-resource")));
        assertThat(wr.getContent(contextBatchUrl("connie", "js")), not(matches("content of skipped-parent")));

        assertThat(wr.getContent(webResourceBatchUrl("test.atlassian:skipped-resource", "js")), matches(
            "content of skipped-resource"
        ));
        assertThat(wr.getContent(webResourceBatchUrl("test.atlassian:skipped-parent", "js")), matches(
            "content of skipped-parent"
        ));
    }

    public void testUrlMappedConditionsAreNotSkipped() throws ClassNotFoundException
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("parent")
                    .context("connie")
                    .resource("parent.js")
                .webResource("foo")
                    .dependency("test.atlassian:parent")
                    .context("connie")
                    .resource("foo.js")
                .webResource("conditioned-parent")
                    .resource("conditioned-parent.js")
                .webResource("conditioned-resource")
                    .condition(AlwaysTrueCondition.class)
                    .dependency("test.atlassian:conditioned-parent")
                    .context("connie")
                    .resource("conditioned-resource.js")
        .end();

        wr.requireContext("connie");

        assertThat(wr.paths(), matches(
            contextBatchUrl("connie", "js", buildMap("always-true", "true"))
        ));

        assertThat(wr.getContent(contextBatchUrl("connie", "js", buildMap("always-true", "true"))), matches(
            "content of parent",
            "content of foo",
            "content of conditioned-parent",
            "content of conditioned-resource"
        ));
    }
}
