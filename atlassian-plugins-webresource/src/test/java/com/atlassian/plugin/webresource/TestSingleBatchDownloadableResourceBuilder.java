package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.integration.stub.UrlReadingConditionStub;
import org.junit.Test;

import java.util.List;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

public class TestSingleBatchDownloadableResourceBuilder extends TestCase
{
    @Test
    public void shouldServeWebResourceBatch() {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("a.css")
                    .resource("b.css")
                    .resource("a.js")
        .end();

        ResponseData response = wr.get(webResourceBatchUrl("app:page", "css"));

        assertThat(response.getContentType(), equalTo("text/css"));
        assertThat(response.getContent(), matches(
            "content of a.css",
            "content of b.css"
        ));

        assertThat(response.getContent(), not(matches(
            "content of a.js"
        )));
    }

    @Test
    public void shouldServeWebResourceBatchWithParams()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .condition(UrlReadingConditionStub.class)
                    .resource("style.css")
        .end();

        UrlReadingConditionStub.params = null;
        wr.get(webResourceBatchUrl("app:page", "css", buildMap("ieOnly", "true", "debug", "false")));
        assertThat(UrlReadingConditionStub.params, equalTo(buildMap("ieOnly", "true", "debug", "false")));
    }

    @Test
    public void shouldServeWebResourceBatchWithParamsAndRandomPrefix()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .condition(UrlReadingConditionStub.class)
                    .resource("style.css")
        .end();

        UrlReadingConditionStub.params = null;
        wr.get("/random/stuff" + webResourceBatchUrl("app:page", "css", buildMap("ieOnly", "true", "debug", "false")));
        assertThat(UrlReadingConditionStub.params, equalTo(buildMap("ieOnly", "true", "debug", "false")));
    }

    @Test
    public void shouldGenerateUrlWithParametersAndServeWebResourceBatch()
    {

        wr.configure()
            .plugin("app")
                .webResource("page")
                    .condition(UrlReadingConditionStub.class)
                    .condition(AlwaysTrueCondition.class)
                    .resource("style.css")
        .end();

        wr.requireResource("app:page");

        List<String> urls = wr.paths();
        assertThat(urls, matches(webResourceBatchUrl("app:page", "css", buildMap("always-true", "true"))));

        String url = urls.get(0);
        UrlReadingConditionStub.params = null;
        String content = wr.getContent(url);
        assertThat(UrlReadingConditionStub.params, equalTo(buildMap("always-true", "true")));
        assertThat(content, matches("content of style.css"));
    }

    @Test
    public void shouldThrowExceptionForInvalidRoutes()
    {
        wr.configure().end();
        try {
            wr.getContent("/download/batch/blah.css");
            fail("Should have thrown exception for invalid url");
        } catch (final RuntimeException e) {
            assertThat(e.getMessage(), matches("no route for /download/batch/blah.css"));
        }
    }
}
