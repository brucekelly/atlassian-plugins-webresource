package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.AddLocationStatic;
import com.atlassian.plugin.webresource.integration.transformers.LegacyAddLocation;
import com.atlassian.plugin.webresource.servlet.PluginResourceDownload;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import org.junit.Test;

import java.util.List;

import static com.atlassian.plugin.webresource.impl.helpers.Helpers.splitLastPathPart;
import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.getField;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.HashBuilder.buildHash;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static com.atlassian.plugin.webresource.util.UrlCdnMatcher.hasCdn;
import static com.atlassian.plugin.webresource.util.UrlHashMatcher.hasHash;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class TestPluginResourceLocatorImpl extends TestCase
{
    @Test
    public void testMatches()
    {
        wr.configure().end();
        PluginResourceDownload pluginResourceDownload = (PluginResourceDownload) getField(wr, "pluginResourceDownload");
        assertTrue(pluginResourceDownload.matches("/download/batch/plugin.key:module-key/plugin.key.js"));
        assertTrue(pluginResourceDownload.matches("/download/resources/plugin.key:module-key/foo.png"));
        assertTrue(pluginResourceDownload.matches("/download/contextbatch/css/contexta/batch.css"));
        assertTrue(pluginResourceDownload.matches("/download/contextbatch/css/contexta/images/blah.png"));
    }

    @Test
    public void testNotMatches()
    {
        wr.configure().end();
        PluginResourceDownload pluginResourceDownload = (PluginResourceDownload) getField(wr, "pluginResourceDownload");
        assertFalse(pluginResourceDownload.matches("/superbatch/batch.css"));
        assertFalse(pluginResourceDownload.matches("/download/blah.css"));
    }

    @Test
    public void testGetAndParseUrl()
    {
        wr.configure().end();
        PluginResourceDownload pluginResourceDownload = (PluginResourceDownload) getField(wr, "pluginResourceDownload");
        assertTrue(pluginResourceDownload.matches(resourceUrl("plugin.key:my-resources", "foo.css")));
    }

    @Test
    public void testGetPluginResourcesWithoutBatching() throws Exception
    {
        wr.configure()
            .disableBatching()
            .plugin("plugin")
                .webResource("widget")
                    .resource("master-ie.css")
                        .param("ieonly", "true")
                    .resource("master.css")
                    .resource("comments.css")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
//            In the previous implementation, in the standalone mode the resource parameters haven't been added to the url.
//            resourceUrl("plugin:widget", "master-ie.css", buildMap("ieonly", "true")),
            resourceUrl("plugin:widget", "master-ie.css"),
            resourceUrl("plugin:widget", "master.css"),
            resourceUrl("plugin:widget", "comments.css")
        ));
        assertThat(paths.get(1), not(matches("ieonly")));
        assertThat(paths.get(2), not(matches("ieonly")));
    }

    @Test
    public void testGetPluginResourcesWithBatching() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("widget")
                    .resource("master-ie.css")
                        .param("ieonly", "true")
                    .resource("master.css")
                    .resource("comments.css")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css", buildMap("ieonly", "true")),
            webResourceBatchUrl("plugin:widget", "css")
        ));
        assertThat(paths.get(1), not(matches("ieonly")));
    }

    @Test
    public void testGetPluginResourcesWithBatchParameter() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("widget")
                    .resource("master.css")
                    .resource("comments.css")
                    .resource("nonbatch.css")
                        .param("batch", "false")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css"),
            resourceUrl("plugin:widget", "nonbatch.css")
        ));
        assertThat(paths.get(0), not(matches("ieonly")));
    }

    public static class FooTransformer implements WebResourceTransformerFactory {
        @Override
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters transformerParams)
        {
            return new TransformerUrlBuilder()
            {
                @Override
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToHash("myhash", "myvalue");
                    urlBuilder.addToQueryString("myquerykey", "myqueryvalue");
                }
            };
        }

        @Override
        public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters transformerParams)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @Test
    public void testGetPluginResourcesWithTransformation() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .transformer("fooTransformer", FooTransformer.class)
                .webResource("widget")
                    .transformation("css", "fooTransformer")
                    .resource("master.css")
                    .resource("comments.css")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css", buildMap("myquerykey", "myqueryvalue"))
        ));
        assertThat(paths.get(0), hasHash(buildHash("myvalue")));
        assertThat(paths.get(0), hasCdn());
    }

     public static class CssTransformer implements WebResourceTransformerFactory {
        @Override
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters transformerParams)
        {
            return new TransformerUrlBuilder()
            {
                @Override
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToHash("myhash-css", "myvalue-css");
                    urlBuilder.addToQueryString("myquerykey-css", "myqueryvalue-css");
                }
            };
        }

        @Override
        public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters transformerParams)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    public static class JsTransformer implements WebResourceTransformerFactory {
        @Override
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters transformerParams)
        {
            return new TransformerUrlBuilder()
            {
                @Override
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToHash("myhash-js", "myvalue-js");
                    urlBuilder.addToQueryString("myquerykey-js", "myqueryvalue-js");
                }
            };
        }

        @Override
        public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters transformerParams)
        {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @Test
    public void testGetPluginResourcesWithMultipleTransformationDifferentType() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .transformer("cssTransformer", CssTransformer.class)
                .transformer("jsTransformer", JsTransformer.class)
                .webResource("widget")
                    .transformation("css", "cssTransformer")
                    .transformation("js", "jsTransformer")
                    .resource("blah.css")
                    .resource("blah.js")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css", buildMap("myquerykey-css", "myqueryvalue-css")),
            webResourceBatchUrl("plugin:widget", "js", buildMap("myquerykey-js", "myqueryvalue-js"))
        ));

        assertThat(paths.get(0), hasHash(buildHash("myvalue-css")));
        assertThat(paths.get(0), hasCdn());

        assertThat(paths.get(1), hasHash(buildHash("myvalue-js")));
        assertThat(paths.get(1), hasCdn());
    }

    public static class UrlReadingConditionGetPluginResourcesWithCondition extends TestUtils.StubUrlReadingCondition
    {
        @Override
        public void addToUrl(UrlBuilder urlBuilder)
        {
            urlBuilder.addToQueryString("myquerykey", "myqueryvalue");
            urlBuilder.addToHash("myhashname", "myhashvalue");
        }
    }

    @Test
    public void testGetPluginResourcesWithCondition() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("widget")
                    .condition(UrlReadingConditionGetPluginResourcesWithCondition.class)
                    .resource("master.css")
                    .resource("comments.css")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css", buildMap("myquerykey", "myqueryvalue"))
        ));
        assertThat(paths.get(0), hasHash(buildHash("myhashvalue")));
        assertThat(paths.get(0), hasCdn());
    }

    public static class GetPluginResourcesWithMultiConditionCondition1 extends TestUtils.StubUrlReadingCondition
    {
        @Override
        public void addToUrl(UrlBuilder urlBuilder)
        {
            urlBuilder.addToQueryString("myquerykey1", "myqueryvalue");
            urlBuilder.addToHash("myhashname1", "myhashvalue");
        }
    }
    public static class GetPluginResourcesWithMultiConditionCondition2 extends TestUtils.StubUrlReadingCondition
    {
        @Override
        public void addToUrl(UrlBuilder urlBuilder)
        {
            urlBuilder.addToQueryString("myquerykey2", "myqueryvalue");
            urlBuilder.addToHash("myhashname2", "myhashvalue");
        }
    };

    @Test
    public void testGetPluginResourcesWithMultiCondition() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("widget")
                    .condition(GetPluginResourcesWithMultiConditionCondition1.class)
                    .condition(GetPluginResourcesWithMultiConditionCondition2.class)
                    .resource("master.css")
                    .resource("comments.css")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css", buildMap("myquerykey1", "myqueryvalue", "myquerykey2", "myqueryvalue"))
        ));

        assertThat(paths.get(0), hasHash(buildHash("myhashvalue", "myhashvalue")));
        assertThat(paths.get(0), hasCdn());
    }

    @Test
    public void testGetPluginResourcesWithForwarding() throws Exception
    {
        wr.configure()
            .plugin("plugin")
                .webResource("widget")
                    .resource("master.css")
                    .resource("comments.css")
                    .resource("forward.css")
                        .param("source", "webContext")
        .end();

        wr.requireResource("plugin:widget");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            webResourceBatchUrl("plugin:widget", "css"),
            resourceUrl("plugin:widget", "forward.css")
        ));
    }

    // old name testGetForwardableResource
    @Test
    public void shouldForwardResource()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("style.css", "", "http://other-domain.com/style.css")
                        .param("source", "webContext")
        .end();

        ResponseData response = wr.get(resourceUrl("app:page", "style.css"));
        assertThat(response.getStatus(), equalTo(301));
        assertThat(response.getLocation(), equalTo("http://other-domain.com/style.css"));
    }

    // old name testGetDownloadableClasspathResource
    @Test
    public void shouldServeResource()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("style.css")
        .end();

        assertThat(wr.getContent(resourceUrl("app:page", "style.css")), equalTo("content of style.css"));
    }

    // old name testGetTransformedDownloadableClasspathResource
    @Test
    public void shouldTransformResource()
    {
        wr.configure()
            .setStaticTransformers(new AddLocationStatic())
            .plugin("app")
                .transformer("addLocation", AddLocation.class)
                .transformer("addLocationLegacy", LegacyAddLocation.class)
                .webResource("page")
                    .transformation("css", "addLocation")
                    .transformation("css", "addLocationLegacy")
                    .resource("style.css")
        .end();

        assertThat(wr.getContent(resourceUrl("app:page", "style.css", buildMap("location", "true"))), equalTo(
            "location=app:page/style.css (static)\n" +
            "location=/style.css (legacy)\n" +
            "location=app:page/style.css (transformer)\n" +
            "content of style.css"
        ));
    }

    // old name testGetUnmatchedTransformDownloadableClasspathResource
    @Test
    public void shouldNotTransformResourceIfTransformerNotMatched()
    {
        wr.configure()
            .plugin("app")
                .transformer("addLocation", AddLocation.class)
                .transformer("addLocationLegacy", LegacyAddLocation.class)
                .webResource("page")
                    .transformation("js", "addLocation")
                    .transformation("js", "addLocationLegacy")
                    .resource("style.css")
        .end();

        assertThat(wr.getContent(resourceUrl("app:page", "style.css", buildMap("location", "true"))), equalTo(
            "content of style.css"
        ));
    }

    // old name testGetMissingTransformerDownloadableClasspathResource
    @Test
    public void shouldSkipMissingTransformers()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .transformation("css", "addLocation")
                    .transformation("css", "addLocationLegacy")
                    .resource("style.css")
        .end();

        assertThat(wr.getContent(resourceUrl("app:page", "style.css", buildMap("location", "true"))), equalTo(
            "content of style.css"
        ));
    }

    // old name testGetDownloadableBatchResource
    @Test
    public void shouldIncludeOnlyResourcesWithMatchingParams()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("style.css")
                    .resource("style-ie.css")
                        .param("ieonly", "true")
        .end();

        String content = wr.getContent(webResourceBatchUrl("app:page", "css"));
        assertThat(content, matches(
            "content of style.css"
        ));
        assertThat(content, not(matches(
            "content of style-ie.css"
        )));

        String contentIe = wr.getContent(webResourceBatchUrl("app:page", "css", buildMap("ieonly", "true")));
        assertThat(contentIe, matches(
            "content of style-ie.css"
        ));
        assertThat(contentIe, not(matches(
            "content of style.css"
        )));
    }

//    // not sure what this test check, seems like it could be deleted.
//    public void testGetDownloadableBatchResourceWithConditionalComments() throws Exception, RedirectException
//    {
//        final String url = "/download/batch/" + TEST_MODULE_COMPLETE_KEY + "/all.css";
//        final String ieResourceName = "master-conditional.css";
//        final Map<String, String> params = new TreeMap<String, String>();
//        params.put("conditionalComment", "IE");
//
//        final List<ResourceDescriptor> resourceDescriptors = asList(TestUtils.createResourceDescriptor(ieResourceName, new HashMap<String, String>(
//            params)), TestUtils.createResourceDescriptor(ieResourceName));
//
//        final Plugin mockPlugin = mock(Plugin.class);
//        final ModuleDescriptor mockModuleDescriptor = mock(ModuleDescriptor.class);
//        when(mockModuleDescriptor.getPluginKey()).thenReturn(TEST_PLUGIN_KEY);
//        when(mockModuleDescriptor.getCompleteKey()).thenReturn(TEST_MODULE_COMPLETE_KEY);
//        when(mockModuleDescriptor.getResourceDescriptors()).thenReturn(resourceDescriptors);
//        when(mockModuleDescriptor.getResourceLocation("download", ieResourceName)).thenReturn(
//            new ResourceLocation("", ieResourceName, "download", "text/css", "", Collections.<String, String> emptyMap()));
//
//        when(mockPluginAccessor.isPluginModuleEnabled(TEST_MODULE_COMPLETE_KEY)).thenReturn(Boolean.TRUE);
//        when(mockPluginAccessor.getEnabledPluginModule(TEST_MODULE_COMPLETE_KEY)).thenReturn(mockModuleDescriptor);
//        when(mockPluginAccessor.getPluginModule(TEST_MODULE_COMPLETE_KEY)).thenReturn(mockModuleDescriptor);
//        when(mockPluginAccessor.getPlugin(TEST_PLUGIN_KEY)).thenReturn(mockPlugin);
//
//        DownloadableResourceWithSourceMap resource = wr.getResource(url);
//
//        assertTrue(resource instanceof BatchDownloadableResource);
//    }

    // old name testGetDownloadableBatchResourceWhenModuleIsUnkown
    @Test
    public void shouldServeEmptyWebResourceBatchIfCompleteKeyNotExist()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("style.css")
        .end();

        ResponseData response = wr.get(webResourceBatchUrl("unknown:unknown", "css"));
        assertThat(response.getStatus(), equalTo(200));
        assertThat(response.getContent(), equalTo(""));
    }

//    moved to TestUseCases.shouldServeSingleResourceRealtiveToBatch
//    public void testGetDownloadableContextBatchSubResource()


    // This test doesn't work actually, the way it's currently done is that it tries to serve batch and if
    // it's empty it searches for single resource. And it kinda works if the type of batch and resource
    // is different - like if the batch is css and resource is png (that how the old test has been implemented).
    //
    // But, if the type is the same - css for batch and css for single resource - it will not work. It will return
    // batch instead. But, it seems that this bug seems not caused any problems for time being, I guess it is ok to have it.
    //
    // old name testGetDownloadableBatchResourceFallbacksToSingle
    // @Test public void shouldServeSingleResourceRealtiveToBatchWithTheSameType() {
    //     wr.configure()
    //         .plugin("app")
    //             .webResource("page")
    //                 .resource("style.css")
    //                 .resource("another-style.css")
    //     .end();
    //
    //     String content = wr.getContent(resourceUrlRelativeToWebResourceBatch("app:page", "style.css"));
    //     assertThat(content, equalTo("content of style.css"));
    // }

//    moved to TestUseCases.shouldServeFilesRelativeToContextBatch
//    public void testGetDownloadableContextBatchSubResource()

    // old name testGetDownloadableContextBatchResource
    // old name testGetDownloadableMergedContextBatchResource
    @Test
    public void shouldServeContextBatch()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("general")
                    .resource("a1.js")
                    .resource("a2.js")
                .webResource("b")
                    .context("general")
                    .resource("b1.js")
                .webResource("c")
                    .context("special")
                    .resource("c1.js")
                .webResource("d")
                    .resource("d1.js")
        .end();

        // unknown context.
        String content = wr.getContent(contextBatchUrl("unknown", "js"));
        assertThat(content, equalTo(""));

        // single context.
        content = wr.getContent(contextBatchUrl("general", "js"));
        assertThat(content, matches(
            "content of a1",
            "content of a2",
            "content of b1"
        ));
        assertThat(content, not(matches(
            "content of c1"
        )));
        assertThat(content, not(matches(
            "content of d1"
        )));

        // multiple contexts.
        content = wr.getContent(contextBatchUrl("general,special", "js"));
        assertThat(content, matches(
            "content of a1",
            "content of a2",
            "content of b1",
            "content of c1"
        ));
        assertThat(content, not(matches(
            "content of d1"
        )));
    }

    // old name testGetDownloadableMergedContextBatchResourceWithOverlap
    @Test
    public void shouldServeBatchWithOverlappedContexts()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .dependency("plugin:a")
                    .context("general")
                    .resource("b1.js")
                .webResource("c")
                    .dependency("plugin:a")
                    .context("special")
                    .resource("c1.js")
        .end();

        // multiple contexts.
        String content = wr.getContent(contextBatchUrl("general,special", "js"));
        assertThat(content, matches(
            "content of a1",
            "content of c1"
        ));
        assertThat(content, matches(
            "content of a1",
            "content of c1"
        ));
        assertThat(content, not(matches(
            "content of a1",
            "content of a1"
        )));
    }

    @Test
    public void shouldSplitLastPathPart()
    {
        final String[] parts = splitLastPathPart("http://localhost:8080/confluence/download/foo/bar/baz");
        assertEquals(2, parts.length);
        assertEquals("http://localhost:8080/confluence/download/foo/bar/", parts[0]);
        assertEquals("baz", parts[1]);

        final String[] anotherParts = splitLastPathPart(parts[0]);
        assertEquals(2, anotherParts.length);
        assertEquals("http://localhost:8080/confluence/download/foo/", anotherParts[0]);
        assertEquals("bar/", anotherParts[1]);

        assertNull(splitLastPathPart("noslashes"));
    }
}
