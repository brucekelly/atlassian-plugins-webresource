package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OffsetTransformerWithSourceMap extends ContentTransformerHelper
{
    public Content transform(final Content content, ResourceLocation resourceLocation, QueryParams params,
            final String sourceUrl)
    {
        return new ContentImpl(content.getContentType(), true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                SourceMap sourceMap = content.writeTo(buff, isSourceMapEnabled);
                final String contentAsString = buff.toString();

                if (isSourceMapEnabled && (sourceMap != null))
                {
                    sourceMap = Util.offset(sourceMap, 1);
                }

                try
                {
                    out.write(("offset-transformer\n" + contentAsString + "\noffset-transformer").getBytes());
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
                return sourceMap;
            }
        };
    }
}