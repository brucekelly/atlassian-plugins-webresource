package com.atlassian.plugin.webresource;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.LegacyAddLocation;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.collect.ImmutableMap;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformerAlias;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformers;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.buildUrl;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestWebResourceTransformation extends TestCase
{
    @Before @After
    public void cleanup()
    {
        WebResourceTransformation.TRANSFORMER_PARAMETERS = WebResourceTransformation.buildTransformerParameters();
    }


    @Test
    public void testMatches() throws DocumentException
    {
        WebResourceTransformation trans = new WebResourceTransformation(DocumentHelper.parseText(
                "<transformation extension=\"js\">\n" +
                    "<transformer key=\"foo\" />\n" +
                "</transformation>").getRootElement());
        ResourceLocation loc = mock(ResourceLocation.class);
        when(loc.getName()).thenReturn("foo.js");
        assertTrue(trans.matches(loc));
    }

    @Test
    public void testNotMatches() throws DocumentException
    {
        WebResourceTransformation trans = new WebResourceTransformation(DocumentHelper.parseText(
                "<transformation extension=\"js\">\n" +
                    "<transformer key=\"foo\" />\n" +
                "</transformation>").getRootElement());
        ResourceLocation loc = mock(ResourceLocation.class);
        when(loc.getName()).thenReturn("foo.cs");
        assertFalse(trans.matches(loc));
    }

    @Test
    public void testMatchesString() throws DocumentException
    {
        WebResourceTransformation trans = new WebResourceTransformation(DocumentHelper.parseText(
                "<transformation extension=\"js\">\n" +
                        "<transformer key=\"foo\" />\n" +
                        "</transformation>").getRootElement());
        assertTrue(trans.matches("js"));
    }

    @Test
    public void testNotMatchesString() throws DocumentException
    {
        WebResourceTransformation trans = new WebResourceTransformation(DocumentHelper.parseText(
                "<transformation extension=\"js\">\n" +
                        "<transformer key=\"foo\" />\n" +
                        "</transformation>").getRootElement());
        assertFalse(trans.matches("css"));
    }

    @Test
    public void testNoExtension() throws DocumentException
    {
        try
        {
            new WebResourceTransformation(DocumentHelper.parseText(
                    "<transformation>\n" +
                            "<transformer key=\"foo\" />\n" +
                            "</transformation>").getRootElement());
            fail("Should have forced extension");
        }
        catch (IllegalArgumentException ex)
        {
            // pass
        }
    }

    // moved to TestUseCases.shouldApplyLegacyTransformers
    // public void testTransformDownloadableResource()

    // moved to TestUseCases.shouldApplyTransformers
    // public void testTransformDownloadableResourceWithUrlReadingPresent()

    // old name testTransformTwoDownloadableResources
    @Test
    public void shouldApplyTwoTransformers()
    {
        wr.configure()
            .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .transformer("LegacyAddLocation", LegacyAddLocation.class)
                .webResource("a")
                    .transformation("js", "AddLocation", "LegacyAddLocation")
                    .resource("a1.js")
        .end();

        String content = wr.getContent(resourceUrl("plugin:a", "a1.js", buildMap("location", "true")));
        assertThat(content, equalTo(
            "location=/a1.js (legacy)\n" +
            "location=plugin:a/a1.js (transformer)\n" +
            "content of a1.js"
        ));
    }

    // moved to testUrlMappedDownloadableResource
//    @Test public void testUrlMappedDownloadableResource()

    @Test
    public void testUrlMappedUrlByAlias() throws DocumentException, DownloadException
    {
        Element element = DocumentHelper.parseText(
                "<transformation extension=\"js\"><transformer key=\"foo-alias\"/></transformation>").getRootElement();
        WebResourceTransformation trans = new WebResourceTransformation(element);
        PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        TransformerCache transformerCache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);

        mockTransformers(pluginAccessor, ImmutableMap.of("foo", new UrlAwarePrefixTransformer("foo-key", "foo-value")));
        mockTransformerAlias(pluginAccessor, "foo", "foo-alias");

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        trans.addTransformParameters(transformerCache, mock(TransformerParameters.class), urlBuilder);

        verify(urlBuilder).addToQueryString("foo-key", "foo-value");
    }

    // old name testUrlMappedDownloadableResourceByAlias
    @Test
    public void shouldResolveTransformerByAlias()
    {
        wr.configure()
            .plugin("plugin")
                .transformer("notUsed", "AddLocationAlias", AddLocation.class)
                .webResource("a")
                    .transformation("js", "AddLocationAlias")
                    .resource("a1.js")
        .end();

        String content = wr.getContent(resourceUrl("plugin:a", "a1.js", buildMap("location", "true")));
        assertThat(content, equalTo(
            "location=plugin:a/a1.js (transformer)\n" +
            "content of a1.js"
        ));
    }

    // old name testUrlMappedDownloadableResourceTakesPrecedenceOverOldTransform
    @Test
    public void shouldChooseUrlReadingTransformerOverLegacyTransformer()
    {
        wr.configure()
            .plugin("plugin")
                .transformer("addLocation", LegacyAddLocation.class)
                .transformer("notUsed", "AddLocation", AddLocation.class)
                .webResource("a")
                    .transformation("js", "AddLocation")
                    .resource("a1.js")
        .end();

        String content = wr.getContent(resourceUrl("plugin:a", "a1.js", buildMap("location", "true")));
        assertThat(content, equalTo(
            "location=plugin:a/a1.js (transformer)\n" +
            "content of a1.js"
        ));
    }

    private static class UrlAwarePrefixTransformer implements WebResourceTransformerFactory
    {
        private String key;
        private String value;

        public UrlAwarePrefixTransformer(String key, String value)
        {
            this.key = key;
            this.value = value;
        }

        @Override
        public TransformerUrlBuilder makeUrlBuilder(final TransformerParameters transformerParams)
        {
            return new TransformerUrlBuilder()
            {
                @Override
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToQueryString(key, value);
                }
            };
        }

        @Override
        public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters transformerParams)
        {
            return new UrlReadingWebResourceTransformer()
            {
                @Override
                public DownloadableResource transform(TransformableResource transformableResource, QueryParams params)
                {
                    final String value = params.get(key);
                    return new CharSequenceDownloadableResource(transformableResource.nextResource())
                    {
                        @Override
                        protected CharSequence transform(CharSequence originalContent)
                        {
                            return value + ": " + originalContent;
                        }
                    };
                }
            };
        }
    }

    @Test
    public void shouldBuildCacheKeyInSuchWayItIncludesOnlyParametersThatAreActuallyUsed()
    {
        wr.configure()
            .plugin("plugin")
                .transformer("AddLocation", AddLocation.class)
                .webResource("a")
                    .transformation("js", "AddLocation")
                    .resource("a.js", "content of a")
        .end();

        RequestCache requestCache = new RequestCache(wr.getGlobals());
        Resource resource = wr.getGlobals().getSnapshot().get("plugin:a").getResources(requestCache).get("a.js");
        String fullResourceName = resource.getKey() + ":" + resource.getName();

        // It should return all the parameters if parameters for transformer is unknown.
        Map<String, String> params = buildMap("location", "true", "some", "thing", RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, "false");
        assertThat(
            Helpers.buildKey(wr.getGlobals(), resource, params),
            equalTo(buildUrl(fullResourceName, buildMap("location", "true", "some", "thing", RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, "false")))
        );

        // It should return only used parameters if parameters for transformer is known,
        WebResourceTransformation.TRANSFORMER_PARAMETERS.put(AddLocation.class.getName(), new String[]{"location"});
        params = buildMap("location", "true", "some", "thing", RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, "false");
        assertThat(
            Helpers.buildKey(wr.getGlobals(), resource, params),
            equalTo(buildUrl(fullResourceName, buildMap("location", "true", RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY, "false")))
        );

        // It should only add known parameters if they are present.
        WebResourceTransformation.TRANSFORMER_PARAMETERS.put(AddLocation.class.getName(), new String[]{"location"});
        params = buildMap("some", "thing");
        assertThat(
            Helpers.buildKey(wr.getGlobals(), resource, params),
            equalTo(buildUrl(fullResourceName, buildMap()))
        );
    }
}
