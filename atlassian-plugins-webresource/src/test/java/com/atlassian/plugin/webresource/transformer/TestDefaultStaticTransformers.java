package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import java.util.Arrays;

import static com.atlassian.plugin.webresource.TestUtils.asContent;
import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultStaticTransformers
{
    @Test public void doNothing(){}
    @Mock
    StaticTransformersSupplier staticTransformersSupplier;

    private StaticTransformers staticTransformers;

    @Before
    public void setup()
    {
        staticTransformers = new DefaultStaticTransformers(staticTransformersSupplier);
    }

    @Test
    public void testAddToUrl()
    {
        TransformerUrlBuilder transformerUrlBuilder = mock(TransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder, mock(UrlReadingContentTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module"), urlBuilder);

        verify(transformerUrlBuilder).addToUrl(urlBuilder);
    }

    @Test
    public void testAddToUrlOrder()
    {
        TransformerUrlBuilder transformerUrlBuilder1 = mock(TransformerUrlBuilder.class);
        TransformerUrlBuilder transformerUrlBuilder2 = mock(TransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder1, mock(UrlReadingContentTransformer.class)),
                trans("css", transformerUrlBuilder2, mock(UrlReadingContentTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module"), urlBuilder);

        InOrder inOrder = inOrder(transformerUrlBuilder1, transformerUrlBuilder2);

        inOrder.verify(transformerUrlBuilder1).addToUrl(urlBuilder);
        inOrder.verify(transformerUrlBuilder2).addToUrl(urlBuilder);
    }

    @Test
    public void testAddToUrlFilter()
    {
        TransformerUrlBuilder transformerUrlBuilder1 = mock(TransformerUrlBuilder.class);
        TransformerUrlBuilder transformerUrlBuilder2 = mock(TransformerUrlBuilder.class);
        mockTransformerSuppliers(
                trans("css", transformerUrlBuilder1, mock(UrlReadingContentTransformer.class)),
                trans("js", transformerUrlBuilder2, mock(UrlReadingContentTransformer.class))
        );

        UrlBuilder urlBuilder = mock(UrlBuilder.class);
        staticTransformers.addToUrl("css", new TransformerParameters("com.test", "my-module"), urlBuilder);

        verify(transformerUrlBuilder1).addToUrl(urlBuilder);
        verify(transformerUrlBuilder2, never()).addToUrl(urlBuilder);
    }

    @Test
    public void testTransform()
    {
        UrlReadingContentTransformer transformer = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(TransformerUrlBuilder.class), transformer)
        );

        Content content = asContent("");
        ResourceLocation resourceLocation = mockResourceLocation("/wow.css", "wow.css");
        QueryParams queryParams = emptyQueryParams();
        TransformerParameters transformerParameters = new TransformerParameters("com.test", "my-module");
        staticTransformers.transform(content, transformerParameters, resourceLocation, queryParams, null);
        verify(transformer).transform(content, resourceLocation, queryParams, null);
    }

    @Test
    public void testTransformOrder()
    {
        UrlReadingContentTransformer transformer1 = mockTransformer();
        UrlReadingContentTransformer transformer2 = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(TransformerUrlBuilder.class), transformer1),
                trans("css", mock(TransformerUrlBuilder.class), transformer2)
        );
        final Content transformed = mock(Content.class);

        Content content = asContent("");
        ResourceLocation resourceLocation = mockResourceLocation("/wow.css", "wow.css");
        QueryParams queryParams = emptyQueryParams();
        when(transformer1.transform(content, resourceLocation, queryParams, null)).thenReturn(transformed);
        staticTransformers.transform(content, new TransformerParameters("com.test", "my-module"), resourceLocation, queryParams, null);

        InOrder inOrder = inOrder(transformer1, transformer2);

        inOrder.verify(transformer1).transform(content, resourceLocation, queryParams, null);
        inOrder.verify(transformer2).transform(eq(transformed), eq(resourceLocation), eq(queryParams), any(String.class));
    }

    @Test
    public void testTransformFilter()
    {
        UrlReadingContentTransformer transformer1 = mockTransformer();
        UrlReadingContentTransformer transformer2 = mockTransformer();
        mockTransformerSuppliers(
                trans("css", mock(TransformerUrlBuilder.class), transformer1),
                trans("js", mock(TransformerUrlBuilder.class), transformer2)
        );

        Content content = asContent("");
        ResourceLocation resourceLocation = mockResourceLocation("/wow.css", "wow.css");
        QueryParams queryParams = emptyQueryParams();
        staticTransformers.transform(content, new TransformerParameters("com.test", "my-module"), resourceLocation, queryParams, null);

        verify(transformer1).transform(content, resourceLocation, queryParams, null);
        verify(transformer2, never()).transform(content, resourceLocation, queryParams, null);
    }

    private static ResourceLocation mockResourceLocation(String location, String name)
    {
        ResourceLocation resourceLocation = mock(ResourceLocation.class);
        when(resourceLocation.getLocation()).thenReturn(location);
        when(resourceLocation.getName()).thenReturn(name);
        return resourceLocation;
    }

    private static TransformerForType trans(String type, TransformerUrlBuilder urlBuilder, UrlReadingContentTransformer transformer)
    {
        ContentTransformerFactory mock = mock(ContentTransformerFactory.class);
        when(mock.makeUrlBuilder(any(TransformerParameters.class))).thenReturn(urlBuilder);
        when(mock.makeResourceTransformer(any(TransformerParameters.class))).thenReturn(transformer);
        return new TransformerForType(type, mock);
    }

    private void mockTransformerSuppliers(final TransformerForType... transformerSpecs)
    {
        when(staticTransformersSupplier.get(any(String.class))).thenAnswer(new Answer<Iterable<ContentTransformerFactory>>()
        {
            @Override
            public Iterable<ContentTransformerFactory> answer(InvocationOnMock invocation) throws Throwable
            {
                final String type = (String) invocation.getArguments()[0];
                return toTransformerFactories(Arrays.asList(transformerSpecs), new Predicate<TransformerForType>()
                {
                    @Override
                    public boolean apply(@Nullable TransformerForType transformerSpec)
                    {
                        return type.equals(transformerSpec.type);
                    }
                });
            }
        });
        when(staticTransformersSupplier.get(any(ResourceLocation.class))).thenAnswer(new Answer<Iterable<ContentTransformerFactory>>()
        {
            @Override
            public Iterable<ContentTransformerFactory> answer(InvocationOnMock invocation) throws Throwable
            {
                final ResourceLocation resourceLocation = (ResourceLocation) invocation.getArguments()[0];
                return toTransformerFactories(Arrays.asList(transformerSpecs), new Predicate<TransformerForType>()
                {
                    @Override
                    public boolean apply(@Nullable TransformerForType transformerSpec)
                    {
                        return resourceLocation.getName().endsWith("." + transformerSpec.type);
                    }
                });
            }
        });
    }

    private static Iterable<ContentTransformerFactory> toTransformerFactories(Iterable<TransformerForType> transformerSpecs, Predicate<TransformerForType> predicate)
    {
        return Iterables.transform(Iterables.filter(transformerSpecs, predicate), new Function<TransformerForType, ContentTransformerFactory>()
        {
            @Override
            public ContentTransformerFactory apply(@Nullable TransformerForType transformerSpec)
            {
                return transformerSpec.transformerFactory;
            }
        });
    }

    private static final class TransformerForType
    {
        final String type;
        final ContentTransformerFactory transformerFactory;

        private TransformerForType(String type, ContentTransformerFactory transformerFactory)
        {
            this.type = type;
            this.transformerFactory = transformerFactory;
        }
    }

    private static UrlReadingContentTransformer mockTransformer()
    {
        UrlReadingContentTransformer transformer = mock(UrlReadingContentTransformer.class);
        when(transformer.transform(any(Content.class), any(ResourceLocation.class), any(QueryParams.class), any(String.class)))
            .thenReturn(asContent(""));
        return transformer;
    }
}
