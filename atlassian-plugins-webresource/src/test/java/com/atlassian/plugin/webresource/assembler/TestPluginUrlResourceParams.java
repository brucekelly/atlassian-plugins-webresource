package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestPluginUrlResourceParams
{
    @Test
    public void testJs()
    {
        PluginJsResourceParams params = new DefaultPluginJsResourceParams(ImmutableMap.of(
                "conditionalComment", "ie lt 10",
                "ieonly", "true",
                "charset", "UTF-16",
                "title", "4shizzle"
        ));
        assertEquals("ie lt 10", params.conditionalComment());
        assertTrue(params.ieOnly());
        assertEquals(ImmutableMap.of("charset", "UTF-16", "title", "4shizzle"), params.other());
        assertEquals(ImmutableMap.of(
                "conditionalComment", "ie lt 10",
                "ieonly", "true",
                "charset", "UTF-16",
                "title", "4shizzle"), params.all());
    }

    @Test
    public void testCss()
    {
        PluginCssResourceParams params = new DefaultPluginCssResourceParams(ImmutableMap.of(
                "conditionalComment", "ie lt 10",
                "ieonly", "true",
                "media", "print",
                "charset", "UTF-16",
                "title", "4shizzle"
        ));
        assertEquals("ie lt 10", params.conditionalComment());
        assertTrue(params.ieOnly());
        assertEquals("print", params.media());
        assertEquals(ImmutableMap.of("charset", "UTF-16", "title", "4shizzle"), params.other());
        assertEquals(ImmutableMap.of(
                "conditionalComment", "ie lt 10",
                "ieonly", "true",
                "media", "print",
                "charset", "UTF-16",
                "title", "4shizzle"), params.all());
    }
}
