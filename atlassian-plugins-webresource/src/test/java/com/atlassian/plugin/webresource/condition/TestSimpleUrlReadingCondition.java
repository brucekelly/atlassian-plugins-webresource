package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestSimpleUrlReadingCondition
{
    private final class TrueSimpleUrlReadingCondition extends SimpleUrlReadingCondition
    {
        @Override
        protected boolean isConditionTrue()
        {
            return true;
        }

        @Override
        protected String queryKey()
        {
            return "ermagherd-yerrs";
        }
    }

    private final class FalseSimpleUrlReadingCondition extends SimpleUrlReadingCondition
    {
        @Override
        protected boolean isConditionTrue()
        {
            return false;
        }

        @Override
        protected String queryKey()
        {
            return "ermagherd-nerrr";
        }
    }

    @Test
    public void testTrue()
    {
        SimpleUrlReadingCondition trueCondition = new TrueSimpleUrlReadingCondition();

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        trueCondition.addToUrl(urlBuilder);

        assertEquals(String.valueOf("true"), urlBuilder.buildParams().get("ermagherd-yerrs"));

        QueryParams queryParams = QueryParams.of(urlBuilder.buildParams());

        assertTrue(trueCondition.shouldDisplay(queryParams));
    }

    @Test
    public void testFalse()
    {
        SimpleUrlReadingCondition falseCondition = new FalseSimpleUrlReadingCondition();

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        falseCondition.addToUrl(urlBuilder);

        assertFalse(urlBuilder.buildParams().containsKey("ermagherd-nerrr"));

        QueryParams queryParams = QueryParams.of(urlBuilder.buildParams());

        assertFalse(falseCondition.shouldDisplay(queryParams));
    }
}
