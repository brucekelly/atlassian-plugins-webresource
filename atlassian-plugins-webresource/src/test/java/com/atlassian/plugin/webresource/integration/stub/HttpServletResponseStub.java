package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.google.common.base.Joiner;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HttpServletResponseStub
{
    protected static class ResponseDataImpl extends ResponseData
    {
        int status = 200;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Map<String, String> headers = new HashMap<String, String>();

        @Override
        public int getStatus()
        {
            return status;
        }

        @Override
        public String getContent()
        {
            return out.toString();
        }

        @Override
        public Map<String, String> getHeaders()
        {
            return headers;
        }

        @Override
        public String toString()
        {
            Joiner.MapJoiner joiner = Joiner.on(", ").withKeyValueSeparator("=");
            return status + " {" + joiner.join(headers) + "} \"" + out.toString() + "\"";
        }
    }

    public static Tuple<HttpServletResponse, ResponseData> build()
    {
        final ResponseDataImpl responseData = new ResponseDataImpl();

        HttpServletResponse response = mock(HttpServletResponse.class);

        // getOutputStream
        ServletOutputStream servletOut = new ServletOutputStream()
        {
            public void write(int b) throws IOException
            {
                responseData.out.write(b);
            }
        };
        try
        {
            when(response.getOutputStream()).thenReturn(servletOut);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        // setHeader
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                responseData.headers.put(invocation.getArguments()[0].toString(),
                        invocation.getArguments()[1].toString());
                return null;
            }
        }).when(response).setHeader(anyString(), anyString());

        // setContentType
        doAnswer(new Answer()
        {
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                responseData.headers.put("Content-Type", invocation.getArguments()[0].toString());
                return null;
            }
        }).when(response).setContentType(anyString());

        // sendError
        try
        {
            doAnswer(new Answer<Void>()
            {
                public Void answer(InvocationOnMock invocation) throws Throwable
                {
                    responseData.status = (Integer) invocation.getArguments()[0];
                    return null;
                }
            }).when(response).sendError(anyInt());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        // setStatus
        doAnswer(new Answer<Void>()
        {
            public Void answer(InvocationOnMock invocation) throws Throwable
            {
                responseData.status = (Integer) invocation.getArguments()[0];
                return null;
            }
        }).when(response).setStatus(anyInt());

        // Redirect.
        try
        {
            doAnswer(new Answer()
            {
                @Override
                public Void answer(InvocationOnMock invocation) throws Throwable
                {
                    responseData.status = 301;
                    responseData.headers.put("Location", invocation.getArguments()[0].toString());
                    return null;
                }
            }).when(response).sendRedirect(anyString());
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return new Tuple<HttpServletResponse, ResponseData>(response, responseData);
    }
}
