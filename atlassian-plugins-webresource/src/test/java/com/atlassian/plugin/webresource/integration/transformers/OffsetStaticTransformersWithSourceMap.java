package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Util;

import java.io.IOException;
import java.io.OutputStream;

import static com.google.common.base.Preconditions.checkNotNull;

public class OffsetStaticTransformersWithSourceMap implements StaticTransformers
{
    public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder)
    {
    }

    @Override
    public Content transform(final Content content, TransformerParameters transformerParameters,
            ResourceLocation resourceLocation, QueryParams queryParams, String sourceUrl)
    {
        return new ContentImpl(content.getContentType(), true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                SourceMap sourceMap;
                try
                {
                    out.write(("offset-static-transformer\n").getBytes());
                    sourceMap = content.writeTo(out, isSourceMapEnabled);
                    out.write(("\noffset-static-transformer").getBytes());
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
                if (isSourceMapEnabled && (sourceMap != null))
                {
                    sourceMap = Util.offset(sourceMap, 1);
                }
                return sourceMap;
            }
        };
    }
}