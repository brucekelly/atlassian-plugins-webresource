package com.atlassian.plugin.webresource.assembler;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.SampleDataProvider;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.plugin.webresource.TestUtils.emptyDataProvider;
import static com.atlassian.plugin.webresource.TestUtils.jsonableOf;
import static com.atlassian.plugin.webresource.TestUtils.stringDataProvider;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedData;
import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

public class TestWebResourceAssemblerData
{
    private AssemblerTestFixture f;

    @Before
    public void setup()
    {
        f = new AssemblerTestFixture();
    }

    @Test
    public void testSingleResource()
    {
        f.mockPlugin("test.atlassian:nodep-resource").data("a", emptyDataProvider());

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:nodep-resource.a");
    }

    @Test
    public void testSingleResourceTwoDataItems()
    {
        f.mockPlugin("test.atlassian:nodep-resource").data("a", emptyDataProvider())
            .data("b", emptyDataProvider());

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:nodep-resource.a", "test.atlassian:nodep-resource.b");
    }

    @Test
    public void testSingleResourceDependency()
    {
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        f.mockPlugin("test.atlassian:b").deps("test.atlassian:a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testContextBatch()
    {
        f.mockPlugin("test.atlassian:a").ctx("context.a").data("a", emptyDataProvider());

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testContextBatchDependency()
    {
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        f.mockPlugin("test.atlassian:b").ctx("context.a").deps("test.atlassian:a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testSuperbatch()
    {
        f.mockSuperbatch("test.atlassian:a");
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());

        WebResourceAssembler assembler = f.create();
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testSuperbatchDependency()
    {
        f.mockSuperbatch("test.atlassian:b");
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        f.mockPlugin("test.atlassian:b").deps("test.atlassian:a");

        WebResourceAssembler assembler = f.create();
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testNoDuplicatesWhenMultipleDependencies()
    {
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        f.mockPlugin("test.atlassian:b").deps("test.atlassian:a");
        f.mockSuperbatch("test.atlassian:b");
        f.mockPlugin("test.atlassian:c").ctx("context.c").deps("test.atlassian:a");
        f.mockPlugin("test.atlassian:d").deps("test.atlassian:a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().requireContext("context.c");
        assembler.resources().requireWebResource("test.atlassian:d");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testSingleResourceMultipleTimes()
    {
        f.mockPlugin("test.atlassian:nodep-resource").data("a", emptyDataProvider());
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources);
    }

    @Test
    public void testRequireData()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", jsonableOf("man"));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "duff");
    }

    @Test
    public void testRequireDataMultipleTimes()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", jsonableOf("man"));
        assembler.assembled().drainIncludedResources();

        assembler.data().requireData("duff", jsonableOf("man"));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources);
    }

    @Test
    public void testSingleResourceUnbatched()
    {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources, "test.atlassian:a.a");
    }

    @Test
    public void testRequireDataUnbatchedMultipleTimes()
    {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").data("a", emptyDataProvider());
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources);
    }

    @Test
    public void testRequireDataValue()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", jsonableOf("man"));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
    }

    @Test
    public void testRequireDataStringValue()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", "man");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"\\\"man\\\"\";\n");
    }

    @Test
    public void testRequireDataNumberValue()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", 1L);
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"1\";\n");
    }

    @Test
    public void testRequireDataBooleanValueTrue()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", true);
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"true\";\n");
    }

    @Test
    public void testRequireDataBooleanValueFalse()
    {
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", false);
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"false\";\n");
    }

    @Test
    public void testRequireDataValueNull()
    {
        String test = null;
        WebResourceAssembler assembler = f.create();
        assembler.data().requireData("duff", test);
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertDataTag(resources, "WRM._unparsedData[\"duff\"]=\"null\";\n");
    }

    @Test
    public void testNotCachingDataResources()
    {
        WebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.data().requireData("duff", "old");
        WebResourceSet resources1 = assembler1.assembled().drainIncludedResources();

        assertDataTag(resources1, "WRM._unparsedData[\"duff\"]=\"\\\"old\\\"\";\n");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.data().requireData("duff", "new");
        WebResourceSet resources2 = assembler2.assembled().drainIncludedResources();

        assertDataTag(resources2, "WRM._unparsedData[\"duff\"]=\"\\\"new\\\"\";\n");
    }

    @Test
    public void testCacheGeneratingDataForRequiredResources()
    {
        WebResourceAssemblerFactory factory = f.factory();
        f.mockPlugin("test.atlassian:a").data("a", stringDataProvider("value"));

        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources1 = assembler1.assembled().drainIncludedResources();

        assertDataTag(resources1, "WRM._unparsedData[\"test.atlassian:a.a\"]=\"\\\"value\\\"\";\n");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources2 = assembler2.assembled().drainIncludedResources();

        assertDataTag(resources2, "WRM._unparsedData[\"test.atlassian:a.a\"]=\"\\\"value\\\"\";\n");
    }

    @Test
    public void testCacheNotGeneratingDataForRequiredResourcesWithFailingCondition1s()
    {
        WebResourceAssemblerFactory factory = f.factory();
        f.mockPlugin("test.atlassian:a").data("a", stringDataProvider("value")).condition(f.condition().shouldDisplay(false).canEncodeStateIntoUrl(false).build());

        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources1 = assembler1.assembled().drainIncludedResources();

        assertResolvedData(resources1);

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources2 = assembler2.assembled().drainIncludedResources();

        assertResolvedData(resources2);
    }

    @Test
    public void testCacheNotGeneratingDataForRequiredResourcesWithFailingCondition2s()
    {
        WebResourceAssemblerFactory factory = f.factory();
        f.mockPlugin("test.atlassian:a").data("a", stringDataProvider("value")).condition(f.condition().shouldDisplay(false).build());

        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources1 = assembler1.assembled().drainIncludedResources();

        assertResolvedData(resources1);

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources2 = assembler2.assembled().drainIncludedResources();

        assertResolvedData(resources2);
    }

    @Test
    public void testNotEvaluateExcludedDataProvidersForLegacyCondition()
    {
        WebResourceAssemblerFactory factory = f.factory();

        WebResourceDataProvider dataProvider = mock(WebResourceDataProvider.class);
        f.mockPlugin("test.atlassian:a").data("a", dataProvider).condition(f.condition().shouldDisplay(false).canEncodeStateIntoUrl(false).build());

        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("test.atlassian:a");
        assembler1.assembled().drainIncludedResources();

        verify(dataProvider, never()).get();
    }

    @Test
    public void testNotEvaluateExcludedDataProviders()
    {
        WebResourceAssemblerFactory factory = f.factory();

        WebResourceDataProvider dataProvider = mock(WebResourceDataProvider.class);
        f.mockPlugin("test.atlassian:b").data("b", dataProvider);
        f.mockPlugin("test.atlassian:a").data("a", dataProvider)
            .deps("test.atlassian:b")
            .condition(f.condition().shouldDisplay(false).canEncodeStateIntoUrl(true).build());

        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("test.atlassian:a");
        assembler1.assembled().drainIncludedResources();

        verify(dataProvider, never()).get();
    }

    @Test
    public void shouldNotIncludeDataProvidersMultipleTimes()
    {
        WebResource wr = new WebResourceImpl();
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("general")
                    .dependency("plugin:b")
                    .data("a1", SampleDataProvider.class)
                    .resource("a1.js")
                .webResource("b")
                    .data("b1", SampleDataProvider.class)
                    .resource("b1.js")
                .webResource("c")
                    .dependency("plugin:d")
                    .data("c1", SampleDataProvider.class)
                    .resource("c1.js")
                .webResource("d")
                    .data("d1", SampleDataProvider.class)
                    .resource("d1.js")
            .end();

        wr.requireContext("general");
        wr.requireResource("plugin:c");

        assertThat(wr.pathsAsHtml(), matches(
            "b1", "value of sample data provider",
            "a1", "value of sample data provider",
            "d1", "value of sample data provider",
            "c1", "value of sample data provider"
        ));

        wr.requireContext("general");
        wr.requireResource("plugin:c");

        assertThat(wr.pathsAsHtml(), isEmptyString());

        // It is important to require it for the third time, not just two times.
        wr.requireContext("general");
        wr.requireResource("plugin:c");

        assertThat(wr.pathsAsHtml(), isEmptyString());
    }
}
