package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.condition.AlwaysTrueLegacyCondition;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.integration.transformers.AddHash;
import com.atlassian.plugin.webresource.integration.transformers.AddLocation;
import com.atlassian.plugin.webresource.integration.transformers.LegacyAddLocation;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.printShortPaths;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.superBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.HashBuilder.buildHash;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class TestContextBatch
{
    private WebResourceImpl wr;

    @Before
    public void setUp() throws Exception
    {
        wr = new WebResourceImpl();
    }

    @Test
    public void testBuildPluginResources()
    {
        wr.configure()
            .plugin("a.b.c")
                .webResource("d")
                    .context("key")
                    .resource("d1.css")
                    .resource("d1.js")
                    .resource("d2.css")
                        .param("ieonly", "true")
                .webResource("e")
                    .context("key")
                    .resource("e1.js")
            .plugin("f.g.h")
                .webResource("i")
                    .context("key")
                    .resource("i.js")
        .end();

        wr.requireContext("key");

        assertThat(wr.paths(), matches(
            contextBatchUrl("key", "css"),
            contextBatchUrl("key", "css", buildMap("ieonly", "true")),
            contextBatchUrl("key", "js")
        ));
    }

    @Test
    public void testBuildPluginResourcesWithTransform()
    {
        wr.configure()
            .plugin("a.b.c")
                .transformer("AddLocation", AddLocation.class)
                .webResource("d")
                    .context("key")
                    .transformation("js", "AddLocation")
                    .resource("d1.js")
        .end();

        wr.requireContext("key");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            contextBatchUrl("key", "js", buildMap("location", "true"))
        ));

        assertThat(paths, matches(buildHash("location")));
        assertThat(paths, matches("-CDN/"));
    }

    @Test
    public void testBuildPluginResourcesWithTransformThatDoesNotSupportCDN()
    {
        wr.configure()
            .plugin("a.b.c")
                .transformer("AddLocation", LegacyAddLocation.class)
                .transformer("AddHash", AddHash.class)
                .webResource("d")
                    .context("key")
                    .transformation("js", "AddLocation")
                    .transformation("js", "AddHash")
                    .resource("d1.js")
        .end();

        wr.requireContext("key");

        List<String> paths = wr.paths();
        assertThat(paths, matches(contextBatchUrl("key", "js")));

        assertThat(paths, matches(buildHash("some-value")));
        assertThat(paths, matches("-T/"));
    }

//    Disabled, same as the previous test.
//    @Test
//    public void testBuildPluginResourcesWithTransformForType()
//    {
//        List<WebResourceModuleDescriptor> moduleDescriptors = new ArrayList<WebResourceModuleDescriptor>(3);
//        moduleDescriptors.add(TestUtils.createWebResourceModuleDescriptor("a.b.c:d", testPlugin,
//                Collections.<ResourceDescriptor>emptyList(), Collections.<String>emptyList(), Collections.<String>emptySet()));
//
//
//        ListMultimap<PluginResourceBatchParams, UrlParameters> resourceParams = ArrayListMultimap.create();
//        resourceParams.put(new PluginResourceBatchParams("js", Collections.<String,String>emptyMap()),
//                createTaintedUrlParameters(createNamedHashes("hash-name-1", "hash-1"), ImmutableMap.of("something", "value")));
//
//        ContextBatch contextBatch = new ContextBatch(globals, "key", Collections.singletonList("includedContext"), Collections.singletonList("excludedContext"),
//                moduleDescriptors, resourceParams);
//
//        Iterable<PluginResource> buildPluginResources = contextBatch.buildPluginResources(false);
//
//        Iterator<PluginResource> iterator = buildPluginResources.iterator();
//
//        assertTrue(iterator.hasNext());
//        PluginResource resource1 = iterator.next();
//        assertEquals(ImmutableMap.of("something", "value"), resource1.getParamsWithBatchableParams());
//        assertEquals(buildHash(createNamedHashes("hash-name-1", "hash-1")), resource1.getHash());
//        assertEquals(false, resource1.isCdnSupported());
//    }

    public static class JsTransformer implements WebResourceTransformerFactory
    {
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
        {
            return new TransformerUrlBuilder()
            {
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToQueryString("js-transformer", "true");
                }
            };
        }

        public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters)
        {
            return null;
        }
    }

    public static class CssTransformer implements WebResourceTransformerFactory
    {
        public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
        {
            return new TransformerUrlBuilder()
            {
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    urlBuilder.addToQueryString("css-transformer", "true");
                }
            };
        }

        public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters)
        {
            return null;
        }
    }

    @Test
    public void testBuildPluginResourcesWithTransformForMultipleType()
    {
        wr.configure()
            .plugin("plugin")
                .transformer("cssTransformer", CssTransformer.class)
                .transformer("jsTransformer", JsTransformer.class)
                .webResource("a")
                    .transformation("css", "cssTransformer")
                    .transformation("js", "jsTransformer")
                    .context("key")
                    .resource("a1.css")
                    .resource("a1.js")
        .end();

        wr.requireContext("key");

        List<String> paths = wr.paths();
        assertThat(paths, matches(
            contextBatchUrl("key", "css", buildMap("css-transformer", "true")),
            contextBatchUrl("key", "js", buildMap("js-transformer", "true"))
        ));

        assertThat(paths.get(0), not(containsString("js")));
        assertThat(paths.get(1), not(containsString("css")));
    }

    @Test
    public void shouldSubtractFromContext()
    {
        // same as testSubtractTwoBatchesFromOperand
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("key")
                    .resource("a1.css")
                .webResource("b")
                    .context("key")
                    .resource("b1.css")
                .webResource("c")
                    .context("key")
                    .resource("c1.css")
                .webResource("d")
                    .context("key")
                    .resource("d1.css")
        .end();

        wr.exclude("plugin:b", "plugin:d");
        wr.requireContext("key");

        assertThat(wr.paths(), matches(
           webResourceBatchUrl("plugin:a", "css"),
           webResourceBatchUrl("plugin:c", "css")
        ));
    }

    @Test
    public void shuldSwitchToWebResourceBatchIfAnyOfContextBatchWebResourcesHaveBeenAlreadyExcluded()
    {
        wr.configure()
          .plugin("plugin")
            .webResource("a")
              .dependency("plugin:b")
              .context("general")
              .resource("a1.js")
            .webResource("b")
              .resource("b1.js")
        .end();

        wr.requireContext("general");
        wr.exclude("plugin:b");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:a", "js")
        ));
    }

    @Test
    public void shuldSwitchToWebResourceBatchIfAnyOfContextBatchWebResourcesHaveBeenAlreadyExcluded2()
    {
        wr.configure()
          .plugin("plugin")
            .webResource("a")
              .context("general")
              .resource("a1.js")
            .webResource("b")
              .context("general")
              .resource("b1.js")
        .end();

        wr.include("_context:general");
        wr.exclude("plugin:b");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:a", "js")
        ));
    }

    // Unspecified behavior, exists only to ensure backward compatibility.
    @Test
    public void shouldChangeOrderingIfContextBatchSplittedIntoWebResourceBatches()
    {
        WebResourceImpl wr = new WebResourceImpl();
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .dependency("plugin:a")
                    .context("general")
                    .resource("b1.js")
                .webResource("c")
                    .context("general")
                    .resource("c2.js")
                .webResource("d")
                    .context("special")
                    .resource("d1.js")
                .webResource("e")
                    .resource("e1.js")
        .end();

        wr.exclude("plugin:c");
        wr.requireContext("general");
        wr.requireContext("special");
        wr.requireResource("plugin:e");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            webResourceBatchUrl("plugin:e", "js"),
            webResourceBatchUrl("plugin:a", "js"),
            webResourceBatchUrl("plugin:b", "js"),
            webResourceBatchUrl("plugin:d", "js")
        ));
    }

    // Unspecified behavior, exists only to ensure backward compatibility.
    @Test
    public void shouldMaintainOrderOfContextBatchKeysTheSameAsInOriginalIncludeClause()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .context("general")
                    .resource("a1.js")
                .webResource("b")
                    .context("general")
                    .resource("b1.js")
                .webResource("c")
                    .context("general")
                    .resource("c1.js")
        .end();

        wr.include("plugin:c", "plugin:a", "_context:general");
        wr.exclude("plugin:b");
        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:c", "js"),
            webResourceBatchUrl("plugin:a", "js")
        ));
    }

    @Test
    public void shouldMaintainProperOrderForLegacyWebResources()
    {
        // Needed to maintain backward compatibility.
        wr.configure()
            .plugin("plugin")
                .webResource("before")
                    .resource("before1.js")
                .webResource("a")
                    .dependency("plugin:c")
                    .dependency("plugin:bLegacy")
                    .dependency("plugin:d")
                    .resource("a1.js")
                .webResource("bLegacy")
                    .context("general")
                    .condition(AlwaysTrueLegacyCondition.class)
                    .resource("bLegacy1.js")
                .webResource("c")
                    .resource("c1.js")
                .webResource("d")
                    .resource("d1.js")
                .webResource("after")
                    .resource("after1.js")
        .end();

        wr.include("plugin:a", "plugin:before", "plugin:after", "_context:general");

        assertThat(wr.paths(), matches(
            webResourceBatchUrl("plugin:c", "js"),
            webResourceBatchUrl("plugin:bLegacy", "js"),
            webResourceBatchUrl("plugin:d", "js"),
            webResourceBatchUrl("plugin:a", "js"),
            webResourceBatchUrl("plugin:before", "js"),
            webResourceBatchUrl("plugin:after", "js")
        ));
    }

    // Unspecified behavior, exists only to ensure backward compatibility.
    @Test
    public void shouldChangeDependenciesOrderInWeirdWayAfterSplittingIntoNonOverlappingContextBatches()
    {
        wr.configure()
            .plugin("p")
                .webResource("a")
                    .dependency("p:c")
                    .context("c3")
                    .resource("resource1.js")
                .webResource("b")
                    .context("c2")
                    .resource("resource2.js")
                .webResource("c")
                    .dependency("p:b")
                    .dependency("p:a")
                    .context("c1")
                    .resource("resource3.js")
        .end();

        wr.include("_context:c1", "_context:c2", "_context:c3");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            "/download/contextbatch/js/c1,c3,c2/batch.js"
        ));
    }

    @Test
    public void shouldOrderContextsSameWayAsInPreviousImplementation()
    {
        wr.configure()
            .plugin("plugin1")
                .webResource("webResource2")
                    .dependency("plugin1:webResource3")
                    .resource("resource8.js")
                .webResource("webResource3")
                    .context("context3")
                    .resource("resource3.js")
                .webResource("webResource4")
                    .dependency("plugin1:webResource2")
                    .context("context1")
                    .context("context2")
                    .resource("resource4.js")
        .end();

        wr.include("_context:context2", "_context:context3", "_context:context1");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
           contextBatchUrl("context2,context1,context3", "js")
        ));
    }

    @Test
    public void shouldExcludeLegacyWebResourceInContextBatch()
    {
        wr.configure()
            .plugin("p")
                .webResource("a")
                    .context("context1")
                    .resource("a1.js")
                .webResource("legacyB")
                    .context("context1")
                    .condition(AlwaysTrueLegacyCondition.class)
                    .resource("legacyB1.js")
        .end();

        wr.include("_context:context1");
        wr.exclude("p:legacyB");

        assertThat(wr.paths(), matches(
           contextBatchUrl("context1", "js")
        ));
    }

    @Test
    public void shouldRemoveContextsThatHaveBeenAlreadyIncludedInSuperBatch()
    {
        wr.configure()
            .addToSuperbatch("p:a")
            .plugin("p")
                .webResource("a")
                    .context("c1")
                    .context("c2")
                    .resource("a1.js")
                .webResource("b")
                    .context("c2")
                    .resource("a1.js")
        .end();

        wr.include("_context:c1", "_context:c2");

        List<String> paths = wr.paths();

        assertThat(paths, matches(
            superBatchUrl("js"),
            contextBatchUrl("c2,-_super", "js")
        ));
    }

    @Test
    public void shouldExcludeDependenciesIncludedInSuperBatchWhenDetectingOverlappedContexts()
    {
        wr.configure()
            .addToSuperbatch("p:super")
            .plugin("p")
                .webResource("super")
                    .resource("super1.js")
                .webResource("b")
                    .dependency("p:super")
                    .context("c2")
                    .resource("b1.js")
                .webResource("d")
                    .dependency("p:super")
                    .context("c1")
                    .resource("d1.js")
        .end();

        wr.include("_context:c1", "_context:c2" );

        assertThat(wr.paths(), matches(
            superBatchUrl("js"),
            contextBatchUrl("c1,-_super", "js"),
            contextBatchUrl("c2,-_super", "js")
        ));
    }

    @Test
    public void testExcludeResourcesWithLegacyConditionsAndItsDependenciesFromContextBatch()
    {
        wr.configure()
            .plugin("p")
                .webResource("a")
                    .context("general")
                    .dependency("p:bLegacy")
                    .resource("a1.js")
                .webResource("bLegacy")
                    .condition(AlwaysTrueLegacyCondition.class)
                    .dependency("p:c")
                    .dependency("p:dLegacy")
                    .context("general")
                    .resource("bLegacy1.js")
                .webResource("c")
                    .resource("c1.js")
                .webResource("dLegacy")
                    .condition(AlwaysTrueLegacyCondition.class)
                    .resource("dLegacy1.js")
        .end();

        wr.requireContext("general");
        String allJs = wr.getContent(wr.paths());
        assertThat(allJs, matches(
            "content of a1",
            "content of c1",
            "content of dLegacy1",
            "content of bLegacy1"
        ));

        String contextBatchJs = wr.getContent(contextBatchUrl("general", "js"));
        assertThat(contextBatchJs, matches("content of a1"));
        assertThat(contextBatchJs, not(matches("content of bLegacy1")));
        assertThat(contextBatchJs, not(matches("content of c1")));
        assertThat(contextBatchJs, not(matches("content of dLegacy1")));
    }
}
