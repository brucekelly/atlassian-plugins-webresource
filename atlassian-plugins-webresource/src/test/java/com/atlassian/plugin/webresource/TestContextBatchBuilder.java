package com.atlassian.plugin.webresource;

import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.printShortPaths;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.setOf;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.indexOf;
import static com.google.common.collect.Iterables.size;
import static org.junit.Assert.assertThat;

public class TestContextBatchBuilder extends TestCase
{
    private WebResourceImpl wr;
    private WebResource.ConfigurationData configurationData;
    private WebResource.PluginData pluginData;

    @Override
    public void setUp() throws Exception
    {
        wr = new WebResourceImpl();
        configurationData = new WebResource.ConfigurationData();
        configurationData.isBatchingEnabled = true;
        pluginData = new WebResource.PluginData();
        pluginData.key = "test.atlassian";
        configurationData.plugins.add(pluginData);
    }

    public void testNoOverlapAndNoDependencies() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .resource("professorx.js")
                    .resource("professorx.css")
                    .resource("cyclops.css")
                .webResource("magneto-resources")
                    .context("brotherhood")
                    .resource("magneto.js")
                    .resource("magneto.css")
                    .resource("sabretooth.css")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen", "css"),
            contextBatchUrl("brotherhood", "css"),
            contextBatchUrl("xmen", "js"),
            contextBatchUrl("brotherhood", "js")
        ));
    }

    public void testOverlappingAndNoDependencies() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .context("brotherhood")
                    .resource("professorx.js")
                    .resource("professorx.css")
                    .resource("cyclops.css")
                .webResource("magneto-resources")
                    .context("xmen")
                    .context("brotherhood")
                    .resource("magneto.js")
                    .resource("magneto.css")
                    .resource("sabretooth.css")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen,brotherhood", "css"),
            contextBatchUrl("xmen,brotherhood", "js")
        ));
    }

    public void testDependenciesNoOverlap() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .resource("professorx.js")
                    .resource("professorx.css")
                    .resource("cyclops.css")
                .webResource("magneto-resources")
                    .context("brotherhood")
                    .resource("magneto.js")
                    .resource("magneto.css")
                    .resource("sabretooth.css")
                .webResource("students-resources")
                    .context("xmen")
                    .resource("iceman.js")
                    .resource("iceman.css")
                    .resource("rogue.css")
                .webResource("evil-students-resources")
                    .context("brotherhood")
                    .resource("pyro.css")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen", "css"),
            contextBatchUrl("brotherhood", "css"),
            contextBatchUrl("xmen", "js"),
            contextBatchUrl("brotherhood", "js")
        ));
    }

    public void testOverlappingDependencies() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .context("government")
                    .resource("professorx.js")
                    .resource("professorx.css")
                    .resource("cyclops.css")
                .webResource("magneto-resources")
                    .context("brotherhood")
                    .resource("magneto.js")
                    .resource("magneto.css")
                    .resource("sabretooth.css")
                .webResource("new-mutants-resources")
                    .context("xmen")
                    .resource("iceman.js")
                    .resource("iceman.css")
                    .resource("rogue.css")
                .webResource("government-resources")
                    .context("government")
                    .resource("beast.css")
                    .resource("beast.js")
                    .resource("deathstrike.css")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("government");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen,government", "css"),
            contextBatchUrl("xmen,government", "js")
        ));
    }

    public void testMultipleOverlappingContexts() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .context("rogue")
                    .resource("professorx.js")
                    .resource("professorx.css")
                    .resource("cyclops.css")
                .webResource("magneto-resources")
                    .context("rogue")
                    .context("brotherhood")
                    .resource("magneto.js")
                    .resource("magneto.css")
                    .resource("sabretooth.css")
                .webResource("new-mutants-resources")
                    .context("xmen")
                    .resource("iceman.js")
                    .resource("iceman.css")
                    .resource("rogue.css")
                .webResource("rogue-resources")
                    .context("rogue")
                    .resource("pyro.js")
                    .resource("phoenix.css")
                    .resource("mystique.css")
                .webResource("normal-resources")
                    .context("normals")
                    .resource("stryker.css")
                    .resource("stryker.js")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");
        wr.requireContext("rogue");
        wr.requireContext("normals");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen,rogue,brotherhood", "css"),
            contextBatchUrl("normals", "css"),
            contextBatchUrl("xmen,rogue,brotherhood", "js"),
            contextBatchUrl("normals", "js")
        ));
    }

    public void testContextParams() throws Exception
    {
        wr.configure()
            .plugin("test.atlassian")
                .webResource("xavier-resources")
                    .context("xmen")
                    .resource("professorx.css")
                    .resource("professorx.js")
                    .resource("storm-ie.css")
                        .param("ieonly", "true")
                .webResource("magneto-resources")
                    .context("brotherhood")
                    .resource("magneto.css")
                    .resource("magneto.js")
                    .resource("sabretooth.css")
                    .resource("mystigue-ie.css")
                        .param("ieonly", "true")
        .end();

        wr.requireContext("xmen");
        wr.requireContext("brotherhood");

        assertThat(wr.paths(), matches(
            contextBatchUrl("xmen", "css"),
            contextBatchUrl("xmen", "css", buildMap("ieonly", "true")),
            contextBatchUrl("brotherhood", "css"),
            contextBatchUrl("brotherhood", "css", buildMap("ieonly", "true")),
            contextBatchUrl("xmen", "js"),
            contextBatchUrl("brotherhood", "js")
        ));
    }

    public void testContextParamsInDependencies() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "rogue";
        final List<String> contexts = new ArrayList<String>();
        contexts.add(context1);
        contexts.add(context2);
        contexts.add(context3);

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx-ie.css", "cyclops.js");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto-ie.css", "sabretooth.css");

        final String dependentModule1 = "rogue-resources";
        final List<ResourceDescriptor> dependentResourceDescriptors1 = TestUtils.createResourceDescriptors("nightcrawler.js", "nightcrawler.css",
                "gambit.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addContext(context1, Arrays.asList(moduleKey1));
        addModuleDescriptor(dependentModule1, dependentResourceDescriptors1);
        addContext(context3, Arrays.asList(moduleKey1, dependentModule1));
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context2, Arrays.asList(moduleKey2));

        Iterable<String> resources = requireContexts(contexts);

        // We currently batch all resource params even if there isn't any overlap in a particular context/param combination
        assertEquals(6, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/brotherhood/batch.css?ieonly=true")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/brotherhood/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/brotherhood/batch.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,rogue/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,rogue/batch.css?ieonly=true")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,rogue/batch.css")));

        assertResourceOrder(resources, "/download/contextbatch/css/brotherhood/batch.css", "/download/contextbatch/css/brotherhood/batch.css?ieonly=true");
        assertResourceOrder(resources, "/download/contextbatch/css/xmen,rogue/batch.css",
            "/download/contextbatch/css/xmen,rogue/batch.css?ieonly=true");
    }

    public void testMultipleContextsWithoutBatching() throws Exception
    {
        configurationData.isBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final List<String> contexts = new ArrayList<String>();
        contexts.add(context1);
        contexts.add(context2);

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2));
        addContext(context2, Arrays.asList(moduleKey1, moduleKey2));

        Iterable<String> resources = requireContexts(contexts);

        assertEquals(6, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test"
            + ".atlassian:magneto-resources/sabretooth.css")));
    }

    public void testExcludedContextWithNoOverlap() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey3));
        addContext(context2, Arrays.asList(moduleKey2));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2));

        assertEquals(2, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen/batch.css")));
    }

    public void testExcludedContextWithOverlap() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, Arrays.asList(moduleKey2));
        addContext(context3, Arrays.asList(moduleKey3));

        final Iterable<String> resources = requireContexts(Arrays.asList(context1, context2), ImmutableSet.of(context3));

        assertEquals(2, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,brotherhood,-xapes/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,brotherhood,"
            + "-xapes/batch.css")));
    }

    public void testMultipleExcludedContextWithOverlap() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "cyclops.js");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, Arrays.asList(moduleKey2));
        addContext(context3, Arrays.asList(moduleKey3));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(1, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,-xapes,-brotherhood/batch.js")));
    }

    public void testMultipleExcludedContextWithPartialOverlap() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2));
        addContext(context2, Arrays.asList(moduleKey2));
        addContext(context3, Arrays.asList(moduleKey3));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(2, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/js/xmen,-brotherhood/batch.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/contextbatch/css/xmen,-brotherhood/batch.css")));
    }

    public void testIncludeAndExcludeLeavesNoResourcesInBatch() throws Exception
    {
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2, moduleKey3));
        addContext(context2, Arrays.asList(moduleKey2, moduleKey1));
        addContext(context3, Arrays.asList(moduleKey3));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(0, size(resources));
    }

    // You shouldn't be doing this but it could be easily done by accident.
    public void testIncludeAndExcludeTheSameBatch() throws Exception
    {
        final String context1 = "xmen";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context1));

        assertEquals(0, size(resources));
    }

    public void testExcludedContextWithNoOverlapUnbatched() throws Exception
    {
        configurationData.isBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, Arrays.asList(moduleKey1));
        addContext(context2, Arrays.asList(moduleKey2));

        final Iterable<String> resources = requireContexts(Collections.singletonList(context2), ImmutableSet.of(context1));

        assertEquals(3, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/magneto.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:magneto-resources/sabretooth.css")));
    }

    public void testExcludedContextWithOverlapUnbatched() throws Exception
    {
        configurationData.isBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey2));
        addContext(context2, Arrays.asList(moduleKey2));

        final Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2));

        assertEquals(3, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
    }

    public void testMultipleExcludedContextWithSomeOverlapUnbatched() throws Exception
    {
        configurationData.isBatchingEnabled = false;
        final String context1 = "xmen";
        final String context2 = "brotherhood";
        final String context3 = "xapes";

        final String moduleKey1 = "xavier-resources";
        final String moduleKey2 = "magneto-resources";
        final String moduleKey3 = "fighting-chimps";
        final List<ResourceDescriptor> resourceDescriptors1 = TestUtils.createResourceDescriptors("professorx.js", "professorx.css", "cyclops.css");
        final List<ResourceDescriptor> resourceDescriptors2 = TestUtils.createResourceDescriptors("magneto.js", "magneto.css", "sabretooth.css");
        final List<ResourceDescriptor> resourceDescriptors3 = TestUtils.createResourceDescriptors("bobo.css", "chobo.css", "hobo.css");

        addModuleDescriptor(moduleKey1, resourceDescriptors1);
        addModuleDescriptor(moduleKey2, resourceDescriptors2);
        addModuleDescriptor(moduleKey3, resourceDescriptors3);
        addContext(context1, Arrays.asList(moduleKey1, moduleKey3));
        addContext(context2, Arrays.asList(moduleKey2, moduleKey3));
        addContext(context3, Arrays.asList(moduleKey3));

        Iterable<String> resources = requireContexts(Collections.singletonList(context1), ImmutableSet.of(context2, context3));

        assertEquals(3, size(resources));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.js")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/professorx.css")));
        assertNotNull(find(resources, new IsResourceWithUrl("/download/resources/test.atlassian:xavier-resources/cyclops.css")));
    }

    private void addContext(final String context, final List<String> descriptors)
    {
        for (final String moduleKey : descriptors)
        {
            for (WebResource.WebResourceData webResourceData : pluginData.webResources)
            {
                if (webResourceData.key.equals(moduleKey))
                {
                    webResourceData.contexts.add(context);
                }
            }
        }
    }

    private void addModuleDescriptor(final String moduleKey, final List<ResourceDescriptor> descriptors)
    {
        WebResource.WebResourceData webResourceData = new WebResource.WebResourceData();
        webResourceData.key = moduleKey;
        pluginData.webResources.add(webResourceData);
        for (ResourceDescriptor d : descriptors)
        {
            WebResource.ResourceData resourceData = new WebResource.ResourceData();
            resourceData.name = d.getName();
            resourceData.location = d.getName();
            if (d.getName().contains("-ie"))
            {
                resourceData.params.put("ieonly", "true");
            }
            webResourceData.resources.add(resourceData);
        }
    }

    private void assertResourceOrder(final Iterable<String> resources, final String supposedlyBefore,
        final String supposedlyAfter)
    {
        int resource1Idx = indexOf(resources, new IsResourceWithUrl(supposedlyBefore));
        int resource2Idx = indexOf(resources, new IsResourceWithUrl(supposedlyAfter));

        assertTrue("resource [" + supposedlyBefore + "] is expected in the result ", resource1Idx >= 0);
        assertTrue("resource [" + supposedlyAfter + "] is expected in the result ", resource2Idx >= 0);
        assertTrue("resource [" + supposedlyBefore + "] must come before resource [" + supposedlyAfter + "] ", resource1Idx < resource2Idx);
    }

    class IsResourceWithUrl implements Predicate<String>
    {
        private final String url;

        public IsResourceWithUrl(String url)
        {
            this.url = url;
        }

        public boolean apply(String actual)
        {
            return  actual.contains(url);
        }
    }

    private Iterable<String> requireContexts(List<String> included)
    {
        return requireContexts(included, new HashSet<String>());
    }

    private Iterable<String> requireContexts(List<String> included, Set<String> excluded)
    {
        wr.applyConfiguration(configurationData);

        wr.getPageBuilderService().assembler().resources().exclude(setOf(), excluded);

        for (String context : included)
        {
            wr.requireContext(context);
        }
        return wr.paths();
    }
}