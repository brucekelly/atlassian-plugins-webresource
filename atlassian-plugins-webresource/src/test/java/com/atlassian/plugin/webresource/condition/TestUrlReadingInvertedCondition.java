package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.collect.ImmutableMap;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.emptyContext;
import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUrlReadingInvertedCondition
{
    @Test
    public void testInvertAddToUrl()
    {
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        inverted.addToUrl(urlBuilder);

        verify(mockCondition).addToUrl(urlBuilder);
    }

    @Test
    public void testInvertShouldDisplayFalse()
    {
        QueryParams queryParams = emptyQueryParams();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        when(mockCondition.shouldDisplay(queryParams)).thenReturn(true);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertFalse(inverted.shouldDisplay(queryParams));
    }

    @Test
    public void testInvertShouldDisplayTrue()
    {
        QueryParams queryParams = emptyQueryParams();
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        when(mockCondition.shouldDisplay(queryParams)).thenReturn(false);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertTrue(inverted.shouldDisplay(queryParams));
    }

    @Test
    public void testInvertShouldDisplayImmediateFalse()
    {
        Map<String, Object> context = emptyContext();
        Condition mockCondition = mock(Condition.class);
        when(mockCondition.shouldDisplay(context)).thenReturn(true);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertFalse(inverted.shouldDisplayImmediate(context));
    }

    @Test
    public void testInvertShouldDisplayImmediateTrue()
    {
        Map<String, Object> context = emptyContext();
        Condition mockCondition = mock(Condition.class);
        when(mockCondition.shouldDisplay(context)).thenReturn(false);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertTrue(inverted.shouldDisplayImmediate(context));
    }

    @Test
    public void testInvertCanEncodeStateIntoUrlFalse()
    {
        Condition mockCondition = mock(Condition.class);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertFalse(inverted.canEncodeStateIntoUrl());
    }

    @Test
    public void testInvertCanEncodeStateIntoUrlTrue()
    {
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        DecoratingCondition inverted = decorate(mockCondition).invertCondition();

        assertTrue(inverted.canEncodeStateIntoUrl());
    }

    @Test
    public void testInvertedLegacyConditionPass()
    {
        Condition condition = mock(Condition.class);
        when(condition.shouldDisplay(any(Map.class))).thenReturn(false);
        DecoratingCondition decoratingInvertedCondition = decorate(condition).invertCondition();

        assertTrue(decoratingInvertedCondition.shouldDisplayImmediate(emptyContext()));
        assertTrue(decoratingInvertedCondition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertedLegacyConditionFail()
    {
        Condition condition = mock(Condition.class);
        when(condition.shouldDisplay(any(Map.class))).thenReturn(true);
        DecoratingCondition decoratingInvertedCondition = decorate(condition).invertCondition();

        assertFalse(decoratingInvertedCondition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testInvertCompositeAndConditionNoLegacyFail()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createUrlReadingCondition(true);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
        assertFalse(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeAndConditionNoLegacyPass()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createUrlReadingCondition(false);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeOrConditionOneLegacyFailImmediate()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(false);
        DecoratingCondition mockCondition2 = createLegacyCondition(true);
        DecoratingCondition condition = invert(createOrCondition(mockCondition1, mockCondition2));

        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testInvertCompositeAndConditionOneLegacyFailImmediate()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(false);
        DecoratingCondition mockCondition2 = createLegacyCondition(true);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeOrConditionOneLegacyFailNonImmediate()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createOrCondition(mockCondition1, mockCondition2));

        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
        assertFalse(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeAndConditionOneLegacyFailNonImmediate()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeOrConditionOneLegacyFail()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(true);
        DecoratingCondition condition = invert(createOrCondition(mockCondition1, mockCondition2));

        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    @Ignore("PLUGWEB-53 ORed conditions with both url-reading and legacy conditions always pass")
    public void testInvertCompositeAndConditionOneLegacyFail()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(true);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertFalse(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeOrConditionOneLegacyPass()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(false);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createOrCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeAndConditionOneLegacyPass()
    {
        DecoratingCondition mockCondition1 = createUrlReadingCondition(false);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeAndConditionBothLegacyFail()
    {
        DecoratingCondition mockCondition1 = createLegacyCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(true);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testInvertCompositeAndConditionBothLegacyFailOne()
    {
        DecoratingCondition mockCondition1 = createLegacyCondition(true);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testInvertCompositeAndConditionBothLegacyPass()
    {
        DecoratingCondition mockCondition1 = createLegacyCondition(false);
        DecoratingCondition mockCondition2 = createLegacyCondition(false);
        DecoratingCondition condition = invert(createAndCondition(mockCondition1, mockCondition2));

        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    private DecoratingCondition createAndCondition(DecoratingCondition... conditions)
    {
        DecoratingAndCompositeCondition compositeCondition = new DecoratingAndCompositeCondition();
        for (DecoratingCondition condition : conditions)
        {
            compositeCondition.addCondition(condition);
        }
        return compositeCondition;
    }

    private DecoratingCondition createOrCondition(DecoratingCondition... conditions)
    {
        DecoratingOrCompositeCondition compositeCondition = new DecoratingOrCompositeCondition();
        for (DecoratingCondition condition : conditions)
        {
            compositeCondition.addCondition(condition);
        }
        return compositeCondition;
    }

    private DecoratingCondition createUrlReadingCondition(boolean shouldDisplay)
    {
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        when(mockCondition.shouldDisplay(any(QueryParams.class))).thenReturn(shouldDisplay);
        return decorate(mockCondition);
    }

    private DecoratingCondition createLegacyCondition(boolean shouldDisplay)
    {
        Condition mockCondition = mock(Condition.class);
        when(mockCondition.shouldDisplay(any(Map.class))).thenReturn(shouldDisplay);
        return decorate(mockCondition);
    }
    
    private DecoratingCondition invert(DecoratingCondition condition)
    {
        return condition.invertCondition();
    }
}
