package com.atlassian.plugin.webresource.util;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ObjectMatcher extends BaseMatcher
{

    private String[] regexes;

    public ObjectMatcher(final String[] regexes)
    {
        this.regexes = regexes;
    }

    @Override
    public void describeTo(final Description description)
    {
        description.appendText("matches `" + StringUtils.join(regexes, ", ") + "`");
    }

    @Override
    public boolean matches(Object actual)
    {
        if (actual instanceof Object[])
        {
            return matchesArray((Object[]) actual);
        }
        else if (actual instanceof Iterable)
        {
            List list = new ArrayList();
            for (Object object : ((Iterable) actual))
            {
                list.add(object);
            }
            return matchesArray(list.toArray(new Object[list.size()]));
        }
        else if (actual != null)
        {
            return matchesString(actual.toString());
        }
        else
        {
            return false;
        }
    }

    private boolean matchesString(String actual)
    {
        return matches(regexes, actual);
    }

    private boolean matchesArray(Object[] actual)
    {
        assertThat(actual.length, equalTo(regexes.length));
        for (int i = 0; i < regexes.length; i++)
        {
            // Matching multiline.
            if (!matches(regexes[i], actual[i].toString()))
            {
                return false;
            }
        }
        return true;
    }

    public static ObjectMatcher matches(String... regexes)
    {
        return new ObjectMatcher(regexes);
    }

    private static boolean matches(String stringOrRegexExpected, String actual)
    {
        try
        {
            // Patching Java regex a little to make it easier to use.
            if (Pattern.compile(".*" + stringOrRegexExpected + ".*", Pattern.DOTALL).matcher(actual).matches())
            {
                return true;
            }
        }
        catch (PatternSyntaxException e)
        {
            // Ignoring because sometimes string used instead of regular expression,
            // and it may contain invalid characters.
        }

        // Some string characters could be threat as regular expresion characters and the
        // result of comparison would be false. To avoid that additionally checking for matching
        // string if regular expression failed.
        if (actual.contains(stringOrRegexExpected))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static boolean matches(String[] stringOrRegexExpected, String actual)
    {
        try
        {
            String regex = StringUtils.join(stringOrRegexExpected, ".*");
            // Patching Java regex a little to make it easier to use.
            if (Pattern.compile(".*" + regex + ".*", Pattern.DOTALL).matcher(actual).matches())
            {
                return true;
            }
        }
        catch (PatternSyntaxException e)
        {
            // Ignoring because sometimes string used instead of regular expression,
            // and it may contain invalid characters.
        }

        // Some string characters could be threat as regular expression characters and the
        // result of comparison would be false. To avoid that additionally checking for matching
        // string if regular expression failed.
        int startFrom = 0;
        for (String item : stringOrRegexExpected)
        {
            int foundPosition = actual.indexOf(item, startFrom);
            if (foundPosition >= 0)
            {
                startFrom += foundPosition + item.length();
            }
            else
            {
                return false;
            }
        }
        return true;
    }
}