package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.integration.transformers.LineReversingTransformerWithSourceMap;
import com.atlassian.plugin.webresource.integration.transformers.LineReversingTransformerWithoutSourceMap;
import com.atlassian.plugin.webresource.integration.transformers.OffsetContentAnnotatorsWithSourceMap;
import com.atlassian.plugin.webresource.integration.transformers.OffsetStaticTransformersWithSourceMap;
import com.atlassian.plugin.webresource.integration.transformers.OffsetTransformerWithSourceMap;
import com.atlassian.plugin.webresource.integration.transformers.SourceMapGeneratingTransformer;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.Mapping;
import com.atlassian.sourcemap.SourceMapImpl;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchSourceMapUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.contextBatchUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.parseSourceMapUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceSourceMapUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.sourceMapUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.sourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.webResourceBatchUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static com.atlassian.sourcemap.Util.generateSourceMapComment;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class TestSourceMapInProduction extends TestCase
{
    protected boolean optimiseSourceMapForDevelopment;

    @Before
    public void configure()
    {
        optimiseSourceMapForDevelopment = false;
    }

    @Test
    public void shouldReturnSourceMapOnlyForJsAndCssResources()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .plugin("plugin")
                .webResource("a")
                    .resource("a.css")
                    .resource("a.js")
                    .resource("a.txt")
        .end();
        assertThat(parseSourceMapUrl(wr.getContent(webResourceBatchUrl("plugin:a", "js"))), matches("a.js.map"));
        assertThat(parseSourceMapUrl(wr.getContent(webResourceBatchUrl("plugin:a", "css"))), matches("a.css.map"));
        assertThat(parseSourceMapUrl(wr.getContent(webResourceBatchUrl("plugin:a", "txt"))), equalTo(null));
    }

    @Test
    public void shouldRespondWithSource()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .plugin("plugin")
                .transformer("offset-transformer-with-source-map", OffsetTransformerWithSourceMap.class)
                .webResource("a")
                    .transformation("js", "offset-transformer-with-source-map")
                    .resource("a.js", "content of a.js")
        .end();

        String transformedContent =
            "offset-static-transformer\n" +
            "offset-transformer\n" +
            "content of a.js\n" +
            "offset-transformer\n" +
            "offset-static-transformer\n" +
            generateSourceMapComment(sourceMapUrl(resourceUrl("plugin:a", "a.js")), "js");

        assertThat(wr.getContent(resourceUrl("plugin:a", "a.js")), equalTo(transformedContent));
        assertThat(wr.getContent(sourceUrl("plugin:a", "a.js")), equalTo("content of a.js"));
    }

    @Test
    public void shouldRespondWithSourceMapForSingleResource()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .plugin("plugin")
                .transformer("source-map-generating-transformer", SourceMapGeneratingTransformer.class)
                .transformer("offset-transformer-with-source-map", OffsetTransformerWithSourceMap.class)
                .transformer("reversing-transformer-with-source-map", LineReversingTransformerWithSourceMap.class)
                .webResource("b")
                    .transformation("js", "source-map-generating-transformer")
                    .transformation("js", "offset-transformer-with-source-map")
                    .transformation("js", "reversing-transformer-with-source-map")
                    .resource("b1.js", "b1 first line\nb1 second line\nb1 third line")
        .end();

        // Checking resource.
        String content = wr.getContent(resourceUrl("plugin:b", "b1.js"));

        String expectedB1 =
            "offset-static-transformer\n" +
            "offset-transformer\n" +
            "b1 third line\n" +
            "b1 second line\n" +
            "b1 first line\n" +
            "offset-transformer\n" +
            "offset-static-transformer";
        assertThat(content, containsString(expectedB1));

        // Checking source map url.
        String sourceMapUrl = parseSourceMapUrl(content);
        assertThat(sourceMapUrl, matches(resourceSourceMapUrl("plugin:b", "b1.js")));

        // Checking source map.
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        List<String> sourceFileNames = sourceMap.getSourceFileNames();
        assertThat(sourceFileNames, matches(
            sourceUrl("plugin:b", "b1.js")
        ));

        String b1SourceUrl = sourceFileNames.get(0);
        Object[] b1SourceMapping = new Object[]{
            2, 0, 2, 0, b1SourceUrl,
            3, 0, 1, 0, b1SourceUrl,
            4, 0, 0, 0, b1SourceUrl
        };
        assertSourceMapContains(sourceMap, b1SourceMapping);
    }

    @Test
    public void shouldRespondWithSourceMapForContextBatch()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .setAnnotators(OffsetContentAnnotatorsWithSourceMap.build())
            .plugin("plugin")
                .transformer("source-map-generating-transformer", SourceMapGeneratingTransformer.class)
                .transformer("offset-transformer-with-source-map", OffsetTransformerWithSourceMap.class)
                .transformer("reversing-transformer-with-source-map", LineReversingTransformerWithSourceMap.class)
                .transformer("reversing-transformer-without-source-map", LineReversingTransformerWithoutSourceMap.class)
                .webResource("a")
                    .transformation("js", "offset-transformer-without-source-map")
                    .resource("a1.js", "a1 first line\na1 second line\na1 third line")
                    .resource("a2.js", "a2 first line\na2 second line\na2 third line")
                .webResource("b")
                    .dependency("plugin:a")
                    .context("general")
                    .transformation("js", "source-map-generating-transformer")
                    .transformation("js", "offset-transformer-with-source-map")
                    .transformation("js", "reversing-transformer-with-source-map")
                    .resource("b1.js", "b1 first line\nb1 second line\nb1 third line")
                    .resource("b2.js", "b2 first line\nb2 second line\nb2 third line")
                .webResource("c")
                    .context("general")
                    .transformation("js", "source-map-generating-transformer")
                    .transformation("js", "offset-transformer-with-source-map")
                    .transformation("js", "reversing-transformer-with-source-map")
                    .resource("c1.js", "c1 first line\nc1 second line\nc1 third line")
                    .resource("c2.js", "c2 first line\nc2 second line\nc2 third line")
        .end();

        wr.requireContext("general");

        // Checking paths.
        List<String> paths = wr.paths();
        assertThat(paths, matches(contextBatchUrl("general", "js")));

        // Checking resource.
        String content = wr.getContent(paths);
        assertThat(content, matches("a1 first line.*b1 first line.*c1 first line"));

        String resourceB1 = selectLines(content, 18, 28);
        String expectedB1 =
            "first-annotator\n" +
            "second-annotator\n" +
            "offset-static-transformer\n" +
            "offset-transformer\n" +
            "b1 third line\n" +
            "b1 second line\n" +
            "b1 first line\n" +
            "offset-transformer\n" +
            "offset-static-transformer\n" +
            "second-annotator\n" +
            "first-annotator";
        assertThat(resourceB1, equalTo(expectedB1));

        // Checking source map url.
        String sourceMapUrl = parseSourceMapUrl(content);
        assertThat(sourceMapUrl, matches(contextBatchSourceMapUrl("general", "js")));

        // Checking source map.
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        List<String> sourceFileNames = sourceMap.getSourceFileNames();

        Collections.sort(sourceFileNames);
        assertThat(sourceFileNames, matches(
            resourceUrl("plugin:a", "a1.js"),
            resourceUrl("plugin:a", "a2.js"),
            sourceUrl("plugin:b", "b1.js"),
            sourceUrl("plugin:b", "b2.js"),
            sourceUrl("plugin:c", "c1.js"),
            sourceUrl("plugin:c", "c2.js")
        ));

        String b1SourceUrl = sourceFileNames.get(2);
        Object[] b1SourceMapping = new Object[]{
            22, 0, 2, 0, b1SourceUrl,
            23, 0, 1, 0, b1SourceUrl,
            24, 0, 0, 0, b1SourceUrl
        };
        assertSourceMapContains(sourceMap, b1SourceMapping);

        // It should also generate source map for non-source map aware web resources.
        String a2SourceUrl = sourceFileNames.get(1);
        Object[] a2SourceMapping = new Object[]{
            11, 0, 0, 0, a2SourceUrl,
            12, 0, 1, 0, a2SourceUrl,
            13, 0, 2, 0, a2SourceUrl,
            14, 0, 3, 0, a2SourceUrl,
            15, 0, 4, 0, a2SourceUrl
        };
        assertSourceMapContains(sourceMap, a2SourceMapping);
    }

    @Test
    public void shouldRespondWithSourceMapForSingleResourceIfThereIsSourceMapAwareTransformers()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .setAnnotators(OffsetContentAnnotatorsWithSourceMap.build())
            .plugin("plugin")
                .transformer("source-map-generating-transformer", SourceMapGeneratingTransformer.class)
                .transformer("reversing-transformer-with-source-map", LineReversingTransformerWithSourceMap.class)
                .webResource("a")
                    .transformation("js", "source-map-generating-transformer")
                    .transformation("js", "reversing-transformer-with-source-map")
                    .resource("a1.js", "a1 first line\na1 second line")
        .end();

        // Checking resource.
        String resourceContent = wr.getContent(resourceUrl("plugin:a", "a1.js"));

        String resourceA1 = selectLines(resourceContent, 0, 3);
        String expectedA1 =
            "offset-static-transformer\n" +
            "a1 second line\n" +
            "a1 first line\n" +
            "offset-static-transformer";
        assertThat(resourceA1, equalTo(expectedA1));

        // Checking source map url.
        String sourceMapUrl = parseSourceMapUrl(resourceContent);
        assertThat(sourceMapUrl, matches(resourceUrl("plugin:a", "a1.js")));

        // Checking source map.
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        assertThat(sourceMap.getSourceFileNames(), matches(
            sourceUrl("plugin:a", "a1.js")
        ));

        String a1SourceUrl = sourceMap.getSourceFileNames().get(0);
        Object[] a1SourceMapping = new Object[]{
            1, 0, 1, 0, a1SourceUrl,
            2, 0, 0, 0, a1SourceUrl
        };
        assertSourceMapContains(sourceMap, a1SourceMapping);
    }

    @Test
    public void shouldRespondWith1to1SourceMapForSingleResourceIfThereIsNoSourceMapAwareTransformers()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .setAnnotators(OffsetContentAnnotatorsWithSourceMap.build())
            .plugin("plugin")
                .transformer("reversing-transformer-without-source-map", LineReversingTransformerWithoutSourceMap.class)
                .webResource("a")
                    .transformation("js", "reversing-transformer-without-source-map")
                    .resource("a1.js", "a1 first line\na1 second line")
        .end();

        // Checking resource.
        String resourceContent = wr.getContent(resourceUrl("plugin:a", "a1.js"));
        String expected =
            "offset-static-transformer\n" +
            "a1 second line\n" +
            "a1 first line\n" +
            "offset-static-transformer\n" +
            generateSourceMapComment(sourceMapUrl(resourceUrl("plugin:a", "a1.js")), "js");;
        assertThat(resourceContent, equalTo(expected));
    }

    @Test
    public void shouldRespondWith1to1SourceMapForSingleResourceIfNotAllTransformersAreSourceMapAware()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .setAnnotators(OffsetContentAnnotatorsWithSourceMap.build())
            .plugin("plugin")
                .transformer("source-map-generating-transformer", SourceMapGeneratingTransformer.class)
                .transformer("reversing-transformer-without-source-map", LineReversingTransformerWithoutSourceMap.class)
                .transformer("offset-transformer-with-source-map", OffsetTransformerWithSourceMap.class)
                .webResource("a")
                    .transformation("js", "source-map-generating-transformer")
                    .transformation("js", "reversing-transformer-without-source-map")
                    .transformation("js", "offset-transformer-with-source-map")
                    .resource("a1.js", "a1 first line\na1 second line")
        .end();

        // Checking resource.
        String content = wr.getContent(resourceUrl("plugin:a", "a1.js"));
        String expected =
            "offset-static-transformer\n" +
            "offset-transformer\n" +
            "a1 second line\n" +
            "a1 first line\n" +
            "offset-transformer\n" +
            "offset-static-transformer\n" +
            generateSourceMapComment(sourceMapUrl(resourceUrl("plugin:a", "a1.js")), "js");
        assertThat(content, equalTo(expected));

        // Checking source map url.
        String sourceMapUrl = parseSourceMapUrl(content);
        assertThat(sourceMapUrl, matches(resourceSourceMapUrl("plugin.a", "a1.js")));

        // Checking source map.
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        List<String> sourceFileNames = sourceMap.getSourceFileNames();
        Collections.sort(sourceFileNames);
        assertThat(sourceFileNames, matches(
            resourceUrl("plugin:a", "a1.js")
        ));

        // Source map is not for original source, it's for transformed single source.
        String aSourceUrl = sourceFileNames.get(0);
        Object[] b1SourceMapping = new Object[]{
            0, 0, 0, 0, aSourceUrl,
            1, 0, 1, 0, aSourceUrl,
            2, 0, 2, 0, aSourceUrl,
            3, 0, 3, 0, aSourceUrl,
            4, 0, 4, 0, aSourceUrl,
            5, 0, 5, 0, aSourceUrl
        };
        assertSourceMapContains(sourceMap, b1SourceMapping);
    }

    @Test
    public void shouldRespondWithSourceMapForBatchesEvenIfThereIsNoSourceMapAwareTransformers()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .setStaticTransformers(new OffsetStaticTransformersWithSourceMap())
            .setAnnotators(OffsetContentAnnotatorsWithSourceMap.build())
            .plugin("plugin")
                .transformer("reversing-transformer-without-source-map", LineReversingTransformerWithoutSourceMap.class)
                .webResource("a")
                    .context("general")
                    .transformation("js", "reversing-transformer-without-source-map")
                    .resource("a1.js", "a1 first line\na1 second line")
                    .resource("a2.js", "a2 first line\na2 second line")
        .end();

        wr.requireContext("general");

        // Checking paths.
        List<String> paths = wr.paths();
        assertThat(paths, matches(contextBatchUrl("general", "js")));

        // Checking resource.
        String content = wr.getContent(paths);

        String resourceA2 = selectLines(content, 8, 15);
        String expectedA2 =
            "first-annotator\n" +
            "second-annotator\n" +
            "offset-static-transformer\n" +
            "a2 second line\n" +
            "a2 first line\n" +
            "offset-static-transformer\n" +
            "second-annotator\n" +
            "first-annotator";
        assertThat(resourceA2, equalTo(expectedA2));

        // Checking source map url.
        String sourceMapUrl = parseSourceMapUrl(content);
        assertThat(sourceMapUrl, matches(contextBatchSourceMapUrl("general", "js")));

        // Checking source map.
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        List<String> sourceFileNames = sourceMap.getSourceFileNames();
        Collections.sort(sourceFileNames);
        assertThat(sourceFileNames, matches(
            resourceUrl("plugin:a", "a1.js"),
            resourceUrl("plugin:a", "a2.js")
        ));

        // Source map is not for original source, it's for transformed single source.
        String a2SourceUrl = sourceFileNames.get(1);
        Object[] b1SourceMapping = new Object[]{
            10, 0, 0, 0, a2SourceUrl,
            11, 0, 1, 0, a2SourceUrl,
            12, 0, 2, 0, a2SourceUrl,
            13, 0, 3, 0, a2SourceUrl
        };
        assertSourceMapContains(sourceMap, b1SourceMapping);
    }

    @Test
    public void shouldSupportSourceMapsForMinifiedVersions()
    {
        SourceMap minifiedBSourceMap = new SourceMapImpl();
        minifiedBSourceMap.addMapping(0, 0, 0, 0, "b1.js");
        minifiedBSourceMap.addMapping(0, 2, 1, 0, "b1.js");

        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                .webResource("b")
                    .dependency("plugin:a")
                    .resource("b1.js", "l1\nl2")
                    .file("b1-min.js", "l1l2")
                    .file("b1-min.js.map", minifiedBSourceMap.generate())
                .webResource("c")
                    .context("general")
                    .dependency("plugin:b")
                    .resource("c1.js")

        .end();

        String content = wr.getContent(contextBatchUrl("general", "js"));
        assertThat(content, matches(
            "content of a1.js",
            "l1l2",
            "content of c1.js"
        ));

        // Checking source map.
        String sourceMapUrl = parseSourceMapUrl(content);
        String sourceMapContent = wr.getContent(sourceMapUrl);
        SourceMap sourceMap = new SourceMapImpl(sourceMapContent);

        String aResourceUrl = resourceUrl("plugin:a", "a1.js");
        String bSourceUrl = sourceUrl("plugin:b", "b1.js");
        String cResourceUrl = resourceUrl("plugin:c", "c1.js");
        Object[] b1SourceMapping = new Object[]{
            2, 0, 0, 0, aResourceUrl,
            5, 0, 0, 0, bSourceUrl,
            5, 2, 1, 0, bSourceUrl,
            8, 0, 0, 0, cResourceUrl
        };
        assertSourceMapContains(sourceMap, b1SourceMapping);
    }

    @Test
    public void shouldServeCustomSourceMapForWebResource()
    {
        wr.configure()
            .setOptimiseSourceMapForDevelopment(optimiseSourceMapForDevelopment)
            .enableSourceMap()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                    .resource("a1.js.map", "custom source map")
        .end();

        String sourceMap = wr.getContent(resourceSourceMapUrl("plugin:a", "a1.js"));
        assertThat(sourceMap, equalTo(
            "custom source map"
        ));
    }

    private static String selectLines(String content, int from, int to)
    {
        String[] lines = content.split("\n");
        return StringUtils.join(Arrays.copyOfRange(lines, from, to + 1), "\n");
    }

    private static void assertSourceMapContains(SourceMap sourceMap, Object[] expectedMappings)
    {
        for (int i = 0; i < expectedMappings.length / 5; i++)
        {
            int offset = i * 5;
            Mapping mapping = sourceMap.getMapping((Integer) expectedMappings[offset],
                    (Integer) expectedMappings[offset + 1]);
            assertThat(mapping, not(equalTo(null)));
            assertThat(mapping.getGeneratedLine(), equalTo(expectedMappings[offset]));
            assertThat(mapping.getGeneratedColumn(), equalTo(expectedMappings[offset + 1]));
            assertThat(mapping.getSourceLine(), equalTo(expectedMappings[offset + 2]));
            assertThat(mapping.getSourceColumn(), equalTo(expectedMappings[offset + 3]));
            assertThat(mapping.getSourceFileName(), containsString(expectedMappings[offset + 4].toString()));
        }
    }
}
