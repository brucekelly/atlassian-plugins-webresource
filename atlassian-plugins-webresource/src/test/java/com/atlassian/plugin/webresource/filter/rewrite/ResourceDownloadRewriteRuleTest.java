package com.atlassian.plugin.webresource.filter.rewrite;

import org.junit.Before;
import org.junit.Test;
import org.tuckey.web.filters.urlrewrite.extend.RewriteMatch;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResourceDownloadRewriteRuleTest
{
    private ResourceDownloadRewriteRule rewriteRule;

    @Before
    public void setUp() throws Exception
    {
        rewriteRule = new ResourceDownloadRewriteRule();
    }

    private static HttpServletRequest mockRequestFor(String requestUri)
    {
        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getContextPath()).thenReturn("");
        when(req.getRequestURI()).thenReturn(requestUri);

        return req;
    }

    private void assertRequestUrlIsRewrittenTo(String requestUri, String forwardedUri) throws ServletException, IOException
    {
        HttpServletRequest req = mockRequestFor(requestUri);

        RewriteMatch rewriteMatch = rewriteRule.matches(req, mock(HttpServletResponse.class));

        assertEquals(rewriteMatch.getMatchingUrl(), forwardedUri);
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequest() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s/en_US/_/download/resource-to-download", "/download/resource-to-download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesDoubleDots() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s/en_US/_/download/resource..to..download", "/download/resource..to..download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesAColon() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s/en_US/_/download/resource:to:download", "/download/resource:to:download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesAPercentEncodedDot() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s/en_US/_/download/%2E./resource:to:download", "/download/%2E./resource:to:download");
    }

    @Test
    public void rewriteFilterFailsWhenDownloadRequestIncludesParentPathSegment() throws Exception
    {
        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getContextPath()).thenReturn("");

        when(req.getRequestURI()).thenReturn("/s/en_US/_/download/../WEB-INF/resource-to-download");

        RewriteMatch matches = rewriteRule.matches(req, mock(HttpServletResponse.class));

        assertNull(matches);
    }
}
