package com.atlassian.plugin.webresource.data;

import com.atlassian.webresource.api.data.PluginDataResource;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;

import static com.atlassian.plugin.webresource.data.DataTestFixture.assertDataTag;

public class TestDataTagWriter
{
    @Test
    public void testEmpty() throws IOException
    {
        assertDataTag(Collections.<PluginDataResource>emptyList(), "");
    }

    @Test
    public void testBasic() throws IOException
    {
        assertDataTag("a", "b", "WRM._unparsedData[\"a\"]=\"b\";");
        StringEscapeUtils.escapeJavaScript("a");
    }

    @Test
    public void testHtml() throws IOException
    {
        assertDataTag("<h1>", "<h2>", "WRM._unparsedData[\"\\u003ch1\\u003e\"]=\"\\u003ch2\\u003e\";");
    }

    @Test
    public void testScriptClose() throws IOException
    {
        assertDataTag("</script>", "</script>", "WRM._unparsedData[\"\\u003c/script\\u003e\"]=\"\\u003c/script\\u003e\";");
    }

    @Test
    public void testQuotes() throws IOException
    {
        assertDataTag("\"'", "\"'", "WRM._unparsedData[\"\\\"\\'\"]=\"\\\"\\'\";");
    }
}
