package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.webresource.Dom4jFluent;
import com.atlassian.plugin.webresource.TestUtils;
import com.google.common.collect.ImmutableMap;
import org.dom4j.Element;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUrlReadingConditionElementParser
{
    @Test
    public void testCreateUrlReadingClass() throws ClassNotFoundException
    {
        Element element = makeCondition(ImmutableMap.of("class", TestUtils.StubUrlReadingCondition.class.getName()));
        UrlReadingCondition instantiatedCondition = new TestUtils.StubUrlReadingCondition();
        DecoratingCondition condition = make(element, instantiatedCondition);

        assertTrue(((DecoratingUrlReadingCondition)condition).urlReadingCondition == instantiatedCondition);
    }

    @Test
    public void testCreateUrlReadingClass2() throws ClassNotFoundException
    {
        Element element = makeCondition(ImmutableMap.of(
                "class", "blahblahblah",
                "class2", TestUtils.StubUrlReadingCondition.class.getName()));
        UrlReadingCondition instantiatedCondition = new TestUtils.StubUrlReadingCondition();
        DecoratingCondition condition = make(element, instantiatedCondition);

        assertTrue(((DecoratingUrlReadingCondition)condition).urlReadingCondition == instantiatedCondition);
    }

    @Test
    public void testCreateLegacy() throws ClassNotFoundException
    {
        Element element = makeCondition(ImmutableMap.of(
                "class", LegacyCondition.class.getName()));

        Condition instantiatedCondition = new LegacyCondition();
        DecoratingCondition condition = make(element, instantiatedCondition);

        assertTrue(condition instanceof DecoratingLegacyCondition);
        assertTrue(((DecoratingLegacyCondition)condition).legacyCondition == instantiatedCondition);
    }

    public static class LegacyCondition implements Condition
    {
        @Override
        public void init(Map<String, String> params) throws PluginParseException
        {
        }

        @Override
        public boolean shouldDisplay(Map<String, Object> context)
        {
            return true;
        }
    }

    private DecoratingCondition make(Element element, Object instantiatedCondition) throws ClassNotFoundException
    {
        Class<?> clazz = instantiatedCondition.getClass();
        Plugin plugin = TestUtils.createTestPlugin("com.test", "1", clazz);
        HostContainer hostContainer = mock(HostContainer.class);
        when(hostContainer.create(clazz)).thenReturn(instantiatedCondition);
        return new UrlReadingConditionElementParser(hostContainer).makeConditions(plugin, element,
                AbstractConditionElementParser.CompositeType.AND);
    }

    private Element makeCondition(Map<String, String> params)
    {
        return Dom4jFluent.element("web-resource", Dom4jFluent.element("condition", params)).toDom();
    }
}
