package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.util.concurrent.Promises;
import com.atlassian.webresource.api.bigpipe.Key;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AbstractFuture;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.junit.Test;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.google.common.base.Preconditions.checkNotNull;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestQueueFutureCompletionService
{
    private static final long TIMEOUT = 100;
    private static final String FAIL = "__FAIL__";

    @Test
    public void testImmediateContent() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate = new Gate(completor, "key1");
        gate.open("peace pipe");
        Iterable<KeyedValue<String>> result = completor.poll();

        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateContentMultiple() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");
        gate1.open("peace pipe");
        gate2.open("crack pipe");
        Iterable<KeyedValue<String>> result = completor.poll();

        assertValues(result, v("key1", "peace pipe"), v("key2", "crack pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateContentWithDelay() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Iterable<KeyedValue<String>> result = completor.poll();

        assertValues(result);
        assertFalse(completor.isComplete());

        gate1.open("peace pipe");

        result = completor.poll();
        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContent() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");

        Future<Iterable<KeyedValue<String>>> future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentAvailableImmediately() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");

        gate1.open("peace pipe");
        Iterable<KeyedValue<String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);

        assertValues(result, v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentSequential() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");

        Future<Iterable<KeyedValue<String>>> future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertFalse(completor.isComplete());

        future = waitForContent(completor);
        gate2.open("crack pipe");

        assertValues(future.get(), v("key2", "crack pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForContentSequentialInverseOrder() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");

        Future<Iterable<KeyedValue<String>>> future = waitForContent(completor);
        gate2.open("crack pipe");

        assertValues(future.get(), v("key2", "crack pipe"));
        assertFalse(completor.isComplete());

        future = waitForContent(completor);
        gate1.open("peace pipe");

        assertValues(future.get(), v("key1", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testFail() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate = new Gate(completor, "key1");
        gate.fail();
        Iterable<KeyedValue<String>> result = completor.poll();

        assertValues(result, v("key1", FAIL));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testFailAndPass() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key2");
        gate1.fail();
        gate2.open("peace pipe");
        Iterable<KeyedValue<String>> result = completor.poll();

        assertValues(result, v("key1", FAIL), v("key2", "peace pipe"));
        assertTrue(completor.isComplete());
    }

    @Test
    public void testImmediateNoContent() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Iterable<KeyedValue<String>> result = completor.poll();

        assertNotNull(result);
        assertValues(result);
        assertTrue(completor.isComplete());
    }

    @Test
    public void testWaitForNoContent() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();

        Iterable<KeyedValue<String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);

        assertNotNull(result);
        assertValues(result);
        assertTrue(completor.isComplete());
    }

    @Test
    public void testDestroy() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();
        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key1");

        completor.destroy(true);

        assertTrue(gate1.isCancelled());
        assertTrue(gate1.wasInterrupted());
        assertTrue(gate2.isCancelled());
        assertTrue(gate2.wasInterrupted());
    }

    @Test
    public void testDestroyFalse() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();
        Gate gate1 = new Gate(completor, "key1");
        Gate gate2 = new Gate(completor, "key1");

        completor.destroy(false);

        assertTrue(gate1.isCancelled());
        assertFalse(gate1.wasInterrupted());
        assertTrue(gate2.isCancelled());
        assertFalse(gate2.wasInterrupted());
    }

    @Test
    public void testDestroyCausesReturnsNoResults() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();
        new Gate(completor, "key1");
        new Gate(completor, "key1");

        completor.destroy(true);

        assertTrue(completor.isComplete());
        assertTrue(Iterables.isEmpty(completor.poll()));
        assertTrue(Iterables.isEmpty(completor.poll(TIMEOUT, TimeUnit.MILLISECONDS)));
    }

    @Test(expected = IllegalStateException.class)
    public void testAddFailsAfterDestroy() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();
        completor.destroy(false);

        new Gate(completor, "jeebus");
    }

    @Test
    public void testMoreAvailableChecksCompletedPromises() throws Exception
    {
        FutureCompletionService<String> completor = createCompletionService();
        Gate gate = new Gate(completor, "key1");
        gate.open("hi");

        assertFalse(completor.isComplete());
    }

    @Test(expected = Error.class)
    public void testPromiseThrowingError()
    {
        FutureCompletionService<String> completor = createCompletionService();
        Gate gate = new Gate(completor, "key1");
        gate.error();
    }

    @Test
    public void spamTest() throws Exception
    {
        // Smoke test; tests that a crapload of concurrent executions does not result in
        // obviously incorrect behaviour.
        // This DOES NOT prove correctness; but it can prove incorrectness.
        spamTest(5, 2000, 5);
    }

    private void spamTest(int numConsumers, int numKeys, int numTasksPerKey) throws Exception
    {
        // Tests multiple threads processing bigpipe tasks
        final ExecutorService executor = Executors.newCachedThreadPool();
        final FutureCompletionService<String> completor = createCompletionService();

        // Create bigpipe consumers
        Collection<Callable<Integer>> consumers = Lists.newArrayList();
        for (int i = 0; i < numConsumers; ++i)
        {
            consumers.add(new Callable<Integer>()
            {
                @Override
                public Integer call() throws Exception
                {
                    int count = 0;
                    boolean complete = false;
                    while (!complete)
                    {
                        Iterable<KeyedValue<String>> result = completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);
                        count += Iterables.size(result);
                        complete = completor.isComplete();
                    }
                    return count;
                }
            });
        }

        // Service generating bigpipe content
        for (int key = 0; key < numKeys; ++key)
        {
            for (int task = 0; task < numTasksPerKey; ++task)
            {
                final int value = task;
                Future<String> future = executor.submit(new Callable<String>()
                {
                    @Override
                    public String call() throws Exception
                    {
                        return String.valueOf(value);
                    }
                });
                completor.add(String.valueOf(value), Promises.forFuture(future));
            }
        }

        // Service consuming bigpipe content
        List<Future<Integer>> futures = Executors.newCachedThreadPool().invokeAll(consumers);
        int count = 0;
        for (Future<Integer> future : futures)
        {
            count += future.get();
        }
        assertEquals(count, numKeys * numTasksPerKey);
    }

    private <V> Future<Iterable<KeyedValue<V>>> waitForContent(final FutureCompletionService<V> completor)
    {
        return Executors.newSingleThreadExecutor().submit(new Callable<Iterable<KeyedValue<V>>>()
        {
            @Override
            public Iterable<KeyedValue<V>> call() throws Exception
            {
                return completor.poll(TIMEOUT, TimeUnit.MILLISECONDS);
            }
        });
    }

    private TestValue v(String key, String value)
    {
        return new TestValue(KeyImpl.of(key), value);
    }

    private static final class TestValue implements Comparable<TestValue>
    {
        final Key key;
        final String value;

        private TestValue(Key key, String value)
        {
            this.key = checkNotNull(key);
            this.value = checkNotNull(value);
        }

        @Override
        public boolean equals(Object o)
        {
            return EqualsBuilder.reflectionEquals(this, o);
        }

        @Override
        public int hashCode()
        {
            return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public int compareTo(TestValue testValue)
        {
            int c = key.compareTo(testValue.key);
            return 0 != c ? c : value.compareTo(value);
        }
    }

    private void assertValues(Iterable<KeyedValue<String>> actual, TestValue... expectedCompleted) throws IOException
    {
        Iterable<TestValue> transformedActual = Iterables.transform(actual, new Function<KeyedValue<String>, 
                TestValue>()
        {
            @Override
            public TestValue apply(KeyedValue<String> value)
            {
                return new TestValue(value.key(), value.value().isRight() ? value.value().right().get() : FAIL);
            }
        });
        Iterable<TestValue> transformedExcepted = null == expectedCompleted ? Collections.<TestValue>emptyList() : Arrays.asList(expectedCompleted);

        assertIterablesEqualIgnoreOrder(transformedExcepted, transformedActual);
    }

    private static void assertIterablesEqualIgnoreOrder(Iterable<TestValue> expected, Iterable<TestValue> actual)
    {
        List<TestValue> expectedList = Lists.newLinkedList(expected);
        List<TestValue> actualList = Lists.newLinkedList(actual);
        // Sort before comparing, as order is unimportant
        Collections.sort(expectedList);
        Collections.sort(actualList);
        assertEquals(expectedList, actualList);
    }

    private static class Gate
    {
        private final SettableFuture<String> future;

        private Gate(final FutureCompletionService<String> futureCompletionService, String key)
        {
            this.future = new SettableFuture<String>();
            futureCompletionService.add(key, Promises.forListenableFuture(this.future));
        }

        void open(String value)
        {
            this.future.set(value);
        }

        void fail()
        {
            this.future.setException(new RuntimeException());
        }

        void error()
        {
            this.future.setException(new Error());
        }

        boolean isCancelled()
        {
            return this.future.isCancelled();
        }

        boolean wasInterrupted()
        {
            return this.future.wasInterrupted.get();
        }
    }

    public static final class SettableFuture<V> extends AbstractFuture<V>
    {
        private final AtomicBoolean wasInterrupted;

        public SettableFuture()
        {
            this.wasInterrupted = new AtomicBoolean(false);
        }

        @Override
        public boolean set(@Nullable V value)
        {
            return super.set(value);
        }

        @Override
        public boolean setException(Throwable throwable)
        {
            return super.setException(throwable);
        }

        @Override
        protected void interruptTask()
        {
            this.wasInterrupted.set(true);
        }
    }


    private QueueFutureCompletionService<String> createCompletionService()
    {
        return new QueueFutureCompletionService<String>();
    }
}

