package com.atlassian.plugin.webresource.servlet;

import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.integration.TestCase;
import com.atlassian.plugin.webresource.integration.stub.ResponseData;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import org.junit.Test;

import java.util.Date;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.getField;
import static com.atlassian.plugin.webresource.TestUtils.setField;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.sourceMapUrl;
import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.sourceUrl;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestPluginResourceDownload extends TestCase
{
    @Test
    public void shouldReturnNotFoundError()
    {
        wr.configure().end();
        assertThat(wr.getStatus(resourceUrl("app:page", "style.css")), equalTo(404));
        assertThat(wr.getStatus(sourceUrl("app:page", "style.css")), equalTo(404));
        assertThat(wr.getStatus(sourceMapUrl(sourceUrl("app:page", "style.css"))), equalTo(404));
    }

    @Test
    public void shouldSetProperContentType()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .context("general")
                    .resource("file.special")
                        .contentType("special-type")
        .end();

        String url = resourceUrl("app:page", "file.special");
        assertThat(wr.getHeaders(url).get("Content-Type"), equalTo("special-type"));
        assertThat(wr.getContent(url), equalTo("content of file.special"));
    }

    @Test
    public void shouldUseContentTypeResolverIfResourceTypeIsNull()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("file")
        .end();

        String url = resourceUrl("app:page", "file");
        when(getContentTypeResolver().getContentType(url)).thenReturn("special-type");
        assertThat(wr.getHeaders(url).get("Content-Type"), equalTo("special-type"));
    }

    @Test
    public void shouldNotSetContentTypeIfItsNull()
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("file")
        .end();

        String url = resourceUrl("app:page", "file");
        when(getContentTypeResolver().getContentType(url)).thenReturn(null);
        assertThat(wr.getHeaders(url).containsKey("Content-Type"), equalTo(false));
    }

    @Test
    public void shouldRespondWithHeadIfFileIsCached() throws DownloadException
    {
        wr.configure()
            .plugin("app")
                .webResource("page")
                    .resource("style.css")
        .end();
        Globals globals = wr.getGlobals();
        setField(globals, "config", spy(globals.getConfig()));

        setField(globals, "router", new Router(globals)
        {
            @Override
            public void dispatch(Request request, Response response)
            {
                (new Controller(globals, request, response)
                {
                    @Override
                    protected boolean checkIfCachedAndNotModified(Date updatedAt)
                    {
                        response.setStatus(304);
                        return true;
                    }
                }).serveResource("app:page", "style.css");
            }
        });

        ResponseData data = wr.get(
            resourceUrl("app:page", "style.css"),
            buildMap("cache", "true", "_statichash", "123"),
            buildMap()
        );

        assertThat(data.getStatus(), equalTo(304));
        assertThat(data.getContent(), equalTo(""));
        verify(globals.getConfig(), never()).getStreamFor(any(Resource.class), any(String.class));
    }

    private ContentTypeResolver getContentTypeResolver()
    {
        return (ContentTypeResolver) getField(wr.getGlobals().getConfig(), "contentTypeResolver");
    }
}
