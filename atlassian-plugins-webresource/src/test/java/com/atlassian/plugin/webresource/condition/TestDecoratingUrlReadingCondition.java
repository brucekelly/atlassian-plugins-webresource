package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestDecoratingUrlReadingCondition
{
    @Test
    public void testCanEncodeStateIntoUrl()
    {
        DecoratingCondition decoratingCondition = decorate(mock(Condition.class));

        assertFalse(decoratingCondition.canEncodeStateIntoUrl());
    }

    @Test
    public void testShouldDisplayImmediate()
    {
        Condition condition = mock(Condition.class);
        Map context = ImmutableMap.of();

        DecoratingCondition decoratingCondition = decorate(condition);
        decoratingCondition.shouldDisplayImmediate(context);

        verify(condition).shouldDisplay(context);
    }
}
