package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.plugin.servlet.DownloadException;
import com.google.common.collect.ImmutableMap;
import junit.framework.TestCase;
import org.dom4j.DocumentException;

import static com.atlassian.plugin.webresource.TestUtils.mockLegacyTransformers;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformerAlias;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestTransformerCache extends TestCase
{
    public void testCleaningCacheOnPluginSystemChanges()
    {
        PluginAccessor pluginAccessor;
        TransformerCache cache;
        WebResourceTransformerFactory transformer = mock(WebResourceTransformerFactory.class);
        WebResourceTransformerFactory anotherTransformer = mock(WebResourceTransformerFactory.class);

        // On plugin enabled.
        pluginAccessor = mock(PluginAccessor.class);
        cache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);

        mockTransformers(pluginAccessor, ImmutableMap.of("a", transformer));
        assertSame(transformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());
        cache.onPluginEnabled(mock(PluginEnabledEvent.class));
        mockTransformers(pluginAccessor, ImmutableMap.of("a", anotherTransformer));
        assertSame(anotherTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());

        // On plugin disabled.
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn("some-plugin-key");
        PluginDisabledEvent event = mock(PluginDisabledEvent.class);
        when(event.getPlugin()).thenReturn(plugin);

        pluginAccessor = mock(PluginAccessor.class);
        cache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);
        mockTransformers(pluginAccessor, ImmutableMap.of("a", transformer));
        assertSame(transformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());

        cache.onPluginDisabled(event);
        mockTransformers(pluginAccessor, ImmutableMap.of("a", anotherTransformer));
        assertSame(anotherTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());

        // On module enabled.
        pluginAccessor = mock(PluginAccessor.class);
        cache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);
        mockTransformers(pluginAccessor, ImmutableMap.of("a", transformer));
        assertSame(transformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());
        cache.onPluginModuleEnabled(mock(PluginModuleEnabledEvent.class));
        mockTransformers(pluginAccessor, ImmutableMap.of("a", anotherTransformer));
        assertSame(anotherTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());

        // On module disabled.
        pluginAccessor = mock(PluginAccessor.class);
        cache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);
        mockTransformers(pluginAccessor, ImmutableMap.of("a", transformer));
        assertSame(transformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());
        cache.onPluginModuleDisabled(mock(PluginModuleDisabledEvent.class));
        mockTransformers(pluginAccessor, ImmutableMap.of("a", anotherTransformer));
        assertSame(anotherTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) cache.getDescriptor("a")).getModule());
    }

    public void test() throws DocumentException, DownloadException
    {
        // Setting things up.
        PluginAccessor pluginAccessor = mock(PluginAccessor.class);

        WebResourceTransformerFactory aTransformer = mock(WebResourceTransformerFactory.class);
        mockTransformers(pluginAccessor, ImmutableMap.of("a", aTransformer));
        mockTransformerAlias(pluginAccessor, "a", "a-deprecated");

        WebResourceTransformer aDeprecatedTransformer = mock(WebResourceTransformer.class);
        mockLegacyTransformers(pluginAccessor, ImmutableMap.of("a-deprecated", aDeprecatedTransformer));
        WebResourceTransformer bDeprecatedTransformer = mock(WebResourceTransformer.class);
        mockLegacyTransformers(pluginAccessor, ImmutableMap.of("b-deprecated", bDeprecatedTransformer));

        // Checking.
        TransformerCache transformerCache = new TransformerCache(mock(PluginEventManager.class), pluginAccessor);

        assertEquals(aTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) transformerCache.getDescriptor("a")).getModule());
        assertEquals(aTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) transformerCache.getDescriptor("a-deprecated")).getModule());

        assertEquals(aTransformer, ((UrlReadingWebResourceTransformerModuleDescriptor) transformerCache.getDescriptor("a-deprecated")).getModule());
        assertEquals(bDeprecatedTransformer, ((WebResourceTransformerModuleDescriptor) transformerCache.getDescriptor("b-deprecated")).getModule());
    }

}