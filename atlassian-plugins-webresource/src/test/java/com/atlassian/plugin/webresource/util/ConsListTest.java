package com.atlassian.plugin.webresource.util;

import java.util.Iterator;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class ConsListTest
{
    @Test
    public void empty()
    {
        assertThat(ConsList.<String> empty().isEmpty(), is(true));
    }

    @Test
    public void ofOneIterator()
    {
        Iterator<Integer> it = ConsList.of(1).iterator();
        assertThat(it.hasNext(), is(true));
        assertThat(it.next(), is(1));
        assertThat(it.hasNext(), is(false));
    }

    @Test
    public void ofOne()
    {
        assertThat(ConsList.of(1), contains(1));
    }

    @Test
    public void ofThree()
    {
        assertThat(ConsList.of(1, 2, 3), contains(1, 2, 3));
    }

    @Test
    public void remove()
    {
        assertThat(ConsList.of(1, 2, 3).remove(2), contains(1, 3));
    }
}
