package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.integration.TestCase;
import org.junit.Test;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestSinglePluginResource extends TestCase
{
    @Test
    public void shouldGetUrl() throws Exception
    {
        wr.configure()
            .disableBatching()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.css")
        .end();

        wr.requireResource("plugin:a");

        assertThat(wr.paths(), matches(resourceUrl("plugin:a", "a1.css")));
    }
}
