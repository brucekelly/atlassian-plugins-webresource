package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

public class LineReversingTransformerWithoutSourceMap extends TransformerWithoutSourceMapHelper
{
    public DownloadableResource transform(TransformableResource transformableResource, final QueryParams params)
    {
        DownloadableResource originalResource = transformableResource.nextResource();
        String content = TestUtils.toString(originalResource);
        String[] lines = content.split("\n");
        ArrayUtils.reverse(lines);
        return new StringDownloadableResource(originalResource, StringUtils.join(lines, "\n"));
    }
}