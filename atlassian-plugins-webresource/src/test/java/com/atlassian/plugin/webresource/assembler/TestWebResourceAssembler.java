package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.atlassian.plugin.webresource.TestUtils.createNamedHashes;
import static com.atlassian.plugin.webresource.TestUtils.createWebResourceTransformation;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertPrefixedUrlIs;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedData;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.assertResolvedResources;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.c;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.m;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.setOf;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.superbatch;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.u;
import static com.atlassian.plugin.webresource.assembler.AssemblerTestFixture.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

public class TestWebResourceAssembler
{
    private AssemblerTestFixture f;

    @Before
    public void setup()
    {
        f = new AssemblerTestFixture();
    }

    @Test
    public void testSingleResourceNoDeps() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:nodep-resource").defRes();

        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:nodep-resource"));
    }

    @Test
    public void testSingleResourceNoDepsWithParams() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:nodep-resource").res("a.js").paramRes("b.js", ImmutableMap.of("ieonly", "true"));

        assembler.resources().requireWebResource("test.atlassian:nodep-resource");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:nodep-resource"), m("test.atlassian:nodep-resource", "js", "ieonly=true"));
    }

    @Test
    public void testSingleResourceWithDeps() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"));
    }

    @Test
    public void testSingleResourceMultiDeps() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-a", "test.atlassian:resource-b");

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"), m("test.atlassian:resource-c"));
    }

    @Test
    public void testMultiResourceMultiDeps() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-a");

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"), m("test.atlassian:resource-c"));
    }

    @Test
    public void testMultiResourceMultiDepsOrdering() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-a");

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-c"), m("test.atlassian:resource-b"));
    }

    @Test
    public void testCascadeResourceDeps() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-c").defRes().deps("test.atlassian:resource-b");

        assembler.resources().requireWebResource("test.atlassian:resource-c");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-a"), m("test.atlassian:resource-b"), m("test.atlassian:resource-c"));
    }

    @Test
    public void testSingleContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    @Test
    public void testMultiContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b");

        assembler.resources().requireContext("context.a");
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"), c("context.b"));
    }

    @Test
    public void testMultiContextOrdering() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b");

        assembler.resources().requireContext("context.b");
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b"), c("context.a"));
    }

    @Test
    public void testMultiContextDependencyOverlap() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-base").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-a").defRes().deps("test.atlassian:resource-base").ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-base").ctx("context.b");

        assembler.resources().requireContext("context.a");
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "context.b"));
    }

    @Test
    public void testMultiContextDependencyOverlapMultipleDrain() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        assembler.resources().requireContext("context.a");

        assembler.assembled().drainIncludedResources();
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
    }

    @Test
    public void testExcludeSingleResource() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleResourceDependency() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.resources().exclude(setOf("test.atlassian:resource-b"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleResourceCascade() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-b"));
    }

    @Test
    public void testExcludeSingleContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleContextInWebResource() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleContextInWebResourceDependency() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a").ctx("context.a");

        assembler.resources().exclude(setOf(), setOf("context.a"));

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeSingleWebResourceInContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf());

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResource() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResourceDependency() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleResourceCascade() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, m("test.atlassian:resource-b"));
    }

    @Test
    public void testRememberSingleContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleContextInWebResource() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleContextInWebResourceDependency() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a").ctx("context.a");

        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberSingleWebResourceInContext() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch());
    }

    @Test
    public void testRememberSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.assembled().drainIncludedResources();
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberModuleInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberModuleInSuperbatchDependency() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-b");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testRememberContextInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testContextAndSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        // Added -_super in v.3.3
        assertResolvedResources(resources, superbatch(), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndSuperbatchWithConditional() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-super");
        f.mockPlugin("test.atlassian:resource-super").res("res-one.js").paramRes("res-two.js", ImmutableMap.of("ieonly", "true"));
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        // Added _super in v. 3.3
        assertResolvedResources(resources, superbatch(), superbatch("?ieonly=true"), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndPartiallyOverlappingSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch(), c("context.a", "-_super"));
    }

    @Test
    public void testContextAndCompletelyOverlappingSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, superbatch());
    }

    @Test
    public void testContextPartiallyInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        assembler.assembled().drainIncludedResources();

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "-_super"));
    }

    @Test
    public void testExcludeSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.resources().exclude(setOf(), setOf());
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeModuleInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes();

        assembler.resources().exclude(setOf(), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeModuleInSuperbatchDependency() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-b");
        f.mockPlugin("test.atlassian:resource-a").defRes();
        f.mockPlugin("test.atlassian:resource-b").defRes().deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();

        assembler.resources().exclude(setOf(), setOf());

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeContextInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        assembler.resources().exclude(setOf(), setOf());

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources);
    }

    @Test
    public void testExcludeContextPartiallyInSuperbatch() throws Exception
    {
        f.mockSuperbatch("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");

        assembler.resources().exclude(setOf(), setOf());

        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a", "-_super"));
    }

    @Test
    public void testSameResourceExcludedAndIncluded() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-c").defRes().ctx("context.c");

        assembler.resources().requireWebResource("test.atlassian:resource-a");
        assembler.resources().requireContext("context.b");
        assembler.resources().requireContext("context.c");
        assembler.resources().exclude(setOf("test.atlassian:resource-a"), setOf("context.b"));

        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.c"));
    }

    @Test
    public void testNotIncludeEmptyWebResourceBatch() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("app:a").defRes();
        assembler.resources().requireContext("a");
        assembler.resources().exclude(setOf(), setOf("a"));
        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertResolvedResources(resources);
    }

    @Test
    public void testNotIncludeEmptyContextBatch() throws Exception
    {
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("a");
        assembler.resources().exclude(setOf("a"), setOf());
        WebResourceSet resources = assembler.assembled().drainIncludedResources();
        assertResolvedResources(resources);
    }

    @Test
    public void testSingleResourceUnbatched()
    {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").res("a.js", "b.js");
        WebResourceAssembler assembler = f.create();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, u("test.atlassian:a/a"), u("test.atlassian:a/b"));
    }

    @Test
    public void testSingleResourceUnbatchedMultipleTimes()
    {
        AssemblerTestFixture f = new AssemblerTestFixture(false, false);
        f.mockPlugin("test.atlassian:a").res("a.js", "b.js");
        WebResourceAssembler assembler = f.create();
        assembler.resources().requireWebResource("test.atlassian:a");
        assembler.assembled().drainIncludedResources();

        assembler.resources().requireWebResource("test.atlassian:a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedData(resources);
    }

    @Test
    public void testCloneBasic()
    {
        WebResourceAssembler assembler = f.create();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler clone = assembler.copy();
        clone.resources().requireContext("context.a");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }

    @Test
    public void testCloneWithSharedResource()
    {
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        assembler.resources().requireContext("context.a");
        assembler.assembled().drainIncludedResources();
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));


        WebResourceAssembler clone = assembler.copy();
        clone.resources().requireContext("context.b");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCloneWithSharedResourceDrainAfterClone()
    {
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        assembler.resources().requireContext("context.a");

        WebResourceAssembler clone = assembler.copy();
        clone.assembled().drainIncludedResources();
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        clone.resources().requireContext("context.b");
        WebResourceSet resources = clone.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCloneMultipleTimesProducesSameResult()
    {
        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler = factory.create().build();
        f.mockPlugin("test.atlassian:base").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a").deps("test.atlassian:base");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:base");
        assembler.resources().requireContext("context.a");

        WebResourceAssembler clone1 = assembler.copy();
        WebResourceSet commonResources1 = clone1.assembled().drainIncludedResources();
        assertResolvedResources(commonResources1, c("context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        clone1.resources().requireContext("context.b");
        WebResourceSet resources1 = clone1.assembled().drainIncludedResources();

        assertResolvedResources(resources1, c("context.b", "-context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));

        WebResourceAssembler clone2 = assembler.copy();
        WebResourceSet commonResources2 = clone2.assembled().drainIncludedResources();
        assertResolvedResources(commonResources2, c("context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));

        clone2.resources().requireContext("context.b");
        WebResourceSet resources2 = clone2.assembled().drainIncludedResources();

        assertResolvedResources(resources2, c("context.b", "-context.a"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(2));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testImplicitSuperbatchEnablement()
    {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceSet set1 = f.create().assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, superbatch());
    }

    @Test
    public void testExplicitSuperbatchEnablement()
    {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, superbatch());
    }

    @Test
    public void testExplicitSuperbatchDisablement()
    {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(false).build();
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1);
    }

    @Test
    public void testExplicitSuperbatchDisablementWithContext()
    {
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes();
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(false).build();
        assembler.resources().requireContext("context.a");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        AssemblerTestFixture.assertResolvedResources(set1, c("context.a"));
    }

    @Test
    public void testCdnSafeTransformResources()
    {
        WebResourceTransformation tx = createWebResourceTransformation(Collections.<String, String >emptyMap(), true, createNamedHashes(), "js");

        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes().transform(tx);
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a").transform(tx);
        f.mockPlugin("plug1:wr3").defRes().transform(tx);

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a").requireWebResource("plug1:wr3");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(3, resources.size());
        assertPrefixedUrlIs(resources.get(0), superbatch(), true);
        // Added -_super in v. 3.3
        assertPrefixedUrlIs(resources.get(1), c("context.a", "-_super"), true);
        assertPrefixedUrlIs(resources.get(2), m("plug1:wr3"), true);
    }

    @Test
    public void testCdnSafeConditionResources()
    {
        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes().condition(f.condition().shouldDisplay(true).canEncodeStateIntoUrl(true).build());
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a").condition(f.condition().shouldDisplay(true).canEncodeStateIntoUrl(true).build());
        f.mockPlugin("plug1:wr3").defRes().condition(f.condition().shouldDisplay(true).canEncodeStateIntoUrl(true).build());

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a").requireWebResource("plug1:wr3");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(3, resources.size());
        assertPrefixedUrlIs(resources.get(0), superbatch(), true);
        // Added -_super in v. 3.3
        assertPrefixedUrlIs(resources.get(1), c("context.a", "-_super"), true);
        assertPrefixedUrlIs(resources.get(2), m("plug1:wr3"), true);
    }

//    Disabled, no need for it.
//    @Test
//    public void testCachingCondition1Resources()
//    {
//        ConditionSupplier changingLegacyCondition = new ConditionSupplier(true);
//        f.mockPlugin("plug1:wr1").defRes().condition(f.condition().shouldDisplay(changingLegacyCondition).canEncodeStateIntoUrl(false).build());
//
//        DefaultWebResourceAssemblerFactory factory = f.factory();
//        WebResourceAssembler assembler1 = factory.create().build();
//        assembler1.resources().requireWebResource("plug1:wr1");
//        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();
//
//        assertResolvedResources(set1, m("plug1:wr1"));
////        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
////        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));
//
//        // Set the condition to false. The webresource should no longer be returned by a WebResourceAssembler.
//        changingLegacyCondition.set(false);
//
//        WebResourceAssembler assembler2 = factory.create().build();
//        assembler2.resources().requireWebResource("plug1:wr1");
//        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();
//
//        assertResolvedResources(set2);
////        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
////        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
//    }

    @Test
    public void testCachingCondition2Resources()
    {
        final AtomicBoolean setParam = new AtomicBoolean(true);
        UrlAdderFunction urlAdderFunction = new UrlAdderFunction()
        {
            @Override
            public Void apply(UrlBuilder input)
            {
                if (setParam.get())
                {
                    input.addToQueryString("added", "true");
                }
                return null;
            }
        };
        DecoratingCondition condition = f.condition().shouldDisplay(true)
            .canEncodeStateIntoUrl(true).urlAdder(urlAdderFunction).build();
        f.mockPlugin("plug1:wr1").defRes().condition(condition);

        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("added", "true")));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // Set the condition to false. The query param should no longer be returned by a WebResourceAssembler.
        setParam.set(false);

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js"));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCachingTransformer2Resources()
    {
        final AtomicReference<String> changingValue = new AtomicReference<String>("original");
        Supplier<Map<String, String>> queryParams = new Supplier<Map<String, String>>()
        {
            @Override
            public Map<String, String> get()
            {
                return ImmutableMap.of("chicken", changingValue.get());
            }
        };
        Supplier<Iterable<String>> hashes = Suppliers.<Iterable<String>>ofInstance(createNamedHashes());
        WebResourceTransformation tx = createWebResourceTransformation(queryParams, true, hashes, "js");
        f.mockPlugin("plug1:wr1").defRes().transform(tx);

        DefaultWebResourceAssemblerFactory factory = f.factory();
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("chicken", "original")));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // change the query param value. Any assembler should now return the updated query param value
        changingValue.set("crispy");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js", q("chicken", "crispy")));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testCachingStaticTransfomers()
    {
        final AtomicReference<String> changingValue = new AtomicReference<String>("original");
        f.mockPlugin("plug1:wr1").defRes();
        StaticTransformers changingTransformers = new StaticTransformers()
        {
            @Override
            public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder)
            {
                urlBuilder.addToQueryString("chicken", changingValue.get());
            }

            @Override
            public Content transform(Content content, TransformerParameters transformerParameters, ResourceLocation resourceLocation, QueryParams queryParams, String sourceUrl)
            {
                return content;
            }
        };
        DefaultWebResourceAssemblerFactory factory = f.factory(changingTransformers);
        WebResourceAssembler assembler1 = factory.create().build();
        assembler1.resources().requireWebResource("plug1:wr1");
        WebResourceSet set1 = assembler1.assembled().drainIncludedResources();

        assertResolvedResources(set1, m("plug1:wr1", "js", q("chicken", "original")));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(1));

        // change the query param value. Any assembler should now return the updated query param value
        changingValue.set("crispy");

        WebResourceAssembler assembler2 = factory.create().build();
        assembler2.resources().requireWebResource("plug1:wr1");
        WebResourceSet set2 = assembler2.assembled().drainIncludedResources();

        assertResolvedResources(set2, m("plug1:wr1", "js", q("chicken", "crispy")));
//        assertThat(factory.getWebResourceSetCache().numIncludeVariants(), is(1));
//        assertThat(factory.getWebResourceSetCache().numEntries(), is(2));
    }

    @Test
    public void testConditionTaintedSuperbatch()
    {
        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes().condition(f.condition().shouldDisplay(true).build());

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));

        // In the 3.1.x version the super batch included resources with the legacy conditions,
        // now all resources with the legacy condition in the super batch would be ignored.
        //
        // It is different from how resources with legacy conditions handled in context batches, in context
        // batches - in context batches they would be extracted and put out of the batch.
        //
        // It is possible to make the super batch behave the same way, but, it would create additional complexities,
        // and, as soon as we do have control over all the resources in the super batch it seems it is
        // easier to just do not include resources with the legacy condition in the super batch.
        //
        // Technical explanation - why it works that way:
        //
        //   - the legacy 3.1.x code used to resolve batches, and, as soon as it serves legacy batches with the super
        //     batch it would return  the `_context:_super` as the result.
        //   - next the modern implementation used to split it into sub batches, process all the resources etc., and it
        //     threats the super batch as the context batch, and skips all the legacy resources inside of it.
        //     And, it doesn't creates additional web resource batch with the legacy condition because it expects
        //     that the batch resolution already done by the legacy 3.1.x code in the previous step.
        //   - as the result the final super batch would be empty, and none urls would be generated.
        //
        //   assertEquals(1, resources.size());
        //   assertPrefixedUrlIs(resources.get(0), superbatch(), false);
        //
        assertEquals(0, resources.size());
    }

    @Test
    public void testTransformTaintedSuperbatch()
    {
        f.prefixUrlsWithHash(true);
        f.mockSuperbatch("plug1:wr1");
        f.mockPlugin("plug1:wr1").defRes().transform(createWebResourceTransformation(Collections.<String, String>emptyMap(), false, createNamedHashes(), "js"));

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(1, resources.size());
        assertPrefixedUrlIs(resources.get(0), superbatch(), false);
    }

    @Test
    public void testConditionTaintedContext()
    {
        f.prefixUrlsWithHash(true);
        f.mockPlugin("plug1:wr1").defRes().ctx("context.a").condition(f.condition().shouldDisplay(true).build());
        f.mockPlugin("plug1:wr2").defRes().ctx("context.a").condition(f.condition().build());
        f.mockPlugin("plug1:wr3").defRes().condition(f.condition().shouldDisplay(true).build());
        f.mockPlugin("plug1:wr4").defRes().condition(f.condition().build());

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a")
            .requireWebResource("plug1:wr3")
            .requireWebResource("plug1:wr4");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(2, resources.size());
        // context is empty - condition forces wr1 out
        // all explicitly included resources next
        assertPrefixedUrlIs(resources.get(0), m("plug1:wr3"), false);
        // all conditional context batch resources next (note that this is out of order!)
        assertPrefixedUrlIs(resources.get(1), m("plug1:wr1"), false);
    }

    @Test
    public void testTransformTaintedContext()
    {
        f.prefixUrlsWithHash(true);
        f.mockPlugin("plug1:wr1").defRes().ctx("context.a").transform(createWebResourceTransformation(Collections.<String, String>emptyMap(), false, createNamedHashes(), "js"));

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(true).build();
        assembler.resources().requireContext("context.a");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(1, resources.size());
        assertPrefixedUrlIs(resources.get(0), c("context.a"), false);
    }

    @Test
    public void testContextWithMixedLegacyConditions()
    {
        f.prefixUrlsWithHash(true);
        f.mockPlugin("plug1:legacy").defRes().ctx("context1").condition(f.condition().shouldDisplay(true).build());
        f.mockPlugin("plug1:new").defRes().ctx("context1").condition(f.condition().shouldDisplay(true).canEncodeStateIntoUrl(true).build());

        WebResourceAssembler assembler = f.factory().create().includeSuperbatchResources(false).build();
        assembler.resources().requireContext("context1");
        WebResourceSet set1 = assembler.assembled().drainIncludedResources();

        ArrayList<PluginUrlResource> resources = Lists.newArrayList(AssemblerTestFixture.getWebUrlResources(set1));
        assertEquals(2, resources.size());
        assertPrefixedUrlIs(resources.get(0), c("context1"), true);
        assertPrefixedUrlIs(resources.get(1), m("plug1:legacy"), false);
    }

    @Test
    public void testContextSubtraction()
    {
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");
        f.mockPlugin("test.atlassian:resource-b").defRes().ctx("context.b").deps("test.atlassian:resource-a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf(), setOf("context.a"));
        assembler.resources().requireContext("context.b");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.b", "-context.a"));
    }

    @Test
    public void testExcludingNonOverlappingResource()
    {
        f.mockPlugin("test.atlassian:resource-z").defRes();
        f.mockPlugin("test.atlassian:resource-a").defRes().ctx("context.a");

        WebResourceAssembler assembler = f.create();
        assembler.resources().exclude(setOf("test.atlassian:resource-z"), setOf());
        assembler.resources().requireContext("context.a");
        WebResourceSet resources = assembler.assembled().drainIncludedResources();

        assertResolvedResources(resources, c("context.a"));
    }
}
