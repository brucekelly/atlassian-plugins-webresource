package com.atlassian.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.mockito.Matchers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.Dom4jFluent.element;
import static java.util.Arrays.asList;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


/**
 *
 */
public class WebResourceModuleDescriptorBuilder
{
    private final Plugin plugin;
    private final String moduleKey;
    private List<Dom4jFluent.Element> resourceDescriptors = new ArrayList<Dom4jFluent.Element>();
    private List<Dom4jFluent.Element> dependencies = new ArrayList<Dom4jFluent.Element>();
    private Dom4jFluent.Element condition = null;
    private List<Dom4jFluent.Element> conditions = new ArrayList<Dom4jFluent.Element>();
    private List<Dom4jFluent.Element> contexts = new ArrayList<Dom4jFluent.Element>();
    private List<Dom4jFluent.Element> transformers = new ArrayList<Dom4jFluent.Element>();
    private List<Dom4jFluent.Element> dataProviders = new ArrayList<Dom4jFluent.Element>();

    public WebResourceModuleDescriptorBuilder(Plugin plugin, String moduleKey)
    {
        this.plugin = plugin;
        this.moduleKey = moduleKey;
    }


    public WebResourceModuleDescriptorBuilder setCondition(Class<?> condition)
    {
        this.condition = element("condition", ImmutableMap.of(
                "class", condition.getName()));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addCondition(Class<?> condition)
    {
        this.conditions.add(element("condition", ImmutableMap.of(
                "class", condition.getName())));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(String downloadableFile)
    {
        return addDescriptor("download", downloadableFile, downloadableFile);
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(String type, String name, String location)
    {
        resourceDescriptors.add(element("resource", ImmutableMap.of(
                "type", type,
                "name", name,
                "location", location)));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDescriptor(String type, String name, String location,
        String contentType, Map<String, String> params)
    {
        List<Dom4jFluent.Element> xmlParams = new ArrayList<Dom4jFluent.Element>();
        if (contentType != null) xmlParams.add(element("param", ImmutableMap.of("name", "content-type", "value", contentType)));
        for (Map.Entry<String, String> entry : params.entrySet()) {
            xmlParams.add(element("param", ImmutableMap.of("name", entry.getKey(), "value", entry.getValue())));
        }
        Dom4jFluent.Element[] asArray = (Dom4jFluent.Element[]) xmlParams.toArray(new Dom4jFluent.Element[xmlParams.size()]);
        resourceDescriptors.add(element("resource", ImmutableMap.of(
                "type", type,
                "name", name,
                "location", location), asArray));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDependency(String moduleKey)
    {
        dependencies.add(element("dependency", moduleKey));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addContext(String context)
    {
        contexts.add(element("context", context));
        return this;
    }

    public void addDataProvider(String key, Class<?> provider)
    {
        dataProviders.add(element("data", ImmutableMap.of(
            "key", key,
            "class", provider.getName()
        )));
    }

    public WebResourceModuleDescriptorBuilder addTransformer(String transformerKey, String extension)
    {
        transformers.add(element("transformation", ImmutableMap.of("extension", extension),
                element("transformer", ImmutableMap.of("key", transformerKey))));
        return this;
    }

    public WebResourceModuleDescriptorBuilder addDataProvider(Plugin plugin, String key, Class providerClass)
    {
        dataProviders.add(element("data", ImmutableMap.of("key", key, "class", providerClass.getName())));
        try
        {
            when(plugin.loadClass(Matchers.eq(providerClass.getName()), (Class<?>) any())).thenReturn(
                (Class<Object>) TestUtils.class.getClassLoader().loadClass(providerClass.getName()));
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException(e);
        }
        return this;
    }

    public WebResourceModuleDescriptorBuilder addTransformers(String extension, List<String> transformerKeys)
    {
        Dom4jFluent.Element[] list = new Dom4jFluent.Element[transformerKeys.size()];
        for (int i = 0; i < transformerKeys.size(); i++) {
            list[i] = element("transformer", ImmutableMap.of("key", transformerKeys.get(i)));
        }
        transformers.add(element("transformation", ImmutableMap.of("extension", extension), list));
        return this;
    }

    public WebResourceModuleDescriptor build()
    {
        return build(new DefaultHostContainer());
    }

    public WebResourceModuleDescriptor build(HostContainer hostContainer)
    {
        List<Dom4jFluent.Element> conditionList = null == condition ? Collections.<Dom4jFluent.Element>emptyList() : ImmutableList.of(condition);
        List<Dom4jFluent.Element> conditionsList = conditions.isEmpty() ? Collections.<Dom4jFluent.Element>emptyList() :
                asList(element("conditions", ImmutableMap.of("type", "and"), Arrays.asList((Iterable<Dom4jFluent.Element>)conditions)));
        WebResourceModuleDescriptor descriptor = new WebResourceModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, hostContainer);
        descriptor.init(plugin, element("web-resource",
                ImmutableMap.of(
                        "key", moduleKey
                ),
                Arrays.<Iterable<Dom4jFluent.Element>>asList(
                        resourceDescriptors,
                        dependencies,
                        conditionList,
                        conditionsList,
                        contexts,
                        transformers,
                        dataProviders
                )).toDom());
        descriptor.enabled();
        return descriptor;
    }
}
