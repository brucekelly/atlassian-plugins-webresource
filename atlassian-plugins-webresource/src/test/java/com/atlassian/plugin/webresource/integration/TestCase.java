package com.atlassian.plugin.webresource.integration;

import com.atlassian.plugin.webresource.integration.stub.WebResource;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import org.junit.Before;

abstract public class TestCase {
    protected WebResource wr;

    @Before
    public void before() throws Exception
    {
        wr = new WebResourceImpl();
    }
}