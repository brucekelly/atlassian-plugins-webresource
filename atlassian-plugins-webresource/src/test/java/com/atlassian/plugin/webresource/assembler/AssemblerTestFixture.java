package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.PluginResourceLocatorImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.TestUtils;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.WebResourceUrlProviderImpl;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.data.PluginDataResource;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.TestUtils.emptyStaticTransformers;
import static com.atlassian.plugin.webresource.TestUtils.setField;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AssemblerTestFixture
{
    public static final String SUPERBATCH_VERSION = "1";

    private PluginResourceLocator pluginResourceLocator;
    private PluginAccessor mockPluginAccessor = mock(PluginAccessor.class);
    private WebResourceIntegration mockWebResourceIntegration = mock(WebResourceIntegration.class);
    private WebResourceUrlProvider webResourceUrlProvider = mock(WebResourceUrlProvider.class);
    private ResourceBatchingConfiguration mockBatchingConfiguration = mock(ResourceBatchingConfiguration.class);
    private PluginEventManager mockEventPublisher = mock(PluginEventManager.class);
    private Plugin testPlugin;
    private boolean useHashInUrls = false;

    private List<WebResourceModuleDescriptor> createdWebResources = Lists.newArrayList();

    public AssemblerTestFixture()
    {
        this(true, true);
    }

    public AssemblerTestFixture(boolean pluginWebResourceBatchingEnabled, boolean contextBatchingEnabled)
    {
        when(mockWebResourceIntegration.getPluginAccessor()).thenReturn(mockPluginAccessor);
        when(mockPluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class)).thenReturn(createdWebResources);

        when(mockBatchingConfiguration.isPluginWebResourceBatchingEnabled()).thenReturn(
                pluginWebResourceBatchingEnabled, contextBatchingEnabled);
        when(mockBatchingConfiguration.isContextBatchingEnabled()).thenReturn(true);
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(mockWebResourceIntegration.getSuperBatchVersion()).thenReturn(SUPERBATCH_VERSION);
        when(webResourceUrlProvider.getStaticResourcePrefix(anyString(), anyString(), any(UrlMode.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (useHashInUrls) {
                    return invocationOnMock.getArguments()[0].toString();
                } else {
                    return "";
                }
            }
        });
        testPlugin = TestUtils.createTestPlugin();
    }

    public AssemblerTestFixture prefixUrlsWithHash(boolean prefix) {
        this.useHashInUrls = prefix;
        return this;
    }

    public DefaultWebResourceAssemblerFactory factory()
    {
        return factory(emptyStaticTransformers());
    }

    public DefaultWebResourceAssemblerFactory factory(StaticTransformers staticTransformers)
    {
        WebResourceUrlProviderImpl urlProvider = new WebResourceUrlProviderImpl(mock(WebResourceIntegration.class));
        pluginResourceLocator = new PluginResourceLocatorImpl(mockWebResourceIntegration, null, urlProvider,
            mockBatchingConfiguration, mockEventPublisher, staticTransformers);
        Globals globals = pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt();
        setField(globals.getConfig(), "urlProvider", webResourceUrlProvider);
        return new DefaultWebResourceAssemblerFactory(globals);
    }

    public WebResourceAssembler create()
    {
        return factory().create().build();
    }

    public static void assertResolvedResources(WebResourceSet actual, String ... expectedResourceNames)
    {
        assertResolvedResources(getWebUrlResources(actual), expectedResourceNames);
    }

    public static <T extends PluginUrlResource> void assertResolvedResources(Iterable<T> resources, String ... expectedResourceNames)
    {
        Iterable<String> actualModuleKeys = Iterables.transform(resources, new Function<PluginUrlResource, String>()
        {
            @Override
            public String apply(PluginUrlResource input)
            {
                return input.getStaticUrl(com.atlassian.webresource.api.UrlMode.RELATIVE);
            }
        });
        assertEquals(Arrays.asList(expectedResourceNames), Lists.newArrayList(actualModuleKeys));
    }

    public static void assertResolvedData(WebResourceSet resources, String ... expectedDataNames)
    {
        Iterable<String> actualModuleKeys = Iterables.transform(getWebDataResources(resources), new Function<PluginDataResource, String>()
        {
            @Override
            public String apply(@Nullable PluginDataResource input)
            {
                return input.getKey();
            }
        });
        assertEquals(Arrays.asList(expectedDataNames), Lists.newArrayList(actualModuleKeys));
    }

    public static Iterable<PluginUrlResource> getWebUrlResources(WebResourceSet webResources)
    {
        return webResources.getResources(PluginUrlResource.class);
    }

    public static Iterable<PluginDataResource> getWebDataResources(WebResourceSet webResources)
    {
        return webResources.getResources(PluginDataResource.class);
    }

    public void mockSuperbatch(String... superbatchModuleKeys)
    {
        when(mockBatchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(mockBatchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(ImmutableList.copyOf(superbatchModuleKeys));
        when(mockWebResourceIntegration.getSuperBatchVersion()).thenReturn(SUPERBATCH_VERSION);
    }

    public MockPluginBuilder mockPlugin(String moduleCompleteKey)
    {
        return new MockPluginBuilder(moduleCompleteKey);
    }

    public class MockPluginBuilder
    {
        private final List<ResourceDescriptor> resourceDescriptors = Lists.newLinkedList();
        private final List<String> dependencies = Lists.newLinkedList();
        private final Set<String> contexts = Sets.newHashSet();
        private final Map<String, WebResourceDataProvider> dataProviders = Maps.newLinkedHashMap();
        private DecoratingCondition condition = null;
        private final List<WebResourceTransformation> transformers = Lists.newLinkedList();

        public MockPluginBuilder(String moduleCompleteKey)
        {
            Supplier<DecoratingCondition> conditionSupplier = new Supplier<DecoratingCondition>() {
                @Override
                public DecoratingCondition get() {
                    return condition;
                }
            };
            WebResourceModuleDescriptor webResource = TestUtils.createWebResourceModuleDescriptor(moduleCompleteKey, testPlugin,
                    resourceDescriptors, dependencies, contexts, transformers, conditionSupplier, dataProviders);
            when(mockPluginAccessor.getEnabledPluginModule(moduleCompleteKey)).thenReturn((ModuleDescriptor) webResource);
            createdWebResources.add(webResource);
        }

        /**
         * Adds a default resource, as resources are not rendered if they contain no resources
         * @return this
         */
        public MockPluginBuilder defRes()
        {
            resourceDescriptors.addAll(TestUtils.createResourceDescriptors("placeholder.js"));
            return this;
        }

        public MockPluginBuilder res(String ... resources)
        {
            resourceDescriptors.addAll(TestUtils.createResourceDescriptors(resources));
            return this;
        }

        public MockPluginBuilder paramRes(String resource, Map<String, String> params)
        {
            resourceDescriptors.add(TestUtils.createResourceDescriptor(resource, params));
            return this;
        }

        public MockPluginBuilder deps(String ... dependencies)
        {
            this.dependencies.addAll(Arrays.asList(dependencies));
            return this;
        }

        public MockPluginBuilder ctx(String ... contexts)
        {
            this.contexts.addAll(Arrays.asList(contexts));
            return this;
        }

        public MockPluginBuilder data(String name, WebResourceDataProvider value)
        {
            this.dataProviders.put(name, value);
            return this;
        }

        public MockPluginBuilder transform(WebResourceTransformation transform)
        {
            transformers.add(transform);
            return this;
        }

        public MockPluginBuilder condition(DecoratingCondition condition)
        {
            this.condition = condition;
            return this;
        }
    }

    public MockConditionBuilder condition()
    {
        return new MockConditionBuilder();
    }

    public static class MockConditionBuilder
    {
        private UrlAdderFunction urlAdderFunction;
        private Supplier<Boolean> shouldDisplay;
        private boolean canEncodeStateIntoUrl;
        private boolean invert;
        public MockConditionBuilder urlAdder(UrlAdderFunction urlAdderFunction)
        {
            this.urlAdderFunction = urlAdderFunction;
            return this;
        }

        public MockConditionBuilder shouldDisplay(boolean shouldDisplay)
        {
            return this.shouldDisplay(new ConditionSupplier(shouldDisplay));
        }

        public MockConditionBuilder shouldDisplay(ConditionSupplier shouldDisplay)
        {
            this.shouldDisplay = shouldDisplay;
            return this;
        }

        public MockConditionBuilder canEncodeStateIntoUrl(boolean canEncodeStateIntoUrl)
        {
            this.canEncodeStateIntoUrl = canEncodeStateIntoUrl;
            return this;
        }

        public MockConditionBuilder invert(boolean invert)
        {
            this.invert = invert;
            return this;
        }

        DecoratingCondition build()
        {
            class MockCondition implements DecoratingCondition {
                @Override
                public void addToUrl(UrlBuilder urlBuilder)
                {
                    if (urlAdderFunction != null)
                    {
                        urlAdderFunction.apply(urlBuilder);
                    }
                }

                @Override
                public boolean shouldDisplay(QueryParams params) {
                    return shouldDisplay.get();
                }

                @Override
                public boolean canEncodeStateIntoUrl() {
                    return canEncodeStateIntoUrl;
                }

                @Override
                public boolean shouldDisplayImmediate(Map<String, Object> context) {
                    if (shouldDisplay == null)
                        return false;
                    return shouldDisplay.get();
                }

                @Override
                public DecoratingCondition invertCondition() {
                    throw new UnsupportedOperationException("Not implemented");
                }
            }
            return new MockCondition();
        }
    }

    public static class ConditionSupplier implements Supplier<Boolean>
    {
        private boolean value;

        public ConditionSupplier(boolean value)
        {
            this.value = value;
        }

        public void set(Boolean value)
        {
            this.value = value;
        }

        @Override
        public Boolean get()
        {
            return this.value;
        }
    }

    public interface UrlAdderFunction extends Function<UrlBuilder, Void>
    {
    }

    public static List<String> listOf(String ... values)
    {
        return Lists.newArrayList(values);
    }

    public static Set<String> setOf(String ... values)
    {
        return Sets.newLinkedHashSet(listOf(values));
    }

    public static String superbatch()
    {
        return superbatch(null);
    }

    public static String superbatch(String queryString)
    {
        String url = "/download/contextbatch/js/_super/batch.js";
        if (null != queryString)
        {
            url += queryString;
        }
        return url;
    }

    public static String m(String moduleCompleteKey)
    {
        return m(moduleCompleteKey, "js");
    }

    public static String m(String moduleCompleteKey, String extension)
    {
        return m(moduleCompleteKey, extension, null);
    }

    public static String q(String key, String value)
    {
        // TODO URL escape!
        return key + "=" + value;
    }

    public static String m(String moduleCompleteKey, String extension, String queryString)
    {
        return "/download/batch/" + moduleCompleteKey + "/" + moduleCompleteKey + "." + extension + ((queryString != null) ? "?" + queryString : "");
    }

    public static String c(String... contexts)
    {
        return "/download/contextbatch/js/" + Joiner.on(",").join(contexts) + "/batch.js";
    }

    public static void assertPrefixedUrlIs(PluginUrlResource resource, String expectedTail, boolean isCdnSupported) {
        String[] actualFullUrl = splitPrefix(resource.getStaticUrl(com.atlassian.webresource.api.UrlMode.ABSOLUTE));
        String actualPrefix = actualFullUrl[0];
        String actualTail = actualFullUrl[1];
        assertEquals(expectedTail, actualTail);

        String expectedCdnSuffix = isCdnSupported ? "-CDN" : "-T";
        assertTrue("expected " + expectedCdnSuffix + " in " + actualPrefix, actualPrefix.endsWith(expectedCdnSuffix));
    }

    private static String[] splitPrefix(String staticUrl) {
        int i = staticUrl.indexOf('/');
        assertTrue("contains a slash " + staticUrl, i != -1);
        return new String[] {staticUrl.substring(0, i), staticUrl.substring(i, staticUrl.length())};
    }

    public static String u(String resource)
    {
        return "/download/resources/" + resource + ".js";
    }
}
