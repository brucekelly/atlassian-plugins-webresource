package com.atlassian.plugin.webresource.assembler;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.integration.stub.WebResourceImpl;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.Map;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestDefaultPageBuilderService
{
    private PageBuilderService create()
    {
        Map<String, Object> requestCache = Maps.newHashMap();
        WebResourceIntegration mockWebResourceIntegration = mock(WebResourceIntegration.class);
        PluginEventManager mockPluginEventManager = mock(PluginEventManager.class);
        when(mockWebResourceIntegration.getRequestCache()).thenReturn(requestCache);

        WebResourceImpl wr = new WebResourceImpl();
        wr.configure().end();
        WebResourceAssemblerFactory webResourceAssemblerFactory = new DefaultWebResourceAssemblerFactory(wr
            .getGlobals());

        return new DefaultPageBuilderService(mockWebResourceIntegration, webResourceAssemblerFactory);
    }

    @Test
    public void testAssembledResources()
    {
        assertNotNull(create().assembler());
    }

    @Test
    public void testAssembledResourcesIsSameInstanceOnMultipleCalls()
    {
        PageBuilderService pageBuilderService = create();
        AssembledResources assembledResources = pageBuilderService.assembler().assembled();

        assertTrue(assembledResources == pageBuilderService.assembler().assembled());
    }

    @Test
    public void testRequiredResources()
    {
        assertNotNull(create().assembler().resources());
    }

    @Test
    public void testRequiredResourcesIsSameInstanceOnMultipleCalls()
    {
        PageBuilderService pageBuilderService = create();
        RequiredResources requiredResources = pageBuilderService.assembler().resources();

        assertTrue(requiredResources == pageBuilderService.assembler().resources());
    }

    @Test
    public void testSeed()
    {
        WebResourceAssembler assembler = mock(WebResourceAssembler.class);
        PageBuilderService pageBuilderService = create();

        pageBuilderService.seed(assembler);

        assertTrue(assembler == pageBuilderService.assembler());
    }

    @Test(expected = IllegalStateException.class)
    public void testSeedFailsIfAlreadySeeded()
    {
        PageBuilderService pageBuilderService = create();
        pageBuilderService.seed(mock(WebResourceAssembler.class));
        pageBuilderService.seed(mock(WebResourceAssembler.class));
    }

    @Test(expected = IllegalStateException.class)
    public void testSeedFailsIfAlreadyRequested()
    {
        PageBuilderService pageBuilderService = create();
        pageBuilderService.assembler();
        pageBuilderService.seed(mock(WebResourceAssembler.class));
    }
}
