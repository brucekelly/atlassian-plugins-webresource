package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.getField;

public class UrlReadingConditionStub implements UrlReadingCondition
{
    public static Map<String, String> params;

    @Override
    public void init(Map<String, String> params)
    {
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        UrlReadingConditionStub.params = (Map<String, String>) getField(params, "map");
        return true;
    }
}
