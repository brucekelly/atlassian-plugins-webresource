package com.atlassian.plugin.webresource.util;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class UrlHashMatcher extends BaseMatcher
{
    private String hash;

    public UrlHashMatcher(String hash)
    {
        this.hash = hash;
    }

    @Override
    public void describeTo(final Description description)
    {
        description.appendText("has hash `" + hash + "`");
    }

    @Override
    public boolean matches(Object actual)
    {
        return (actual != null) && actual.toString().contains("/" + hash);
    }

    public static UrlHashMatcher hasHash(String hash)
    {
        return new UrlHashMatcher(hash);
    }
}