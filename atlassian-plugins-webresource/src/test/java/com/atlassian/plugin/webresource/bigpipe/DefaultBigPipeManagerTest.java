package com.atlassian.plugin.webresource.bigpipe;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableNumber;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.util.concurrent.Promises;
import com.atlassian.webresource.api.bigpipe.BigPipeManager;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import org.junit.Test;

import static org.junit.Assert.*;

public class DefaultBigPipeManagerTest
{

    // https://jira.atlassian.com/browse/CONFDEV-35854
    @Test
    public void testGetBigPipe_Concurrent() throws Exception
    {
        final CountDownLatch pushLatch = new CountDownLatch(1);
        final CountDownLatch readLatch = new CountDownLatch(1);
        final BigPipeManager bigPipeManager = new DefaultBigPipeManager();

        Runnable r1 = new Runnable()
        {
            @Override
            public void run()
            {
                String requestId = UUID.randomUUID().toString();
                //push content and wait for other thread to push
                BigPipeManager.BigPipe bigPipe = bigPipeManager.getBigPipe(requestId);
                bigPipe.push("1",  Promises.<Jsonable>promise(new JsonableString("")));

                try
                {
                    pushLatch.await();
                }
                catch (InterruptedException e)
                {
                    fail("await is interrupted");
                }

                //pull content and let other thread to do the same
                List<Jsonable> result = getResults(bigPipeManager.getConsumableBigPipe(requestId));
                assertEquals("We should fetch stuff only pushed by this thread", 1, result.size());
                assertTrue("We should fetch stuff only pushed by this thread", result.get(0) instanceof JsonableString);

                //let other thread to pull content
                readLatch.countDown();
            }
        };

        Runnable r2 = new Runnable()
        {
            @Override
            public void run()
            {
                String requestId = UUID.randomUUID().toString();
                //push content and let other thread to go
                BigPipeManager.BigPipe bigPipe = bigPipeManager.getBigPipe(requestId);
                bigPipe.push("1",  Promises.<Jsonable>promise(new JsonableNumber(10)));
                pushLatch.countDown();

                //wait for another thread to pull its content and trying to pull ours content
                try
                {
                    readLatch.await();
                }
                catch (InterruptedException e)
                {
                    fail("await is interrupted");
                }

                List<Jsonable> result = getResults(bigPipeManager.getConsumableBigPipe(requestId));
                assertEquals("We should fetch stuff only pushed by this thread", 1, result.size());
                assertTrue("We should fetch stuff only pushed by this thread", result.get(0) instanceof JsonableNumber);
            }
        };

        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        t1.start();
        t2.start();

        t1.join();
        t2.join();
    }

    private List<Jsonable> getResults(BigPipeManager.ConsumableBigPipe consumableBigPipe)
    {
        List<Jsonable> result = new ArrayList<Jsonable>();
        for(KeyedValue<Jsonable> jsonable: consumableBigPipe.getAvailableContent())
        {
            result.add(jsonable.value().right().get());
        }
        return result;
    }
}