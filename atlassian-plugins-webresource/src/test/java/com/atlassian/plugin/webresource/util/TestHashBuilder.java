package com.atlassian.plugin.webresource.util;

import com.google.common.collect.ImmutableList;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestHashBuilder
{
    @Test
    public void testThatHashBuilderPreservesOrder()
    {
        String hash1 = HashBuilder.buildHash(ImmutableList.of("abc", "xyz"));

        String hash2 = HashBuilder.buildHash(ImmutableList.of("xyz", "abc"));

        assertTrue(!hash1.equals(hash2));
    }
}
