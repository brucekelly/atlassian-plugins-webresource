package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.http.Router;

import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.getField;

// Overrides `buildUrlWithPrefix` method in order to prevent router from generating hashes.
public class RouterWithoutHashes extends Router
{
    public RouterWithoutHashes(Router router)
    {
        this((Globals) getField(router, "globals"), (List<Route>) getField(router, "routes"),
                (Boolean) getField(router, "useAbsoluteUrl"));
    }

    public RouterWithoutHashes(Globals globals)
    {
        super(globals);
    }

    protected RouterWithoutHashes(Globals globals, List<Route> routes, boolean useAbsoluteUrl)
    {
        super(globals, routes, useAbsoluteUrl);
    }

    public String buildUrlWithPrefix(String url, Map<String, String> params, boolean isCacheSupported,
            boolean isCdnSupported, String hash, String version)
    {
        return buildUrl("/download" + url, params);
    }

    public Router cloneWithNewUrlMode(boolean useAbsoluteUrl)
    {
        return new RouterWithoutHashes(globals, routes, useAbsoluteUrl);
    }
}