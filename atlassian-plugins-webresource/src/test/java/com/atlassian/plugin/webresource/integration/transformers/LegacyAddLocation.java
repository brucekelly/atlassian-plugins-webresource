package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import org.dom4j.Element;

public class LegacyAddLocation implements WebResourceTransformer
{
    public DownloadableResource transform(final Element configElement, final ResourceLocation location,
            String filePath, final DownloadableResource resource)
    {
        return new CharSequenceDownloadableResource(resource)
        {
            protected CharSequence transform(CharSequence originalContent)
            {
                return "location=/" + location.getName() + " (legacy)\n" + originalContent;
            }
        };
    }
}