package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

public abstract class TransformerWithoutSourceMapHelper implements WebResourceTransformerFactory
{
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
    {
        return new TransformerUrlBuilder()
        {
            public void addToUrl(UrlBuilder urlBuilder)
            {
            }
        };
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters parameters)
    {
        final TransformerWithoutSourceMapHelper _this = this;
        return new UrlReadingWebResourceTransformer()
        {
            public DownloadableResource transform(TransformableResource transformableResource, QueryParams params)
            {
                return _this.transform(transformableResource, params);
            }
        };
    }

    public abstract DownloadableResource transform(TransformableResource transformableResource, QueryParams params);
}