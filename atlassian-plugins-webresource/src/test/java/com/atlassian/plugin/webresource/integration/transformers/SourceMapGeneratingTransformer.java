package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.sourcemap.SourceMap;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static com.atlassian.sourcemap.Util.create1to1SourceMap;

public class SourceMapGeneratingTransformer extends ContentTransformerHelper
{
    public Content transform(final Content content, ResourceLocation resourceLocation, QueryParams params,
            final String sourceUrl)
    {
        return new ContentImpl(content.getContentType(), true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                if (!isSourceMapEnabled)
                {
                    return content.writeTo(out, isSourceMapEnabled);
                }

                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                SourceMap inputSourceMap = content.writeTo(buff, isSourceMapEnabled);
                final String contentAsString = buff.toString();

                if (inputSourceMap != null)
                {
                    throw new RuntimeException("invalid usage, source map generating transformer should be the first "
                            + "one!");
                }

                String content = buff.toString();
                try
                {
                    out.write(contentAsString.getBytes());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
                return create1to1SourceMap(content, sourceUrl);
            }
        };
    }
}