package com.atlassian.plugin.webresource.transformer.instance;

import com.atlassian.plugin.elements.ResourceLocation;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class TestRelativeUrlTransformerMatcher
{
    private final RelativeUrlTransformerMatcher matcher = new RelativeUrlTransformerMatcher();

    @Test
    public void testMatchesLess()
    {
        assertThat(matcher.matches("less"), Matchers.is(true));
    }

    @Test
    public void testMatchesCss()
    {
        assertThat(matcher.matches("css"), Matchers.is(true));
    }

    @Test
    public void testMatchesWebResourceEndingWithCss()
    {
        final ResourceLocation cssResourceLocation = new ResourceLocation(null, "abcd.css", "css", null, null, Maps.<String, String>newHashMap());

        assertThat(matcher.matches(cssResourceLocation), Matchers.is(true));
    }

    @Test
    public void testMatchesWebResourceLocationEndingWithLess()
    {
        final ResourceLocation resourceLocation = new ResourceLocation(null, "abcd.less", "less", null, null, Maps.<String, String>newHashMap());

        assertThat(matcher.matches(resourceLocation), Matchers.is(true));
    }

    @Test
    public void testHandlesNullExtension()
    {
        assertThat(matcher.matches((String) null), Matchers.is(false));
    }

    @Test
    public void testHandlesNullFileNameAndType()
    {
        final ResourceLocation resourceLocation = new ResourceLocation(null, null, null, null, null, Maps.<String, String>newHashMap());

        assertThat(matcher.matches(resourceLocation), Matchers.is(false));
    }

}