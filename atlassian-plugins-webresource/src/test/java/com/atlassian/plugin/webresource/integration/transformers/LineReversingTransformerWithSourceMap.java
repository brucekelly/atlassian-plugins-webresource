package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.SourceMapImpl;
import com.atlassian.sourcemap.Util;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LineReversingTransformerWithSourceMap extends ContentTransformerHelper
{
    public Content transform(final Content content, ResourceLocation resourceLocation, QueryParams params,
            final String sourceUrl)
    {
        return new ContentImpl(content.getContentType(), true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                ByteArrayOutputStream buff = new ByteArrayOutputStream();
                SourceMap inputSourceMap = content.writeTo(buff, isSourceMapEnabled);
                String contentAsString = buff.toString();

                final String[] lines = contentAsString.split("\n");
                ArrayUtils.reverse(lines);

                SourceMap sourceMap = null;
                if (isSourceMapEnabled && (inputSourceMap != null))
                {
                    sourceMap = new SourceMapImpl();
                    int length = lines.length;
                    for (int i = length - 1; i >= 0; i--)
                    {
                        sourceMap.addMapping(length - 1 - i, 0, i, 0, sourceUrl);
                    }
                    sourceMap = Util.rebase(sourceMap, inputSourceMap);
                }

                try
                {
                    out.write(StringUtils.join(lines, "\n").getBytes());
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
                return sourceMap;
            }
        };
    }
}