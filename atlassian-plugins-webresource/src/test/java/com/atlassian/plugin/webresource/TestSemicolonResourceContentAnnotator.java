package com.atlassian.plugin.webresource;

import java.io.ByteArrayOutputStream;

import junit.framework.TestCase;

public class TestSemicolonResourceContentAnnotator extends TestCase {

    public void testSemicolonIsAlwaysAppendedAndPrepended() throws Exception
    {
        ResourceContentAnnotator annotator = new SemicolonResourceContentAnnotator();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        annotator.before(null, stream);
        annotator.after(null, stream);

        String script = new String(stream.toByteArray());
        assertTrue(script.startsWith(";"));
        assertTrue(script.endsWith(";"));
    }
}
