package com.atlassian.plugin.webresource.integration.stub;

import java.util.Map;

public abstract class ResponseData
{
    public abstract int getStatus();

    public abstract String getContent();

    public abstract Map<String, String> getHeaders();

    public String getContentType()
    {
        return getHeaders().get("Content-Type");
    }

    public String getLocation()
    {
        return getHeaders().get("Location");
    }
}