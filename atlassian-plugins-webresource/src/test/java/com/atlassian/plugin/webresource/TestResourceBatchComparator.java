package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import junit.framework.TestCase;

import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;

// Note, the previous version of comparator used lexicographical order of resource url to sort resources. But, as soon
// as the urls for sub batches are the same except the querystring parts, it seems that using querystring only
// should be sufficient to maintain the same order.
public class TestResourceBatchComparator extends TestCase
{
    static Helpers.ParamsComparator PARAMS_COMPARATOR = new Helpers.ParamsComparator();

    private static int compare(Map<String, String> a, Map<String, String> b)
    {
        return PARAMS_COMPARATOR.compare(a, b);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResources()
    {
        assertTrue(compare(buildMap("media", "print"), buildMap()) > 0);
    }

    public void testResourceBatchComparatorFirstPriorityToUnconditionalResourcesOnly()
    {
        assertTrue(compare(
            buildMap("content-type", "application/xml", "cache", "no-cache"),
            buildMap("media", "print")
        ) < 0);
    }

    public void testResourceBatchComparatorFewerConditionsComesFirst()
    {
        assertTrue(compare(
            buildMap("media", "blah", "conditionalComment", "lte+IE+8"),
            buildMap("media", "print")
        ) > 0);
    }

    public void testResourceBatchComparatorMediaConditionComesBeforeIeOnly()
    {
        assertTrue(compare(buildMap("media", "print"), buildMap("ieonly", "true")) < 0);
    }

    public void testResourceBatchComparatorOrdersByUrlAmongstUnconditionals()
    {
        assertTrue(compare(buildMap("media", "a"), buildMap("media", "b")) == 0);
    }

    public void testResourceBatchComparatorOrdersByUrlAmongstSameSetOfConditions()
    {
        assertTrue(compare(
            buildMap("media", "blah", "conditionalComment", "lte+IE+9"),
            buildMap("media", "blah", "conditionalComment", "lte+IE+8")
        ) == 0);
    }

    // Disabling it, it is the same as the previous.
//    public void testResourceBatchComparatorOrdersByConditionsAmongstSameSetOfConditions()
//    {
//        when(resource1.getParams()).thenReturn(ImmutableMap.of("media", "blah", "conditionalComment", "lte+IE+8"));
//        when(resource1.getUrl()).thenReturn("/aaaa");
//        when(resource2.getParams()).thenReturn(ImmutableMap.of("media", "blah", "conditionalComment", "lte+IE+9"));
//        when(resource2.getUrl()).thenReturn("/aaaa");
//
//        List list = Lists.newArrayList(resource1, resource2);
//        Collections.sort(list, new BatchResourceComparator());
//        assertTrue(list.get(0).equals(resource1));
//        assertTrue(list.get(1).equals(resource2));
//    }

    public void testResourceBatchComparatorNumberOfConditionsTrumpsOrderByUrl()
    {
        assertTrue(compare(
            buildMap("media", "blah"),
            buildMap("media", "blah", "conditionalComment", "lte+IE+8")
        ) < 0);
    }

    public void testResourceBatchComparatorCacheParamVsLocale()
    {
        assertTrue(compare(
            buildMap("cache", "true", "locale", "en"),
            buildMap("locale", "en")
        ) > 0);
    }

    public void testResourceBatchComparatorCacheParam()
    {
        assertTrue(compare(
            buildMap("cache", "true"),
            buildMap()
        ) > 0);
    }

    public void testResourceBatchComparatorCacheParamBeforeConditions()
    {
        assertTrue(compare(
            buildMap("cache", "true"),
            buildMap("conditionalComment", "lte+IE+8")
        ) < 0);
    }
}