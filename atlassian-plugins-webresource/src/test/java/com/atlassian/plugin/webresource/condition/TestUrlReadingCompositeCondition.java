package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import org.junit.Test;

import java.util.Map;

import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestUrlReadingCompositeCondition
{
    @Test
    public void testAddToUrl()
    {
        UrlReadingCondition mockCondition = mock(UrlReadingCondition.class);
        DecoratingCompositeCondition condition = createCompositeCondition(
                decorate(mockCondition));

        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);

        verify(mockCondition).addToUrl(urlBuilder);
    }

    @Test
    public void testCanEncodeStateIntoUrlFalse()
    {
        UrlReadingCondition mockCondition1 = mock(UrlReadingCondition.class);
        Condition mockCondition2 = mock(Condition.class);
        DecoratingCompositeCondition condition = createCompositeCondition(
                decorate(mockCondition1),
                decorate(mockCondition2));

        assertFalse(condition.canEncodeStateIntoUrl());
    }

    @Test
    public void testCanEncodeStateIntoUrlTrue()
    {
        UrlReadingCondition mockCondition1 = mock(UrlReadingCondition.class);
        UrlReadingCondition mockCondition2 = mock(UrlReadingCondition.class);
        DecoratingCompositeCondition condition = createCompositeCondition(
                decorate(mockCondition1),
                decorate(mockCondition2));

        assertTrue(condition.canEncodeStateIntoUrl());
    }

    private DecoratingCompositeCondition createCompositeCondition(final DecoratingCondition... conditions)
    {
        DecoratingCompositeCondition parentCondition = new DecoratingCompositeCondition()
        {
            @Override
            public boolean shouldDisplay(QueryParams params)
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public boolean shouldDisplayImmediate(Map<String, Object> context)
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public DecoratingCondition invertCondition()
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
        for (DecoratingCondition condition : conditions)
        {
            parentCondition.addCondition(condition);
        }
        return parentCondition;
    }
}
