package com.atlassian.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.collect.ImmutableMap;
import junit.framework.TestCase;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Set;

import static com.atlassian.plugin.webresource.TestUtils.emptyStaticTransformers;
import static com.atlassian.plugin.webresource.TestUtils.mockTransformers;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestWebResourceModuleDescriptor extends TestCase
{
    private static final String TEST_PLUGIN_KEY = "atlassian.test.plugin";

    private WebResourceModuleDescriptor descriptor;
    private Plugin mockPlugin;
    private PluginAccessor mockPluginAccessor;
    private TransformerCache transformerCache;

    protected void setUp() throws Exception
    {
        super.setUp();
        descriptor = new WebResourceModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, new DefaultHostContainer());
        mockPlugin = mock(Plugin.class);
        when(mockPlugin.getKey()).thenReturn(TEST_PLUGIN_KEY);
        mockPluginAccessor = mock(PluginAccessor.class);
        transformerCache = new TransformerCache(mock(PluginEventManager.class), mockPluginAccessor);
    }

    public void testInitWithDependencies() throws Exception
    {
        String xml = "<web-resource key=\"test-resources\">\n" +
                        "<dependency>atlassian.test.plugin:jquery</dependency>\n" +
                        "<dependency>atlassian.test.plugin:ajs</dependency>\n" +
                    "</web-resource>";

        initDescriptorFromXml(xml);

        List<String> dependencies = descriptor.getDependencies();
        assertEquals(2, dependencies.size());
        assertEquals("atlassian.test.plugin:jquery", dependencies.get(0));
        assertEquals("atlassian.test.plugin:ajs", dependencies.get(1));
    }

    public void testInitWithContexts() throws Exception
    {
        String xml = "<web-resource key=\"test-resources\">\n" +
                        "<context>C1</context>\n" +
                    "</web-resource>";

        initDescriptorFromXml(xml);

        Set<String> contexts = descriptor.getContexts();
        assertEquals(2, contexts.size());
        assertTrue(contexts.contains("atlassian.test.plugin:test-resources")); // implicit context
        assertTrue(contexts.contains("C1"));

        assertEquals(0, descriptor.getDependencies().size());
    }

//    // Disabling, no need for this test.
//    public void testKeyIsPassedToTransformers() throws Exception
//    {
//        WebResourceTransformerFactory factory = mock(WebResourceTransformerFactory.class);
//        TransformerUrlBuilder transformerUrlBuilder = mock(TransformerUrlBuilder.class);
//        when(factory.makeUrlBuilder(any(TransformerParameters.class))).thenReturn(transformerUrlBuilder);
//        mockTransformers(mockPluginAccessor, ImmutableMap.of("doge-transformer", factory));
//        String xml = "<web-resource key=\"doge-resources\">\n" +
//                "<transformation extension=\"js\"><transformer key=\"doge-transformer\" /></transformation>\n" +
//                "<resource name=\"doge.js\" location=\"doge.js\" />\n" +
//                "</web-resource>";
//
//        initDescriptorFromXml(xml);
//
//        descriptor.addToUrl("js", emptyStaticTransformers(), transformerCache, new DefaultUrlBuilder());
//
//        ArgumentCaptor<TransformerParameters> transformerParamsCaptor = ArgumentCaptor.forClass(TransformerParameters.class);
//        verify(factory).makeUrlBuilder(transformerParamsCaptor.capture());
//        TransformerParameters params = transformerParamsCaptor.getValue();
//        assertEquals(TEST_PLUGIN_KEY, params.getPluginKey());
//        assertEquals("doge-resources", params.getModuleKey());
//    }
//
//    // Disabling, no need for this test.
//    public void testAddToUrlCallsStaticTransformers() throws Exception
//    {
//        StaticTransformers staticTransformers = mock(StaticTransformers.class);
//
//        WebResourceTransformerFactory factory = mock(WebResourceTransformerFactory.class);
//        TransformerUrlBuilder transformerUrlBuilder = mock(TransformerUrlBuilder.class);
//        when(factory.makeUrlBuilder(any(TransformerParameters.class))).thenReturn(transformerUrlBuilder);
//        mockTransformers(mockPluginAccessor, ImmutableMap.of("doge-transformer", factory));
//        String xml = "<web-resource key=\"doge-resources\">\n" +
//                "<transformation extension=\"js\"><transformer key=\"doge-transformer\" /></transformation>\n" +
//                "<resource name=\"doge.js\" location=\"doge.js\" />\n" +
//                "</web-resource>";
//
//        initDescriptorFromXml(xml);
//
//        descriptor.addToUrl("js", staticTransformers, transformerCache, new DefaultUrlBuilder());
//
//        ArgumentCaptor<TransformerParameters> transformerParamsCaptor = ArgumentCaptor.forClass(TransformerParameters.class);
//        verify(staticTransformers).addToUrl(eq("js"), transformerParamsCaptor.capture(), any(UrlBuilder.class));
//        TransformerParameters params = transformerParamsCaptor.getValue();
//        assertEquals(TEST_PLUGIN_KEY, params.getPluginKey());
//        assertEquals("doge-resources", params.getModuleKey());
//    }

    private void initDescriptorFromXml(String xml) throws DocumentException {
        descriptor.init(mockPlugin, DocumentHelper.parseText(xml).getRootElement());
    }
}
