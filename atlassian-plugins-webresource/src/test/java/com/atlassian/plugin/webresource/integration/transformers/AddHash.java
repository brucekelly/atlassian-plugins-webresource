package com.atlassian.plugin.webresource.integration.transformers;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.CharSequenceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

public class AddHash implements WebResourceTransformerFactory
{
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters)
    {
        return new TransformerUrlBuilder()
        {
            public void addToUrl(UrlBuilder urlBuilder)
            {
                urlBuilder.addToHash("some-hash", "some-value");
            }
        };
    }

    public UrlReadingWebResourceTransformer makeResourceTransformer(final TransformerParameters parameters)
    {
        return new UrlReadingWebResourceTransformer()
        {
            public DownloadableResource transform(final TransformableResource resource, final QueryParams params)
            {
                return resource.nextResource();
            }
        };
    }
}