package com.atlassian.plugin.webresource;

import com.atlassian.fugue.Suppliers;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.hostcontainer.DefaultHostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingCondition;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.integration.stub.RouterWithoutHashes;
import com.atlassian.plugin.webresource.transformer.ContentTransformerFactory;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import javax.annotation.Nullable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUtils
{
    public static Globals createGlobals()
    {
        return createGlobals(mock(PluginResourceLocator.class));
    }

    public static Globals createGlobals(WebResourceIntegration integration, Class<? extends Router> klass)
    {
        return createGlobals(mock(PluginResourceLocator.class), integration, klass);
    }

    public static Globals createGlobals(PluginResourceLocator resourceLocator)
    {
        return createGlobals(resourceLocator, createWebResourceIntegration(), RouterWithoutHashes.class);
    }

    public static Globals createGlobals(WebResourceIntegration integration, WebResourceUrlProvider urlProvider, Class<? extends Router> klass)
    {
        Globals globals = createGlobals(integration, klass);
        setField(globals.getConfig(), "urlProvider", urlProvider);
        return globals;
    }

    public static Globals createGlobals(PluginResourceLocator resourceLocator, WebResourceIntegration integration, Class<? extends Router> routerClass)
    {
        TransformerCache transformerCache = new TransformerCache(mock(PluginEventManager.class), mock(PluginAccessor.class));
        DefaultResourceBatchingConfiguration batchingConfig = new DefaultResourceBatchingConfiguration();
        Config config = new Config(batchingConfig, integration,
            new WebResourceUrlProviderImpl(mock(WebResourceIntegration.class)), mock(ServletContextFactory.class),
            mock(StaticTransformers.class), transformerCache);

        Globals globals = new Globals(resourceLocator, config);

        Router router;
        try
        {
            Constructor<? extends Router> routerConstructor = routerClass.getDeclaredConstructor(Globals.class);
            router = routerConstructor.newInstance(globals);
        }
        catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new RuntimeException(e);
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }

        setField(globals, "router", router);
        when(resourceLocator.temporaryWayToGetGlobalsDoNotUseIt()).thenReturn(globals);

        return globals;
    }

    public static WebResourceIntegration createWebResourceIntegration()
    {
        PluginInformation pluginInformation = mock(PluginInformation.class);
        when(pluginInformation.getVersion()).thenReturn("0");

        Plugin plugin = mock(Plugin.class);
        when(plugin.getPluginInformation()).thenReturn(pluginInformation);

        ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        when(moduleDescriptor.getPlugin()).thenReturn(plugin);

        PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        when(pluginAccessor.getEnabledPluginModule(any(String.class))).thenReturn(moduleDescriptor);

        WebResourceIntegration integration = mock(WebResourceIntegration.class);
        when(integration.getPluginAccessor()).thenReturn(pluginAccessor);
        return integration;
    }

    static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey, final Plugin p)
    {
        return createWebResourceModuleDescriptor(completeKey, p, Collections.<ResourceDescriptor>emptyList(), Collections.<String>emptyList());
    }

    static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey,
        final Plugin p, final List<ResourceDescriptor> resourceDescriptors)
    {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, Collections.<String>emptyList());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey,
        final Plugin p, final List<ResourceDescriptor> resourceDescriptors, final List<String> dependencies)
    {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, dependencies, Collections.<String>emptySet());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey,
        final Plugin p, final List<ResourceDescriptor> resourceDescriptors, final List<String> dependencies, final Set<String> contexts)
    {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, dependencies, contexts, Collections.<WebResourceTransformation>emptyList());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey,
        final Plugin p, final List<ResourceDescriptor> resourceDescriptors, final List<String> dependencies, final Set<String> contexts,
        final List<WebResourceTransformation> transformations)
    {
        return createWebResourceModuleDescriptor(completeKey, p, resourceDescriptors, dependencies, contexts, transformations,
            Suppliers.<DecoratingCondition>alwaysNull(), Collections.<String, WebResourceDataProvider>emptyMap());
    }

    public static WebResourceModuleDescriptor createWebResourceModuleDescriptor(final String completeKey,
         final Plugin p, final List<ResourceDescriptor> resourceDescriptors, final List<String> dependencies, final Set<String> contexts,
         final List<WebResourceTransformation> transformations, final Supplier<DecoratingCondition> condition,
         final Map<String, WebResourceDataProvider> dataProviders)
    {
        return new TestWebResourceModuleDescriptor(ModuleFactory.LEGACY_MODULE_FACTORY, new DefaultHostContainer(), completeKey,
                resourceDescriptors, p, dependencies, contexts, transformations, condition, dataProviders);
    }

    public static class TestWebResourceModuleDescriptor extends WebResourceModuleDescriptor
    {
        private final String completeKey;
        private final List<ResourceDescriptor> resourceDescriptors;
        private final Plugin p;
        private final List<String> dependencies;
        private final Set<String> contexts;
        private final List<WebResourceTransformation> transformations;
        private final Supplier<DecoratingCondition> condition;
        private final Map<String, WebResourceDataProvider> dataProviders;

        public TestWebResourceModuleDescriptor(ModuleFactory legacyModuleFactory, DefaultHostContainer defaultHostContainer,
            String completeKey, List<ResourceDescriptor> resourceDescriptors, Plugin p, List<String> dependencies, Set<String> contexts,
            List<WebResourceTransformation> transformations, Supplier<DecoratingCondition> condition, Map<String, WebResourceDataProvider> dataProviders)
        {
            super(legacyModuleFactory, defaultHostContainer);
            this.completeKey = completeKey;
            this.resourceDescriptors = resourceDescriptors;
            this.p = p;
            this.dependencies = dependencies;
            this.contexts = contexts;
            this.transformations = transformations;
            this.condition = condition;
            this.dataProviders = dataProviders;
        }

        @Override
        public String getCompleteKey()
        {
            return completeKey;
        }

        @Override
        public List<ResourceDescriptor> getResourceDescriptors()
        {
            return resourceDescriptors;
        }

        @Override
        public List<ResourceDescriptor> getResourceDescriptors(String type)
        {
            return resourceDescriptors;
        }

        @Override
        public String getPluginKey()
        {
            return p.getKey();
        }

        @Override
        public Plugin getPlugin()
        {
            return p;
        }

        @Override
        public List<String> getDependencies()
        {
            return dependencies;
        }

        @Override
        public Set<String> getContexts()
        {
            return contexts;
        }

        @Override
        public List<WebResourceTransformation> getTransformations()
        {
            return transformations;
        }

        @Override
        public DecoratingCondition getCondition() {
            return condition.get();
        }

        @Override
        public ResourceLocation getResourceLocation(String type, String name)
        {
            if ("download".equals(type))
            {
                return new ResourceLocation("", name, type, "", "", Collections.<String, String>emptyMap());
            }
            return super.getResourceLocation(type, name);
        }

        @Override
        public Map<String, WebResourceDataProvider> getDataProviders()
        {
            return dataProviders;
        }
    }

    public static List<ResourceDescriptor> createResourceDescriptors(String... resourceNames)
    {
        return createResourceDescriptors(Arrays.asList(resourceNames));
    }

    public static List<ResourceDescriptor> createResourceDescriptors(List<String> resourceNames)
    {
        List<ResourceDescriptor> resourceDescriptors = new ArrayList<ResourceDescriptor>();
        for(String resourceName : resourceNames)
        {
            resourceDescriptors.add(createResourceDescriptor(resourceName));
        }
        return resourceDescriptors;
    }

    public static ResourceDescriptor createResourceDescriptor(String resourceName)
    {
        return createResourceDescriptor(resourceName, new TreeMap<String, String>());
    }

    public static ResourceDescriptor createResourceDescriptor(String resourceName, Map<String, String> parameters)
    {
        try
        {
            String xml = "<resource type=\"download\" name=\"" + resourceName + "\" location=\"/includes/css/" + resourceName + "\">\n" +
                                "<param name=\"source\" value=\"webContextStatic\"/>\n";

            if(resourceName.indexOf("ie") != -1)
                parameters.put("ieonly", "true");

            for(String key : parameters.keySet())
            {
                xml += "<param name=\"" + escapeXMLCharacters(key) + "\" value=\"" + escapeXMLCharacters(parameters.get(key)) + "\"/>\n";
            }

            xml += "</resource>";
            return new ResourceDescriptor(DocumentHelper.parseText(xml).getRootElement());
        }
        catch (DocumentException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public static List<String> createNamedHashes(String... values)
    {
        if (values.length % 2 != 0)
        {
            throw new IllegalArgumentException("Must provide an even number of parameters");
        }
        List<String> hashes = Lists.newLinkedList();
        Iterator<String> iter = Arrays.asList(values).iterator();
        while (iter.hasNext())
        {
            hashes.add(iter.next());
        }
        return Collections.unmodifiableList(hashes);
    }

    public static void mockTransformers(PluginAccessor pluginAccessor, Map<String, ? extends WebResourceTransformerFactory> transformers)
    {
        Iterable<UrlReadingWebResourceTransformerModuleDescriptor> descriptors = Iterables.transform(transformers.entrySet(), new Function<Map.Entry<String, ? extends WebResourceTransformerFactory>, UrlReadingWebResourceTransformerModuleDescriptor>()
        {
            @Override
            public UrlReadingWebResourceTransformerModuleDescriptor apply(@Nullable Map.Entry<String, ? extends WebResourceTransformerFactory> input)
            {
                UrlReadingWebResourceTransformerModuleDescriptor descriptor = mock(UrlReadingWebResourceTransformerModuleDescriptor.class);
                when(descriptor.getKey()).thenReturn(input.getKey());
                when(descriptor.getModule()).thenReturn(input.getValue());
                return descriptor;
            }
        });

        List<UrlReadingWebResourceTransformerModuleDescriptor> descriptorList = Lists.newArrayList(descriptors);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class))
                .thenReturn(descriptorList);
    }

    public static UrlReadingWebResourceTransformerModuleDescriptor mockTransformer(String key, String aliasKey, Class klass)
    {
        try
        {
            UrlReadingWebResourceTransformerModuleDescriptor descriptor = mock(UrlReadingWebResourceTransformerModuleDescriptor.class);
            when(descriptor.getKey()).thenReturn(key);
            when(descriptor.getAliasKey()).thenReturn(aliasKey);
            when(descriptor.getModule()).thenReturn((WebResourceTransformerFactory) klass.newInstance());
            return descriptor;
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static ContentTransformerModuleDescriptor mockContentTransformer(String key, Class klass)
    {
        try
        {
            ContentTransformerModuleDescriptor descriptor = mock(ContentTransformerModuleDescriptor.class);
            when(descriptor.getKey()).thenReturn(key);
            when(descriptor.getModule()).thenReturn((ContentTransformerFactory) klass.newInstance());
            return descriptor;
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static WebResourceTransformerModuleDescriptor mockLegacyTransformer(String key, Class klass)
    {
        try
        {
            WebResourceTransformerModuleDescriptor descriptor = mock(WebResourceTransformerModuleDescriptor.class);
            when(descriptor.getKey()).thenReturn(key);
            when(descriptor.getModule()).thenReturn((WebResourceTransformer) klass.newInstance());
            return descriptor;
        }
        catch (InstantiationException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void mockTransformerAlias(PluginAccessor pluginAccessor, final String key, final String alias)
    {
        List<UrlReadingWebResourceTransformerModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class);
        List<UrlReadingWebResourceTransformerModuleDescriptor> transformed = Lists.transform(descriptors, new Function<UrlReadingWebResourceTransformerModuleDescriptor, UrlReadingWebResourceTransformerModuleDescriptor>()
        {
            @Override
            public UrlReadingWebResourceTransformerModuleDescriptor apply(@Nullable UrlReadingWebResourceTransformerModuleDescriptor urlReadingWebResourceTransformerModuleDescriptor)
            {
                if (key.equals(urlReadingWebResourceTransformerModuleDescriptor.getKey()))
                {
                    when(urlReadingWebResourceTransformerModuleDescriptor.getAliasKey()).thenReturn(alias);
                }
                return urlReadingWebResourceTransformerModuleDescriptor;
            }
        });
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class)).thenReturn(transformed);
    }

    public static void mockLegacyTransformers(PluginAccessor pluginAccessor, Map<String, ? extends WebResourceTransformer> transformers)
    {
        Iterable<WebResourceTransformerModuleDescriptor> descriptors = Iterables.transform(transformers.entrySet(), new Function<Map.Entry<String, ? extends WebResourceTransformer>, WebResourceTransformerModuleDescriptor>()
        {
            @Override
            public WebResourceTransformerModuleDescriptor apply(@Nullable Map.Entry<String, ? extends WebResourceTransformer> input)
            {
                WebResourceTransformerModuleDescriptor descriptor = mock(WebResourceTransformerModuleDescriptor.class);
                when(descriptor.getKey()).thenReturn(input.getKey());
                when(descriptor.getModule()).thenReturn(input.getValue());
                return descriptor;
            }
        });

        List<WebResourceTransformerModuleDescriptor> descriptorList = Lists.newArrayList(descriptors);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceTransformerModuleDescriptor.class))
                .thenReturn(descriptorList);
    }

    /**
     * Creates a web resource transformation that adds the given queryParams to the querystring and the given hashes to the hash
     * @param queryParams Map of query params
     * @param hashes List of hashes
     */
    public static WebResourceTransformation createWebResourceTransformation(
            final Map<String, String> queryParams, final boolean isUrlReadingTransformer, final Iterable<String> hashes,
            final String transformationType)
    {
        return createWebResourceTransformation(Suppliers.ofInstance(queryParams), isUrlReadingTransformer, Suppliers.ofInstance(hashes), transformationType);
    }

    /**
     * Creates a web resource transformation that adds the given queryParams to the querystring and the given hashes to the hash
     * @param queryParams Map of query params
     * @param hashes List of hashes
     */
    public static WebResourceTransformation createWebResourceTransformation(
        final Supplier<Map<String, String>> queryParams, final boolean isUrlReadingTransformer, final Supplier<Iterable<String>> hashes,
        final String transformationType)
    {
        WebResourceTransformation webResourceTransformation = mock(WebResourceTransformation.class);
        doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                UrlBuilder urlBuilder = (UrlBuilder) args[2];
                for (Map.Entry<String, String> entry : queryParams.get().entrySet())
                {
                    urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
                }
                for (String hash : hashes.get())
                {
                    urlBuilder.addToHash("", hash);
                }
                return null;
            }
        }).when(webResourceTransformation).addTransformParameters(any(TransformerCache.class), any(TransformerParameters.class), any(DefaultUrlBuilder.class));
        doAnswer(new Answer<Object>() {
            public Object answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                String type = (String) args[0];
                return transformationType.equals(type);
            }
        }).when(webResourceTransformation).matches(any(String.class));
        when(webResourceTransformation.containsOnlyPureUrlReadingTransformers(any(TransformerCache.class))).thenReturn(isUrlReadingTransformer);
        return webResourceTransformation;
    }

    private static String escapeXMLCharacters(String input)
    {
        return input.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt");
    }

    public static Plugin createTestPlugin()
    {
        return createTestPlugin("test.atlassian", "1");
    }

    public static Plugin createTestPlugin(String pluginKey, String version, Class... loadableClasses)
    {
        try
        {
            final Plugin plugin = mock(Plugin.class);
            PluginInformation pluginInfo = new PluginInformation();
            pluginInfo.setVersion(version);
            when(plugin.getPluginInformation()).thenReturn(pluginInfo);
            when(plugin.getKey()).thenReturn(pluginKey);
            when(plugin.getDateLoaded()).thenReturn(new Date());
            for (Class loadableClass : loadableClasses)
            {
                when(plugin.loadClass(Matchers.eq(loadableClass.getName()), (Class<?>) any())).thenReturn((Class<Object>) TestUtils.class.getClassLoader().loadClass(loadableClass.getName()));
            }
            return plugin;
        }
        catch (ClassNotFoundException ex)
        {
            throw new RuntimeException(ex);
        }
    }

    public static StaticTransformers emptyStaticTransformers()
    {
        return new StaticTransformers()
        {
            @Override
            public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder) {}

            @Override
            public Content transform(Content content, TransformerParameters transformerParameters,
                ResourceLocation resourceLocation, QueryParams queryParams, String sourceUrl)
            {
                return content;
            }
        };
    }

    static List<String> emptyHashes()
    {
        return Collections.emptyList();
    }

    public static Map<String, String> emptyParams()
    {
        return Collections.emptyMap();
    }

    public static void setupSuperBatch(ResourceBatchingConfiguration batchingConfiguration)
    {
        when(batchingConfiguration.isSuperBatchingEnabled()).thenReturn(true);
        when(batchingConfiguration.isContextBatchingEnabled()).thenReturn(true);
        when(batchingConfiguration.getSuperBatchModuleCompleteKeys()).thenReturn(
                Arrays.asList("test.atlassian:superbatch", "test.atlassian:superbatch2", "test.atlassian:missing-plugin"));
    }

    public static Map<String, Object> emptyContext()
    {
        return Collections.emptyMap();
    }

    public static QueryParams emptyQueryParams()
    {
        return QueryParams.of(Collections.<String, String>emptyMap());
    }

    public static class StubUrlReadingCondition implements UrlReadingCondition
    {
        @Override
        public void addToUrl(UrlBuilder urlBuilder)
        {
        }

        @Override
        public boolean shouldDisplay(QueryParams params)
        {
            return true;
        }

        @Override
        public void init(Map<String, String> params) throws PluginParseException
        {
        }
    }

    public static WebResourceDataProvider emptyDataProvider()
    {
        return stringDataProvider("");
    }

    public static WebResourceDataProvider stringDataProvider(final String str)
    {
        return new WebResourceDataProvider()
        {
            @Override
            public Jsonable get()
            {
                return jsonableOf(str);
            }
        };
    }

    public static Jsonable jsonableOf(final String str)
    {
        return new Jsonable()
        {
            @Override
            public void write(Writer writer) throws IOException
            {
                writer.write("\"" + str + "\"");
            }
        };
    }

    public static String toString(DownloadableResource downloadableResource)
    {
        ByteArrayOutputStream buff = new ByteArrayOutputStream();
        try
        {
            downloadableResource.streamResource(buff);
        }
        catch (DownloadException e)
        {
            throw new RuntimeException(e);
        }
        return buff.toString();
    }

    public static Object getField(Object obj, String name)
    {
        try
        {
            return getFieldAndEnsureItsAccessible(obj, name).get(obj);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void setField(Object obj, String name, Object value)
    {
        try
        {
            getFieldAndEnsureItsAccessible(obj, name).set(obj, value);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static Field getFieldAndEnsureItsAccessible(Object obj, String name)
    {
        Class klass = obj instanceof Class ? (Class) obj : obj.getClass();
        Field field = null;
        do
        {
            try
            {
                field = klass.getDeclaredField(name);
            }
            catch (NoSuchFieldException e)
            {
            }
            klass = klass.getSuperclass();
            if (klass == null)
            {
                throw new NoSuchFieldError();
            }
        }
        while (field == null);

        if (!field.isAccessible())
        {
            field.setAccessible(true);
        }

        try
        {
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
        }
        catch (NoSuchFieldException e)
        {
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e)
        {
            throw new RuntimeException(e);
        }

        return field;
    }

    public static Map<String, String> buildMap(String... keysAndValues)
    {
        Map<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < keysAndValues.length; )
        {
            map.put(keysAndValues[i], keysAndValues[i + 1]);
            i += 2;
        }
        return map;
    }

    public static List<String> buildList(String... values)
    {
        return Arrays.asList(values);
    }

    public static Content asContent(final String string)
    {
        return new ContentImpl(null, false)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                try
                {
                    out.write(string.getBytes());
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
                return null;
            }
        };
    }

    public static void printShortPaths(Iterable<String> paths)
    {
        for (String path : paths)
        {
            System.out.println(path.replaceAll("/s.*?/_", ""));
        }
    }

    public static void assertUrlsWithoutHashesEquals(String expected, String actual)
    {
        assertEquals(removeSomeHashesFromUrl(expected), removeSomeHashesFromUrl(actual));
    }

    private static String removeSomeHashesFromUrl(String url)
    {
        return url.replaceAll(".*/s/[^-]*-", "").replaceAll("/.*/_/", "/")
            .replaceAll("superbatch/[a-z]+/batch", "normalized-superbatch")
            .replaceAll("contextbatch/[a-z]+/_super/batch", "normalized-superbatch")
            .replaceAll(",-_super", "");
    }

    public static String escapeStringForJava(String string, String indent)
    {
        if (string.equals("")) return indent + "\"\"\n";
        string = indent + "\"" + string + "\"";
        if (string.contains("\n"))
        {
            string = string.replaceAll("\n", "\\\\n\" +\n" + indent + "\"");
            string = string.replaceAll(" \\+\n" + indent + "\"\"$", "");
        }
        return string;
    }

    public static void assertEqualsIgnoringFormatting(String actual, String expected)
    {
        assertEquals(clearContent(expected), clearContent(actual));
    }

    private static String clearContent(String content)
    {
        return content.replaceAll(";", "").replaceAll("/\\*[^\\/]*\\*/", "").replaceAll("\n+", "\n");
    }

    public static String removeWebResourceLogs(String html)
    {
        return Pattern.compile("\\n*<!-- WebResource logs[\\s\\S]*?-->\\n*|\\n*\\Z", Pattern.MULTILINE).matcher(html).replaceAll("");
    }

    public static String removeTrailingSpaces(String str)
    {
        return str.replaceAll("[\\n\\s]*\\Z", "");
    }
}
