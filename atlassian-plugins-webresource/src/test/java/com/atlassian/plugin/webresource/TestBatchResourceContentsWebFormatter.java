package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.assembler.DefaultPluginCssResource;
import com.atlassian.plugin.webresource.assembler.DefaultPluginJsResource;
import com.atlassian.plugin.webresource.assembler.DefaultWebResourceSet;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.TestUtils.buildList;
import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestBatchResourceContentsWebFormatter
{
    @Test
    public void testNoAttributesForSinglePluginResource()
    {
        ResourceUrl resource = mockResourceUrl("abc", "a.b:c", "js", "", buildMap());

        String formattedResource = "<script></script>";

        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginJsResource(resource)), formattedResource);

        assertEquals(formattedResource, modified);
    }

    @Test
    public void testInsertDependencies()
    {
        ResourceUrl resource = mockContextResourceUrl("js", "", buildMap(), buildMap("apple.banana:spoon", "2.3", "spam.spam:spam", "3.4.6-m2"), buildList(), buildList());

        String formattedResource = "<SCRIPT src=\"somewhere\"></SCRIPT>";
        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginJsResource(resource)), formattedResource);

        // We can't guarantee the ordering of the data attribute values hence the slight weird assertions here
        assertThat(modified, startsWith("<SCRIPT "));
        assertThat(modified, endsWith(" src=\"somewhere\"></SCRIPT>"));
        assertThat(modified, containsString(" data-atlassian-webresource-contents="));
        assertThat(modified, containsString("apple.banana:spoon[2.3]"));
        assertThat(modified, containsString("spam.spam:spam[3.4.6-m2]"));
    }

    @Test
    public void testInsertDependenciesAndContexts()
    {
        List<String> includedContexts = ImmutableList.of("contextA", "contextB", "contextC");
        List<String> excludedContexts = ImmutableList.of("excludedcontextA");

        ResourceUrl resource = mockContextResourceUrl("css", "", buildMap(), buildMap("food.fruit:apple", "1"), includedContexts, excludedContexts);

        String formattedResource = "<LINK href=\"somewhere\"></LINK>";
        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginCssResource(resource)), formattedResource);

        // We can't guarantee the ordering of the data attribute values hence the slight weird assertions here
        assertThat(modified, startsWith("<LINK "));
        assertThat(modified, endsWith(" href=\"somewhere\"></LINK>"));
        assertThat(modified, containsString(" data-atlassian-webresource-contents=\"food.fruit:apple[1]\""));
        assertThat(modified, containsString(" data-atlassian-webresource-contexts=\"context"));
        assertThat(modified, containsString("contextA"));
        assertThat(modified, containsString("contextB"));
        assertThat(modified, containsString("contextC"));
        assertThat(modified, containsString(" data-atlassian-webresource-excluded-contexts=\"excludedcontextA\""));
    }

    @Test
    public void testInsertDependenciesWithUnsafeNames()
    {
        ResourceUrl resource = mockWebResourceUrl("js", "", buildMap(), buildMap("apple.banana:\"spork\"", "2.3-\"m\""));

        String formattedResource = "<SCRIPT src=\"somewhere\"></SCRIPT>";
        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginJsResource(resource)), formattedResource);

        // We can't guarantee the ordering of the data attribute values hence the slight weird assertions here
        assertThat(modified, startsWith("<SCRIPT "));
        assertThat(modified, endsWith(" src=\"somewhere\"></SCRIPT>"));
        assertThat(modified, containsString(" data-atlassian-webresource-contents="));
        assertThat(modified, containsString("apple.banana:&quot;spork&quot;[2.3-&quot;m&quot;]"));
    }

    @Test
    public void testOnConditionallyWrapperResources()
    {
        ResourceUrl resource = mockWebResourceUrl("js", "", buildMap(), buildMap("apple.banana:spoon", "2.3"));

        String formattedResource = "<!--[if lte IE 9]>\n<link href=\"something\">\n<!--[endif]-->";
        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginJsResource(resource)), formattedResource);

        assertThat(modified, startsWith("<!--[if lte IE 9]>\n<link "));
        assertThat(modified, endsWith(" href=\"something\">\n<!--[endif]-->"));
        assertThat(modified, containsString(" data-atlassian-webresource-contents=\"apple.banana:spoon[2.3]\""));
    }

    @Test
    public void testBatchPluginResource()
    {
        final String completeKey = "abc.def.ghi:jkl";
        ResourceUrl resource = mockWebResourceUrl("js", completeKey, buildMap(), buildMap(completeKey, "1.3-m4"));

        String formattedResource = "<script src=\"abc\"></script>";
        String modified = BatchResourceContentsWebFormatter.insertBatchResourceContents(
            new DefaultWebResourceSet.Container(resource, new DefaultPluginJsResource(resource)), formattedResource);

        assertThat(modified, startsWith("<script "));
        assertThat(modified, endsWith(" src=\"abc\"></script>"));
        assertThat(modified, containsString("data-atlassian-webresource-contents=\"abc.def.ghi:jkl[1.3-m4]\" "));
    }

        private static void setBaseAttributes(ResourceUrl resourceUrl, String name, String key, String type, String url, Map<String, String> params)
    {
        when(resourceUrl.getName()).thenReturn(name);
        when(resourceUrl.getKey()).thenReturn(key);
        when(resourceUrl.getType()).thenReturn(type);
        when(resourceUrl.getUrl(anyBoolean())).thenReturn(url);
        when(resourceUrl.getParams()).thenReturn(params);
    }

    private static ResourceUrl mockResourceUrl(String name, String key, String type, String url, Map<String, String> params)
    {
        ResourceUrl resourceUrl = mock(ResourceUrl.class);
        setBaseAttributes(resourceUrl, name, key, type, url, params);
        return resourceUrl;
    }

    private static ContextSubBatchResourceUrl mockContextResourceUrl(String type, String url, Map<String, String> params,
        Map<String, String> bundlesKeysAndVersions, List<String> includedContexts, List<String> excludedContexts)
    {
        ContextSubBatchResourceUrl resourceUrl = mock(ContextSubBatchResourceUrl.class);
        setBaseAttributes(resourceUrl, null, null, type, url, params);

        List<Bundle> bundles = new ArrayList<Bundle>();
        for (Map.Entry<String, String> entry : bundlesKeysAndVersions.entrySet())
        {
            Bundle bundle = mock(Bundle.class);
            when(bundle.getKey()).thenReturn(entry.getKey());
            when(bundle.getVersion()).thenReturn(entry.getValue());
            bundles.add(bundle);
        }
        when(resourceUrl.getBatchedBundles()).thenReturn(bundles);
        when(resourceUrl.getIncludedContexts()).thenReturn(includedContexts);
        when(resourceUrl.getExcludedContexts()).thenReturn(new LinkedHashSet<String>(excludedContexts));

        return resourceUrl;
    }

    private static WebResourceSubBatchUrl mockWebResourceUrl(String type, String url, Map<String, String> params,
        Map<String, String> bundleKeyAndVersion)
    {
        WebResourceSubBatchUrl resourceUrl = mock(WebResourceSubBatchUrl.class);
        setBaseAttributes(resourceUrl, null, null, type, url, params);

        for (Map.Entry<String, String> entry : bundleKeyAndVersion.entrySet())
        {
            Bundle bundle = mock(Bundle.class);
            when(bundle.getKey()).thenReturn(entry.getKey());
            when(bundle.getVersion()).thenReturn(entry.getValue());
            when(resourceUrl.getBundle()).thenReturn(bundle);
        }

        return resourceUrl;
    }
}
