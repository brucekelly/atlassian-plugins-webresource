package com.atlassian.plugin.webresource.integration.stub;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.impl.http.Controller;
import com.atlassian.plugin.webresource.impl.http.Router;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;
import com.google.common.base.Supplier;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.plugin.webresource.TestUtils.buildMap;
import static com.atlassian.plugin.webresource.TestUtils.setField;
import static com.atlassian.plugin.webresource.impl.http.Router.parseWithRe;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Support for integration tests. Provides Web Resource environment similar to the real application.
 * <p/>
 * Some thoughts: - Testing API should be kept close to the real public API, so we don't have to learn two APIs. - Black
 * box testing, only public API available, implementation details of Web Resource should be hidden. - Usage of mocks and
 * stubs should be avoided to keep tests simple and don't expose Web Resource internals.
 * <p/>
 * How it works, there are two parts: - Web Resource environment assembled and ready to use without need to setup
 * additional mocks or stubs. - DSL wrapper around it, to make it easier to use, mimic the XML configuration etc.
 * <p/>
 * Code here is a work in progress and not finished, support for missed features would be added over time.
 */
public abstract class WebResource
{
    protected List<String> lastPaths;
    protected Globals globals;

    abstract public void requireResource(String key);

    abstract public void requireContext(String key);

    abstract public void include(List<String> keys);

    abstract public void exclude(List<String> keys);

    public void include(String... keys)
    {
        include(Arrays.asList(keys));
    }

    public void exclude(String... keys)
    {
        exclude(Arrays.asList(keys));
    }

    public abstract void requireData(String key, String value);

    public abstract void reset();

    public ResponseData get(String url, Map<String, String> params, Map<String, String> headers)
    {
        if (params == null)
        {
            params = buildMap();
        }

        // Extracting parameters from url if there's any.
        String[] parts = url.split("\\?");
        String path = parts[0];
        if (parts.length > 1)
        {
            String queryString = parts[1];
            for (String keyValueString : queryString.split("&"))
            {
                String[] keyValue = keyValueString.split("=");
                params.put(keyValue[0], keyValue[1]);
            }
        }

        // Downloading resource.
        Tuple<HttpServletResponse, ResponseData> responseWithData = HttpServletResponseStub.build();
        get(HttpServletRequestStub.build(path, params, headers), responseWithData.getFirst());

        return responseWithData.getLast();
    }

    public ResponseData get(String url, Map<String, String> params)
    {
        return get(url, params, buildMap());
    }

    public ResponseData get(String url)
    {
        return get(url, buildMap(), buildMap());
    }

    public String getContent(String url, Map<String, String> params)
    {
        return get(url, params, buildMap()).getContent();
    }

    public String getContent(String url)
    {
        return getContent(url, null);
    }

    public String getContent()
    {
        return getContent(lastPaths);
    }

    // Get all resources in url list and join it in single string split by newlines.
    public String get(String[] urls)
    {
        StringBuilder buff = new StringBuilder();
        for (String url : urls)
        {
            buff.append(getContent(url) + "\n");
        }
        return buff.toString();
    }

    public String getContent(List<String> urls)
    {
        return get(urls.toArray(new String[urls.size()]));
    }

    public Map<String, String> getHeaders(String url)
    {
        return get(url).getHeaders();
    }

    public int getStatus(String url)
    {
        return get(url).getStatus();
    }

    public Resource getResource(String url, Map<String, String> params)
    {
        Router previousRouter = globals.getRouter();
        final Resource[] servedResource = new Resource[1];
        setField(globals, "router", new Router(globals)
        {
            @Override
            protected Controller createController(Globals globals, Request request, Response response)
            {
                return new Controller(globals, request, response)
                {
                    @Override
                    protected void serveResource(Resource resource, boolean applyTransformations)
                    {
                        servedResource[0] = resource;
                    }
                };
            }
        });
        get(url, params);
        setField(globals, "router", previousRouter);
        return servedResource[0];
    }

    public Resource getResource(String url)
    {
        return getResource(url, buildMap());
    }

    public Collection<Resource> getResources(String url, Map<String, String> params)
    {
        Router previousRouter = globals.getRouter();
        final Supplier<Collection<Resource>>[] servedResources = new Supplier[1];
        setField(globals, "router", new Router(globals)
        {
            @Override
            protected Controller createController(Globals globals, Request request, Response response)
            {
                return new Controller(globals, request, response)
                {
                    @Override
                    protected void serveResources(Supplier<Collection<Resource>> resources)
                    {
                        servedResources[0] = resources;
                        super.serveResources(resources);
                    }
                };
            }
        });
        get(url, params);
        setField(globals, "router", previousRouter);
        return servedResources[0].get();
    }

    public Collection<Resource> getResources(String url)
    {
        return getResources(url, buildMap());
    }

    public Globals getGlobals()
    {
        return globals;
    }

    abstract public void get(HttpServletRequest request, HttpServletResponse response);

    // Returns HTML that should be included in the head of the page to include all the required resources.
    abstract public String pathsAsHtml();

    abstract public WebResourceAssemblerFactory getWebResourceAssemblerFactory();

    public List<String> paths()
    {
        Element root;
        String html = pathsAsHtml();

        // dom4j can't parse HTML because some tags ain't properly closed, fixing it.
        String xml = Pattern.compile("(<link[^<>]+>)", Pattern.DOTALL).matcher(html).replaceAll("$1</link>");

        // it also can't parse conditional comments, removing it.
        xml = Pattern.compile("<!--\\[if[^\\]]*\\]>", Pattern.DOTALL).matcher(xml).replaceAll("");
        xml = Pattern.compile("<!\\[endif\\]-->", Pattern.DOTALL).matcher(xml).replaceAll("");

        try
        {
            root = DocumentHelper.parseText("<root>" + xml + "</root>").getRootElement();
        }
        catch (DocumentException e)
        {
            throw new RuntimeException(e);
        }
        List<String> paths = new ArrayList<String>();
        for (Object elementObject : root.elements())
        {
            Element element = (Element) elementObject;
            if ("script".equals(element.getName().toLowerCase()))
            {
                paths.add(element.attribute("src").getValue());
            }
            else if (element.getName().toLowerCase().equals("link") && element.attribute("type").getValue()
                    .toLowerCase().equals("text/css"))
            {
                paths.add(element.attribute("href").getValue());
            }
        }
        lastPaths = paths;
        return paths;
    }

    // Path helpers, for easy path generation in tests.
    public static class PathHelpers
    {
        private static Router createRouter()
        {
            Config config = mock(Config.class);
            when(config.getBaseUrl()).thenReturn("");
            when(config.getBaseUrl(anyBoolean())).thenReturn("");
            Globals globals = mock(Globals.class);
            when(globals.getConfig()).thenReturn(config);
            return new Router(globals);
        }

        public static String pluginResourceUrl(String pluginKey, String resourceName)
        {
            return createRouter().pluginResourceUrl(pluginKey, resourceName, buildMap(), false, false, null, null);
        }

        public static String webResourceBatchUrl(String completeKey, String type, Map<String, String> params)
        {
            return createRouter().webResourceBatchUrl(completeKey, type, params, false, false, null, null);
        }

        public static String webResourceBatchUrl(String completeKey, String type)
        {
            return webResourceBatchUrl(completeKey, type, buildMap());
        }

        public static String resourceUrlRelativeToWebResourceBatch(String completeKey, String resourceName)
        {
            return createRouter().resourceUrlRelativeToWebResourceBatch(completeKey, resourceName, buildMap(), false,
                    false, null, null);
        }

        public static String contextBatchUrl(String contextKey, String type)
        {
            return contextBatchUrl(contextKey, type, buildMap());
        }

        public static String superBatchUrl(String type)
        {
            return contextBatchUrl("_super", type, buildMap());
        }

        public static String contextBatchUrl(String contextKey, String type, Map<String, String> params)
        {
            return createRouter().contextBatchUrl(contextKey, type, params, false, false, null, null);
        }

        public static String contextBatchSourceMapUrl(String contextKey, String type)
        {
            return createRouter().contextBatchSourceMapUrl(contextKey, type, buildMap(), false, false, null, null);
        }

        public static String resourceUrlRelativeToContextBatch(String contextKey, String type, String resourceName)
        {
            return createRouter().resourceUrlRelativeToContextBatch(contextKey, type, resourceName, buildMap(),
                    false, false, null, null);
        }

        public static String resourceUrl(String completeKey, String resourceName, Map<String, String> params)
        {
            return createRouter().resourceUrl(completeKey, resourceName, params, false, false, null, null);
        }

        public static String resourceUrl(String completeKey, String resourceName)
        {
            return resourceUrl(completeKey, resourceName, buildMap());
        }

        public static String resourceSourceMapUrl(String completeKey, String resourceName)
        {
            return createRouter().resourceSourceMapUrl(completeKey, resourceName, buildMap(), false, false, null, null);
        }

        public static String sourceUrl(String completeKey, String resourceName)
        {
            return createRouter().sourceUrl(completeKey, resourceName, buildMap(), false, false, null, null);
        }

        public static String sourceMapUrl(String resourceUrl)
        {
            return createRouter().sourceMapUrl(resourceUrl, buildMap());
        }

        public static String parseSourceMapUrl(String content)
        {
            List<String> matches = parseWithRe(content, "(//|/\\*)# sourceMappingURL\\=(.+)(\\s*\\*/)?");
            return matches.size() > 0 ? matches.get(1) : null;
        }
    }

    // Configure web resources using XML DSL.
    public ConfigurationDsl configure()
    {
        return new ConfigurationDsl(this);
    }

    // Configure web resources according to given XML configuration.
    abstract public void applyConfiguration(ConfigurationData configurationData);

    // Configuration Data.
    public static class ConfigurationData
    {
        public List<PluginData> plugins = new ArrayList<PluginData>();
        public List<String> webResourcesToAddToSuperbatch = new ArrayList<String>();
        public boolean addEverythingToSuperbatch = false;
        public StaticTransformers staticTransformers;
        public ResourceContentAnnotator[] annotators;
        public boolean isSourceMapEnabled = false;
        public boolean isJavaScriptTryCatchWrappingEnabled = false;
        public Boolean isBatchingEnabled = true;
        public boolean isSuperBatchEnabled = true;
        public boolean isMinificationEnabled = false;
        public boolean optimiseSourceMapForDevelopment;

        public String toXml()
        {
            StringBuilder buff = new StringBuilder();
            buff.append("<web-resource-configuration");
            if (webResourcesToAddToSuperbatch.size() > 0)
            {
                buff.append(" superBatchKeys=\"" + StringUtils.join(webResourcesToAddToSuperbatch, ", ") + "\"");
            }
            buff.append(">\n");
            for (PluginData plugin : plugins)
            {
                buff.append(plugin.toXml("    ") + "\n");
            }
            buff.append("</web-resource-configuration>");
            return buff.toString();
        }

        public String toString(String indent)
        {
            StringBuilder buff = new StringBuilder();
            buff.append(indent + "wr.configure()\n");
            if (webResourcesToAddToSuperbatch.size() > 0)
            {
                buff.append(indent + "    .addToSuperbatch(\"" + StringUtils.join(webResourcesToAddToSuperbatch, "\", \"") + "\")\n");
            }
            for (PluginData plugin : plugins)
            {
                buff.append(plugin.toString(indent + "    ") + "\n");
            }
            buff.append(indent + ".end();");
            return buff.toString();
        }

        @Override
        public String toString()
        {
            return toString("");
        }
    }

    public static class PluginData
    {
        public String key;
        public List<WebResourceData> webResources = new ArrayList<WebResourceData>();
        public Map<String, Tuple<String, Class>> transformers = new LinkedHashMap<String, Tuple<String, Class>>();
        public List<ResourceData> resources = new ArrayList<ResourceData>();

        public String toXml(String indent)
        {
            StringBuilder buff = new StringBuilder();
            buff.append(indent + "<atlassian-plugin key=\"" + key + "\">\n");
            for (Map.Entry<String, Tuple<String, Class>> entry : transformers.entrySet())
            {
                buff.append(indent + "    <transformer key=\"" + entry.getKey() + "\" class=\""
                    + entry.getValue().getLast().getName() + "\"/>\n");
            }
            for (WebResourceData webResource : webResources)
            {
                buff.append(webResource.toXml(indent + "    ") + "\n");
            }
            buff.append(indent + "</atlassian-plugin>");
            return buff.toString();
        }

        public String toString(String indent)
        {
            StringBuilder buff = new StringBuilder();
            buff.append(indent + ".plugin(\"" + key + "\")\n");
            for (Map.Entry<String, Tuple<String, Class>> entry : transformers.entrySet())
            {
                buff.append(indent + "    .transformer(\"" + entry.getKey() + "\", "
                    + entry.getValue().getLast().getSimpleName() + ".class)\n");
            }
            for (WebResourceData webResource : webResources)
            {
                buff.append(webResource.toString(indent + "    ") + "\n");
            }
            return buff.toString().replaceAll("\n+$", "");
        }

        public String toString()
        {
            return toString("");
        }
    }

    public static class WebResourceData
    {
        public List<Class> conditions = new ArrayList<Class>();
        public String key;
        public List<String> dependencies = new ArrayList<String>();
        public List<ResourceData> resources = new ArrayList<ResourceData>();
        public List<String> contexts = new ArrayList<String>();
        public Map<String, List<String>> transformations = new LinkedHashMap<String, List<String>>();
        List<FileData> files = new ArrayList<FileData>();
        public List<Tuple<String, Class<?>>> dataProviders = new ArrayList<Tuple<String, Class<?>>>();

        public String toXml(String indent)
        {
            StringBuilder buff = new StringBuilder();
            buff.append(indent + "<web-resource key=\"" + key + "\">\n");
            for (Class klass : conditions)
            {
                buff.append(indent + "    <condition class=\"" + klass.getName() + "\"/>\n");
            }
            for (String dependency : dependencies)
            {
                buff.append(indent + "    <dependency key=\"" + dependency + "\"/>\n");
            }
            for (String context : contexts)
            {
                buff.append(indent + "    <context key=\"" + context + "\"/>\n");
            }
            for (Map.Entry<String, List<String>> entry : transformations.entrySet())
            {
                for (String transformerKey : entry.getValue())
                {
                    buff.append(indent + "    <transformation extension=\"" + entry.getKey() + "\" transformer=\""
                        + transformerKey + "\"/>\n");
                }
            }
            for (ResourceData resource : resources)
            {
                buff.append(resource.toXml(indent + "  "));
            }
            buff.append(indent + "</web-resource>");
            return buff.toString();
        }

        public String toString(String indent)
        {
            StringBuilder buff = new StringBuilder();
            buff.append(indent + ".webResource(\"" + key + "\")\n");
            for (String dependency : dependencies)
            {
                buff.append(indent + "    .dependency(\"" + dependency + "\")\n");
            }
            for (String context : contexts)
            {
                buff.append(indent + "    .context(\"" + context + "\")\n");
            }
            for (Class klass : conditions)
            {
                buff.append(indent + "    .condition(" + klass.getSimpleName() + ".class)\n");
            }
            for (Map.Entry<String, List<String>> entry : transformations.entrySet())
            {
                for (String transformerKey : entry.getValue())
                {
                    buff.append(indent + "    .transformation(\"" + entry.getKey() + "\", \""
                        + transformerKey + "\")\n");
                }
            }
            for (ResourceData resource : resources)
            {
                buff.append(resource.toString(indent + "    ") + "\n");
            }
            return buff.toString().replaceAll("\n+$", "");
        }

        public String toString()
        {
            return toString("");
        }
    }

    static class FileData
    {
        String name;
        String content;

        FileData(final String name, final String content)
        {
            this.name = name;
            this.content = content;
        }

        public String toString(String indent)
        {
            return indent + "<minified-resource name=\"" + name + "\">" + content + "</resource>\n";
        }

        public String toString()
        {
            return toString("");
        }
    }

    public static class ResourceData
    {
        public String name;
        public String location;
        public String content;
        public String contentType;
        public Map<String, String> params = new LinkedHashMap<String, String>();
        public String filePath;

        public String toXml(String indent)
        {
            if (params.size() > 0)
            {
                StringBuilder buff = new StringBuilder();
                buff.append(indent + "<resource name=\"" + name + "\" location=\"" + location + "\">\n");
                for (Map.Entry<String, String> entry : params.entrySet())
                {
                    buff.append(indent + "    <param key=\"" + entry.getKey() + "\" value=\""
                        + entry.getValue() + "\"/>\n");
                }
                buff.append(indent + "    <content>" + content + "</content>\n");
                buff.append(indent + "</resource>\n");
                return buff.toString();
            }
            else
            {
                return indent + "<resource name=\"" + name + "\" location=\"" + location + "\">" +
                    content + "</resource>\n";
            }
        }

        public String toString(String indent)
        {
            if (params.size() > 0)
            {
                StringBuilder buff = new StringBuilder();
                buff.append(indent + ".resource(\"" + location + "\", \"" + content + "\")\n");
                for (Map.Entry<String, String> entry : params.entrySet())
                {
                    buff.append(indent + "    .param(\"" + entry.getKey() + "\", \""
                        + entry.getValue() + "\")\n");
                }
                return buff.toString().replaceAll("\n+$", "");
            }
            else
            {
                return indent + ".resource(\"" + location + "\", \"" + content + "\")";
            }
        }

        public String toString()
        {
            return toString("");
        }
    }

    // Configuration DSL.
    public static class ConfigurationDsl
    {
        private final ConfigurationData data = new ConfigurationData();
        private final WebResource wr;

        public ConfigurationDsl(WebResource wr)
        {
            this.wr = wr;
        }

        public PluginDsl plugin(String key)
        {
            return new PluginDsl(this, key);
        }

        public void end()
        {
            wr.applyConfiguration(data);
        }

        public ConfigurationDsl enableMinification()
        {
            data.isMinificationEnabled = true;
            return this;
        }

        public ConfigurationDsl addToSuperbatch(String... keys)
        {
            data.webResourcesToAddToSuperbatch.addAll(Arrays.asList(keys));
            return this;
        }

        public ConfigurationDsl addEverythingToSuperbatch()
        {
            data.addEverythingToSuperbatch = true;
            return this;
        }

        public ConfigurationDsl setStaticTransformers(StaticTransformers staticTransformers)
        {
            data.staticTransformers = staticTransformers;
            return this;
        }

        public ConfigurationDsl setAnnotators(ResourceContentAnnotator[] annotators)
        {
            data.annotators = annotators;
            return this;
        }

        public ConfigurationDsl setOptimiseSourceMapForDevelopment(boolean enabled)
        {
            data.optimiseSourceMapForDevelopment = enabled;
            return this;
        }

        public ConfigurationDsl enableSourceMap()
        {
            data.isSourceMapEnabled = true;
            return this;
        }

        public ConfigurationDsl disableBatching()
        {
            data.isBatchingEnabled = false;
            return this;
        }

        public ConfigurationDsl enableJavaScriptTryCatchWrapping()
        {
            data.isJavaScriptTryCatchWrappingEnabled = true;
            return this;
        }

        public ConfigurationDsl disableJavaScriptTryCatchWrapping()
        {
            data.isJavaScriptTryCatchWrappingEnabled = false;
            return this;
        }

        public ConfigurationDsl disableSuperBatch()
        {
            data.isSuperBatchEnabled = false;
            return this;
        }
    }

    public static class PluginDsl
    {
        private boolean submitted = false;
        private final ConfigurationDsl configurationDsl;
        private final PluginData data = new PluginData();

        public PluginDsl(ConfigurationDsl configurationDsl, String key)
        {
            this.configurationDsl = configurationDsl;
            this.data.key = key;
        }

        public WebResourceDsl webResource(String key)
        {
            return new WebResourceDsl(this, key);
        }

        public PluginDsl transformer(String key, Class klass)
        {
            return transformer(key, null, klass);
        }

        public PluginDsl transformer(String key, String alias, Class klass)
        {
            if (data.transformers.containsKey(key))
            {
                throw new RuntimeException("transformer with such key already registered!");
            }
            data.transformers.put(key, new Tuple<String, Class>(alias, klass));
            return this;
        }

        public void end()
        {
            submit();
            configurationDsl.end();
        }

        private void submit()
        {
            if (submitted)
            {
                return;
            }
            submitted = true;
            configurationDsl.data.plugins.add(this.data);
        }

        public ConfigurationDsl addToSuperbatch(String... keys)
        {
            submit();
            return configurationDsl.addToSuperbatch(keys);
        }

        public ConfigurationDsl addEverythingToSuperbatch()
        {
            submit();
            return configurationDsl.addEverythingToSuperbatch();
        }

        public PluginDsl plugin(String key)
        {
            submit();
            return configurationDsl.plugin(key);
        }

        public ResourceDsl resource(String name)
        {
            return resource(name, "content of " + name);
        }

        public ResourceDsl resource(String name, String content)
        {
            return resource(name, content, name);
        }

        public ResourceDsl resource(String name, String content, String location)
        {
            return resource(name, content, location, "");
        }

        public ResourceDsl resource(String name, String content, String location, String filePath)
        {
            return new ResourceDsl(this, name, content, location, filePath);
        }

        public static class ResourceDsl
        {
            private boolean submitted = false;
            private PluginDsl pluginDsl;
            private final ResourceData data = new ResourceData();

            public ResourceDsl(PluginDsl pluginDsl, String name, String content, String location, String filePath)
            {
                this.pluginDsl = pluginDsl;
                data.name = name;
                data.content = content;
                data.location = location;
                data.filePath = filePath;
            }

            public ResourceDsl resource(String key, String content)
            {
                submit();
                return pluginDsl.resource(key, content);
            }

            public ResourceDsl resource(String key)
            {
                submit();
                return pluginDsl.resource(key);
            }

            private void submit()
            {
                if (submitted)
                {
                    return;
                }
                submitted = true;
                pluginDsl.data.resources.add(this.data);
            }

            public WebResourceDsl webResource(String key)
            {
                submit();
                return pluginDsl.webResource(key);
            }

            public PluginDsl plugin(String key)
            {
                submit();
                return pluginDsl.plugin(key);
            }

            public void end()
            {
                submit();
                pluginDsl.end();
            }
        }
    }

    public static class WebResourceDsl
    {
        private boolean submitted = false;
        private final PluginDsl pluginDsl;
        private final WebResourceData data = new WebResourceData();

        public WebResourceDsl(PluginDsl pluginDsl, String key)
        {
            this.pluginDsl = pluginDsl;
            this.data.key = key;
        }

        public ResourceDsl resource(String location)
        {
            return resource(location, "content of " + location);
        }

        public ResourceDsl resource(String location, String content)
        {
            return resource(location, content, location);
        }

        public WebResourceDsl file(String name, String content)
        {
            data.files.add(new FileData(name, content));
            return this;
        }

        public ResourceDsl resource(String location, String content, String filePath)
        {
            return resource(location, content, filePath, "");
        }

        public ResourceDsl resource(String location, String content, String filePath, String extraFilePath)
        {
            return new ResourceDsl(this, location, content, filePath, extraFilePath);
        }

        public WebResourceDsl condition(Class klass)
        {
            this.data.conditions.add(klass);
            return this;
        }

        private void submit()
        {
            if (submitted)
            {
                return;
            }
            submitted = true;
            pluginDsl.data.webResources.add(this.data);
        }

        public void end()
        {
            submit();
            pluginDsl.end();
        }

        public WebResourceDsl dependency(String key)
        {
            data.dependencies.add(key);
            return this;
        }

        public WebResourceDsl context(String key)
        {
            data.contexts.add(key);
            return this;
        }

        public WebResourceDsl transformation(String extension, String... transformerKeys)
        {
            if (!data.transformations.containsKey(extension))
            {
                data.transformations.put(extension, new ArrayList<String>());
            }
            List<String> transformers = data.transformations.get(extension);
            transformers.addAll(Arrays.asList(transformerKeys));
            return this;
        }

        public WebResourceDsl webResource(String key)
        {
            submit();
            return pluginDsl.webResource(key);
        }

        public ConfigurationDsl addToSuperbatch(String... keys)
        {
            submit();
            return pluginDsl.addToSuperbatch(keys);
        }

        public ConfigurationDsl addEverythingToSuperbatch()
        {
            submit();
            return pluginDsl.addEverythingToSuperbatch();
        }

        public PluginDsl plugin(String key)
        {
            submit();
            return pluginDsl.plugin(key);
        }

        public WebResourceDsl data(String key, Class<?> value)
        {
            data.dataProviders.add(new Tuple<String, Class<?>>(key, value));
            return this;
        }

        public static class ResourceDsl
        {
            private boolean submitted = false;
            private WebResourceDsl webResourceDsl;
            private final ResourceData data = new ResourceData();

            public ResourceDsl(WebResourceDsl webResourceDsl, String name, String content, String location,
                    String filePath)
            {
                this.webResourceDsl = webResourceDsl;
                data.name = name;
                data.content = content;
                data.location = location;
                data.filePath = filePath;
            }

            public ResourceDsl resource(String key, String content)
            {
                submit();
                return webResourceDsl.resource(key, content);
            }

            public ResourceDsl resource(String key)
            {
                submit();
                return webResourceDsl.resource(key);
            }

            public ResourceDsl contentType(String contentType)
            {
                data.contentType = contentType;
                return this;
            }

            public ResourceDsl param(String key, String value)
            {
                data.params.put(key, value);
                return this;
            }

            private void submit()
            {
                if (submitted)
                {
                    return;
                }
                submitted = true;
                webResourceDsl.data.resources.add(this.data);
            }

            public WebResourceDsl webResource(String key)
            {
                submit();
                return webResourceDsl.webResource(key);
            }

            public void end()
            {
                submit();
                webResourceDsl.end();
            }

            public PluginDsl plugin(String key)
            {
                submit();
                return webResourceDsl.plugin(key);
            }

            public ConfigurationDsl addToSuperbatch(String... keys)
            {
                submit();
                return webResourceDsl.addToSuperbatch(keys);
            }

            public ConfigurationDsl addEverythingToSuperbatch()
            {
                submit();
                return webResourceDsl.addEverythingToSuperbatch();
            }

            public WebResourceDsl file(String name, String content)
            {
                submit();
                return webResourceDsl.file(name, content);
            }
        }
    }
}