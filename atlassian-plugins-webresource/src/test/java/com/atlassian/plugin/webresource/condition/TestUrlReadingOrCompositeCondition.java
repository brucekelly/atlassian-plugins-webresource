package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.AlwaysFalseCondition;
import com.atlassian.plugin.webresource.AlwaysTrueCondition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.plugin.webresource.TestUtils.emptyContext;
import static com.atlassian.plugin.webresource.TestUtils.emptyQueryParams;
import static com.atlassian.plugin.webresource.condition.ConditionTestUtils.decorate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestUrlReadingOrCompositeCondition
{
    @Test
    public void testPasses()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testFails()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        condition.addToUrl(urlBuilder);
        assertFalse(condition.shouldDisplay(QueryParams.of(urlBuilder.buildParams())));
    }

    @Test
    public void testImmediatePasses()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testImmediateFails()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertFalse(condition.shouldDisplayImmediate(emptyContext()));
    }

    @Test
    public void testCombinationPass1()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testCombinationPass2()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysTrueCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    public void testCombinationPass3()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysTrueLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertTrue(condition.shouldDisplay(emptyQueryParams()));
    }

    @Test
    @Ignore("PLUGWEB-53 ORed conditions with both url-reading and legacy conditions always pass")
    public void testCombinationFail()
    {
        DecoratingCompositeCondition condition = new DecoratingOrCompositeCondition();
        condition.addCondition(decorate(new AlwaysFalseCondition()));
        condition.addCondition(decorate(new AlwaysFalseLegacyCondition()));
        assertTrue(condition.shouldDisplayImmediate(emptyContext()));
        assertFalse(condition.shouldDisplay(emptyQueryParams()));
    }
}
