package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestDefaultStaticTransformersSupplier
{
    @Mock
    private WebResourceIntegration webResourceIntegration;
    @Mock
    private WebResourceUrlProvider webResourceUrlProvider;

    private StaticTransformersSupplier staticTransformersSupplier;

    @Before
    public void setup()
    {
        staticTransformersSupplier = new DefaultStaticTransformersSupplier(webResourceIntegration, webResourceUrlProvider);
    }

    @Test
    public void testCssType()
    {
        ArrayList<ContentTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get("css"));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testCssLocation()
    {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.css", "blah.css");
        ArrayList<ContentTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    @Test
    public void testLessLocation()
    {
        ResourceLocation resourceLocation = createResourceLocation("my/path/blah.less", "blah.css");
        ArrayList<ContentTransformerFactory> transformers = Lists.newArrayList(staticTransformersSupplier.get(resourceLocation));
        assertEquals(1, transformers.size());
        assertTrue(transformers.get(0) instanceof RelativeUrlTransformerFactory);
    }

    private static ResourceLocation createResourceLocation(String location, String name)
    {
        return new ResourceLocation(location, name, null, null, null, Collections.<String, String>emptyMap());
    }
}
