package com.atlassian.plugin.webresource.util;

import org.apache.commons.lang.StringUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class UrlCdnMatcher extends BaseMatcher
{
    @Override
    public void describeTo(final Description description)
    {
        description.appendText("has CDN");
    }

    @Override
    public boolean matches(Object actual)
    {        
        return (actual != null) && actual.toString().contains("-CDN/");
    }
    
    public static UrlCdnMatcher hasCdn()
    {
        return new UrlCdnMatcher();
    }
}