package com.atlassian.plugin.webresource.integration;

import org.junit.Test;

import static com.atlassian.plugin.webresource.integration.stub.WebResource.PathHelpers.resourceUrl;
import static com.atlassian.plugin.webresource.util.ObjectMatcher.matches;
import static org.junit.Assert.assertThat;

public class TestMinificationTransformer extends TestCase
{
    @Test
    public void shouldUseMinifiedVersions()
    {
        wr.configure()
            .plugin("plugin")
                .webResource("a")
                    .resource("a1.js")
                    .file("a1-min.js", "content of a1-min.js")
                    .resource("a2.js")
                    .file("a2.min.js", "content of a2.min.js")
        .end();

        assertThat(wr.getContent(resourceUrl("plugin:a", "a1.js")), matches(
           "content of a1-min.js"
        ));
        assertThat(wr.getContent(resourceUrl("plugin:a", "a2.js")), matches(
           "content of a2.min.js"
        ));
    }
}
