package com.atlassian.plugin.webresource.impl.config;

/**
 * Types of batch.
 *
 * @since v3.4
 */
public enum BatchType
{
    css("css"), js("js");

    private final String name;

    BatchType(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return super.toString();
    }
}