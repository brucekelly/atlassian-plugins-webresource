package com.atlassian.plugin.webresource.data;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.webresource.util.PluginClassLoader;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.dom4j.Element;

import javax.annotation.Nullable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Parses &lt;data&gt; elements into {@link WebResourceDataProvider} instances
 * @since v3.0
 */
public class WebResourceDataProviderParser
{
    private final HostContainer hostContainer;
    private final List<KeyedDataProvider> keyedDataProviders;

    public WebResourceDataProviderParser(HostContainer hostContainer, List<Element> dataElements)
    {
        this.hostContainer = hostContainer;
        this.keyedDataProviders = ImmutableList.copyOf(Lists.transform(dataElements, new Function<Element, KeyedDataProvider>() {
            @Override
            public KeyedDataProvider apply(@Nullable Element e) {
                return new KeyedDataProvider(e);
            }
        }));
    }

    public Map<String, WebResourceDataProvider> createDataProviders(Plugin plugin, Class<?> callingClass)
            throws ClassNotFoundException, PluginParseException
    {
        Map<String, WebResourceDataProvider> dps = new LinkedHashMap<String, WebResourceDataProvider>(keyedDataProviders.size());
        for (KeyedDataProvider dataKey : keyedDataProviders)
        {
            dps.put(dataKey.getKey(), createDataProvider(plugin, callingClass, dataKey.getClassName()));
        }
        return ImmutableMap.copyOf(dps);
    }

    private WebResourceDataProvider createDataProvider(Plugin plugin, Class<?> callingClass, String className)
            throws ClassNotFoundException
    {
        return PluginClassLoader.create(plugin, callingClass, hostContainer, className);
    }
}
