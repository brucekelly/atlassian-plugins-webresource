package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.SourceMapJoiner;

/**
 * Stub for SourceMapJoiner, needed to replace it when Source Map generation not needed.
 */
public class SourceMapJoinerStub extends SourceMapJoiner
{
    @Override
    public void add(SourceMap sourceMap, int length)
    {
    }

    @Override
    public void add(SourceMap sourceMap, int length, int offset)
    {
    }

    @Override
    public SourceMap join()
    {
        return null;
    }
}
