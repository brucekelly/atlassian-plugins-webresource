package com.atlassian.plugin.webresource.impl.support.http;

import com.atlassian.plugin.webresource.impl.Globals;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Generates urls and route requests to proper handlers.
 * <p/>
 * Contains basic support for url generation and recognition.
 *
 * @since 3.3
 */
public abstract class BaseRouter<Controller extends BaseController>
{
    /**
     * Handler for routes.
     */
    public abstract class Handler
    {
        /**
         * Would be called for route if the route has zero parameters.
         */
        public void apply(Controller controller)
        {
        }

        /**
         * Would be called for route if the route has one parameters.
         */
        public void apply(Controller controller, String arg)
        {
        }

        /**
         * Would be called for route if the route has two parameters.
         */
        public void apply(Controller controller, String arg1, String arg2)
        {
        }

        /**
         * Would be called for route if the route has three parameters.
         */
        public void apply(Controller controller, String arg1, String arg2, String arg3)
        {
        }

        /**
         * Always called, used in case when route have more than three arguments or need access to request.
         */
        public void apply(Request request, Response response, Controller controller, String[] arguments)
        {
        }
    }

    /**
     * Override this method and provide instance of the controller.
     */
    protected abstract Controller createController(Globals globals, Request request, Response response);

    /**
     * Structure containing regular expression and handler for the route.
     */
    protected class Route
    {
        Pattern routeRe;
        Handler handler;

        public Route(Pattern routeRe, Handler handler)
        {
            this.routeRe = routeRe;
            this.handler = handler;
        }
    }

    final protected Globals globals;
    final protected List<Route> routes;
    final protected boolean useAbsoluteUrl;

    public BaseRouter(Globals globals)
    {
        this.globals = globals;
        routes = new ArrayList<Route>();
        // By default relative urls used.
        useAbsoluteUrl = false;
    }

    /**
     * Needed to support legacy API.
     */
    protected BaseRouter(Globals globals, List<Route> routes, boolean useAbsoluteUrl)
    {
        this.globals = globals;
        this.routes = routes;
        this.useAbsoluteUrl = useAbsoluteUrl;
    }

    /**
     * Add route and its handler.
     *
     * @param route use same rules as in Backbone.js to define the route.
     */
    public void addRoute(String route, Handler handler)
    {
        routes.add(new Route(routeToRe(route), handler));
    }

    /**
     * Deprecated, use dispatch instead.
     *
     * @deprecated since 3.3.2
     */
    @Deprecated
    public boolean canDispatch(String path)
    {
        for (Route route : routes)
        {
            if (route.routeRe.matcher(path).find())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Dispatches request to proper handlers.
     */
    public void dispatch(Request request, Response response)
    {
        for (Route route : routes)
        {
            Matcher matcher = route.routeRe.matcher(request.getPath());
            if (matcher.find())
            {
                List<String> matches = new ArrayList<String>();
                for (int i = 1; i <= matcher.groupCount(); i++)
                {
                    matches.add(matcher.group(i));
                }

                String args[] = matches.toArray(new String[matches.size()]);

                Controller controller = createController(globals, request, response);
                controller.before();
                callHandler(route.handler, controller, request, response, args);
                return;
            }
        }
        throw new RuntimeException("no route for " + request.getPath());
    }

    protected void callHandler(Handler handler, Controller controller, Request request, Response response,
            String... args)
    {
        if (args.length == 0)
        {
            handler.apply(controller);
        }
        else if (args.length == 1)
        {
            handler.apply(controller, args[0]);
        }
        else if (args.length == 2)
        {
            handler.apply(controller, args[0], args[1]);
        }
        else if (args.length == 3)
        {
            handler.apply(controller, args[0], args[1], args[2]);
        }
        handler.apply(request, response, controller, args);
    }

    /**
     * Build url from patch and params.
     */
    public static String buildUrl(String path, Map<String, String> params)
    {
        try
        {
            StringBuilder buff = new StringBuilder();
            buff.append(path);
            boolean isFirst = true;
            // Ensuring params are traversed in sorted manner and generated url is always the same.
            params = new TreeMap<String, String>(params);
            for (Map.Entry<String, String> entry : params.entrySet())
            {
                if (entry.getKey().isEmpty() || entry.getValue().isEmpty())
                {
                    continue;
                }
                if (isFirst)
                {
                    buff.append("?");
                    isFirst = false;
                }
                else
                {
                    buff.append("&");
                }
                buff.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                buff.append("=");
                buff.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
            return buff.toString();
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Shortcut for `buildUrl(path, params)`.
     */
    public static String buildUrl(String path)
    {
        return buildUrl(path, new HashMap<String, String>());
    }

    /**
     * Parses string with regular expressions and returns list of results.
     *
     * @return list of found matches
     */
    public static List<String> parseWithRe(String string, String re)
    {
        Pattern pattern = Pattern.compile(re);
        Matcher matcher = pattern.matcher(string);
        List<String> results = new ArrayList<String>();
        if (matcher.find())
        {
            for (int i = 1; i <= matcher.groupCount(); i++)
            {
                results.add(matcher.group(i));
            }
        }
        return results;
    }

    /**
     * Convert string expression of the route like `/posts/:id/show` into the regular expression pattern, it uses
     * Backbone.js rules for route expression.
     */
    public static Pattern routeToRe(String route)
    {
        String optionalParam = "\\((.*?)\\)";
        String namedParam = "(\\(\\?)?:\\w+";
        String splatParam = "\\*\\w+";
        String escapeRegExp = "[\\-{}\\[\\]+?.,\\\\\\^$|#\\s]";

        String routeRe = ".*" + route.replaceAll(escapeRegExp, "\\\\$0").replaceAll(optionalParam,
                "(?:$1)?").replaceAll(namedParam, "([^/?]+)").replaceAll(splatParam, "([^?]*?)") + "$";

        return Pattern.compile(routeRe);
    }

    /**
     * Same as `String.interpolate` but allows to use named parts to improve readability, like
     * `interpolate("/posts/:id", id)`.
     */
    public static String interpolate(String string, String... args)
    {
        return String.format(string.replaceAll(":[A-Za-z0-9]+", "%s"), args);
    }

    /**
     * Builds path by joining strings with slashes, skips strings that are null.
     */
    public static String joinWithSlashWithoutEmpty(String... parts)
    {
        StringBuilder b = new StringBuilder();
        boolean isFirst = true;
        for (int i = 0; i < parts.length; i++)
        {
            if (!StringUtils.isEmpty(parts[i]))
            {
                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    b.append("/");
                }
                b.append(parts[i]);
            }
            // If the first part is empty adding leading slash.
            else if (isFirst)
            {
                b.append("/");
            }
        }
        return b.toString();
    }
}
