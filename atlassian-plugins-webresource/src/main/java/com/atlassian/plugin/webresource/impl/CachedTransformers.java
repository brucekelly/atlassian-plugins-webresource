package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.List;

/**
 * Wrapper around transformer to cache it.
 *
 * @since v3.3.2
 */
public class CachedTransformers
{
    private final List<WebResourceTransformation> transformations;

    public CachedTransformers(List<WebResourceTransformation> transformations)
    {
        this.transformations = transformations;
    }


    /**
     * Add transformer parameters to url. If error wil be thrown it will be intercepted and ignored.
     * @return if there are legacy transformers.
     */
    public boolean addToUrlSafely(UrlBuilder urlBuilder, String type, TransformerCache transformerCache, TransformerParameters transformerParameters)
    {
        boolean containsLegacyTransformers = false;
        for (WebResourceTransformation transformation : transformations)
        {
            if (transformation.matches(type))
            {
                try
                {
                    transformation.addTransformParameters(transformerCache, transformerParameters, urlBuilder);
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("error thrown in transformer during url generation", e);
                }
                if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache))
                {
                    containsLegacyTransformers = true;
                }
            }
        }
        return containsLegacyTransformers;
    }

    public List<WebResourceTransformation> getTransformations()
    {
        return transformations;
    }
}
