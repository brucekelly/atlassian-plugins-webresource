package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.http.Router.encodeContexts;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;

/**
 * An intermediary object used for constructing and merging context batches.
 * This is a bean that holds the different resources and parameters that apply
 * to a particular batch.
 * The batch can both include and exclude one or more contexts
 * Resources are expected to be in dependency order, with no duplicates.
 */
public class ContextBatch
{
    private static final String UTF8 = "UTF-8";
    private static final String MD5 = "MD5";
    private static final Ordering<ModuleDescriptor<?>> MODULE_KEY_ORDERING = Ordering.natural().onResultOf(new TransformDescriptorToKey());

    private final List<String> contexts;
    private final Iterable<String> excludedContexts;
    private final Iterable<WebResourceModuleDescriptor> resources;
    private final Iterable<String> resourceKeys;
    private final boolean removeSuperResources;
    
    /**
     *
     * @param contexts the ordering of contexts is important since it determines the ordering of resources within a batch (which could be
     * important for badly written Javascripts).
     * @param excludedContexts the ordering of excluded contexts is not important.
     * @param resources
     */
    public ContextBatch(final List<String> contexts, Iterable<String> excludedContexts,
        final Iterable<WebResourceModuleDescriptor> resources, boolean removeSuperResources)
    {
        this.contexts = copyOf(contexts);
        if (excludedContexts == null)
        {
            this.excludedContexts = Collections.emptyList();
        }
        else
        {
            this.excludedContexts = copyOf(excludedContexts);
        }


        // Note: Ordering is important in producing a consistent hash.
        // But, dependency order is not important when producing the PluginResource,
        // it is only important when we serve the resource. So it is safe to reorder this.
        this.resources = ImmutableSortedSet.copyOf(MODULE_KEY_ORDERING, resources);
        // A convenience object to make searching easier
        this.resourceKeys =  transform(resources, new TransformDescriptorToKey());
        this.removeSuperResources = removeSuperResources;
    }

    public boolean isRemoveSuperResources() {
        return removeSuperResources;
    }

    /**
     * Iterates over the batch parameters for the context ({@link PluginResourceBatchParams}) and
     * creates a {@link com.atlassian.plugin.webresource.legacy.ContextBatchPluginResource} for each.
     * <p/>
     * It should be noted that the created {@link com.atlassian.plugin.webresource.legacy.ContextBatchPluginResource} will not actually contain any
     * {@link DownloadableResource}. The {@link PluginResource} created is primarily representing a
     * URL to be used in constructing a particular resource/download reference.
     *
     * @return ContextBatchPluginResource instances, although containing no {@link DownloadableResource}s.
     */
    public Iterable<PluginResource> buildPluginResources(boolean resplitMergedBatches)
    {
        // add the BatchedWebResourceDescriptors for this ContextBatch to each of the generated ContextBatchPluginResource
        // so that we can return ContextBatches to the client with embedded information about the dependencies contained.
        Set<String> descriptors = new HashSet<String>();
        for (WebResourceModuleDescriptor wrmd : this.getResources())
        {
            descriptors.add(wrmd.getCompleteKey());
        }

        List<ContextBatchPluginResource> resources = new ArrayList<ContextBatchPluginResource>();

        resources.add(new ContextBatchPluginResource(contexts,
                    excludedContexts, descriptors, removeSuperResources));

        return postContextBatchesProcess(resources, resplitMergedBatches);
    }

    private static Iterable<PluginResource> postContextBatchesProcess(List<ContextBatchPluginResource> contextBatchResources, boolean resplitMergedBatches)
    {
        List<PluginResource> result = new LinkedList<PluginResource>();
        if (resplitMergedBatches) {
            for (ContextBatchPluginResource batchResource : contextBatchResources)
            {
                result.addAll(batchResource.splitIntoParts());
            }
        }
        else
        {
            result.addAll(contextBatchResources);
        }
        return result;
    }

    public List<String> getContexts()
    {
        return contexts;
    }
    
    public Iterable<String> getExcludedContexts()
    {
        return excludedContexts;
    }

    public Iterable<WebResourceModuleDescriptor> getResources()
    {
        return resources;
    }
    
    Iterable<String> getResourceKeys()
    {
        return transform(resources, new Function<WebResourceModuleDescriptor,String>() 
        {
            public String apply(WebResourceModuleDescriptor input)
            {
                return input.getCompleteKey();
            }
        });
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
            return false;
        
        if (!(obj instanceof ContextBatch))
            return false;
        
        if (obj == this)
            return true;
        
        ContextBatch other = (ContextBatch)obj;
        
        return this.contexts.equals(other.contexts) && this.excludedContexts.equals(other.excludedContexts);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(contexts, excludedContexts);
    }

    public String getKey()
    {
        return encodeContexts(contexts, excludedContexts);
    }
}
