package com.atlassian.plugin.webresource.data;

import com.atlassian.html.encode.JavascriptEncoder;
import com.atlassian.html.encode.JavascriptEncodingWriter;
import com.atlassian.webresource.api.data.PluginDataResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Writer;

/**
 * Writes data provided by a &lt;data&gt; element to a page.
 * @since v3.0
 */
public class DataTagWriter
{
    private static final Logger log = LoggerFactory.getLogger(DataTagWriter.class);

    private static final String PRE = "<script>\nwindow.WRM=window.WRM||{};window.WRM._unparsedData=window.WRM._unparsedData||{};\n";
    private static final String POST = "</script>\n";

    public void write(final Writer writer, final Iterable<PluginDataResource> data) throws IOException
    {
        boolean scriptTagWritten = false;
        for (PluginDataResource datum : data)
        {
            if (!scriptTagWritten)
            {
                writer.write(PRE);
                scriptTagWritten = true;
            }
            write(writer, datum);
        }
        if (scriptTagWritten)
        {
            writer.write(POST);
        }
    }

    private void write(final Writer writer, final PluginDataResource data) throws IOException
    {
        try
        {
            writer.write("WRM._unparsedData[\"");
            JavascriptEncoder.escape(writer, data.getKey());
            writer.write("\"]=\"");
            data.getJsonable().write(new JavascriptEncodingWriter(writer));
            writer.write("\";\n");
        }
        catch (IOException ex)
        {
            log.error("IOException encountered rendering data resource '{}'", new String[]{ data.getKey() }, ex);
        }
        catch (RuntimeException ex)
        {
            // Catch plugins throwing random exceptions
            log.error("Exception encountered rendering data resource '{}'", new String[]{ data.getKey() }, ex);
        }
    }
}
