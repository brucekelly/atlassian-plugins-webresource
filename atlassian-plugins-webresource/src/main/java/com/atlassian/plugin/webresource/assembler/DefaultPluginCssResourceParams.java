package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import java.util.Map;

/**
 * @since v3.0
 */
class DefaultPluginCssResourceParams extends DefaultPluginUrlResourceParams implements PluginCssResourceParams
{
    public DefaultPluginCssResourceParams(Map<String, String> params)
    {
        super(params);
    }

    @Override
    public String media()
    {
        return params.get(Config.MEDIA_PARAM_NAME);
    }



    @Override
    public Map<String, String> other()
    {
        return Maps.filterEntries(super.other(), new Predicate<Map.Entry<String, String>>()
        {
            @Override
            public boolean apply(@Nullable Map.Entry<String, String> input)
            {
                return !input.getKey().equals(Config.MEDIA_PARAM_NAME);
            }
        });
    }
}
