package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.webresource.api.bigpipe.BigPipeManager;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import javax.annotation.concurrent.ThreadSafe;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of BigPipeManager
 * @since v3.3
 */
@ThreadSafe
public class DefaultBigPipeManager implements BigPipeManager
{
    private final ConcurrentHashMap<String, DefaultBigPipe> bigPipes;

    public DefaultBigPipeManager()
    {
        this.bigPipes = new ConcurrentHashMap<String, DefaultBigPipe>();
    }

    @Override
    public BigPipe getBigPipe(String requestId)
    {
        DefaultBigPipe bigPipe = bigPipes.get(requestId);
        if (null == bigPipe)
        {
            DefaultBigPipe newBigPipe = new DefaultBigPipe(requestId);
            bigPipe = bigPipes.putIfAbsent(requestId, newBigPipe);
            if (null == bigPipe)
            {
                bigPipe = newBigPipe;
            }
        }
        return bigPipe;
    }

    @Override
    public ConsumableBigPipe getConsumableBigPipe(String requestId)
    {
        return bigPipes.get(requestId);
    }

    static final class DefaultBigPipe implements BigPipe, ConsumableBigPipe
    {
        private final String requestId;
        private final FutureCompletionService<Jsonable> completor;

        DefaultBigPipe(String requestId)
        {
            this.requestId = requestId;
            this.completor = new QueueFutureCompletionService<Jsonable>();
        }

        @Override
        public BigPipe push(final String key, Promise<Jsonable> promise)
        {
            completor.add(key, promise);
            return this;
        }

        @Override
        public void cancelRemaining()
        {
            completor.destroy(true);
        }

        @Override
        public Iterable<KeyedValue<Jsonable>> getAvailableContent()
        {
            return completor.poll();
        }

        @Override
        public Iterable<KeyedValue<Jsonable>> waitForContent(long timeout, TimeUnit unit) throws InterruptedException
        {
            return completor.poll(timeout, unit);
        }

        @Override
        public boolean isComplete()
        {
            return completor.isComplete();
        }
    }
}
