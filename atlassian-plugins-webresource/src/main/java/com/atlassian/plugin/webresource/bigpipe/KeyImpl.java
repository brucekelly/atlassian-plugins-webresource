package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.webresource.api.bigpipe.Key;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 3.3
 */
public class KeyImpl implements Key
{
    private final Comparable key;

    public static Key of(Comparable key)
    {
        return new KeyImpl(key);
    }

    public KeyImpl(Comparable key)
    {
        this.key = checkNotNull(key);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return key.equals(((KeyImpl) o).key);
    }

    @Override
    public int hashCode()
    {
        return key.hashCode();
    }

    @Override
    public String toString()
    {
        return key.toString();
    }

    @Override
    public Comparable key()
    {
        return key;
    }

    @Override
    public int compareTo(Key o)
    {
        return key.compareTo(o.key());
    }
}
