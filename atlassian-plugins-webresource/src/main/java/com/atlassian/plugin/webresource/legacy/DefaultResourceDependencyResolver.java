package com.atlassian.plugin.webresource.legacy;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.util.PluginUtils;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.util.concurrent.ResettableLazyReference;

import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.contains;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableMap;

public class DefaultResourceDependencyResolver implements ResourceDependencyResolver
{
    private static final Logger log = LoggerFactory.getLogger(DefaultResourceDependencyResolver.class);

    private final WebResourceIntegration webResourceIntegration;
    private final Cache cached = new Cache();
    private final boolean isSuperBatchingEnabled;
    private final List<String> superBatchModuleCompleteKeys;
    private final Globals globals;

    public DefaultResourceDependencyResolver(Globals globals, final WebResourceIntegration webResourceIntegration,
        boolean isSuperBatchingEnabled, List<String> superBatchModuleCompleteKeys)
    {
        this.globals = globals;
        this.webResourceIntegration = webResourceIntegration;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.superBatchModuleCompleteKeys = superBatchModuleCompleteKeys;
    }

    public Iterable<WebResourceModuleDescriptor> getSuperBatchDependencies()
    {
        return cached.resourceMap().values();
    }

    private Iterable<String> getSuperBatchDependencyKeys()
    {
        return cached.resourceMap().keySet();
    }

    public Iterable<WebResourceModuleDescriptor> getDependencies(RequestCache requestCache, final String moduleKey, final boolean excludeSuperBatchedResources, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        final LinkedHashMap<String, WebResourceModuleDescriptor> orderedResources = new LinkedHashMap<String, WebResourceModuleDescriptor>();
        final Iterable<String> superBatchResources = excludeSuperBatchedResources ? getSuperBatchDependencyKeys() : Collections.<String> emptyList();
        resolveDependencies(requestCache, moduleKey, orderedResources, superBatchResources, new Stack<String>(), null, includeDependenciesForFailedUrlReadingConditions);
        return orderedResources.values();
    }

    public Iterable<WebResourceModuleDescriptor> getDependenciesInContext(RequestCache requestCache, final String context, final Set<String> skippedResources, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        return getDependenciesInContext(requestCache, context, true, skippedResources, includeDependenciesForFailedUrlReadingConditions);
    }

    public Iterable<WebResourceModuleDescriptor> getDependenciesInContext(RequestCache requestCache, final String context, boolean excludeSuperBatchedResources, final Set<String> skippedResources, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        final Set<WebResourceModuleDescriptor> contextResources = new LinkedHashSet<WebResourceModuleDescriptor>();
        final Class<WebResourceModuleDescriptor> clazz = WebResourceModuleDescriptor.class;
        for (final WebResourceModuleDescriptor descriptor : webResourceIntegration.getPluginAccessor().getEnabledModuleDescriptorsByClass(clazz))
        {
            if (descriptor != null && descriptor.getContexts().contains(context))
            {
                final LinkedHashMap<String, WebResourceModuleDescriptor> dependencies = new LinkedHashMap<String, WebResourceModuleDescriptor>();
                final Iterable<String> superBatchResources = excludeSuperBatchedResources ? getSuperBatchDependencyKeys() : Collections.<String> emptyList();
                resolveDependencies(requestCache, descriptor.getCompleteKey(), dependencies, superBatchResources, new Stack<String>(), skippedResources, includeDependenciesForFailedUrlReadingConditions);
                for (final WebResourceModuleDescriptor dependency : dependencies.values())
                {
                    contextResources.add(dependency);
                }
            }
        }
        return unmodifiableCollection(contextResources);
    }

    /**
     * Adds the resources as well as its dependencies in order to the given ordered set. This method uses recursion
     * to add a resouce's dependent resources also to the set. You should call this method with a new stack
     * passed to the last parameter.
     *
     * Note that resources already in the given super batch will be excluded when resolving dependencies. You
     * should pass in an empty set for the super batch to include super batch resources.
     *
     * @param moduleKey the module complete key to add as well as its dependencies
     * @param orderedResourceKeys an ordered list set where the resources are added in order
     * @param superBatchResources the set of super batch resources to exclude when resolving dependencies
     * @param stack where we are in the dependency tree
     * @param skippedResources if not null, all resources with conditions are skipped and added to this set.
     * @param includeDependenciesForFailedUrlReadingConditions if dependencies resolved for context batch we need to
     * include all dependencies for UrlReadingConditions even if it failed, all those dependencies would be
     * be cached. Later, those failed dependencies would be removed at the time when content served.
     */
    private void resolveDependencies(RequestCache requestCache, String moduleKey, Map<String,
        WebResourceModuleDescriptor> orderedResourceKeys, Iterable<String> superBatchResources, Stack<String> stack,
        Set<String> skippedResources, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        if (contains(superBatchResources, moduleKey))
        {
            log.debug("Not requiring resource: {} because it is part of a super-batch", moduleKey);
            return;
        }
        if (stack.contains(moduleKey))
        {
            log.warn("Cyclic plugin resource dependency has been detected with: {} \nStack trace: {}", moduleKey, stack);
            return;
        }

        final ModuleDescriptor<?> moduleDescriptor;
        try {
            moduleDescriptor = webResourceIntegration.getPluginAccessor().getEnabledPluginModule(moduleKey);
        } catch (IllegalArgumentException e) {
            // this can happen if the module key is an invalid format (e.g. has no ":")
            log.warn("Cannot find web resource module for: {}", moduleKey);
            return;
        }
        if (!(moduleDescriptor instanceof WebResourceModuleDescriptor))
        {
            if (Boolean.getBoolean(PluginUtils.ATLASSIAN_DEV_MODE))
            {
                if (webResourceIntegration.getPluginAccessor().getPluginModule(moduleKey) != null)
                {
                    log.warn("Cannot include disabled web resource module: {}", moduleKey);
                }
                else
                {
                    log.warn("Cannot find web resource module for: {}", moduleKey);
                }
            }
            return;
        }

        final WebResourceModuleDescriptor webResourceModuleDescriptor = (WebResourceModuleDescriptor) moduleDescriptor;

        // If the webresource cannot encode its state into the URL, it must be broken out of the batch and served
        // separately. This is normally due to the presence of a DecoratingLegacyCondition
        boolean skipDependencies = false;
        if (!webResourceModuleDescriptor.canEncodeStateIntoUrl())
        {
            if (null != skippedResources)
            {
                skippedResources.add(moduleKey);
                return;
            }
            else if (!webResourceModuleDescriptor.shouldDisplayImmediate())
            {
                log.debug("Cannot include web resource module {} as its condition fails", moduleDescriptor.getCompleteKey());
                return;
            }
        }
        else if (!includeDependenciesForFailedUrlReadingConditions)
        {
            Bundle bundle = requestCache.getSnapshot().get(webResourceModuleDescriptor.getCompleteKey());
            if (bundle != null)
            {
                CachedCondition condition = bundle.getCondition();
                if (condition != null)
                {
                    skipDependencies = !condition.evaluateSafely(requestCache);
                }
            }
        }

        final List<String> dependencies = webResourceModuleDescriptor.getDependencies();
        log.debug("About to add resource [{}] and its dependencies: {}", moduleKey, dependencies);
        stack.push(moduleKey);
        try
        {
            if (!skipDependencies)
            {
                for (final String dependency : dependencies)
                {
                    if (orderedResourceKeys.get(dependency) == null)
                    {
                        resolveDependencies(requestCache, dependency, orderedResourceKeys, superBatchResources, stack, skippedResources, includeDependenciesForFailedUrlReadingConditions);
                    }
                }
            }
        }
        finally
        {
            stack.pop();
        }
        orderedResourceKeys.put(moduleKey, webResourceModuleDescriptor);
    }

    final class Cache
    {
        ResettableLazyReference<SuperBatch> lazy = new ResettableLazyReference<SuperBatch>()
        {
            @Override
            protected SuperBatch create() throws Exception
            {
                // The linked hash map ensures that order is preserved
                final String version = webResourceIntegration.getSuperBatchVersion();
                return new SuperBatch(version, loadDescriptors(superBatchModuleCompleteKeys));
            }

            Map<String, WebResourceModuleDescriptor> loadDescriptors(final Iterable<String> moduleKeys)
            {
                if (Iterables.isEmpty(moduleKeys))
                {
                    return emptyMap();
                }
                final Map<String, WebResourceModuleDescriptor> resources = new LinkedHashMap<String, WebResourceModuleDescriptor>();
                for (final String moduleKey : moduleKeys)
                {
                    RequestCache requestCache = new RequestCache(globals);
                    resolveDependencies(requestCache, moduleKey, resources, Collections.<String> emptyList(), new Stack<String>(), null, true);
                }
                return unmodifiableMap(resources);
            }
        };

        Map<String, WebResourceModuleDescriptor> resourceMap()
        {
            if (!isSuperBatchingEnabled)
            {
                log.debug("Super batching not enabled, but getSuperBatchDependencies() called. Returning empty.");
                return emptyMap();
            }
            while (true)
            {
                final SuperBatch batch = lazy.get();
                if (batch.version.equals(webResourceIntegration.getSuperBatchVersion()))
                {
                    return batch.resources;
                }

                // The super batch has been updated so recreate the batch
                lazy.reset();
            }
        }
    }

    static final class SuperBatch
    {
        final String version;
        final Map<String, WebResourceModuleDescriptor> resources;

        SuperBatch(final String version, final Map<String, WebResourceModuleDescriptor> resources)
        {
            this.version = checkNotNull(version);
            this.resources = checkNotNull(resources);
        }
    }
}
