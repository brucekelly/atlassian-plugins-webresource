package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Basic utility functions.
 *
 * @since v6.3
 */
public class Support
{
    protected static Set<String> printedWarnings = new HashSet<String>();

    public static final Logger LOGGER = LoggerFactory.getLogger("webresource");

    /**
     * Join list of predicates with AND operand.
     */
    public static Predicate<Bundle> efficientAndPredicate(List<Predicate<Bundle>> predicates)
    {
        if (predicates.size() == 0)
        {
            return Predicates.<Bundle>alwaysTrue();
        }
        else if (predicates.size() == 1)
        {
            return predicates.get(0);
        }
        else
        {
            return Predicates.and(predicates);
        }
    }

    /**
     * Warn only once.
     */
    public static void warnOnce(String msg)
    {
        // The set is not synchronized and not thread safe, but I guess in case of warnings we can ignore this,
        // it's not a big deal if some warnings would be printed twice or not printed at all.
        // myquestion - it shouldn't throw concurrent access exception, right?

        // Preventing warnings to eat up too much memory in a wrong use case.
        if (printedWarnings.size() < 100)
        {
            if (!printedWarnings.contains(msg))
            {
                printedWarnings.add(msg);
                LOGGER.warn(msg);
            }
        }
        else
        {
            LOGGER.warn(msg);
            LOGGER.warn("wrong usage of warnOnce, too much different messages!");
        }
    }

    /**
     * Indent string, multiline also supported.
     */
    public static String indent(String string, String indent)
    {
        return indent + string.replaceAll("\\n", "\n" + indent);
    }

    /**
     * Helper to check for equality without bothering to handle nulls.
     */
    public static boolean equals(Object a, Object b)
    {
        return a == null ? b == null : a.equals(b);
    }

    /**
     * Copy in into out and close streams safely.
     */
    public static void copy(final InputStream in, final OutputStream out)
    {
        try
        {
            IOUtils.copy(in, out);
        }
        catch (final IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            IOUtils.closeQuietly(in);
            try
            {
                out.flush();
            }
            catch (final IOException e)
            {
                Support.LOGGER.debug("Error flushing output stream", e);
            }
        }
    }
}
