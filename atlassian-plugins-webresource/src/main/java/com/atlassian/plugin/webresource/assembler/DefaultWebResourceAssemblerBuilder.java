package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;

/**
 * Implementation of WebResourceAssemblerBuilder
 * @since v3.0
 */
public class DefaultWebResourceAssemblerBuilder implements WebResourceAssemblerBuilder
{
    private final Globals globals;
    private boolean isSuperBatchingEnabled;

    public DefaultWebResourceAssemblerBuilder(Globals globals)
    {
        this.globals = globals;
        this.isSuperBatchingEnabled = globals.getConfig().isSuperBatchingEnabled();
    }

    @Override
    public WebResourceAssembler build()
    {
        DefaultWebResourceAssembler assembler = new DefaultWebResourceAssembler(new RequestState(globals));
        if (isSuperBatchingEnabled)
        {
            assembler.resources().requireContext(Config.SUPER_BATCH_CONTEXT_KEY);
        }
        return assembler;
    }

    @Override
    public WebResourceAssemblerBuilder includeSuperbatchResources(boolean include)
    {
        this.isSuperBatchingEnabled = include;
        return this;
    }
}
