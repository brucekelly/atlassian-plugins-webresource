package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * Applies a fixed set of transformers to all resources by type.
 * These transformers are always applied after pluggable transformers.
 *
 * @since v3.1.0
 */
public interface StaticTransformers
{
    // TODO refactor it into the interface of the transformer.
    //
    // It's a temporary way to get list of parameters used by static transformers, it should be refactored
    // into proper transformer interface.
    public static final String[] PARAMETERS_USED = new String[]{ RelativeUrlTransformerFactory.RELATIVE_URL_QUERY_KEY};

    /**
     * Contributes to the URL for all static transformers matching the given type
     * @param type type of resource (eg js, css, less)
     * @param transformerParameters parameters to pass to transformers
     * @param urlBuilder url building to contribute to
     */
    public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder);

    /**
     * Performs transformation for all static transformers for the given type.
     * @param content content to transform
     * @param transformerParameters parameters to pass to transformers
     * @param resourceLocation resource location
     * @param queryParams url querystring containing information for transformers
     * @param sourceUrl url of original source file
     * @return a DownloadableResource representing the completed transformation
     */
    public Content transform(Content content, TransformerParameters transformerParameters,
            ResourceLocation resourceLocation, QueryParams queryParams, String sourceUrl);
}
