package com.atlassian.plugin.webresource.impl.config;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.ServletContextFactory;
import com.atlassian.plugin.util.PluginUtils;
import com.atlassian.plugin.util.validation.ValidationException;
import com.atlassian.plugin.webresource.LocationContentAnnotator;
import com.atlassian.plugin.webresource.PluginResourceContainer;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.SemicolonResourceContentAnnotator;
import com.atlassian.plugin.webresource.TryCatchJsResourceContentAnnotator;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.support.ConditionInstanceCache;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.util.HashBuilder;
import com.atlassian.sourcemap.Util;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.joinWithSlashWithoutEmpty;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Unites all the configurations in one place. Not exposed as API, so it is possible to change it and store settings
 * that should not be exposed publicly.
 *
 * @since 3.3
 */
public class Config
{
    public static final String IEONLY_PARAM_NAME = "ieonly";
    public static final String SOURCE_PARAM_NAME = "source";
    public static final String BATCH_PARAM_NAME = "batch";
    public static final String MEDIA_PARAM_NAME = "media";
    public static final String CONTENT_TYPE_PARAM_NAME = "content-type";
    public static final String CACHE_PARAM_NAME = "cache";
    public static final String CONDITIONAL_COMMENT_PARAM_NAME = "conditionalComment";

    public static final String DOWNLOAD_PARAM_VALUE = "download";

    public static final String[] HTTP_PARAM_NAMES = new String[] {
        IEONLY_PARAM_NAME,
        MEDIA_PARAM_NAME,
        CONTENT_TYPE_PARAM_NAME,
        CACHE_PARAM_NAME,
        CONDITIONAL_COMMENT_PARAM_NAME
    };

    public static final List<String> PARAMS_SORT_ORDER = ImmutableList.of(
        CACHE_PARAM_NAME,
        MEDIA_PARAM_NAME,
        CONDITIONAL_COMMENT_PARAM_NAME,
        IEONLY_PARAM_NAME
    );

    public static final String CONTEXT_PREFIX = "_context";
    public static final String SUPER_BATCH_CONTEXT_KEY = "_super";
    public static final String SUPERBATCH_KEY = Config.CONTEXT_PREFIX + ":" + SUPER_BATCH_CONTEXT_KEY;

    private static final String DISABLE_MINIFICATION = "atlassian.webresource.disable.minification";

    private final WebResourceIntegration integration;
    private final ResourceBatchingConfiguration batchingConfiguration;
    private final WebResourceUrlProvider urlProvider;
    private ContentTypeResolver contentTypeResolver;
    private final ServletContextFactory servletContextFactory;
    private final StaticTransformers staticTransformers;
    private final TransformerCache transformerCache;

    public static final String INCREMENTAL_CACHE_SIZE = new String("plugin.webresource.incrementalcache.size");
    private static final boolean CACHE_ENABLED = !Boolean.getBoolean(PluginUtils.WEBRESOURCE_DISABLE_FILE_CACHE) || Boolean.getBoolean(PluginUtils
                .ATLASSIAN_DEV_MODE);

    public Config(ResourceBatchingConfiguration batchingConfiguration, WebResourceIntegration integration,
            WebResourceUrlProvider urlProvider, ServletContextFactory servletContextFactory,
            StaticTransformers staticTransformers, TransformerCache transformerCache)
    {
        this.batchingConfiguration = batchingConfiguration;
        this.integration = integration;
        this.urlProvider = urlProvider;
        this.servletContextFactory = servletContextFactory;
        this.staticTransformers = staticTransformers;
        this.transformerCache = transformerCache;
    }

    /**
     * Needed because `contentTypeResolver` isn't available at the time Config is created and set up a bit later.
     */
    public void setContentTypeResolver(ContentTypeResolver contentTypeResolver)
    {
        if (this.contentTypeResolver != null)
        {
            throw new RuntimeException("content type resolver already set!");
        }
        this.contentTypeResolver = contentTypeResolver;
    }

    /**
     * @return if cache should be enabled.
     */
    public boolean isContentCacheEnabled()
    {
        return CACHE_ENABLED;
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getContentCacheSize()
    {
        return Integer.getInteger(PluginUtils.WEBRESOURCE_FILE_CACHE_SIZE, 1000);
    }

    /**
     * @return amount of files to be stored in cache.
     */
    public int getIncrementalCacheSize()
    {
        return Integer.getInteger(INCREMENTAL_CACHE_SIZE, 1000);
    }

    /**
     * @return directory where cache content would be stored as files.
     */
    public File getCacheDirectory()
    {
        return integration.getTemporaryDirectory();
    }

    /**
     * @param typeOrContentType type or content type.
     * @return if source map enabled for this type or content type.
     */
    public boolean isSourceMapEnabledFor(String typeOrContentType)
    {
        return isSourceMapEnabled() && Util.isSourceMapSupportedBy(typeOrContentType);
    }

    /**
     * @return if source map enabled.
     */
    public boolean isSourceMapEnabled()
    {
        return batchingConfiguration.isSourceMapEnabled();
    }

    /**
     * @return if CDN enabled.
     */
    public boolean isCdnEnabled()
    {
        return null != integration.getCDNStrategy() && integration.getCDNStrategy().supportsCdn();
    }

    /**
     * @return get current locale.
     */
    public String getCurrentLocale()
    {
        return integration.getStaticResourceLocale();
    }

    /**
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl()
    {
        return getBaseUrl(true);
    }

    /**
     * @param isAbsolute if url should be absolute or relative.
     * @return base url, where web resources is mounted to.
     */
    public String getBaseUrl(boolean isAbsolute)
    {
        try {
            return joinWithSlashWithoutEmpty(urlProvider.getBaseUrl(isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE),
                AbstractFileServerServlet.SERVLET_PATH);
        }
        catch (AssertionError e)
        {
            // For unknown reason some links in Confluence (like /confluence/setup/setupstart.action ) doesn't
            // support the absolute base url, catching such cases and trying to use relative base url.
            if (isAbsolute && e.getMessage().contains("Unsupported URLMode"))
            {
                return getBaseUrl(false);
            }
            else
            {
                throw e;
            }
        }
    }

    /**
     * @return url prefix for resource, including base url.
     */
    public String getResourceUrlPrefix(String hash, String version, boolean isAbsolute)
    {
        return urlProvider.getStaticResourcePrefix(hash, version, (isAbsolute ? UrlMode.ABSOLUTE : UrlMode.RELATIVE))
                + "/" + AbstractFileServerServlet.SERVLET_PATH;
    }

    /**
     * @return url prefixed with CDN prefix.
     */
    public String getResourceCdnPrefix(String url)
    {
        return integration.getCDNStrategy().transformRelativeUrl(url);
    }

    /**
     * @return content type for given path.
     */
    public String getContentType(String path)
    {
        return contentTypeResolver.getContentType(path);
    }

    /**
     * @return content annotators for given type.
     */
    public ResourceContentAnnotator[] getContentAnnotators(String type)
    {
        if (BatchType.js.toString().equals(type))
        {
            if (batchingConfiguration.isJavaScriptTryCatchWrappingEnabled())
            {
                return new ResourceContentAnnotator[] { new SemicolonResourceContentAnnotator(),
                        new TryCatchJsResourceContentAnnotator(), new LocationContentAnnotator() };
            }
            else
            {
                return new ResourceContentAnnotator[] { new SemicolonResourceContentAnnotator(),
                        new LocationContentAnnotator() };
            }
        }
        else if (BatchType.css.toString().equals(type))
        {
            return new ResourceContentAnnotator[] { new LocationContentAnnotator() };
        }
        else
        {
            return new ResourceContentAnnotator[] { };
        }
    }

    /**
     * @return static transformers.
     */
    public StaticTransformers getStaticTransformers()
    {
        return staticTransformers;
    }

    /**
     * If the key is context or web resource key.
     */
    public static boolean isContextKey(String key)
    {
        return key.contains(Config.CONTEXT_PREFIX);
    }

    /**
     * Analyzes current state of the plugin system and return web resources.
     */
    protected void ensureNoLegacyStuff(Snapshot snapshot)
    {
        if (integration.forbidCondition1AndTransformer1())
        {
            // Checking for legacy conditions.
            List<String> resourcesWithLegacyConditions = new LinkedList<String>();
            for (Bundle bundle : snapshot.getWebResourcesWithLegacyConditions())
            {
                if (!integration.allowedCondition1Keys().contains(bundle.getKey()))
                {
                    resourcesWithLegacyConditions.add(bundle.getKey());
                }
            }

            // Checking for legacy transformers.
            List<String> resourcesWithLegacyTransformers = new LinkedList<String>();
            for (Bundle bundle : snapshot.getWebResourcesWithLegacyTransformers())
            {
                if (!integration.allowedTransform1Keys().contains(bundle.getKey()))
                {
                    resourcesWithLegacyTransformers.add(bundle.getKey());
                }
            }

            if (resourcesWithLegacyConditions.size() > 0 || resourcesWithLegacyTransformers.size() > 0)
            {
                List<String> messages = new ArrayList<String>();
                if (resourcesWithLegacyConditions.size() > 0)
                {
                    messages.add("legacy conditions: \"" + Joiner.on("\", \"").join(resourcesWithLegacyConditions) + "\"");
                }
                if (resourcesWithLegacyTransformers.size() > 0)
                {
                    messages.add("legacy transformers: \"" + Joiner.on("\", \"").join(resourcesWithLegacyTransformers) + "\"");
                }
                throw new ValidationException("there are web resources with " + Joiner.on(", and ").join(messages),
                    ImmutableList.<String>of());
            }
        }
    }

    /**
     * Analyzes current state of the plugin system and return web resources.
     */
    public Snapshot getWebResourcesWithoutCache()
    {
        class IntermediaryContextData
        {
            public List<String> dependencies = new ArrayList<String>();
            public Date updatedAt;
            public String version = "";
        }

        Map<WebResource, CachedTransformers> webResourcesTransformations = new HashMap<WebResource, CachedTransformers>();
        Map<WebResource, CachedCondition> webResourcesCondition = new HashMap<WebResource, CachedCondition>();
        Set<WebResource> webResourcesWithLegacyConditions = new HashSet<WebResource>();
        Set<WebResource> webResourcesWithLegacyTransformers = new HashSet<WebResource>();
        Set<WebResource> webResourcesWithDisabledMinification = new HashSet<WebResource>();
        Snapshot snapshot = new Snapshot(this, webResourcesTransformations, webResourcesCondition,
            webResourcesWithLegacyConditions, webResourcesWithLegacyTransformers, webResourcesWithDisabledMinification);

        // Collecting web resource descriptors.
        Map<String, IntermediaryContextData> intermediaryContexts = new HashMap<String, IntermediaryContextData>();
        List<WebResourceModuleDescriptor> webResourceDescriptors = integration.getPluginAccessor()
                .getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class);

        // Processing descriptors and building web resources graph.
        ConditionInstanceCache conditionInstanceCache = new ConditionInstanceCache();
        for (final WebResourceModuleDescriptor webResourceDescriptor : webResourceDescriptors)
        {
            if (webResourceDescriptor == null)
            {
                continue;
            }

            Plugin plugin = webResourceDescriptor.getPlugin();
            Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();

            // Checking for legacy conditions.
            String completeKey = webResourceDescriptor.getCompleteKey();

            DecoratingCondition condition = webResourceDescriptor.getCondition();
            boolean hasLegacyConditions = (condition != null) && !condition.canEncodeStateIntoUrl();

            // Checking for legacy transformers.
            List<WebResourceTransformation> transformations = webResourceDescriptor.getTransformations();
            boolean hasLegacyTransformers = false;
            // Strictly speaking we should check the extension of the transformer,
            // because legacy CSS transformers would not affect JS.
            // But, it would hurt the performance to check transformers against all the resources in the web resource.
            // So, to make it faster we just check if there's any legacy transformer at all.
            for (WebResourceTransformation transformation : transformations)
            {
                if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache))
                {
                    hasLegacyTransformers = true;
                    break;
                }
            }

            // Adding web resource.
            TransformerParameters transformerParameters = TransformerParameters.of(webResourceDescriptor);
            WebResource webResource = new WebResource(snapshot, completeKey, webResourceDescriptor.getDependencies(),
                    updatedAt, plugin.getPluginInformation().getVersion(), true, transformerParameters);
            if (hasLegacyConditions)
            {
                webResourcesWithLegacyConditions.add(webResource);
            }
            if (hasLegacyTransformers)
            {
                webResourcesWithLegacyTransformers.add(webResource);
            }
            if (webResourceDescriptor.isDisableMinification())
            {
                webResourcesWithDisabledMinification.add(webResource);
            }
            snapshot.put(completeKey, webResource);

            // Collecting dependencies and dates for contexts, we would need this information later when transform
            // contexts into
            // virtual web resources.
            for (String context : webResourceDescriptor.getContexts())
            {
                if (!context.equals(completeKey))
                {
                    String contextResourceKey = CONTEXT_PREFIX + ":" + context;
                    IntermediaryContextData contextData = intermediaryContexts.get(contextResourceKey);
                    if (contextData == null)
                    {
                        contextData = new IntermediaryContextData();
                        intermediaryContexts.put(contextResourceKey, contextData);
                    }
                    contextData.dependencies.add(completeKey);
                    if (contextData.updatedAt == null || contextData.updatedAt.before(updatedAt))
                    {
                        contextData.updatedAt = updatedAt;
                    }
                    contextData.version = HashBuilder.buildHash(contextData.version, webResource.getVersion());
                }
            }

            // Adding conditions.
            if (condition != null)
            {
                // Trying to optimize conditions and replace multiple instances of the same UrlReadingCondition with
                // one instance.
                CachedCondition cachedCondition = conditionInstanceCache.intern(condition);
                webResourcesCondition.put(webResource, cachedCondition);
            }

            // Adding transformers.
            if (!transformations.isEmpty())
            {
                webResourcesTransformations.put(webResource, new CachedTransformers(transformations));
            }
        }

        // Turning contexts into virtual resources.
        for (Map.Entry<String, IntermediaryContextData> entry : intermediaryContexts.entrySet())
        {
            String contextResourceKey = entry.getKey();
            IntermediaryContextData contextData = entry.getValue();
            snapshot.put(contextResourceKey, new Bundle(snapshot, contextResourceKey, contextData.dependencies,
                    contextData.updatedAt, contextData.version, true));
        }

        // Adding super batch as virtual resource.
        if (batchingConfiguration.isSuperBatchingEnabled())
        {
            List<String> dependencies = batchingConfiguration.getSuperBatchModuleCompleteKeys();
            // If super batch is empty setting very old date as it's update date.
            Date updatedAt = new Date(0);
            String version = "";
            for (String completeKey : dependencies)
            {
                Bundle superbatch = snapshot.get(completeKey);
                if (superbatch != null)
                {
                    if (superbatch.getUpdatedAt().after(updatedAt))
                    {
                        updatedAt = superbatch.getUpdatedAt();
                    }
                    version = HashBuilder.buildHash(version, superbatch.getVersion());
                }
            }
            snapshot.put(SUPERBATCH_KEY, new Bundle(snapshot, SUPERBATCH_KEY, dependencies, updatedAt, version, true));
        }

        ensureNoLegacyStuff(snapshot);
        return snapshot;
    }

    /**
     * Queries the plugin system and return list of Resources for Web Resource.
     */
    public Map<String, Resource> getResourcesWithoutCache(Bundle bundle)
    {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        Map<String, Resource> resources = new LinkedHashMap<String, Resource>();

        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(bundle.getKey());
        if (webResourceDescriptor != null)
        {
            for (ResourceDescriptor resourceDescriptor : webResourceDescriptor.getResourceDescriptors())
            {
                if (DOWNLOAD_PARAM_VALUE.equals(resourceDescriptor.getType()))
                {
                    ResourceLocation resourceLocation = resourceDescriptor.getResourceLocationForName(null);
                    Resource resource = buildResource(bundle, resourceLocation);
                    resources.put(resource.getName(), resource);
                }
            }
        }
        return resources;
    }

    /**
     * Get data resources for web resource.
     */
    public LinkedHashMap<String, Jsonable> getWebResourceData(String key)
    {
        // Adding resources, it is important to use `LinkedHashMap` because order of resources is important.
        LinkedHashMap<String, Jsonable> data = new LinkedHashMap<String, Jsonable>();
        WebResourceModuleDescriptor webResourceDescriptor = getWebResourceModuleDescriptor(key);
        if (webResourceDescriptor != null)
        {
            for (Map.Entry<String, WebResourceDataProvider> entry : webResourceDescriptor.getDataProviders().entrySet())
            {
                data.put(entry.getKey(), entry.getValue().get());
            }
        }
        return data;
    }

    private WebResourceModuleDescriptor getWebResourceModuleDescriptor(String key)
    {
        ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(key);
        if (moduleDescriptor == null)
        {
            return null;
        }
        if (!(moduleDescriptor instanceof WebResourceModuleDescriptor))
        {
            return null;
        }
        return (WebResourceModuleDescriptor) moduleDescriptor;
    }

    /**
     * Returns stream for file with given location for given resource.
     */
    public InputStream getStreamFor(final Resource resource, final String path)
    {
        ResourceLocation resourceLocation = resource.getResourceLocation();
        String sourceParam = resourceLocation.getParameter(SOURCE_PARAM_NAME);
        final boolean isWebContextStatic = "webContextStatic".equalsIgnoreCase(sourceParam);

        if (isWebContextStatic)
        {
            // Tomcat 8 requires resource path to start with the slash.
            String pathWithSlash = path.startsWith("/") ? path : "/" + path;
            return servletContextFactory.getServletContext().getResourceAsStream(pathWithSlash);
        }
        else
        {
            return getPlugin(resource.getKey()).getResourceAsStream(path);
        }
    }

    private Plugin getPlugin(String key)
    {
        // Detecting if it's a web resource key or plugin key.
        if (key.contains(":"))
        {
            ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(key);
            return checkNotNull(moduleDescriptor.getPlugin());
        }
        else
        {
            return checkNotNull(integration.getPluginAccessor().getEnabledPlugin(key));
        }
    }

    /**
     * If minification enabled.
     */
    public boolean isMinificationEnabled()
    {
        // If we have a System property set to true that DISABLES
        // the minification
        try
        {
            if (Boolean.getBoolean(DISABLE_MINIFICATION) || Boolean.getBoolean(PluginUtils.ATLASSIAN_DEV_MODE))
            {
                return false;
            }
        }
        catch (final SecurityException se)
        {
            // some app servers might have protected access to system
            // properties. Unlikely but lets be defensive
        }
        return true;
    }

    /**
     * Helper to hide bunch of integration code and simplify resource creation.
     */
    protected Resource buildResource(Bundle bundle, ResourceLocation resourceLocation)
    {
        // The code based on `SingleDownloadableResourceFinder.getDownloadablePluginResource`.
        String type = ResourceUtils.getType(!StringUtils.isEmpty(resourceLocation.getName()) ? resourceLocation
            .getName() : resourceLocation.getName());
        if (type != null)
        {
            type = type.toLowerCase();
        }
        Resource resource = new Resource(bundle, resourceLocation, type);
        return resource;
    }

    /**
     * Get not declared Resource.
     */
    public Resource getModuleResource(String completeKey, String name)
    {
        // Confluence throws error if trying to query module with not complete key (with the plugin key for example).
        if (!isWebResourceKey(completeKey))
        {
            return null;
        }
        ModuleDescriptor<?> moduleDescriptor = integration.getPluginAccessor().getEnabledPluginModule(completeKey);
        // In case of virtual context or super batch resource there would be null returned.
        if (moduleDescriptor == null)
        {
            return null;
        }
        ResourceLocation resourceLocation = moduleDescriptor.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null)
        {
            return null;
        }
        Plugin plugin = moduleDescriptor.getPlugin();
        Date updatedAt = (plugin.getDateLoaded() == null) ? new Date() : plugin.getDateLoaded();
        PluginResourceContainer resourceContainer = new PluginResourceContainer(new Snapshot(this), completeKey,
            updatedAt, plugin.getPluginInformation().getVersion());
        return buildResource(resourceContainer, resourceLocation);
    }

    /**
     * Get Resource for Plugin.
     */
    public Resource getPluginResource(String pluginKey, String name)
    {
        Plugin plugin = integration.getPluginAccessor().getPlugin(pluginKey);
        if (plugin == null)
        {
            return null;
        }
        ResourceLocation resourceLocation = plugin.getResourceLocation(DOWNLOAD_PARAM_VALUE, name);
        if (resourceLocation == null)
        {
            return null;
        }
        PluginResourceContainer resourceContainer = new PluginResourceContainer(new Snapshot(this), pluginKey,
                plugin.getDateLoaded(), plugin.getPluginInformation().getVersion());
        Resource resource = buildResource(resourceContainer, resourceLocation);
        return resource;
    }

    public boolean optimiseSourceMapsForDevelopment()
    {
        return batchingConfiguration.optimiseSourceMapsForDevelopment();
    }

    public boolean isSuperBatchingEnabled()
    {
        return batchingConfiguration.isSuperBatchingEnabled();
    }

    public boolean isBatchContentTrackingEnabled()
    {
        return batchingConfiguration.isBatchContentTrackingEnabled();
    }

    public boolean isContextBatchingEnabled()
    {
        return batchingConfiguration.isContextBatchingEnabled();
    }

    public boolean isWebResourceBatchingEnabled()
    {
        return batchingConfiguration.isPluginWebResourceBatchingEnabled();
    }

    /**
     * If incremental cache enabled.
     */
    public boolean isIncrementalCacheEnabled()
    {
        return integration.isIncrementalCacheEnabled();
    }

    /**
     * If `completeKey` is Web Resource key or Plugin key.
     */
    public static boolean isWebResourceKey(String completeKey)
    {
        return completeKey.contains(":");
    }

    /**
     * Hash code representing the state of config. State of the system could change not only on plugin system changes
     * but also some properties of config could change too. In such cases the cache additionally checked against the
     * version of config it's build for.
     * <p/>
     * Strictly speaking this hash should include the full state of config, but it would be too slow, so instead we
     * check only for some of its properties.
     */
    public int partialHashCode()
    {
        return integration.getSuperBatchVersion().hashCode();
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public WebResourceIntegration getIntegration()
    {
        return integration;
    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public TransformerCache getTransformerCache()
    {
        return transformerCache;
    }

    public boolean isUrlGenerationCacheEnabled()
    {
        return true;
    }

    public int getUrlCacheSize()
    {
        return Integer.getInteger(PluginUtils.WEBRESOURCE_FILE_CACHE_SIZE, 200);
    }

    public boolean showClientLogs()
    {
        return false;
    }

    @Deprecated
    public ResourceBatchingConfiguration getBatchingConfiguration()
    {
        return batchingConfiguration;
    }
}