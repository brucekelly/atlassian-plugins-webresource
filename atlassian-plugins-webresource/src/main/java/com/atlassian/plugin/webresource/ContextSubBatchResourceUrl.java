package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.config.BatchType;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.util.HashBuilder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.http.Router.encodeContexts;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public class ContextSubBatchResourceUrl extends ResourceUrl
{
    private final String key;
    private final Globals globals;
    private final BatchType type;
    private final Helpers.ContextBatch contextBatch;
    private final Helpers.SubBatch subBatch;
    private final String hash;
    private final Map<String, String> params;
    private final boolean hasLegacyTransformers;

    public ContextSubBatchResourceUrl(Globals globals, Helpers.ContextBatch contextBatch,
        Helpers.SubBatch subBatch, BatchType type, Map<String, String> params, String hash,
        boolean hasLegacyTransformers)
    {
        this.globals = globals;
        this.contextBatch = contextBatch;
        this.subBatch = subBatch;
        this.hash = hash;
        this.params = params;
        this.key = encodeContexts(contextBatch.included, contextBatch.excluded);
        this.type = type;
        this.hasLegacyTransformers = hasLegacyTransformers;
    }

    public List<Bundle> getBatchedBundles()
    {
        return subBatch.bundles;
    }

    public List<String> getIncludedContexts()
    {
        return contextBatch.included;
    }

    public LinkedHashSet<String> getExcludedContexts()
    {
        return contextBatch.excluded;
    }

    @Override
    public String getName()
    {
        return getKey() + "." + type;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public String getType()
    {
        return type.toString();
    }

    public String getUrl(boolean isAbsolute)
    {
        boolean isCacheable = Resource.isCacheableStatic(subBatch.resourcesParams);

        boolean hasLegacyConditions = false;
        for (Bundle bundle : subBatch.bundles)
        {
            if (bundle.hasLegacyConditions())
            {
                hasLegacyConditions = true;
                break;
            }
        }

        boolean isCdnSupported = !hasLegacyConditions && !hasLegacyTransformers;
        return globals.getRouter().cloneWithNewUrlMode(isAbsolute).contextBatchUrl(getKey(),
            getType(), getParams(), isCacheable, isCdnSupported, hash, calculateHash());
    }

    @Override
    public Map<String, String> getParams()
    {
        return params;
    }

    private String calculateHash()
    {
        HashBuilder versionHashBuilder = new HashBuilder();
        for (Bundle bundle : subBatch.bundles)
        {
            versionHashBuilder.add(bundle.getKey());
            versionHashBuilder.add(bundle.getVersion());
        }
        return versionHashBuilder.build();
    }
}
