package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Implemented by classes that want to annotate the write of a plugin resource.
 */
public interface ResourceContentAnnotator
{
    /**
     * Called before the resource content is written.
     *
     * @param resource
     * @param stream  @throws IOException if there is a problem writing the annotation.
     */
    void before(Resource resource, OutputStream stream) throws IOException;
    
    /**
     * Called after the resource content is written.
     *
     * @param resource
     * @param stream  @throws IOException if there is a problem writing the annotation.
     */
    void after(Resource resource, OutputStream stream) throws IOException;

    /**
     * Used for source map generation.
     * @return amount of lines the annotator adds to the source.
     *
     * @since 3.3
     */
    int beforeOffset();
}
