package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableSet;

import java.util.Map;

import static com.atlassian.plugin.servlet.AbstractFileServerServlet.PATH_SEPARATOR;
import static com.atlassian.plugin.servlet.AbstractFileServerServlet.SERVLET_PATH;

/**
 * Represents a batch of plugin resources. <p/>
 * <p/>
 * It provides methods to parse and generate urls to locate a batch of plugin resources. <p/>
 * <p/>
 * @since 2.2
 */
public class BatchPluginResource extends AbstractPluginResource
{
    private final String key;

    /**
     * This constructor should only ever be used internally within this class. It does not ensure that the resourceName's
     * file extension is the same as the given type. It is up to the calling code to ensure this.
     */
    public BatchPluginResource(String key, String completeKey)
    {
        super(ImmutableSet.of(completeKey));
        this.key = key;
    }

    public String getModuleCompleteKey()
    {
        return key;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
            return true;

        if ((o == null) || (getClass() != o.getClass()))
            return false;

        final BatchPluginResource that = (BatchPluginResource) o;

        return Objects.equal(key, that.key);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(key);
    }
}
