package com.atlassian.plugin.webresource.impl.support.http;

import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.impl.Globals;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.http.Router.buildUrl;
import static com.atlassian.plugin.webresource.impl.http.Router.parseWithRe;

/**
 * Possibly would be replaced by original request, currently original request is hard to use because parsed `path` and
 * `params` not available on it, it also hides some other imlementation details that wont be needed after the
 * refactoring.
 * <p/>
 * Also after the refactoring it would contain the `state` object - the only thing that will change during the request,
 * all other resources would be stateless.
 *
 * @since 3.3
 */
public class Request
{
    private final String path;
    private final Map<String, String> params;
    private final HttpServletRequest originalRequest;
    private final boolean isCacheable;
    private final Globals globals;
    private final String url;

    public Request (Globals globals, String path, Map<String, String> params)
    {
        this.globals = globals;
        originalRequest = null;
        this.params = params;
        this.path = path;
        isCacheable = ResourceUtils.canRequestedResourcesContentBeAssumedConstant(getParams());
        this.url = buildUrl(path, params);
    }

    public Request(Globals globals, HttpServletRequest request, String encoding)
    {
        this.globals = globals;
        originalRequest = request;
        params = ResourceUtils.getQueryParameters(request);
        try
        {
            path = URLDecoder.decode(request.getRequestURI(), encoding);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }

        isCacheable = ResourceUtils.canRequestedResourcesContentBeAssumedConstant(getParams());
        url = buildUrl(path, params);
    }

    /**
     * @return path of HTTP request, part of URL without query string.
     */
    public String getPath()
    {
        return path;
    }

    /**
     * @return HTTP params.
     */
    public Map<String, String> getParams()
    {
        return params;
    }

    public HttpServletRequest getOriginalRequest()
    {
        return originalRequest;
    }

    /**
     * @return type of request, `js`, `css`, determined same as file type / extension.
     */
    public String getType()
    {
        return getType(path);
    }

    public String getContentType()
    {
        return globals.getConfig().getContentType(getPath());
    }

    /**
     * @param path path of HTTP request.
     * @return type of request, `js`, `css`, determined same as file type / extension.
     */
    public static String getType(String path)
    {
        List<String> parsed = parseWithRe(path, "\\.([^\\.]+)$");
        return parsed.size() > 0 ? parsed.get(0) : null;
    }

    /**
     * @return URL of HTTP request, with query string.
     */
    public String getUrl()
    {
        return url;
    }

    // Calculating downloadableResource.isResourceModified can be moderately expensive. Avoiding if possible.
    public boolean isCacheable()
    {
        return isCacheable;
    }

    public Globals getGlobals()
    {
        return globals;
    }
}