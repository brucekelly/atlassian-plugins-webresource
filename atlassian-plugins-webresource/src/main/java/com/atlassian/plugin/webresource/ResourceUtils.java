package com.atlassian.plugin.webresource;

import com.atlassian.util.concurrent.NotNull;

import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;

public class ResourceUtils
{
    /**
     * @since 2.13
     */
    public static final String STATIC_HASH = "_statichash";

    /**
     * Determines the type (css/js) of the resource from the given path.
     * @param path - the path to use
     * @return the type of resource
     */
    public static String getType(@NotNull String path)
    {
        int index = path.lastIndexOf('.');
        if (index > -1 && index < path.length())
            return path.substring(index + 1);

        return "";
    }

    /**
     * Determines the base name (name without the extension) of the resource from the given path.
     * @param path - the path to use
     * @return the basename of resource
     *
     * @since 3.3
     */
    public static String getBasename(@NotNull String path)
    {
        int index = path.lastIndexOf('.');
        if (index > -1 && index < path.length())
        {
            return path.substring(0, index);
        }
        return path;
    }

    /**
     * Returns a Map of query parameters from the request. If there are multiple values for the same
     * query parameter, the first value is used.
     *
     * @see {@link javax.servlet.ServletRequest#getParameterMap()}
     */
    public static Map<String, String> getQueryParameters(HttpServletRequest request)
    {
        Map<String, String> result = new TreeMap<String, String>();
        Map<String, String[]> parameters = request.getParameterMap();

        for (Map.Entry<String, String[]> entry : parameters.entrySet())
        {
            if (entry.getValue() != null && entry.getValue().length > 0)
                result.put(entry.getKey(), entry.getValue()[0]);
        }

        final String hash = (String) request.getAttribute(STATIC_HASH);
        if (hash != null)
        {
            result.put(STATIC_HASH, hash);
        }

        return result;
    }

    /**
     * Given a parameter map, determine if a web resource is cacheable.
     *
     * This determines whether we store in the file cache or not, as well as dictating 304 behaviour.
     *
     * @param params
     * @return if the PluginResourceDownload.STATIC_HASH is set and cache does not equal false.
     */
    public static boolean canRequestedResourcesContentBeAssumedConstant(Map<String, String> params) {
        final boolean nocache = "false".equals(params.get("cache"));
        final boolean nohash = !params.containsKey(STATIC_HASH);
        return !(nohash || nocache);
    }
}
