package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Contains Transformer Descriptors grouped by its keys. There are two groups of Transformer Descriptors - modern ones
 * with UrlReading and deprecated without it.
 *
 * Deprecated transformer should be used only if there's no modern one with the same key.
 *
 * @since 3.1.7
 */
public class TransformerCache
{
    // Needed for thread safe without sync.
    private final ResettableLazyReference<Map<String, Object>> lazyReferenceTransformerCache;

    public TransformerCache(final PluginEventManager pluginEventManager, final PluginAccessor pluginAccessor)
    {
        lazyReferenceTransformerCache = new ResettableLazyReference<Map<String, Object>> ()
        {
            @Override
            protected Map<String, Object> create()
            {
                // Preparing data into more efficient format for querying, mapping descriptors by keys.
                Map<String, Object> keysToDescriptors = new HashMap<String, Object>();

                // Content transformers.
                List<ContentTransformerModuleDescriptor> contentTransformerDescriptor = pluginAccessor.getEnabledModuleDescriptorsByClass(ContentTransformerModuleDescriptor.class);
                for (ContentTransformerModuleDescriptor descriptor : contentTransformerDescriptor)
                {
                    if (!keysToDescriptors.containsKey(descriptor.getKey()))
                    {
                        keysToDescriptors.put(descriptor.getKey(), descriptor);
                    }

                    String aliasKey = descriptor.getAliasKey();
                    if ((aliasKey != null) && !keysToDescriptors.containsKey(aliasKey))
                    {
                        keysToDescriptors.put(aliasKey, descriptor);
                    }
                }

                // Modern transformers.
                List<UrlReadingWebResourceTransformerModuleDescriptor> descriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(UrlReadingWebResourceTransformerModuleDescriptor.class);
                for (UrlReadingWebResourceTransformerModuleDescriptor descriptor : descriptors)
                {
                    if (!keysToDescriptors.containsKey(descriptor.getKey()))
                    {
                        keysToDescriptors.put(descriptor.getKey(), descriptor);
                    }

                    String aliasKey = descriptor.getAliasKey();
                    if ((aliasKey != null) && !keysToDescriptors.containsKey(aliasKey))
                    {
                        keysToDescriptors.put(aliasKey, descriptor);
                    }
                }

                // Legacy transformers.
                List<WebResourceTransformerModuleDescriptor> deprecatedDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceTransformerModuleDescriptor.class);
                for (WebResourceTransformerModuleDescriptor descriptor : deprecatedDescriptors)
                {
                    // Using deprecated transformer only if there's no modern transformer with the same key or alias-key.
                    if (!keysToDescriptors.containsKey(descriptor.getKey()))
                    {
                        keysToDescriptors.put(descriptor.getKey(), descriptor);
                    }
                }

                // Note - deprecated descriptor should be used only if there'skeysToDescriptorsptor with the same key.
                return keysToDescriptors;
            }
        };

        // This method should be called at the end of the constructor.
        // TODO find better way to register, move it outside of the constructor.
        pluginEventManager.register(this);
    }

    @PluginEventListener
    public void onPluginDisabled(PluginDisabledEvent event)
    {
        lazyReferenceTransformerCache.reset();
    }

    @PluginEventListener
    public void onPluginEnabled(PluginEnabledEvent event)
    {
        lazyReferenceTransformerCache.reset();
    }

    @PluginEventListener
    public void onPluginModuleEnabled(final PluginModuleEnabledEvent event)
    {
        lazyReferenceTransformerCache.reset();
    }

    @PluginEventListener
    public void onPluginModuleDisabled(final PluginModuleDisabledEvent event)
    {
        lazyReferenceTransformerCache.reset();
    }

    public Object getDescriptor(String transformerKey)
    {
        return lazyReferenceTransformerCache.get().get(transformerKey);
    }

}