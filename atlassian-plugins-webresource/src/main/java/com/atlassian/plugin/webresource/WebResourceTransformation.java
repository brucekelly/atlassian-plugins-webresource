package com.atlassian.plugin.webresource;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.transformer.ContentTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.UrlReadingContentTransformer;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import org.apache.commons.lang.Validate;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.helpers.Helpers.asContent;
import static com.atlassian.plugin.webresource.impl.helpers.Helpers.asDownloadableResource;

/**
 * Represents a set of transformer invocations for a specific web resource set and extension.  Transformers are retrieved
 * from the plugin system on request, not plugin initialisation, since plugin start order is indeterminate.
 *
 * @since 2.5.0
 */
public class WebResourceTransformation
{
    private final String extension;
    private final String type;
    private final Map<String, Element> transformerElements;
    private final Iterable<String> transformerKeys;
    private Logger log = LoggerFactory.getLogger(WebResourceTransformation.class);

    // TODO refactor it into the interface of the transformer.
    //
    // Mapping between transformer class and params it uses. It's a temporary workaround, the keys should be
    // returned by the transformer itself. But, it would needs too much changes in existing plugins.
    // So, instead we just manually checked parameters for most used transformers and hardcoded it here.
    // Eventually it should be deleted and replaced with a proper transformer API.
    public static Map<String, String[]> TRANSFORMER_PARAMETERS = buildTransformerParameters();
    public static Map<String, String[]> buildTransformerParameters()
    {
        HashMap<String, String[]> params = new HashMap<String, String[]>();
        params.put("com.atlassian.webresource.plugin.i18n.JsI18nTransformer", new String[]{"locale"});
        params.put("com.atlassian.plugins.less.LessWebResourceTransformerFactory", new String[]{});
        params.put("com.atlassian.soy.impl.webresource.SoyWebResourceTransformerFactory", new String[]{"locale"});
        return params;
    }

    public WebResourceTransformation(Element element)
    {
        Validate.notNull(element.attribute("extension"));

        this.type = element.attributeValue("extension");
        this.extension = "." + type;

        LinkedHashMap<String, Element> transformers = new LinkedHashMap<String, Element>();
        for (Element transformElement : (List<Element>)element.elements("transformer"))
        {
            transformers.put(transformElement.attributeValue("key"), transformElement);
        }
        transformerElements = Collections.unmodifiableMap(transformers);
        transformerKeys = transformerElements.keySet();
    }

    // TODO there are two matches - one that accepts ResourceLocation and another that accepts the type (as String).
    // And, they not exactly the same, because there are cases when transformers matched against
    //     "some-name.public.js".matches("public.js")
    // so, how it works isn't very obvious, maybe we should deprecate or change it.
    // See https://ecosystem.atlassian.net/browse/PLUGWEB-195

    public boolean matches(ResourceLocation location)
    {
        String loc = location.getLocation();
        if (loc == null || "".equals(loc.trim()))
        {
            loc = location.getName();
        }
        return loc.endsWith(extension);
    }

    public boolean matches(String location)
    {
        return location.equals(type);
    }

    public void addTransformParameters(TransformerCache transformerCache,
                                       TransformerParameters transformerParameters, UrlBuilder urlBuilder)
    {
        for (String key : transformerKeys)
        {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null))
            {
                if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor)
                {
                    UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorAsObject;
                    descriptor.getModule().makeUrlBuilder(transformerParameters).addToUrl(urlBuilder);
                }
                else if (descriptorAsObject instanceof ContentTransformerModuleDescriptor)
                {
                    ContentTransformerModuleDescriptor descriptor = (ContentTransformerModuleDescriptor) descriptorAsObject;
                    descriptor.getModule().makeUrlBuilder(transformerParameters).addToUrl(urlBuilder);
                }
                else if (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor)
                {
                    // Do nothing.
                }
                else
                {
                    throw new RuntimeException("invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }
            }
        }
    }

    public boolean containsOnlyPureUrlReadingTransformers(TransformerCache transformerCache)
    {
        for (String key : transformerKeys)
        {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null) && (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor))
            {
                return false;
            }
        }
        return true;
    }

    public Iterable<WebResourceTransformerModuleDescriptor> getDeprecatedTransformers(TransformerCache transformerCache)
    {
        List<WebResourceTransformerModuleDescriptor> deprecatedTransformers = new LinkedList<WebResourceTransformerModuleDescriptor>();
        for (String key : transformerKeys)
        {
            Object descriptorAsObject = transformerCache.getDescriptor(key);
            if ((descriptorAsObject != null) && (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor))
            {
                deprecatedTransformers.add((WebResourceTransformerModuleDescriptor) descriptorAsObject);
            }
        }
        return deprecatedTransformers;
    }

    public Content transform(TransformerCache transformerCache, final Content content, ResourceLocation resourceLocation,
        String pluginKey, String filePath, String webResourceKey, QueryParams params, String sourceUrl)
    {
        Content lastContent = content;

        for (String transformerKey : transformerKeys)
        {
            Object descriptorAsObject = transformerCache.getDescriptor(transformerKey);
            if (descriptorAsObject != null)
            {
                if (descriptorAsObject instanceof ContentTransformerModuleDescriptor)
                {
                    ContentTransformerModuleDescriptor descriptor = (ContentTransformerModuleDescriptor) descriptorAsObject;
                    TransformerParameters transformerParameters = new TransformerParameters(pluginKey, webResourceKey);
                    UrlReadingContentTransformer transformer = descriptor.getModule().makeResourceTransformer(transformerParameters);
                    lastContent = transformer.transform(lastContent, resourceLocation, params, sourceUrl);
                }
                else if (descriptorAsObject instanceof UrlReadingWebResourceTransformerModuleDescriptor)
                {
                    UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorAsObject;
                    TransformableResource transformableResource = new TransformableResource(resourceLocation, filePath, asDownloadableResource(lastContent));
                    TransformerParameters transformerParameters = new TransformerParameters(pluginKey, webResourceKey);
                    UrlReadingWebResourceTransformer transformer = descriptor.getModule().makeResourceTransformer(transformerParameters);
                    lastContent = asContent(transformer.transform(transformableResource, params), null, true);
                }
                else if (descriptorAsObject instanceof WebResourceTransformerModuleDescriptor)
                {
                    WebResourceTransformerModuleDescriptor descriptor = (WebResourceTransformerModuleDescriptor) descriptorAsObject;
                    lastContent = asContent(
                        descriptor.getModule().transform(transformerElements.get(transformerKey), resourceLocation,
                            filePath, asDownloadableResource(lastContent)), null, true
                    );
                }
                else
                {
                    throw new RuntimeException("invalid usage, transformer descriptor expected but got " + descriptorAsObject);
                }
            }
            else
            {
                log.warn("Web resource transformer {} not found for resource {}, skipping", transformerKey,
                    resourceLocation.getName());
            }
        }
        return lastContent;
    }

    public String toString(){
        return getClass().getSimpleName() + " with " + transformerKeys.toString() + " transformers";
    }

    /**
     * Filters the parameters and returns only those actually used by this set of transformers.
     */
    public Map<String, String> leaveOnlyUsedParameters(TransformerCache transformerCache, Map<String, String> params)
    {
        // Getting list of transformer classes.
        List<String> transformerClasses = new ArrayList<String>();
        for (String key : transformerKeys)
        {
            Object descriptorObj = transformerCache.getDescriptor(key);
            if (descriptorObj instanceof UrlReadingWebResourceTransformerModuleDescriptor)
            {
                UrlReadingWebResourceTransformerModuleDescriptor descriptor = (UrlReadingWebResourceTransformerModuleDescriptor) descriptorObj;
                transformerClasses.add(descriptor.getModule().getClass().getName());
            }
            else if (descriptorObj instanceof ContentTransformerModuleDescriptor)
            {
                ContentTransformerModuleDescriptor descriptor = (ContentTransformerModuleDescriptor) descriptorObj;
                transformerClasses.add(descriptor.getModule().getClass().getName());
            }
            else if (descriptorObj == null)
            {
                // Missing transformer, do nothing.
            }
            else
            {
                throw new RuntimeException("error, trying to determine used parameters for legacy transformer!");
            }
        }

        // Calculating list of keys used by transformers.
        Set<String> usedKeys = new HashSet<String>();
        for (String klass : transformerClasses)
        {
            String[] keys = TRANSFORMER_PARAMETERS.get(klass);
            if (keys == null)
            {
                // If there's at least one transformer missing we can't calculate used parameters and returning
                // the full original parameters.
                return params;
            }
            Collections.addAll(usedKeys, keys);
        }

        // Filtering only used parameters.
        Map<String, String> usedParameters = new HashMap<String, String>();
        for (String key : usedKeys)
        {
            if (params.containsKey(key))
            {
                usedParameters.put(key, params.get(key));
            }
        }
        return usedParameters;
    }
}