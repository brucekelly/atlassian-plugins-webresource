package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A helper classes that knows how to perform certain operations on ContextBatch beans.
 */
public class ContextBatchOperations
{
    /**
     * Merges context batches into a single context batch. 
     * <p/>
     * <strong>Note:</strong>you can only merge batches if all batches <strong>do not</strong> 
     * have any excluded contexts. The problem is that with excluded contexts you can't know
     * if the batch you are merging with should have resources (and resource parameters)
     * removed and you are therefore going to end up with a merged batch with a potentially
     * wrong hash. (See {@link com.atlassian.plugin.webresource.legacy.ContextBatch#createHash()})
     * 
     * @param batchesToMerge - one or more batches to merge together 
     * @return a single context batch which is the result of merging all the supplied batches.
     * @throw IllegalArgumentException if any of the batches have excludedContexts
     */
    public ContextBatch merge(final Collection<ContextBatch> batchesToMerge)
    {
        if (CollectionUtils.isEmpty(batchesToMerge))
            return null;
        
        if (batchesToMerge.size() == 1)
            return batchesToMerge.iterator().next();

        // the ordering of contexts must be maintained for consistency of resources within the batch
        final LinkedHashSet<String> includedContexts = new LinkedHashSet<String>();
        final Set<WebResourceModuleDescriptor> resources = new HashSet<WebResourceModuleDescriptor>();

        boolean removeSuperResources = false;
        for (ContextBatch batch : batchesToMerge)
        {
            if (!Iterables.isEmpty(batch.getExcludedContexts()))
                throw new IllegalArgumentException("The ContextBatch " + batch.getKey() + " has excludedContexts.");
            removeSuperResources |= batch.isRemoveSuperResources();
            includedContexts.addAll(batch.getContexts());
            Iterables.addAll(resources, batch.getResources());
        }

        return new ContextBatch(new ArrayList<String>(includedContexts), null, resources, removeSuperResources);
    }
    
    /**
     * Subtract ContextBatches from the supplied operand, creating a new ContextBatch (unless there are no
     * batches to subtract in which case the supplied operand is returned unchanged).
     * <p/>
     * Subtraction for a ContextBatch means the removal of any WebResourceModuleDescriptor that exist within
     * the ContextBatch being subtracted. Consequently it also means that some PluginResourceBatchParams
     * may also be removed (if there is no longer an applicable PluginResource). 
     * <p/> 
     * <strong>Note:</strong>you can only subtract batches if all batches <strong>do not</strong> 
     * have any excluded contexts. The problem is that with excluded contexts you end up with the
     * possibility of subtracting a subtracted context. And since the already subtracted context no
     * longer has reference to the WebResourceModuleDescriptor it caused to be removed you cannot
     * ensure that the resultant ContextBatch also has them removed.
     * 
     * @param operand the ContextBatch to be operated upon
     * @param batchesToSubtract the ContextBatches to be subtracted.
     * @return the ContextBatch resulting in subtracting the batchesToSubtract.
     * @throw IllegalArgumentException if any of the batchesToSubtract already have excluded contexts.
     */
    public ContextBatch subtract(final ContextBatch operand, final Collection<ContextBatch> batchesToSubtract)
    {
        if (CollectionUtils.isEmpty(batchesToSubtract))
            return operand;

        // use linked hash map: preserve a consistent deterministic ordering irrespective of hash algorithm
        Collection<String> excludedContexts = new LinkedHashSet<String>();
        Iterables.addAll(excludedContexts, operand.getExcludedContexts());
        final Collection<WebResourceModuleDescriptor> resources = new LinkedHashSet<WebResourceModuleDescriptor>();
        Iterables.addAll(resources, operand.getResources());

        boolean removeSuperResources = false;
        for (ContextBatch subtract : batchesToSubtract)
        {
            if (!Iterables.isEmpty(subtract.getExcludedContexts()))
            {
                throw new IllegalArgumentException("The ContextBatch " + subtract.getKey() + " has excludedContexts.");
            }

            removeSuperResources |= subtract.isRemoveSuperResources();
            
            Iterables.addAll(excludedContexts, subtract.getContexts());
            Iterable<WebResourceModuleDescriptor> subtractResources = subtract.getResources();
            for (WebResourceModuleDescriptor resource : subtractResources)
            {
                resources.remove(resource);
            }
        }

        return new ContextBatch(operand.getContexts(),excludedContexts, resources, removeSuperResources);
    }}
