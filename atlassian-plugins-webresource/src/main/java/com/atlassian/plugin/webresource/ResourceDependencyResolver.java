package com.atlassian.plugin.webresource;

/**
 * @deprecated since v3.3.2
 */
@Deprecated
public interface ResourceDependencyResolver
{
}
