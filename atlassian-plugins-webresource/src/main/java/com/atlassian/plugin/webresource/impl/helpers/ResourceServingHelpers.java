package com.atlassian.plugin.webresource.impl.helpers;

import com.atlassian.plugin.cache.filecache.Cache;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.ResourceContentAnnotator;
import com.atlassian.plugin.webresource.WebResourceTransformation;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.WebResource;
import com.atlassian.plugin.webresource.impl.support.LineCountingProxyOutputStream;
import com.atlassian.plugin.webresource.impl.support.SourceMapJoinerStub;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.transformer.MinificationTransformer;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.SourceMapJoiner;
import com.atlassian.sourcemap.Util;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.buildUrl;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Stateless helper functions providing support for serving resources.
 *
 * @since v6.3
 */
public class ResourceServingHelpers extends UrlGenerationHelpers
{
    /**
     * Get resource for Web Resource or Plugin, also resolves relative paths.
     *
     * @param completeKey key of web resource or plugin.
     * @param resourceName name of Resource.
     */
    public static Resource getResource(RequestCache requestCache, String completeKey, String resourceName)
    {
        Resource resource = getWebResourceResource(requestCache, completeKey, resourceName);
        if (resource == null)
        {
            resource = getResourceRelativeToWebResource(requestCache, completeKey, resourceName);
        }
        if (resource == null)
        {
            resource = getModuleResource(requestCache.getGlobals(), completeKey, resourceName);
        }
        if (resource == null)
        {
            resource = getPluginResource(requestCache.getGlobals(), completeKey, resourceName);
        }
        if (resource == null)
        {
            resource = getResourceRelativeToPlugin(requestCache.getGlobals(), completeKey, resourceName);
        }
        return resource;
    }

    /**
     * Get Resource for one of Web Resources.
     *
     * @param bundles list of keys of Web Resources.
     * @param resourceName name of Resource.
     */
    public static Resource getResource(RequestCache requestCache, Collection<String> bundles, String resourceName)
    {
        Resource resource = null;
        for (String key : bundles)
        {
            if ((resource = getResource(requestCache, key, resourceName)) != null)
            {
                break;
            }
        }
        return resource;
    }

    /**
     * Apply conditions and transformations to list of Resources and generate resulting Batch.
     *
     * @param resources list of Resources
     * @param params http params.
     * @return resulting content.
     */
    public static Content transform(final Globals globals, final String type,
        final Supplier<Collection<Resource>> resources, final Map<String, String> params)
    {
        return new ContentImpl(null, true)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                ResourceContentAnnotator[] annotators = globals.getConfig().getContentAnnotators(type);
                try
                {
                    SourceMapJoiner sourceMapJoiner = isSourceMapEnabled ? new SourceMapJoiner() : new
                        SourceMapJoinerStub();
                    boolean isFirst = true;
                    for (Resource resource : resources.get())
                    {
                        // Resources should be delimited by new line, it's needed for source map to work properly.
                        if (isFirst)
                        {
                            isFirst = false;
                        }
                        else
                        {
                            out.write('\n');
                        }

                        int offset = 0;
                        // Byte counting proxy needed to calculate the length of the resource,
                        // needed for the source map generation.
                        LineCountingProxyOutputStream lineCountingStream = new LineCountingProxyOutputStream(out);
                        if (isSourceMapEnabled)
                        {
                            out = lineCountingStream;
                        }
                        for (ResourceContentAnnotator annotator : annotators)
                        {
                            offset += annotator.beforeOffset();
                            annotator.before(resource, out);
                        }

                        Content content = transformSafely(globals, resource, params);
                        SourceMap sourceMap = content.writeTo(out, isSourceMapEnabled);

                        int resourceLength = lineCountingStream.getLinesCount() - offset;

                        // Apply the after annotators in reverse order
                        for (int i = annotators.length - 1; i >= 0; i--)
                        {
                            annotators[i].after(resource, out);
                        }

                        // If there's no source map generated by transformers,
                        // the 1to1 source map should be generated because
                        // the source map needed to generate the batch source map.
                        // And, instead of the source url the url of transformed single resource should be used (because
                        // transformers without source map support may be already applied).
                        if (isSourceMapEnabled && sourceMap == null)
                        {
                            String singleResourceUrl = globals.getRouter().resourceUrlWithoutHash(resource, params);
                            sourceMap = Util.create1to1SourceMap(resourceLength, singleResourceUrl);
                        }

                        // Adding the resource source map to the batch source map.
                        sourceMapJoiner.add(sourceMap, lineCountingStream.getLinesCount(), offset);
                    }
                    return sourceMapJoiner.join();
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     *
     * @param resource the resource.
     * @param params http params.
     * @return resulting content.
     */
    public static Content transform(final Globals globals, final Resource resource, final Map<String, String> params)
    {
        final Content content = transformWithoutCache(globals, resource, params);
        // It is possible to cache in case of enabled source map too, but it could be costly because the source map
        // needs to be serialized and de-serialized.
        if (resource.getParent().hasLegacyTransformers() || globals.getConfig().isSourceMapEnabledFor(resource.getType()))
        {
            return content;
        }
        else
        {
            return new ContentImpl(content.getContentType(), content.isTransformed())
            {
                @Override
                public SourceMap writeTo(final OutputStream out, final boolean isSourceMapEnabled)
                {
                    if (isSourceMapEnabled)
                    {
                        Support.LOGGER.warn("internal error, source map could not be used with incremental transformer cache!");
                    }

                    String key = buildUrl(resource.getKey() + ":" + resource.getName(), params);
                    globals.getTemporaryIncrementalCache().cache("transformer", key, out, new Cache.StreamProvider()
                    {
                        @Override
                        public void write(OutputStream out)
                        {
                            content.writeTo(out, isSourceMapEnabled);
                        }
                    });
                    return null;
                }
            };
        }
    }

    /**
     * The `params` contains all the params from url, so if we build the key using all params it will
     * be correct but inefficient for the caching. It's inefficient because not all params are used by transformers for
     * the given resource.
     *
     * If we can detect the subset of params that's actually used by transformers
     * fot the given resource we would be able to provide better cache hit ratio.
     * This method calculates the key in the smart way, using only those parameters that are actually used.
     */
    public static String buildKey(Globals globals, Resource resource, Map<String, String> params)
    {
        if (resource.getParent() instanceof WebResource)
        {
            // Calculating parameters used by transformers.
            WebResource webResource = (WebResource) resource.getParent();
            List<WebResourceTransformation> transformations = webResource.getTransformers().getTransformations();
            Map<String, String> usedParameters = new HashMap<String, String>();
            for (WebResourceTransformation transformation : transformations)
            {
                usedParameters.putAll(transformation.leaveOnlyUsedParameters(
                    globals.getConfig().getTransformerCache(), params));
            }

            // Calculating parameters used by static transformers.
            for (String key : StaticTransformers.PARAMETERS_USED)
            {
                if (params.containsKey(key))
                {
                    usedParameters.put(key, params.get(key));
                }
            }

            // Creating key using only params actually used by transformer.
            return buildUrl(resource.getKey() + ":" + resource.getName(), usedParameters);
        }
        else
        {
            // If it's not a web resource there's no transformers and it's always the same.
            return resource.getKey();
        }
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     *
     * @param resource the resource.
     * @param params http params.
     * @return resulting content.
     */
    public static Content transformWithoutCache(final Globals globals, final Resource resource, final Map<String, String> params)
    {
        checkArgument(!resource.isRedirect(), "can't transform redirect resource!");

        Content content = resource.getContent();
        if (!resource.isTransformable())
        {
            return content;
        }

        String sourceUrl = globals.getRouter().sourceUrl(resource);
        content = MinificationTransformer.minify(globals.getConfig(), resource, content, sourceUrl);
        content = applyTransformers(globals, resource, content, params, sourceUrl);
        content = applyStaticTransformers(globals, resource, content, params, sourceUrl);
        return content;
    }

    /**
     * Transform given Resource by applying Transformers and Static Transformers.
     * In case of any error it will be intercepted and empty content returned.
     *
     * @param resource the resource.
     * @param params http params.
     * @return resulting content.
     */
    public static Content transformSafely(Globals globals, Resource resource, Map<String, String> params)
    {
        Content content;
        try
        {
            content = transform(globals, resource, params);
        }
        catch (RuntimeException e)
        {
            Support.LOGGER.warn("can't transform resource " + resource.getKey() + ":" + resource.getName(), e);
            content = buildEmptyContent(null);
        }
        return buildSafeContent(content);
    }

    /**
     * Select Resources that should be included in batch.
     *
     * @param type type of batch.
     * @param params http params of batch.
     * @return list of selected Resources.
     */
    public static Predicate<Resource> shouldBeIncludedInBatch(final String type, final Map<String, String> params)
    {
        return new Predicate<Resource>()
        {
            @Override
            public boolean apply(Resource resource)
            {
                return type.equals(resource.getType()) && resource.isBatchable(params);
            }
        };
    }

       /**
     * Adapter, turns Content into DownloadableResource.
     */
    public static DownloadableResource asDownloadableResource(final Content content)
    {
        return new DownloadableResource()
        {
            @Override
            public boolean isResourceModified(HttpServletRequest request, HttpServletResponse response)
            {
                throw new RuntimeException("not supported for content wrapper!");
            }

            @Override
            public void serveResource(HttpServletRequest request, HttpServletResponse response)
            {
                throw new RuntimeException("not supported for content wrapper!");
            }

            @Override
            public void streamResource(OutputStream out)
            {
                content.writeTo(out, false);
            }

            @Override
            public String getContentType()
            {
                return content.getContentType();
            }
        };
    }

    /**
     * Adapter, turns DownloadableResource into Content.
     */
    public static Content asContent(final DownloadableResource downloadableResource, final SourceMap sourceMap,
            boolean isTransformed)
    {
        return new ContentImpl(downloadableResource.getContentType(), isTransformed)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                try
                {
                    downloadableResource.streamResource(out);
                }
                catch (DownloadException e)
                {
                    throw new RuntimeException(e);
                }
                return sourceMap;
            }
        };
    }

    public static Content buildEmptyContent(String contentType)
    {
        return new ContentImpl(contentType, false)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                return null;
            }
        };
    }

    /**
     * Intercept any exception and ignores it.
     */
    public static Content buildSafeContent(final Content content)
    {
        return new Content()
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                try
                {
                    return content.writeTo(out, isSourceMapEnabled);
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("error in `Content.writeTo`", e);
                    return null;
                }
            }

            @Override
            public String getContentType()
            {
                try
                {
                    return content.getContentType();
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("error in `Content.getContentType`", e);
                    return null;
                }
            }

            @Override
            public boolean isTransformed()
            {
                try
                {
                    return content.isTransformed();
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("error in `Content.isTransformed`", e);
                    return false;
                }
            }
        };
    }

    /**
     * Apply Transformers to Resource.
     *
     * @param resource resource.
     * @param content content of resource.
     * @param params http params.
     * @param sourceUrl url of source code for resource.
     * @return resulting content.
     */
    protected static Content applyTransformers(final Globals globals, final Resource resource, final Content content,
            final Map<String, String> params, final String sourceUrl)
    {
        if (resource.getParent() instanceof WebResource)
        {
            WebResource webResource = (WebResource) resource.getParent();
            CachedTransformers transformers = webResource.getTransformers();
            if (transformers == null)
            {
                return content;
            }

            Content lastContent = content;
            for (WebResourceTransformation transformation : transformers.getTransformations())
            {
                // There's another version of `matches` that matches against extension,
                // but it can't be used because sometimes
                // transformers are matched against things like "some-name.public.js".matches("public.js") so if it
                // would be
                // changed to matching the extension it wouldn't work.
                if (transformation.matches(resource.getResourceLocation()))
                {
                    lastContent = transformation.transform(globals.getConfig().getTransformerCache(), lastContent,
                            resource.getResourceLocation(), webResource.getPluginKey(), resource.getFilePath(),
                            webResource.getWebResourceKey(), QueryParams.of(params), sourceUrl);
                }
            }
            return lastContent;
        }
        else
        {
            return content;
        }
    }

    /**
     * Apply Static Transformers to Resource.
     *
     * @param resource resource.
     * @param content content of resource.
     * @param params http params.
     * @param sourceUrl url of source code for resource.
     * @return resulting content.
     */
    protected static Content applyStaticTransformers(final Globals globals, Resource resource, Content content,
            Map<String, String> params, String sourceUrl)
    {
        if (resource.getParent() instanceof WebResource)
        {
            WebResource webResource = (WebResource) resource.getParent();
            return globals.getConfig().getStaticTransformers().transform(content,
                    new TransformerParameters(webResource.getPluginKey(), webResource.getWebResourceKey()),
                    resource.getResourceLocation(), QueryParams.of(params), sourceUrl);
        }
        else
        {
            return content;
        }
    }

    /**
     * Get Resource for Web Resource.
     *
     * @param completeKey complete key of Web Resource.
     * @param resourceName name of Resource.
     */
    protected static Resource getWebResourceResource(RequestCache requestCache, String completeKey,
            String resourceName)
    {
        Bundle bundle = requestCache.getSnapshot().get(completeKey);
        if (bundle == null)
        {
            return null;
        }
        return bundle.getResources(requestCache).get(resourceName);
    }

    /**
     * Get Resource for non WebResource Module.
     *
     * @param completeKeyOrPluginKey complete key or plugin key.
     * @param resourceName name of Resource.
     */
    public static Resource getModuleResource(Globals globals, String completeKeyOrPluginKey, String resourceName)
    {
        return globals.getConfig().getModuleResource(completeKeyOrPluginKey, resourceName);
    }

    /**
     * Get Resource relative to Web Resource.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    protected static Resource getResourceRelativeToWebResource(RequestCache requestCache, String completeKey,
            String resourceName)
    {
        Bundle bundle = requestCache.getSnapshot().get(completeKey);
        if (bundle == null)
        {
            return null;
        }
        String filePath = "";
        Resource resource = null;
        while (resource == null)
        {
            String[] parts = splitLastPathPart(resourceName);
            if (parts == null)
            {
                return null;
            }
            resourceName = parts[0];
            filePath = parts[1] + filePath;
            resource = bundle.getResources(requestCache).get(resourceName);
        }

        final String finalFilePath = filePath;
        return new Resource(resource.getParent(), resource.getResourceLocation(), resource.getType())
        {
            @Override
            public String getFilePath()
            {
                return finalFilePath;
            }
        };
    }

    /**
     * Get Resource for Plugin.
     *
     * @param completeKeyOrPluginKey complete key or plugin key.
     * @param resourceName name of Resource.
     */
    protected static Resource getPluginResource(Globals globals, String completeKeyOrPluginKey, String resourceName)
    {
        return globals.getConfig().getPluginResource(getPluginKey(completeKeyOrPluginKey), resourceName);
    }

    /**
     * Get Resource relative to Plugin.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    protected static Resource getResourceRelativeToPlugin(Globals globals, String completeKeyOrPluginKey,
            String resourceName)
    {
        String pluginKey = getPluginKey(completeKeyOrPluginKey);
        String filePath = "";
        Resource resource = null;
        while (resource == null)
        {
            String[] parts = splitLastPathPart(resourceName);
            if (parts == null)
            {
                return null;
            }
            resourceName = parts[0];
            filePath = parts[1] + filePath;
            resource = globals.getConfig().getPluginResource(pluginKey, resourceName);
        }

        final String finalFilePath = filePath;
        return new Resource(resource.getParent(), resource.getResourceLocation(), resource.getType())
        {
            @Override
            public String getFilePath()
            {
                return finalFilePath;
            }
        };
    }

    /**
     * Split path into parent folder and name.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    public static String[] splitLastPathPart(String resourcePath)
    {
        int indexOfSlash = resourcePath.lastIndexOf('/');
        // skip over the trailing slash
        if (resourcePath.endsWith("/"))
        {
            indexOfSlash = resourcePath.lastIndexOf('/', indexOfSlash - 1);
        }
        if (indexOfSlash < 0)
        {
            return null;
        }
        return new String[] { resourcePath.substring(0, indexOfSlash + 1), resourcePath.substring(indexOfSlash + 1) };
    }

        /**
     * In case of Plugin Key - returns the same key, in case of Web Resource key - extracts Plugin key from it.
     */
    protected static String getPluginKey(String completeKeyOrPluginKey)
    {
        return completeKeyOrPluginKey.split(":")[0];
    }
}
