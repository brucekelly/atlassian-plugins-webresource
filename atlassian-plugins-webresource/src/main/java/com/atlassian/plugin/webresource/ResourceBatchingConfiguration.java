package com.atlassian.plugin.webresource;

import com.atlassian.annotations.ExperimentalApi;

import java.util.List;

/**
 * Resource batching configuration for the {@link WebResourceManagerImpl}.
 *
 * Currently contains the configuration for batch support.
 */
public interface ResourceBatchingConfiguration
{
    /**
     * Gets whether web resources in different resource modules should be batched together.
     * @return true if web resources in different resource modules should be batched together
     */
    boolean isSuperBatchingEnabled();

    /**
     * Gets the list of resource plugin modules that should be included in the superbatch, in the order that
     * they should be batched. No dependency resolution is performed, so it is important that the configuration
     * includes all dependent resources in the right order.
     *
     * Any call to {@link WebResourceManager#requireResource} for one of these resources will be a no-op,
     * and any dependency resolution for resources will stop if the dependency is in the superbatch.
     * @return a list of resource plugin modules that should be included in the superbatch.
     */
    List<String> getSuperBatchModuleCompleteKeys();

    /**
     * Determines whether web resources in the same context should be batched together.
     * @return true if web resources in the same context are batched together.
     * @since 2.9.0
     */
    boolean isContextBatchingEnabled();

    /**
     * Determines whether plugin resources of the same type defined within a single web resource
     * are batched into one file.
     * @return true if plugin resources in the same web resource are batched together.
     * @since 2.9.0
     */
    boolean isPluginWebResourceBatchingEnabled();
    
    /**
     * When JavaScript resources are batched, each script within the batch can be wrapped in 
     * its own try/catch block to prevent an exception breaking the whole batch. This behaviour
     * is controlled by this property.
     * 
     * @return true if try/catch wrapping of JavaScript batches is enabled.
     * @since 2.14
     */
    boolean isJavaScriptTryCatchWrappingEnabled();

    /**
     * @return true if the HTML tags created for batch resources should include data attributes listing the 
     * contents of the batch. This would be useful to turn on if you want the front end client to be able
     * to track what resources and contexts it is loading. 
     * 
     * @since 2.13.0
     */
    boolean isBatchContentTrackingEnabled();

    /**
     * @return true if you want the WRM to split context batches it would otherwise merge together.
     * This may be necessary in IE8,IE9 because they have a 4095 rules-per-css limit, and merged context
     * batches are likely to produce big files.
     * <p>
     * This method is called multiple times (at least once per request) and its value is not cached.
     * Therefore, implementations can perform request-local logic. That is, they can browser-sniff
     * for IE.
     */
    @ExperimentalApi
    boolean resplitMergedContextBatchesForThisRequest();

    /**
     * Enables generation of the Source Map.
     *
     * Could be enabled in production too with `optimiseSourceMapsForDevelopment` set to `false`, with such setting
     * it won't slow down the instance.
     *
     * @since 3.3
     */
    @ExperimentalApi
    boolean isSourceMapEnabled();

    /**
     * Source Map could be optimised for production or development.
     *
     * In production the JS & CSS generation will be fast (exactly the same as without the Source Map) but the
     * Source Map generation will be slow.
     *
     * In development the JS & CSS generation will be a little slower but the Source Map generation will be fast.
     *
     * It's better to enable this option during development (it **must** be disabled in production).
     *
     * @since 3.4
     */
    @ExperimentalApi
    boolean optimiseSourceMapsForDevelopment();
}