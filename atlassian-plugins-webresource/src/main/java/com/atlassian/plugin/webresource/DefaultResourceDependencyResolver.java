package com.atlassian.plugin.webresource;

/**
 * @deprecated since 3.3.2
 */
public class DefaultResourceDependencyResolver implements ResourceDependencyResolver
{
    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public DefaultResourceDependencyResolver(final WebResourceIntegration webResourceIntegration, final ResourceBatchingConfiguration batchingConfiguration)
    {
    }
}
