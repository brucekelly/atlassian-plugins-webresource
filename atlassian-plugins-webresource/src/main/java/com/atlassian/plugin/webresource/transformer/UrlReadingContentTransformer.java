package com.atlassian.plugin.webresource.transformer;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;


/**
 * The part of a transformer that reads the config and url params and does the actual resource transformation and
 * generates the Source Map.
 *
 * @since 3.3
 */
@ExperimentalApi
public interface UrlReadingContentTransformer
{
    /**
     * Transforms the downloadable resource by returning a new one.  This is invoked when the resource is being served.
     *
     * @param content content to transform
     * @param params query params
     * @return The new resource representing the transformed resource and the source map.
     */
    Content transform(Content content, ResourceLocation location, QueryParams params, String sourceUrl);
}