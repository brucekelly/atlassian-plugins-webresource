package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * Simple wrapper for {@link com.atlassian.plugin.webresource.condition.UrlReadingCondition} implementations.
 *
 * Handles encoding a single boolean into the url. The convention here is that if the condition is true, it is added
 * the querystring; if false it is not added to the querystring.
 *
 * @since v3.1.0
 */
public abstract class SimpleUrlReadingCondition implements UrlReadingCondition
{
    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {
        // Default implementation is a no-op
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
        if (isConditionTrue())
        {
            urlBuilder.addToQueryString(queryKey(), String.valueOf("true"));
        }
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        return Boolean.valueOf(params.get(queryKey()));
    }

    /**
     * @return true if this condition passes. This is evaluated at url generation time.
     */
    protected abstract boolean isConditionTrue();

    /**
     * @return the key to append to the query string
     */
    protected abstract String queryKey();
}
