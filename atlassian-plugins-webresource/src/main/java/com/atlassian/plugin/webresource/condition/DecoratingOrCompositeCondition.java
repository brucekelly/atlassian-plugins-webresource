package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.QueryParams;

import java.util.Map;

/**
 * Implementation of DecoratingCompositeCondition that fits the new UrlReadingCondition interface
 * @since v3.0
 */
class DecoratingOrCompositeCondition extends DecoratingCompositeCondition
{
    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        // If this is being called, there are either no legacy components OR at least one legacy condition returned true in
        // shouldDisplayImmediate(). DecoratingLegacyCondition.shouldDisplay() returns true - which is how this is
        // evaluating the OR of legacy + non-legacy conditions.
        for (DecoratingCondition condition : conditions)
        {
            if (condition.shouldDisplay(params))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean shouldDisplayImmediate(Map<String, Object> context)
    {
        // This is evaluated at URL render time, and is only called if isLegacy() returns false - ie there is at least
        // one legacy Condition. If no conditions returns true here, the resource is not served to the client
        // (either standalone or in a batch).
        for (DecoratingCondition condition : conditions)
        {
            if (condition.shouldDisplayImmediate(context))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public DecoratingCondition invertCondition()
    {
        DecoratingAndCompositeCondition andCondition = new DecoratingAndCompositeCondition();
        for (DecoratingCondition condition : conditions)
        {
            andCondition.addCondition(condition.invertCondition());
        }
        return andCondition;
    }
}
