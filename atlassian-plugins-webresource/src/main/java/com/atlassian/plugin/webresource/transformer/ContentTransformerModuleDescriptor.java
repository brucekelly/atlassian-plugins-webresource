package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.NotNull;
import com.atlassian.util.concurrent.ResettableLazyReference;
import org.dom4j.Element;

/**
 * Defines a module descriptor for a {@link ContentTransformerFactory}.
 *
 * @since 3.3
 */
public class ContentTransformerModuleDescriptor extends AbstractModuleDescriptor<ContentTransformerFactory>
{
    private String aliasKey;

    public ContentTransformerModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    private final ResettableLazyReference<ContentTransformerFactory> moduleLazyReference = new
            ResettableLazyReference<ContentTransformerFactory>()
    {
        @Override
        protected ContentTransformerFactory create() throws Exception
        {
            return moduleFactory.createModule(moduleClassName, ContentTransformerModuleDescriptor.this);
        }
    };

    @Override
    public void init(@NotNull Plugin plugin, @NotNull Element element) throws PluginParseException
    {
        this.aliasKey = element.attributeValue("alias-key");
        super.init(plugin, element);
    }

    @Override
    public void disabled()
    {
        moduleLazyReference.reset();
        super.disabled();
    }

    @Override
    public ContentTransformerFactory getModule()
    {
        return moduleLazyReference.get();
    }

    public String getAliasKey()
    {
        return aliasKey;
    }
}