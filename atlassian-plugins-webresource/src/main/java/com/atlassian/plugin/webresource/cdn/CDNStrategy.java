package com.atlassian.plugin.webresource.cdn;

/**
 * Interface for CDN strategies. CDN strategies take a URL and transform it to another URL.
 *
 * @since v3.0.5
 */
public interface CDNStrategy
{
    /**
     * @return true if a CDN is supported
     */
    boolean supportsCdn();

    /**
     * Transforms the given URL to be served from the CDN. The given URL is assumed to be relative to the current host.
     * @param url relative URL to transformed
     * @return the URL to a CDN version of the given URL.
     */
    String transformRelativeUrl(String url);
}
