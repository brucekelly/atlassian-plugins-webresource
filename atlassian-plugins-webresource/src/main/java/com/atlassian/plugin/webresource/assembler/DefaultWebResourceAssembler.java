package com.atlassian.plugin.webresource.assembler;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableBoolean;
import com.atlassian.json.marshal.wrapped.JsonableNumber;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.helpers.UrlGenerationHelpers;
import com.atlassian.plugin.webresource.data.DefaultPluginDataResource;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.webresource.api.assembler.AssembledResources;
import com.atlassian.webresource.api.assembler.RequiredData;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.data.PluginDataResource;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

/**
 * Implementation of WebResourceAssembler.
 * @since v3.0
 */
class DefaultWebResourceAssembler implements WebResourceAssembler
{
    private final RequestState requestState;
    private AssembledResources assembledResourcesStub;
    private RequiredResources requiredResourcesStub;
    private RequiredData requiredDataStub;

    public DefaultWebResourceAssembler(final RequestState requestState)
    {
        this.requestState = requestState;

        assembledResourcesStub = new AssembledResources()
        {
            @Override
            public WebResourceSet drainIncludedResources()
            {
                Tuple<DefaultWebResourceSet, LinkedHashSet<String>> resolved = resolve();
                requestState.clearIncludedAndUpdateExcluded(resolved.getLast());
                return resolved.getFirst();
            }

            @Override
            public WebResourceSet peek()
            {
                return resolve().getFirst();
            }

            private Tuple<DefaultWebResourceSet, LinkedHashSet<String>> resolve()
            {
                UrlGenerationHelpers.Resolved resolved = Helpers.resolve(requestState);

                List<PluginDataResource> pluginDataResources = new LinkedList<PluginDataResource>();
                for (Map.Entry<String, Jsonable> entry : resolved.data.entrySet())
                {
                    pluginDataResources.add(new DefaultPluginDataResource(entry.getKey(), entry.getValue()));
                }
                return new Tuple<DefaultWebResourceSet, LinkedHashSet<String>>(
                    new DefaultWebResourceSet(requestState, pluginDataResources, resolved.urls),
                    resolved.excludedResolved
                );
            }
        };

        requiredResourcesStub = new RequiredResources()
        {
            @Override
            public RequiredResources requireWebResource(String completeKey)
            {
                requestState.getIncluded().add(completeKey);
                return this;
            }

            @Override
            public RequiredResources requireContext(String context)
            {
                requestState.getIncluded().add(Config.CONTEXT_PREFIX + ":" + context);
                return this;
            }

            @Override
            public RequiredResources exclude(Set<String> excludeWebResources, Set<String> excludeContexts)
            {
                excludeWebResources = null == excludeWebResources ? Collections.<String>emptySet() : Sets.newHashSet(excludeWebResources);
                excludeContexts = null == excludeContexts ? Sets.<String>newHashSet() : Sets.newHashSet(excludeContexts);

                LinkedHashSet<String> exclude = new LinkedHashSet<String>();
                for (String context : excludeContexts)
                {
                    exclude.add(Config.CONTEXT_PREFIX + ":" + context);
                }
                exclude.addAll(excludeWebResources);


                // the semantics of exclude() mean excluding the superbatch totally from the next drain()
                exclude.add(Config.SUPERBATCH_KEY);
                requestState.getIncluded().remove(Config.SUPERBATCH_KEY);

                LinkedHashSet<String> excludedResolved = Helpers.resolveExcluded(
                    requestState.getCache(), new ArrayList<String>(exclude), requestState.getExcluded());

                requestState.getExcluded().clear();
                requestState.getExcluded().addAll(excludedResolved);

                return this;
            }
        };

        requiredDataStub = new RequiredData()
        {
            @Override
            public RequiredData requireData(String key, Jsonable content)
            {
                requestState.getIncludedData().put(key, content);
                return this;
            }

            @Override
            public RequiredData requireData(String key, Number content)
            {
                requestState.getIncludedData().put(key, new JsonableNumber(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, String content)
            {
                requestState.getIncludedData().put(key, new JsonableString(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, Boolean content)
            {
                requestState.getIncludedData().put(key, new JsonableBoolean(content));
                return this;
            }

            @Override
            public RequiredData requireData(String key, Future<Jsonable> content)
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    @Override
    public AssembledResources assembled()
    {
        return assembledResourcesStub;
    }

    @Override
    public RequiredResources resources()
    {
        return requiredResourcesStub;
    }

    @Override
    public RequiredData data()
    {
        return requiredDataStub;
    }

    @Override
    public WebResourceAssembler copy()
    {
        return new DefaultWebResourceAssembler(requestState.deepClone());
    }
}
