package com.atlassian.plugin.webresource.legacy;

import com.atlassian.fugue.Option;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

//import static com.atlassian.plugin.webresource.legacy.AbstractBatchResourceBuilder.skipBatch;

/**
 * Default implementation of {@link PluginResourceLocator}.
 *
 * @since 2.2
 */
public class PluginResourceLocatorImpl implements PluginResourceLocator
{
    private static final Logger log = LoggerFactory.getLogger(PluginResourceLocatorImpl.class);

    final private PluginAccessor pluginAccessor;

    public PluginResourceLocatorImpl(WebResourceIntegration webResourceIntegration)
    {
        pluginAccessor = webResourceIntegration.getPluginAccessor();
    }

    @Override
    public List<PluginResource> getPluginResources(final String moduleCompleteKey)
    {
        Option<WebResourceModuleDescriptor> option = getDescriptor(moduleCompleteKey);
        if (option.isEmpty())
        {
            return Collections.emptyList();
        }

        WebResourceModuleDescriptor wrmd = option.get();
        final Set<PluginResource> resources = new LinkedHashSet<PluginResource>();

        for (final ResourceDescriptor resourceDescriptor : wrmd.getResourceDescriptors())
        {
            resources.add(new BatchPluginResource(moduleCompleteKey, wrmd.getCompleteKey()));
        }
        return ImmutableList.copyOf(resources);
    }

    private Option<WebResourceModuleDescriptor> getDescriptor(String moduleCompleteKey)
    {
        final ModuleDescriptor<?> moduleDescriptor = pluginAccessor.getEnabledPluginModule(moduleCompleteKey);
        if ((moduleDescriptor == null) || !(moduleDescriptor instanceof WebResourceModuleDescriptor))
        {
            log.error("Error loading resource \"{}\". Resource is not a Web Resource Module", moduleCompleteKey);
            return Option.none();
        }
        return Option.some((WebResourceModuleDescriptor) moduleDescriptor);
    }
}
