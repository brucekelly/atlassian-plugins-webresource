package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;

/**
 * Implementation of PluginUrlResource
 * @since 3.0
 */
abstract class DefaultPluginUrlResource<T extends PluginUrlResourceParams> implements PluginUrlResource<T>
{
    protected final ResourceUrl resourceUrl;

    public DefaultPluginUrlResource(ResourceUrl resourceUrl)
    {
        this.resourceUrl = resourceUrl;
    }

    @Override
    public String getStaticUrl(UrlMode urlMode)
    {
        return resourceUrl.getUrl(urlMode == UrlMode.ABSOLUTE);
    }

    @Override
    public String toString()
    {
        return resourceUrl.getKey() + ":" + resourceUrl.getName();
    }
}
