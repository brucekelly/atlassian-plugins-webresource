package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.assembler.PageBuilderService;

/**
 * Legacy subinterface of PageBuilderService that provides a clearRequestLocal() method
 * @since v3.0
 * @deprecated since 3.0. This only exists to support the broken behaviour of WebResourceManagerImpl.
 */
@Deprecated
public interface LegacyPageBuilderService extends PageBuilderService
{
    /**
     * Clears the request-local variables. This is not in the public API - it only exists to support WebResourceManager's
     * legacy behaviour that ignores already-included dependencies over multiple calls to methods like
     * {@link com.atlassian.plugin.webresource.WebResourceManager#requireResource(String)}.
     *
     * @deprecated since 3.0. This only exists to support the broken behaviour of WebResourceManagerImpl.
     */
    @Deprecated
    public void clearRequestLocal();
}
