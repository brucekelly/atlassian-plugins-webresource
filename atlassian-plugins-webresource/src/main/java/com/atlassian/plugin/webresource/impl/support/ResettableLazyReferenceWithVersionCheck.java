package com.atlassian.plugin.webresource.impl.support;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Cache result and its version, when retrieving it checks the version of the cached result and rebuild it if it's
 * different.
 *
 * @since 3.3
 */
public abstract class ResettableLazyReferenceWithVersionCheck<T>
{
    AtomicReference<Tuple<Integer, T>> atomicReference = new AtomicReference<Tuple<Integer, T>>(new Tuple<Integer,
            T>(null, null));

    public T get()
    {
        Tuple<Integer, T> original;
        Tuple<Integer, T> updated;
        do {
            original = atomicReference.get();
            if (original.getLast() != null && original.getFirst() == getVersion())
            {
                return original.getLast();
            }
            updated = new Tuple<Integer, T>(getVersion(), create());
        } while (!atomicReference.compareAndSet(original, updated));
        return updated.getLast();
    }

    public void reset()
    {
        atomicReference.set(new Tuple<Integer, T>(null, null));
    }

    protected abstract int getVersion();

    protected abstract T create();
}
