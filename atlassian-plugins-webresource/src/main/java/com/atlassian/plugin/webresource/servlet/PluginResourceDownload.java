package com.atlassian.plugin.webresource.servlet;

import com.atlassian.plugin.servlet.ContentTypeResolver;
import com.atlassian.plugin.servlet.DownloadException;
import com.atlassian.plugin.servlet.DownloadStrategy;
import com.atlassian.plugin.servlet.util.LastModifiedHandler;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;
import com.atlassian.plugin.webresource.impl.Globals;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


/**
 * A downloadable plugin resource, as described here: http://confluence.atlassian.com/display/JIRA/Downloadable+plugin+resource
 * It supports the download of single plugin resources as well as batching.
 * <p/>
 *
 * The URL that it parses for a single resource looks like this: <br>
 * <code>{server root}/download/resources/{plugin key}:{module key}/{resource name}</code>

 * The URL that it parses for a batch looks like this: <br>
 * <code>{server root}/download/batch/{plugin key}:{module key}/all.css?ieonly=true</code>
 */
public class PluginResourceDownload implements DownloadStrategy
{
    private Globals globals;
    private String characterEncoding = "UTF-8"; // default to sensible encoding

    public PluginResourceDownload()
    {
    }

    public PluginResourceDownload(PluginResourceLocator pluginResourceLocator, ContentTypeResolver contentTypeResolver, String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
        this.globals = pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt();
        globals.getConfig().setContentTypeResolver(contentTypeResolver);
    }

    public boolean matches(String urlPath)
    {
        return globals.getRouter().canDispatch(urlPath);
    }

    public void serveFile(HttpServletRequest originalRequest, final HttpServletResponse originalResponse) throws DownloadException
    {
        Request request = new Request(globals, originalRequest, characterEncoding);
        Response response = new Response(globals, request, originalResponse);

        // Checking if response required and responding with HEAD if not.
        if (request.isCacheable())
        {
            final LastModifiedHandler lastModifiedHandler = new LastModifiedHandler(new Date(originalRequest.getDateHeader("If-Modified-Since")));
            if (lastModifiedHandler.checkRequest(originalRequest, response.getOriginalResponse())) {
                // 304
                return;
            }
        }

        // Dispatching.
        globals.getRouter().dispatch(request, response);
    }

    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }

    public void setContentTypeResolver(ContentTypeResolver contentTypeResolver)
    {
        globals.getConfig().setContentTypeResolver(contentTypeResolver);
    }

    public void setPluginResourceLocator(PluginResourceLocator pluginResourceLocator)
    {
        this.globals = pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt();
    }
}

