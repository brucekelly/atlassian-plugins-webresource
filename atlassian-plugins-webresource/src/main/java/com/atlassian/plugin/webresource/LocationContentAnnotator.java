package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Add comment with information about the single resource included in batch.
 *
 * @since 3.3
 */
public class LocationContentAnnotator implements ResourceContentAnnotator
{
    @Override
    public void before(Resource resource, OutputStream out) throws IOException
    {
        out.write(String.format("/* module-key = '%s', location = '%s' */\n", resource.getKey(),
                resource.getLocation()).getBytes());
    }

    @Override
    public void after(Resource resource, OutputStream stream) throws IOException
    {
    }

    @Override
    public int beforeOffset()
    {
        return 1;
    }
}