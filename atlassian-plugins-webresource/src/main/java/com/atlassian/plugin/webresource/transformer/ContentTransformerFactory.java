 package com.atlassian.plugin.webresource.transformer;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Factory to create URL aware web resource transformers with Source Map.
 *
 * @since v3.3
 */
@ExperimentalApi
public interface ContentTransformerFactory
{
    /**
     * Return the URL builder for this transform
     *
     * @param parameters transformer parameters
     * @return an builder that contributes parameters to the URL
     */
    TransformerUrlBuilder makeUrlBuilder(TransformerParameters parameters);

    /**
     * Return the transformer for this transform
     *
     * @param parameters transformer parameters
     * @return a transformer that reads values from the url and transforms a webresource
     */
    UrlReadingContentTransformer makeResourceTransformer(TransformerParameters parameters);
}
