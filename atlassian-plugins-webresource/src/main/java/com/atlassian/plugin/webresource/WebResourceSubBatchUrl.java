package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.config.BatchType;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.util.Map;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public class  WebResourceSubBatchUrl extends ResourceUrl
{
    private final Globals globals;
    private final BatchType type;
    private final Map<String, String> params;
    private final String hash;
    private final String key;
    private final Helpers.SubBatch subBatch;
    private final boolean hasLegacyTransformers;

    public WebResourceSubBatchUrl(Globals globals, String key, Helpers.SubBatch subBatch, BatchType type,
        Map<String, String> params, String hash, boolean hasLegacyTransformers)
    {
        this.globals = globals;
        this.key = key;
        this.subBatch = subBatch;
        this.type = type;
        this.hash = hash;
        this.params = params;
        this.hasLegacyTransformers = hasLegacyTransformers;
    }

    @Override
    public String getName()
    {
        return getKey() + "." + type;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public String getType()
    {
        return type.toString();
    }

    public String getUrl(final boolean isAbsolute)
    {
        boolean isCacheable = Resource.isCacheableStatic(subBatch.resourcesParams);
        boolean isCdnSupported = !getBundle().hasLegacyConditions() && !hasLegacyTransformers;
        return globals.getRouter().cloneWithNewUrlMode(isAbsolute).webResourceBatchUrl(getKey(), getType(),
            getParams(), isCacheable, isCdnSupported, hash, getBundle().getVersion());
    }

    @Override
    public Map<String, String> getParams()
    {
        return params;
    }

    public Bundle getBundle()
    {
        return subBatch.bundles.get(0);
    }
}