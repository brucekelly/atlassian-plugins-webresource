package com.atlassian.plugin.webresource;

import java.awt.font.TextLayout;
import java.util.List;
import java.util.Map;

/**
 * An adapter between the current URL Generation code and previous URL Output code.
 *
 * @since v3.3
 */
public abstract class ResourceUrl
{
    public abstract String getName();

    public abstract String getKey();

    public abstract String getType();

    public abstract String getUrl(boolean isAbsolute);

    public abstract Map<String, String> getParams();
}