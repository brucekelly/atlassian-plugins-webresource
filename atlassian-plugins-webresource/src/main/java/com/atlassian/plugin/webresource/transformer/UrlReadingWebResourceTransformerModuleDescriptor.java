package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.atlassian.util.concurrent.NotNull;

import org.dom4j.Element;

/**
 * Defines a module descriptor for a {@link WebResourceTransformerFactory}.
 *
 * @since 3.0
 */
public class UrlReadingWebResourceTransformerModuleDescriptor extends AbstractModuleDescriptor<WebResourceTransformerFactory>
{
    private String aliasKey;

    public UrlReadingWebResourceTransformerModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    private final ResettableLazyReference<WebResourceTransformerFactory> moduleLazyReference = new ResettableLazyReference<WebResourceTransformerFactory>()
    {
        @Override
        protected WebResourceTransformerFactory create() throws Exception
        {
            return moduleFactory.createModule(moduleClassName, UrlReadingWebResourceTransformerModuleDescriptor.this);
        }
    };

    @Override
    public void init(@NotNull Plugin plugin, @NotNull Element element) throws PluginParseException
    {
        this.aliasKey = element.attributeValue("alias-key");
        super.init(plugin, element);
    }

    @Override
    public void disabled()
    {
        moduleLazyReference.reset();
        super.disabled();
    }

    @Override
    public WebResourceTransformerFactory getModule()
    {
        return moduleLazyReference.get();
    }

    public String getAliasKey()
    {
        return aliasKey;
    }
}
