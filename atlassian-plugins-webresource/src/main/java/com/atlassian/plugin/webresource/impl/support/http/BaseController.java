package com.atlassian.plugin.webresource.impl.support.http;

import com.atlassian.plugin.webresource.impl.Globals;

/**
 * Support for HTTP controller.
 *
 * @since 3.3
 */
public class BaseController
{
    final protected Globals globals;
    final protected Request request;
    final protected Response response;

    public BaseController(Globals globals, Request request, Response response)
    {
        this.globals = globals;
        this.request = request;
        this.response = response;
    }

    public void before()
    {
    }
}