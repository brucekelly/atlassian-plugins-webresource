package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResourceParams;
import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import org.apache.commons.lang.BooleanUtils;

import javax.annotation.Nullable;
import java.util.Collections;
import java.util.Map;

/**
 * @since v3.0
 */
abstract class DefaultPluginUrlResourceParams implements PluginUrlResourceParams
{
    protected final Map<String, String> params;

    public DefaultPluginUrlResourceParams(Map<String, String> params)
    {
        this.params = params;
    }

    @Override
    public String conditionalComment()
    {
        return params.get(Config.CONDITIONAL_COMMENT_PARAM_NAME);
    }

    @Override
    public boolean ieOnly()
    {
        return BooleanUtils.toBoolean(params.get(Config.IEONLY_PARAM_NAME));
    }

    @Override
    public Map<String, String> other()
    {
        return Maps.filterEntries(params, new Predicate<Map.Entry<String, String>>()
        {
            @Override
            public boolean apply(@Nullable Map.Entry<String, String> input)
            {
                String key = input.getKey();
                return !key.equals(Config.CONDITIONAL_COMMENT_PARAM_NAME)
                        && !key.equals(Config.IEONLY_PARAM_NAME);
            }
        });
    }

    @Override
    public Map<String, String> all()
    {
        return Collections.unmodifiableMap(params);
    }
}
