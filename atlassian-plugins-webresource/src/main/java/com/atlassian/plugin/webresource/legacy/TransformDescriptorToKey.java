package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.ModuleDescriptor;
import com.google.common.base.Function;

public class TransformDescriptorToKey implements Function<ModuleDescriptor<?>, String>
{
    public String apply(final ModuleDescriptor<?> resource)
    {
        return resource.getCompleteKey();
    }
}
