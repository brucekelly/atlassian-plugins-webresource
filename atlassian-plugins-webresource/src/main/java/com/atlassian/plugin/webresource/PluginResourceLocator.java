package com.atlassian.plugin.webresource;

import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.transformer.TransformerCache;

import java.util.Map;

/**
 * Assists in locating plugin resources in different ways.  
 * @since 2.2
 *
 * @deprecated since 3.3.2
 */
@Deprecated
public interface PluginResourceLocator
{
    /**
     * Returns true if this locator can parse the given url.
     *
     * @deprecated since 3.3.2
     */
    @Deprecated
    boolean matches(String url);

    /**
     * This method should be deleted when Confluence would be updated and stop using
     * PluginResourceLocator.getDownloadableResource
     * See https://ecosystem.atlassian.net/browse/PLUGWEB-193
     *
     * Returns a {@link DownloadableResource} represented by the given url and query params.
     * {@link #matches(String)} should be called before invoking this method. If the url is
     * not understood by the locator or the resource cannot be found, null will be returned.
     *
     * @deprecated since 3.3.2
     */
    @Deprecated
    DownloadableResource getDownloadableResource(String url, Map<String, String> queryParams);

    /**
     * Returns TransformerCache, instantiating it here instead of making it component in order to
     * avoid its explicit declaration in JIRA in Pico Container.
     *
     * @deprecated since 3.3.2
     */
    @com.atlassian.annotations.Internal
    @Deprecated
    TransformerCache getTransformerCache();

    /**
     * Temporary, would be removed in future, currently needed as a way to pass some variables into places where
     * they are needed.
     *
     * @return context with global objects.
     *
     * @since 3.3
     *
     * @deprecated since 3.3.2
     */
    @com.atlassian.annotations.Internal
    @Deprecated
    Globals temporaryWayToGetGlobalsDoNotUseIt();
}