package com.atlassian.plugin.webresource;

import com.atlassian.cache.CacheFactory;
import com.atlassian.plugin.PluginAccessor;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;

import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * The integration points between the Web Resource layer, and specific applications (eg JIRA, Confluence).
 *
 * @see com.atlassian.plugin.webresource.assembler.DefaultPageBuilderService#DefaultPageBuilderService(WebResourceIntegration, com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory)
 * @see com.atlassian.plugin.webresource.assembler.DefaultWebResourceAssemblerFactory#DefaultWebResourceAssemblerFactory(ResourceBatchingConfiguration, WebResourceIntegration, WebResourceUrlProvider, PluginResourceLocator, com.atlassian.plugin.webresource.transformer.StaticTransformers)
 * @see WebResourceManagerImpl#WebResourceManagerImpl(PluginResourceLocator, WebResourceIntegration, WebResourceUrlProvider)
 */
public interface WebResourceIntegration
{
    /**
     * Applications must implement this method to get access to the application's PluginAccessor
     */
    PluginAccessor getPluginAccessor();

    /**
     * Applications must implement this method to get access to the application's PluginEventManager
     * @since 3.1.1
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    PluginEventManager getPluginEventManager();

    /**
     * Applications must implement this method to get access to the application's CacheFactory
     * @since 3.1.1
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    CacheFactory getCacheFactory();

    /**
     * This must be a thread-local cache that will be accessible from both the page, and the decorator
     */
    Map<String, Object> getRequestCache();

    /**
     * Represents the unique number for this system, which when updated will flush the cache. This should be a number
     * and is generally stored in the global application-properties.
     *
     * @return A string representing the count
     */
    String getSystemCounter();

    /**
     * Represents the last time the system was updated.  This is generally obtained from BuildUtils or similar.
     */
    String getSystemBuildNumber();

    /**
     * Returns the base URL for this application.  This method may return either an absolute or a relative URL.
     * Implementations are free to determine which mode to use based on any criteria of their choosing. For example, an
     * implementation may choose to return a relative URL if it detects that it is running in the context of an HTTP
     * request, and an absolute URL if it detects that it is not.  Or it may choose to always return an absolute URL, or
     * always return a relative URL.  Callers should only use this method when they are sure that either an absolute or
     * a relative URL will be appropriate, and should not rely on any particular observed behavior regarding how this
     * value is interpreted, which may vary across different implementations.
     * <p/>
     * In general, the behavior of this method should be equivalent to calling {@link
     * #getBaseUrl(UrlMode)} with a {@code urlMode} value of {@link
     * UrlMode#AUTO}.
     *
     * @return the string value of the base URL of this application
     */
    String getBaseUrl();

    /**
     * Returns the base URL for this application in either relative or absolute format, depending on the value of {@code
     * urlMode}.
     * <p/>
     * If {@code urlMode == {@link UrlMode#ABSOLUTE}}, this method returns an absolute URL, with URL
     * scheme, hostname, port (if non-standard for the scheme), and context path.
     * <p/>
     * If {@code urlMode == {@link UrlMode#RELATIVE}}, this method returns a relative URL containing
     * just the context path.
     * <p/>
     * If {@code urlMode == {@link UrlMode#AUTO}}, this method may return either an absolute or a
     * relative URL.  Implementations are free to determine which mode to use based on any criteria of their choosing.
     * For example, an implementation may choose to return a relative URL if it detects that it is running in the
     * context of an HTTP request, and an absolute URL if it detects that it is not.  Or it may choose to always return
     * an absolute URL, or always return a relative URL.  Callers should only use {@code
     * WebResourceManager.UrlMode#AUTO} when they are sure that either an absolute or a relative URL will be
     * appropriate, and should not rely on any particular observed behavior regarding how this value is interpreted,
     * which may vary across different implementations.
     *
     * @param urlMode specifies whether to use absolute URLs, relative URLs, or allow the concrete implementation to
     *                decide
     * @return the string value of the base URL of this application
     * @since 2.3.0
     */
    String getBaseUrl(UrlMode urlMode);

    /**
     * This version number is used for caching URL generation, and needs to be incremented every time the contents
     * of the superbatch may have changed. Practically this means updating every time the plugin system state changes
     *
     * @return a version number
     */
    String getSuperBatchVersion();

    /**
     * The locale identifier that should be inserted into static resource urls for the current request, if appropriate.
     * @return null if the url should not have a locale component
     */
    String getStaticResourceLocale();

    /**
     * A reference to the temporary directory the application want the plugin system to use. The temporary directory can
     * be cleared at any time by the application and can be used by the plugin system to cache things like batches or
     * other things that can easily be re-generated. It is recommended that this directory be /apphome/tmp/webresources.
     * The plugin system can delete any or all files it sees fit that exists under this directory at any time.
     * The directory does not need to exist.
     * @return a File reference to the temporary directory. This can not return null.
     * @since 2.9.0
     */
    File getTemporaryDirectory();

    /**
     * Returns the CDNStrategy for serving resources via CDN. This may return null, in which case no resources should
     * be served via CDN.
     * @return CDN strategy
     */
    CDNStrategy getCDNStrategy();

    /**
     * @return the current user's locale. Must return a default locale if none is found.
     */
    Locale getLocale();

    /**
     * Retrieve the unformatted message text associated with this key.
     * @param locale locale in which to look up keys
     * @param key key for the i18ned message
     * @return the unformatted message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nRawText(Locale locale, String key);

    /**
     * Retrieve the message text associated with this key.
     * @param locale locale in which to look up keys
     * @param key key for the i18ned message
     * @return the message text if key is found for the given locale, otherwise returns key itself.
     */
    String getI18nText(Locale locale, String key);

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     * @since 3.3.0
     */
    Set<String> allowedCondition1Keys();

    /**
     * Works together with `forbidCondition1AndTransformer1` and temporarily allows for given set of plugins to have
     * legacy stuff. Eventually when all plugins will be updated it will ratchet down to an empty list and will be
     * removed.
     * @since 3.3.0
     */
    Set<String> allowedTransform1Keys();

    /**
     * If enabled and there's at leas one Condition1 or Transformer1 Web Resource will throw an error and stop working.
     * Should be enabled in on-demand only.
     * @since 3.3.0
     */
    boolean forbidCondition1AndTransformer1();

    /**
     * If incremental cache should be used. It's the experimental feature that potentially could slow down
     * the instance and should be disabled by default.
     *
     * Please note that if it's enabled the size of the cache should be big enough. It caches the
     * transformed content of the individual JS / CSS files, and there are lots of such files. The size defined by
     * `plugin.webresource.incrementalcache.size` system property.
     *
     * See https://extranet.atlassian.com/display/~apetrushin/Incremental+build+Cache+for+Context+Batch for details.
     *
     * @since 3.4.0
     */
    boolean isIncrementalCacheEnabled();
}
