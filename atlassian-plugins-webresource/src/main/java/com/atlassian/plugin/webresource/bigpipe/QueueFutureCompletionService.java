package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.plugin.webresource.util.ConsList;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.util.concurrent.atomic.AtomicReference;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.FutureCallback;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CancellationException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of FutureCompletionService that uses an internal {@link BlockingQueue}
 * @since v3.3
 */
class QueueFutureCompletionService<V> implements FutureCompletionService<V>
{
    /**
     * The implementation of this class switches from an open queue (when it's constructed) to a closed queue
     * (after {@link #destroy(boolean)} has been called. We switch from open to closed using this
     * reference.
     * The bulk of the implementation is in {@link OpenInnerQueue}.
     */
    private final AtomicReference<InnerQueue> queue = new AtomicReference<InnerQueue>(new OpenInnerQueue<V>());

    @Override
    public FutureCompletionService<V> add(Comparable key, Promise<V> promise)
    {
        queue.get().add(key, promise);
        return this;
    }

    @Override
    public void destroy(boolean mayInterruptIfRunning)
    {
        InnerQueue inner = queue.getAndSet(new ClosedInnerQueue<V>());
        inner.cancelPending(mayInterruptIfRunning);
    }

    @Override
    public Iterable<KeyedValue<V>> poll()
    {
        return queue.get().poll();
    }

    @Override
    public Iterable<KeyedValue<V>> poll(long timeout, TimeUnit unit) throws InterruptedException
    {
        return queue.get().poll(timeout, unit);
    }

    @Override
    public boolean isComplete()
    {
        return queue.get().isComplete();
    }

    private static interface InnerQueue<T>
    {
        void add(Comparable key, Promise<T> promise);
        void cancelPending(boolean mayInterruptIfRunning);
        Iterable<KeyedValue<T>> poll();
        Iterable<KeyedValue<T>> poll(long timeout, TimeUnit unit) throws InterruptedException;
        boolean isComplete();
    }

    /**
     * InnerQueue for a destroyed service - assumes that all promises have completed, and rejects new calls to
     * {@link InnerQueue#add(java.lang.Comparable, com.atlassian.util.concurrent.Promise)}
     * @param <T> type of successful value
     */
    private static final class ClosedInnerQueue<T> implements InnerQueue<T>
    {
        @Override
        public void add(Comparable key, Promise<T> promise)
        {
            throw new IllegalStateException("Cannot add a promise after this queue has been destroyed");
        }

        @Override
        public void cancelPending(boolean mayInterruptIfRunning)
        {
            // no-op
        }

        @Override
        public Iterable<KeyedValue<T>> poll()
        {
            return Collections.emptyList();
        }

        @Override
        public Iterable<KeyedValue<T>> poll(long timeout, TimeUnit unit) throws InterruptedException
        {
            return Collections.emptyList();
        }

        @Override
        public boolean isComplete()
        {
            return true;
        }
    }

    /**
     * Default inner queue.
     * @param <T>
     */
    private static final class OpenInnerQueue<T> implements InnerQueue<T>
    {
        private final BlockingQueue<KeyedValue<T>> completionQueue = new LinkedBlockingQueue<KeyedValue<T>>();
        private final PendingPromises<T> pendingPromises = new PendingPromises<T>();

        @Override
        public void cancelPending(boolean mayInterruptIfRunning)
        {
            for (Promise<T> promise : pendingPromises.getAllAndRemove())
            {
                promise.cancel(mayInterruptIfRunning);
            }
        }

        @Override
        public void add(final Comparable key, final Promise<T> promise)
        {
            pendingPromises.add(promise);
            promise.then(new FutureCallback<T>()
            {
                @Override
                public void onSuccess(T result)
                {
                    complete(key, promise, result);
                }

                @Override
                public void onFailure(Throwable t)
                {
                    complete(key, promise, t);
                }
            });
        }

        @Override
        public boolean isComplete()
        {
            return pendingPromises.isEmpty() && completionQueue.isEmpty();
        }

        private void complete(Promise<T> promise, KeyedValue<T> value)
        {
            // Note that order is important here to ensure consistency with isComplete() above.
            // We must ensure that the value is added to completionQueue before it is removed from pendingPromises.
            // If not, the following interleaving could occur:
            //   Thread A: pendingPromises.remove(promise);
            //   Thread B: pendingPromises.isEmpty() && completionQueue.isEmpty() <-- returns true
            //   Thread A: completionQueue.add(value);
            completionQueue.add(value);
            pendingPromises.remove(promise);
        }

        private void complete(Comparable key, Promise<T> promise, T result)
        {
            complete(promise, KeyedValueImpl.success(KeyImpl.of(key), result));
        }

        private void complete(Comparable key, Promise<T> promise, Throwable t)
        {
            if (t instanceof CancellationException)
            {
                // caused by cancelPending, ignore
            }
            else if (t instanceof Exception)
            {

                complete(promise, KeyedValueImpl.<T>fail(KeyImpl.of(key), (Exception) t));
            }
            else
            {
                pendingPromises.remove(promise);
                if (t instanceof Error)
                {
                    throw (Error) t;
                }
                throw new AssertionError(t); // should never happen
            }
        }

        @Override
        public Iterable<KeyedValue<T>> poll()
        {
            return getResult(completionQueue.poll());
        }

        @Override
        public Iterable<KeyedValue<T>> poll(long timeout, TimeUnit unit) throws InterruptedException
        {
            return getResult(completionQueue.poll(timeout, unit));
        }

        private Iterable<KeyedValue<T>> getResult(KeyedValue<T> promise)
        {
            List<KeyedValue<T>> results = Lists.newLinkedList();
            if (null != promise)
            {
                results.add(promise);
            }
            completionQueue.drainTo(results);
            return results;
        }
    }

    private static final class PendingPromises<A>
    {
        private final AtomicReference<ConsList<Promise<A>>> promises = new AtomicReference<ConsList<Promise<A>>>(ConsList.<Promise<A>> empty());

        /**
         * Adds a new promise to the list of pending promises.
         * @param p promise to add
         */
        void add(final Promise<A> p) 
        {
            promises.update(new Function<ConsList<Promise<A>>, ConsList<Promise<A>>>()
            {
                public ConsList<Promise<A>> apply(ConsList<Promise<A>> input)
                {
                    return input.prepend(p);
                }
            });
        }

        /**
         * Removes the given promise from the list of pending promises.
         * @param p promise to remove
         */
        void remove(final Promise<A> p) 
        {
            promises.update(new Function<ConsList<Promise<A>>, ConsList<Promise<A>>>()
            {
                public ConsList<Promise<A>> apply(ConsList<Promise<A>> input)
                {
                    return input.remove(p);
                }
            });
        }

        /**
         * @return true if there are promises remaining.
         */
        boolean isEmpty()
        {
            return Iterables.isEmpty(promises.get());
        }

        /**
         * Returns all promises then sets the list of promises to an empty list.
         * @return all pending promises.
         */
        Iterable<Promise<A>> getAllAndRemove()
        {
            return promises.getAndSet(ConsList.<Promise<A>>empty());
        }
    }
}
