package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerBuilder;
import com.atlassian.webresource.api.assembler.WebResourceAssemblerFactory;

/**
 * Default implementation of WebResourceAssemblerFactory
 * @since v3.0
 */
public class DefaultWebResourceAssemblerFactory implements WebResourceAssemblerFactory
{
    private final Globals globals;

    public DefaultWebResourceAssemblerFactory(Globals globals)
    {
        this.globals = globals;
    }

    public DefaultWebResourceAssemblerFactory(
        ResourceBatchingConfiguration batchingConfiguration,
        WebResourceIntegration webResourceIntegration, WebResourceUrlProvider webResourceUrlProvider,
        PluginResourceLocator pluginResourceLocator, StaticTransformers staticTransformers)
    {
        this(pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt());
    }

    @Override
    public WebResourceAssemblerBuilder create()
    {
        return new DefaultWebResourceAssemblerBuilder(globals);
    }

    /**
     * @deprecated since 3.3.2
     */
    @Override
    @Deprecated
    public void clearCache()
    {
        globals.triggerStateChange();
    }
}
