package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.baseconditions.BaseCondition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * A condition interface that wraps either {@link UrlReadingCondition} or {@link com.atlassian.plugin.web.Condition}
 * instances.
 *
 * @since v3.0
 */
public interface DecoratingCondition extends BaseCondition
{
    /**
     * Called when constructing the URL as the hosting HTML page is being served. Can add parameters to the query
     * string and alter the resource hash.
     * @param urlBuilder interface for contributing to the URL
     */
    void addToUrl(UrlBuilder urlBuilder);

    /**
     * Determine whether the web fragment should be displayed. This method should only read values from its config
     * and the query params.
     * @param params query params
     * @return true if the user should see the fragment, false otherwise
     */
    boolean shouldDisplay(QueryParams params);

    /**
     * @return true if this condition can fully encode its state into the URL; false if it cannot.
     */
    boolean canEncodeStateIntoUrl();

    /**
     * Calls the legacy condition's wrapped shouldDisplay(Map context) method. UrlReadingConditions should always
     * return true here. If {@link #canEncodeStateIntoUrl()} returns true, this should never be called.
     * @param context passed by conditions
     * @return true if this should be displayed
     */
    boolean shouldDisplayImmediate(Map<String, Object> context);

    /**
     * @return a version of this condition with inverted boolean logic.
     */
    DecoratingCondition invertCondition();
}