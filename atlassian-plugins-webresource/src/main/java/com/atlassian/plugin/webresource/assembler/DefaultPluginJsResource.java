package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginJsResource;
import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;

/**
 * Implementation of PluginUrlResource
 * @since 3.0
 */
public class DefaultPluginJsResource extends DefaultPluginUrlResource<PluginJsResourceParams> implements PluginJsResource
{
    public DefaultPluginJsResource( ResourceUrl resourceUrl)
    {
        super(resourceUrl);
    }

    @Override
    public PluginJsResourceParams getParams()
    {
        return new DefaultPluginJsResourceParams(resourceUrl.getParams());
    }
}
