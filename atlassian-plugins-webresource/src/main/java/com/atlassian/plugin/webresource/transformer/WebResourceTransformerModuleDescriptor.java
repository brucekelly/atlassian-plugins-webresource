package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.webresource.WebResourceIntegration;

import java.util.Map;

/**
 * Defines a module descriptor for a {@link WebResourceTransformer}.
 *
 * @deprecated since 3.0. Use {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory}
 * @since 2.5.0
 */
@Deprecated
public class WebResourceTransformerModuleDescriptor extends AbstractModuleDescriptor<WebResourceTransformer>
{
    private final WebResourceIntegration webResourceIntegration;

    public WebResourceTransformerModuleDescriptor(ModuleFactory moduleFactory, WebResourceIntegration webResourceIntegration)
    {
        super(moduleFactory);
        this.webResourceIntegration = webResourceIntegration;
    }

    @Override
    public WebResourceTransformer getModule()
    {
        String moduleAttributeName = getQualifiedAttributeName(
                WebResourceTransformerModuleDescriptor.class, moduleClass.toString());

        Map<String,Object> requestCache = webResourceIntegration.getRequestCache();

        WebResourceTransformer module = (WebResourceTransformer) requestCache.get(moduleAttributeName);
        if (module == null) {
            module = moduleFactory.createModule(moduleClassName, this);
            requestCache.put(moduleAttributeName, module);
        }

        return module;
    }

    private String getQualifiedAttributeName(Class enclosingClass, String attributeName)
    {
        return enclosingClass.getName() + "." + attributeName;
    }
}
