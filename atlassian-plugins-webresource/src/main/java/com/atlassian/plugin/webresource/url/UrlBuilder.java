package com.atlassian.plugin.webresource.url;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.util.concurrent.Nullable;

/**
 * Interface for conditions / transformers to contribute information to a resource URL.
 *
 * The contract for URL contributors is as follows:
 * <ul>
 *     <li>The transformer must always return exactly the same resource for the same url.</li>
 *     <li>When generating a URL, add values to the hash via {@link #addToHash(String, Object)} and / or the
 *     querystring {@link #addToQueryString(String, String)}. This is done at page-render time (when rendering the URLs).</li>
 *     <li>If you need to reference values at resource-fetch time (when the js / css
 *     resources are being served), add values to the querystring. An example is the locale parameter in the i18n transformer;
 *     this adds the user's locale to the URL at page-render time and uses that locale value to transform the resource at
 *     resource-fetch time.</li>
 *     <li>If you don't need them at resource-fetch time (ie it is there only to add uniqueness), use the hash.</li>
 *     <li>You do not need to add a value to the hash AND querystring - adding to the querystring ensures uniqueness.</li>
 *     <li>Be sparing in contributing to the querystring. If a single batch has many different transformers
 *     contributing to the URL, we will see exponential explosion in the number of combinations of query values
 *     that can be seen.</li>
 * </ul>
 *
 * @since v3.0
 */
@ExperimentalApi
public interface UrlBuilder
{
    /**
     * Contributes to the url's static hash
     * @param name name of the hash. This is used for logging and debugging only.
     * @param value value to contribute to the hash. The value will be converted to a String using value.toString()
     * when hashing. Can be null.
     */
    void addToHash(String name, @Nullable Object value);

    /**
     * Contributes to the url's querystring
     * @param key query key
     * @param value query value
     */
    void addToQueryString(String key, String value);
}
