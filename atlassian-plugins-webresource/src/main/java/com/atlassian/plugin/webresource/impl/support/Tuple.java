package com.atlassian.plugin.webresource.impl.support;

import com.google.common.base.Objects;

/**
 * Needed to return two results from the method.
 *
 * @since 3.3
 */
public class Tuple<A, B>
{
    private final A first;
    private final B last;

    public Tuple(A first, B last)
    {
        this.first = first;
        this.last = last;
    }

    public A getFirst()
    {
        return first;
    }

    public B getLast()
    {
        return last;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass()))
        {
            return false;
        }
        Tuple other = (Tuple) o;
        return Support.equals(first, other.first) && Support.equals(last, other.last);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(first, last);
    }

    @Override
    public String toString()
    {
        return first + ", " + last;
    }
}
