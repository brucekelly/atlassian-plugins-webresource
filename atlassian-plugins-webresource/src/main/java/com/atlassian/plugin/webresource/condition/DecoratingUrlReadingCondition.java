package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * Instance of a DecoratingCondition that wraps a UrlReadingCondition
 * @since v3.0
 */
public class DecoratingUrlReadingCondition implements DecoratingCondition
{
    protected final UrlReadingCondition urlReadingCondition;
    protected final Map<String, String> params;

    public DecoratingUrlReadingCondition(UrlReadingCondition urlReadingCondition, Map<String, String> params)
    {
        this.urlReadingCondition = urlReadingCondition;
        this.params = params;
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
        urlReadingCondition.addToUrl(urlBuilder);
    }

    @Override
    public boolean canEncodeStateIntoUrl()
    {
        return true;
    }

    @Override
    public boolean shouldDisplayImmediate(Map<String, Object> context)
    {
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        addToUrl(urlBuilder);
        return shouldDisplay(QueryParams.of(urlBuilder.buildParams()));
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        return urlReadingCondition.shouldDisplay(params);
    }

    @Override
    public DecoratingCondition invertCondition()
    {
        // It's never used, maybe it would be better to throw not implemented exception.
        return new DecoratingCondition()
        {
            @Override
            public void addToUrl(UrlBuilder urlBuilder)
            {
                DecoratingUrlReadingCondition.this.addToUrl(urlBuilder);
            }

            @Override
            public boolean shouldDisplay(QueryParams params)
            {
                return !DecoratingUrlReadingCondition.this.shouldDisplay(params);
            }

            @Override
            public boolean canEncodeStateIntoUrl()
            {
                return DecoratingUrlReadingCondition.this.canEncodeStateIntoUrl();
            }

            @Override
            public boolean shouldDisplayImmediate(Map<String, Object> context)
            {
                return !DecoratingUrlReadingCondition.this.shouldDisplayImmediate(context);
            }

            @Override
            public DecoratingCondition invertCondition()
            {
                return DecoratingUrlReadingCondition.this;
            }
        };
    }

    public Map<String, String> getParams()
    {
        return params;
    }

    public UrlReadingCondition getUrlReadingCondition()
    {
        return urlReadingCondition;
    }
}
