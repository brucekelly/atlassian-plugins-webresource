package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

/**
 * Performs a calculation on many referenced contexts, and produces an set of intermingled batched-contexts and residual
 * (skipped) resources. Some of the input contexts may have been merged into cross-context batches.
 * The batches are constructed in such a way that no batch is dependent on another.
 * The output batches and resources may be intermingled so as to preserve the input order as much as possible.
 *
 * @since 2.9.0
 */
public class ContextBatchBuilder
{
    private static final Logger log = LoggerFactory.getLogger(ContextBatchBuilder.class);

    private final ResourceDependencyResolver dependencyResolver;

    private final List<String> allIncludedResources = new ArrayList<String>();
    private final Set<String> skippedResources = new HashSet<String>();
    private final boolean resplitMergedContextBatchesForThisRequest;
    private final boolean isSuperBatchingEnabled;
    private final boolean includeDependenciesForFailedUrlReadingConditions;

    public ContextBatchBuilder(ResourceDependencyResolver dependencyResolver,
        boolean resplitMergedContextBatchesForThisRequest,
        boolean isSuperBatchingEnabled, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        this.dependencyResolver = dependencyResolver;
        this.resplitMergedContextBatchesForThisRequest = resplitMergedContextBatchesForThisRequest;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.includeDependenciesForFailedUrlReadingConditions = includeDependenciesForFailedUrlReadingConditions;
    }
        
    /**
     * @param includedContexts the ordering of these contexts is important since their placement within the resultant URL determines the order that resources
     * will be included in the batch.
     * @param excludedContexts order of these contexts is not important, they do not affect the position of resources. Instead they cause resources not to
     * be present.
     * @return
     * @since 2.12
     */
    public Iterable<PluginResource> buildBatched(RequestCache requestCache, final List<String> includedContexts, final Set<String>
        excludedContexts)
    {
        WebResourceKeysToContextBatches includedBatches = WebResourceKeysToContextBatches.create(requestCache, includedContexts, dependencyResolver, isSuperBatchingEnabled, includeDependenciesForFailedUrlReadingConditions);
        WebResourceKeysToContextBatches excludedBatches = null;
        if (excludedContexts != null && !Iterables.isEmpty(excludedContexts)) 
        {
            excludedBatches = WebResourceKeysToContextBatches.create(requestCache, excludedContexts, dependencyResolver, isSuperBatchingEnabled, includeDependenciesForFailedUrlReadingConditions);
        }
        
        skippedResources.addAll(includedBatches.getSkippedResources());
        
        // There are three levels to consider here. In order:
        // 1. Type (CSS/JS)
        // 2. Parameters (ieOnly, media, etc)
        // 3. Context
        final List<ContextBatch> batches = new ArrayList<ContextBatch>();

        // This working list will be reduced as each context is handled.
        final List<ContextBatch> batchesToProcess = new ArrayList<ContextBatch>(includedBatches.getContextBatches());
        
        final ContextBatchOperations contextBatchOperations = new ContextBatchOperations();
        
        while (!batchesToProcess.isEmpty())
        {
            ContextBatch contextBatch = batchesToProcess.remove(0);
            Set<ContextBatch> alreadyProcessedBatches = new HashSet<ContextBatch>();
            alreadyProcessedBatches.add(contextBatch);

            Iterator<WebResourceModuleDescriptor> resourceIterator = contextBatch.getResources().iterator();
            while (resourceIterator.hasNext())
            {
                WebResourceModuleDescriptor contextResource = resourceIterator.next();
                String resourceKey = contextResource.getCompleteKey();
                // check for an overlap with the other batches (take into account only the batches not yet processed).
                List<ContextBatch> additionalContexts = includedBatches.getAdditionalContextsForResourceKey(resourceKey, alreadyProcessedBatches);

                if (CollectionUtils.isNotEmpty(additionalContexts))
                {
                    if (log.isDebugEnabled())
                    {
                        for (ContextBatch additional : additionalContexts)
                        {
                            log.debug("Context: {} shares a resource with {}: {}", new String[] { contextBatch.getKey(), additional.getKey(), contextResource.getCompleteKey() });
                        }
                    }

                    List<ContextBatch> contextsToMerge = new ArrayList<ContextBatch>(1 + additionalContexts.size());
                    contextsToMerge.add(contextBatch);
                    contextsToMerge.addAll(additionalContexts);
                    contextBatch = contextBatchOperations.merge(contextsToMerge);
                
                    // remove the merged batches from those to be processed
                    batchesToProcess.removeAll(additionalContexts);
                    alreadyProcessedBatches.addAll(additionalContexts);

                    // As a new overlapping context is merged, restart the resource iterator so we can check for new resources 
                    // that may have been added via the merge.
                    resourceIterator = contextBatch.getResources().iterator();
                }
            }
            
            // We separate the search for excluded batches since we want to perform the subtraction
            // after all the merging has been done. If you do a subtraction then a merge you cannot
            // ensure (with ContextBatch as it is currently implemented) that the merge will not
            // bring back in previously excluded resources.
            if (excludedBatches != null)
            {                
                resourceIterator = contextBatch.getResources().iterator();
                while (resourceIterator.hasNext())
                {
                    WebResourceModuleDescriptor contextResource = resourceIterator.next();
                    String resourceKey = contextResource.getCompleteKey();
                    
                    List<ContextBatch> excludeContexts = excludedBatches.getContextsForResourceKey(resourceKey);
                    if (!excludeContexts.isEmpty())
                    {
                        contextBatch = contextBatchOperations.subtract(contextBatch, excludeContexts);
                    }
                }
                
                skippedResources.removeAll(excludedBatches.getSkippedResources());
            }
            
            // check that we still have resources in this batch - if not, the batch is not required.
            if (excludedBatches == null || Iterables.size(contextBatch.getResources()) != 0)
            {
                Iterables.addAll(allIncludedResources, contextBatch.getResourceKeys());
                batches.add(contextBatch);                
            }
            else if (log.isDebugEnabled())
            {
                log.debug("The context batch {} contains no resources so will be dropped.", contextBatch.getKey());
            }                
        }

        // Build the batch resources
        return concat(transform(batches, new Function<ContextBatch, Iterable<PluginResource>>()
        {
            public Iterable<PluginResource> apply(final ContextBatch batch)
            {
                return batch.buildPluginResources(resplitMergedContextBatchesForThisRequest);
            }
        }));
    }

    public Iterable<String> getAllIncludedResources()
    {
        return allIncludedResources;
    }

    public Iterable<String> getSkippedResources()
    {
        return skippedResources;
    }
    
    
    private static class WebResourceKeysToContextBatches
    {
        /**
         * Create a Map of {@link WebResourceModuleDescriptor} key to the contexts that includes them. If a 
         * {@link WebResourceModuleDescriptor} exists in multiple contexts then each context will be 
         * referenced. The ContextBatches created at this point are pure - they do not take into account
         * overlaps or exclusions, they simply contain all the resources for their context name.
         * 
         * @param contexts the contexts to create a mapping for
         * @param dependencyResolver used to construct the identified contexts
         * @return a WebResourceToContextsMap containing the required mapping.
         */        
        static WebResourceKeysToContextBatches create(RequestCache requestCache, final Iterable<String> contexts, final ResourceDependencyResolver dependencyResolver,
            boolean isSuperBatchingEnabled, boolean includeDependenciesForFailedUrlReadingConditions)
        {
            final Map<String, List<ContextBatch>> resourceKeyToContext = new HashMap<String,List<ContextBatch>>();
            final List<ContextBatch> batches = new ArrayList<ContextBatch>();
            final Set<String> skippedResources = new HashSet<String>();
            
            for (String context : contexts)
            {
                Iterable<WebResourceModuleDescriptor> dependencies = dependencyResolver.getDependenciesInContext(requestCache, context, skippedResources, includeDependenciesForFailedUrlReadingConditions);
                
                ContextBatch batch = new ContextBatch(ImmutableList.of(context), null, dependencies, isSuperBatchingEnabled);
                for (WebResourceModuleDescriptor moduleDescriptor : dependencies)
                {
                    String key = moduleDescriptor.getCompleteKey();

                    if (!resourceKeyToContext.containsKey(key))
                    {
                        resourceKeyToContext.put(key, new ArrayList<ContextBatch>());
                    }

                    resourceKeyToContext.get(key).add(batch);

                    if (!batches.contains(batch))
                    {
                        batches.add(batch);
                    }
                }
                
            }
            
            return new WebResourceKeysToContextBatches(resourceKeyToContext, batches, skippedResources);
        }
        
        private final Map<String, List<ContextBatch>> resourceToContextBatches; 
        private final List<ContextBatch> knownBatches;
        private final Set<String> skippedResources;        
        
        private WebResourceKeysToContextBatches(Map<String, List<ContextBatch>> resourceKeyToContext, List<ContextBatch> allBatches, Set<String> skippedResources)
        {
            this.resourceToContextBatches = resourceKeyToContext;
            this.knownBatches = allBatches;
            this.skippedResources = skippedResources;
        }
        
        /**
         * @param key the resource key to find contexts for
         * @return all contexts the specified resource is included in. An empty List is returned if none.
         */
        List<ContextBatch> getContextsForResourceKey(String key)
        {
            return getAdditionalContextsForResourceKey(key, null);
        }        
        
        /**
         * @param key the resource key to be mapped to contexts
         * @param knownContexts the contexts we already know about (may be null)
         * @return a List of any additional contexts that the identified resource key can be found in. If there
         * are no additional contexts then an empty List is returned.
         */
        List<ContextBatch> getAdditionalContextsForResourceKey(String key, Collection<ContextBatch> knownContexts)
        {
            List<ContextBatch> allContexts = resourceToContextBatches.get(key);
            if (CollectionUtils.isEmpty(allContexts))
            {
                return Collections.emptyList();
            }
            
            LinkedHashSet<ContextBatch> contexts = new LinkedHashSet<ContextBatch>(allContexts);
            if (CollectionUtils.isNotEmpty(knownContexts))
            {
                contexts.removeAll(knownContexts);
            }

            return new ArrayList<ContextBatch>(contexts);
        }
        
        /**
         * @return all the ContextBatches referenced in this class.
         */
        List<ContextBatch> getContextBatches()
        {
            return new ArrayList<ContextBatch>(knownBatches);
        }

        /**
         * @return the conditional resources that could not be mapped
         */
        public Set<String> getSkippedResources()
        {
            return skippedResources;
        }
    }
}
