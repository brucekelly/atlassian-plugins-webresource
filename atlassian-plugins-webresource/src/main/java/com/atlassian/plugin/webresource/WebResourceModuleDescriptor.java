package com.atlassian.plugin.webresource;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.condition.UrlReadingConditionElementParser;
import com.atlassian.plugin.webresource.data.WebResourceDataProviderParser;
import com.atlassian.plugin.webresource.transformer.StaticTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.dom4j.Attribute;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A way of linking to web 'resources', such as javascript or css.  This allows us to include resources once
 * on any given page, as well as ensuring that plugins can declare resources, even if they are included
 * at the bottom of a page.
 */
public class WebResourceModuleDescriptor extends AbstractModuleDescriptor<Void>
{
    private List<String> dependencies = Collections.emptyList();
    private boolean disableMinification;
    private Set<String> contexts = Collections.emptySet();
    private final HostContainer hostContainer;
    private WebResourceDataProviderParser dataProviderParser;
    private Map<String, WebResourceDataProvider> dataProviders = Collections.emptyMap();
    private List<WebResourceTransformation> webResourceTransformations = Collections.emptyList();
    private UrlReadingConditionElementParser conditionElementParser;
    private Element element;
    private DecoratingCondition condition;

    /**
     * @deprecated Since 3.0.0 - use {@link #WebResourceModuleDescriptor(com.atlassian.plugin.module.ModuleFactory, com.atlassian.plugin.hostcontainer.HostContainer)}
     * @param hostContainer
     */
    @Deprecated
    public WebResourceModuleDescriptor(final HostContainer hostContainer)
    {
        this(ModuleFactory.LEGACY_MODULE_FACTORY, hostContainer);
    }

    public WebResourceModuleDescriptor(ModuleFactory moduleFactory, final HostContainer hostContainer)
    {
        super(moduleFactory);
        this.conditionElementParser = new UrlReadingConditionElementParser(hostContainer);
        this.hostContainer = hostContainer;
    }

    @Override
    public void init(final Plugin plugin, final Element element) throws PluginParseException
    {
        super.init(plugin, element);

        List dependencies = element.elements("dependency");
        if (!dependencies.isEmpty())
        {
            final List<String> deps = new ArrayList<String>(dependencies.size());
            for (Element dependency : (List<Element>) dependencies)
            {
                deps.add(dependency.getTextTrim());
            }
            this.dependencies = ImmutableList.copyOf(deps);
        }

        final Set<String> ctxs = new HashSet<String>(contexts.size());
        ctxs.add(getCompleteKey()); // every <web-resource> has an implicit context based on it's name

        List<Element> contexts = (List<Element>) element.elements("context");
        if (!contexts.isEmpty())
        {
            for (Element contextElement : contexts)
            {
                ctxs.add(contextElement.getTextTrim());
            }
        }
        this.contexts = ImmutableSet.copyOf(ctxs);

        List<Element> transformations = (List<Element>) element.elements("transformation");
        if (!transformations.isEmpty())
        {
            final List<WebResourceTransformation> trans = new ArrayList<WebResourceTransformation>(transformations.size());
            for (Element e : transformations)
            {
                trans.add(new WebResourceTransformation(e));
            }
            webResourceTransformations = ImmutableList.copyOf(trans);
        }

        dataProviderParser = new WebResourceDataProviderParser(hostContainer, element.elements("data"));

        final Attribute minifiedAttribute = element.attribute("disable-minification");
        disableMinification = minifiedAttribute == null ? false : Boolean.valueOf(minifiedAttribute.getValue());
        this.element = element;
    }

    /**
     * As this descriptor just handles resources, you should never call this
     */
    @Override
    public Void getModule()
    {
        throw new UnsupportedOperationException("There is no module for Web Resources");
    }

    @Override
    public void enabled()
    {
        super.enabled();
        try
        {
            condition = conditionElementParser.makeConditions(plugin, element, AbstractConditionElementParser.CompositeType.AND);
        }
        catch (final PluginParseException e)
        {
            // is there a better exception to throw?
            throw new RuntimeException("Unable to enable web resource due to issue processing condition", e);
        }
        try
        {
            dataProviders = dataProviderParser.createDataProviders(plugin, this.getClass());
        }
        catch (ClassNotFoundException e)
        {
            throw new RuntimeException("Unable to enable web resource due to an isue processing data-provider", e);
        }
        catch (final PluginParseException e)
        {
            throw new RuntimeException("Unable to enable web resource due to an isue processing data-provider", e);
        }
    }

    @Override
    public void disabled()
    {
        super.disabled();
        condition = null;
        dataProviders = Collections.emptyMap();
    }

    /**
     * Returns the web resource contexts this resource is associated with.
     *
     * @return  the web resource contexts this resource is associated with.
     * @since 2.5.0
     */
    public Set<String> getContexts()
    {
        return contexts;
    }

    /**
     * Returns a list of dependencies on other web resources.
     * @return a list of module complete keys
     */
    public List<String> getDependencies()
    {
        return dependencies;
    }

    public List<WebResourceTransformation> getTransformations()
    {
        return webResourceTransformations;
    }

    public DecoratingCondition getCondition() {
        return condition;
    }

    /**
     * @return <code>true</code> if resource minification should be skipped, <code>false</code> otherwise.
     */
    public boolean isDisableMinification()
    {
        return disableMinification;
    }

    /**
     * @return true if this resource's conditions can fully encode themselves into the URL, false if they can't.
     */
    public boolean canEncodeStateIntoUrl()
    {
        return getCondition() == null || getCondition().canEncodeStateIntoUrl();
    }

    /**
     * @param params querystring params
     * @return true if this resource should be displayed for the given query parameters, otherwise false
     */
    public boolean shouldDisplay(QueryParams params)
    {
        return getCondition() == null || getCondition().shouldDisplay(params);
    }

    /**
     * @return True if this web resource should be displayed based on the optional condition
     * @since 2.7.0
     */
    public boolean shouldDisplayImmediate()
    {
        return getCondition() == null || getCondition().shouldDisplayImmediate(ImmutableMap.<String, Object>of());
    }

    /**
     * @return data providers specified by this web-resource
     * @since 3.0
     */
    public Map<String, WebResourceDataProvider> getDataProviders()
    {
        return dataProviders;
    }

    /**
     * Returns all condition-1 keys used by this descriptor.
     * This is a temporary method, used to extract all condition1's in a running instance.
     * @deprecated in 3.0.5
     */
    @Deprecated
    public Set<String> getDeprecatedConditionKeys()
    {
        final Set<String> allConditions = Sets.newHashSet();
        UrlReadingConditionElementParser parser = new UrlReadingConditionElementParser(hostContainer) {
            @Override
            protected DecoratingCondition makeConditionImplementation(Plugin plugin, Element element) throws PluginParseException
            {
                DecoratingCondition condition = super.makeConditionImplementation(plugin, element);
                if (!condition.canEncodeStateIntoUrl())
                {
                    allConditions.add(element.attributeValue("class"));
                }
                return condition;
            }
        };
        parser.makeConditions(plugin, element, AbstractConditionElementParser.CompositeType.AND);
        return allConditions;
    }
    /**
     * Returns all transform-1 keys used by this descriptor.
     * This is a temporary method, used to extract all transform1's in a running instance.
     * @deprecated in 3.0.5
     */
    @Deprecated
    public Set<String> getDeprecatedTransformKeys(TransformerCache transformerCache)
    {
        final Set<String> allTransforms= Sets.newHashSet();
        for (WebResourceTransformation transformation : getTransformations())
        {
            for (WebResourceTransformerModuleDescriptor descriptor : transformation.getDeprecatedTransformers(transformerCache))
            {
                allTransforms.add(descriptor.getCompleteKey());
            }
        }
        return allTransforms;
    }

    public boolean isTainted(String type, TransformerCache transformerCache)
    {
        if (null != getCondition())
        {
            if (!getCondition().canEncodeStateIntoUrl())
            {
                return true;
            }
        }
        for (WebResourceTransformation transformation : getTransformations())
        {
            if (transformation.matches(type))
            {
                if (!transformation.containsOnlyPureUrlReadingTransformers(transformerCache))
                {
                    return true;
                }
            }
        }
        return false;
    }
}
