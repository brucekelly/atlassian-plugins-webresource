package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.webresource.QueryParams;

import java.util.Map;

/**
 * Implementation of DecoratingCompositeCondition that fits the new UrlReadingCondition interface.
 * @since v3.0
 */
public class DecoratingAndCompositeCondition extends DecoratingCompositeCondition
{
    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        // If this is being called, there are either no legacy components OR all legacy conditions returned true in
        // shouldDisplayImmediate(). DecoratingLegacyCondition.shouldDisplay() returns true - which is how this is
        // evaluating the AND of legacy + non-legacy conditions
        for (DecoratingCondition condition : conditions)
        {
            if (!condition.shouldDisplay(params))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean shouldDisplayImmediate(Map<String, Object> context)
    {
        // This is evaluated at URL render time, and is only called if canEncodeStateIntoUrl() returns false - ie there is at least
        // one legacy Condition. If at least one condition returns false here, the resource is not served to the client
        // (either standalone or in a batch).
        for (DecoratingCondition condition : conditions)
        {
            if (!condition.shouldDisplayImmediate(context))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public DecoratingCondition invertCondition()
    {
        DecoratingOrCompositeCondition orCondition = new DecoratingOrCompositeCondition();
        for (DecoratingCondition condition : conditions)
        {
            orCondition.addCondition(condition.invertCondition());
        }
        return orCondition;
    }
}
