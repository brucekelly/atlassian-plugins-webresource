package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.sourcemap.Mapping;
import com.atlassian.sourcemap.SourceMap;
import com.atlassian.sourcemap.SourceMapImpl;

import java.io.InputStream;
import java.io.OutputStream;

import static com.atlassian.plugin.webresource.impl.support.Support.copy;

/**
 * Minification transformer, it's not a standard transformer and doesn't follow the transformer API.
 *
 * @since v 3.3.1
 */
public class MinificationTransformer
{
    public static Content minify(Config config, final Resource resource, final Content content, final String sourceUrl)
    {
        if (isMinificationEnabledFor(config, resource))
        {
            return new ContentImpl(content.getContentType(), true)
            {
                @Override
                public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
                {
                    String pathForMinifiedVersion = getPathForMinifiedVersion(resource.getPath());
                    InputStream minifiedStream = resource.getStreamFor(pathForMinifiedVersion);

                    // Checking alternate path for minified resource.
                    if (minifiedStream == null)
                    {
                        pathForMinifiedVersion = getAlternatePathForMinifiedVersion(resource.getPath());
                        minifiedStream = resource.getStreamFor(pathForMinifiedVersion);
                    }

                    if (minifiedStream == null)
                    {
                        // If there's no minified version serving original version.
                        content.writeTo(out, isSourceMapEnabled);
                        return null;
                    }
                    else
                    {
                        copy(minifiedStream, out);
                        if (isSourceMapEnabled)
                        {
                            // Loading source map.
                            String pathForSourceMap = pathForMinifiedVersion + ".map";
                            InputStream sourceMapAsStream = resource.getStreamFor(pathForSourceMap);
                            if (sourceMapAsStream == null)
                            {
                                // Currently almost no one plugin has source maps, there will be too many
                                // warnings in the log. Maybe it will be enabled later.
                                // Support.LOGGER.warn("no source map for " + resource.getKey() + "/" + resource.getName());
                                return null;
                            }
                            else
                            {
                                // Making sure errors in source map doesn't prevent from serving the resource.
                                try
                                {
                                    SourceMap sourceMap = new SourceMapImpl(sourceMapAsStream);
                                    // During the compilation step the source url is unknown so the file name used
                                    // as the source url. It is replaced to the actual source url at runtime.
                                    return replaceSourceUrl(sourceMap, sourceUrl);
                                }
                                catch (RuntimeException e)
                                {
                                    Support.LOGGER.warn("can't parse source map for " + resource.getKey() + "/" +
                                        resource.getName(), e);
                                    return null;
                                }
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            };
        }
        else
        {
            return content;
        }
    }

    /**
     * Replaces source url in source map.
     */
    private static SourceMap replaceSourceUrl(SourceMap sourceMap, final String sourceUrl)
    {
        final SourceMap result = new SourceMapImpl();
        sourceMap.eachMapping(new SourceMap.EachMappingCallback()
        {
            @Override
            public void apply(Mapping mapping)
            {
                result.addMapping(
                    mapping.getGeneratedLine(), mapping.getGeneratedColumn(),
                    mapping.getSourceLine(), mapping.getSourceColumn(),
                    sourceUrl
                );
            }
        });
        return result;
    }

    /**
     * If minification enabled for given resource.
     */
    private static boolean isMinificationEnabledFor(Config config, Resource resource)
    {
        // check if minification has been turned off for this resource (at the
        // module level)
        if (!resource.getParent().isMinificationEnabled())
        {
            return false;
        }

        if (!config.isMinificationEnabled())
        {
            return false;
        }

        String path = resource.getPath();
        // We only minify .js or .css files
        if (path.endsWith(".js"))
        {
            // Check if it is already the minified version of the file
            return !(path.endsWith("-min.js") || path.endsWith(".min.js"));
        }
        if (path.endsWith(".css"))
        {
            // Check if it is already the minified version of the file
            return !(path.endsWith("-min.css") || path.endsWith(".min.css"));
        }
        // Not .js or .css, don't bother trying to find a minified version (may
        // save some file operations)
        return false;
    }

    /**
     * The path to the minified version of the resource.
     */
    private static String getPathForMinifiedVersion(String path)
    {
        final int lastDot = path.lastIndexOf(".");
        // this can never but -1 since the method call is protected by a call to
        // minificationStrategyInPlay() first
        return path.substring(0, lastDot) + "-min" + path.substring(lastDot);
    }

    /**
     * The path to the minified version of the resource.
     */
    private static String getAlternatePathForMinifiedVersion(String path)
    {
        final int lastDot = path.lastIndexOf(".");
        // this can never but -1 since the method call is protected by a call to
        // minificationStrategyInPlay() first
        return path.substring(0, lastDot) + ".min" + path.substring(lastDot);
    }
}