package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.util.concurrent.Promise;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import java.util.concurrent.TimeUnit;

/**
 * Decouples registration of asynchronous tasks from the consumption of completed tasks. Producers add promises
 * representing executing tasks. Consumers take / poll for completed tasks in the order that they complete.
 * <p>
 * This service is similar to a {@link java.util.concurrent.CompletionService}, but differs in the the following
 * aspects:
 * <ul>
 *  <li>CompletionService accepts {@link java.util.concurrent.Callable} and
 *  {@link java.lang.Runnable} instances and invokes those tasks, FutureCompletionService accepts
 *  {@link Promise} instances that have been invoked by threads outside the FutureCompletionService.</li>
 *  <li>CompletionService's take and poll methods return a single completed task; FutureCompletionService's
 *  {@link #poll()} and {@link #poll(long, TimeUnit)} methods return
 *  all tasks that have completed at the time of invocation.</li>
 *  <li>FutureCompletionService provides an {@link #isComplete()} method to ask if all promises
 *  have finished executing./li>
 *  <li>CompletionService has no way to map individual tasks to their results; FutureCompletionService takes
 *  a key in its {@link #add(java.lang.Comparable, com.atlassian.util.concurrent.Promise)} that is returned to
 *  consumers via
 *  {@link KeyedValue#key()}. This is provided
 *  so that consumers can map returned values back to certain known producers, even if the task failed.</li>
 * </ul>
 *
 * Note this is not public by design; it is effectively a dark feature until a future release.
 *
 * @since v3.3
 */
@ExperimentalApi
interface FutureCompletionService<V>
{
    /**
     * Adds a value-returning promise to be redeemed by consumers on completion.
     * @param key identifier of the promise to return. This is not required to be unique; multiple promises can be
     *            added with the same key.
     * @param promise the promise being added
     * @return this, for chaining.
     */
    FutureCompletionService<V> add(Comparable key, Promise<V> promise);

    /**
     * Cancels all currently running promises. This method should be used to cancel execution of added tasks
     * when no consumers are interested in their results.
     * <p />
     * After calling this method, calls to {@link #isComplete()} will always return true and {@link #poll()} /
     * {@link #poll(long, java.util.concurrent.TimeUnit)} will return an empty iterable.
     * @param mayInterruptIfRunning true if each promise should be interrupted - this is passed through to
     *                              {@link Promise#cancel(boolean)}.
     */
    void destroy(boolean mayInterruptIfRunning);

    /**
     * Retrieves and removes the values returned for all completed tasks. This method always returns immediately.
     * @return {@link KeyedValue}s representing all completed tasks, either failed or successful. If
     * no completed tasks are available, returns an empty iterable.
     */
    Iterable<KeyedValue<V>> poll();

    /**
     * Retrieves and removes the values returned for all completed tasks, waiting if necessary up to the specified
     * wait time if none are yet present.
     * @param timeout how long to wait before giving up, in units of unit
     * @param unit a TimeUnit determining how to interpret the timeout parameter
     * @return {@link KeyedValue}s representing all completed tasks, either failed or successful. If the timeout is reached
     * before any completed tasks become available, returns an empty iterable.
     * @throws InterruptedException if interrupted while executing
     */
    Iterable<KeyedValue<V>> poll(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * Returns false if more tasks may be returned by future calls to {@link #poll()} or
     * {@link #poll(long, java.util.concurrent.TimeUnit)}. This method will return false until all added tasks have completed.
     * @return true if all tasks have completed; false if more tasks may be retrieved.
     */
    boolean isComplete();

}
