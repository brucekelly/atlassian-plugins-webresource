package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;

/**
 * This should only be used for annotating Javascript resources. It will wrap each Javascript resource
 * in a try/catch block with console logging in the catch.
 * 
 * This implementation is stateless.
 */
public class TryCatchJsResourceContentAnnotator implements ResourceContentAnnotator
{
    private static final byte[] BEFORE_CHUNK = "try {\n".getBytes();
    private static final byte[] AFTER_CHUNK = "\n} catch (err) {\n    if (console && console.log && console.error) {\n        console.log(\"Error running batched script.\");\n        console.error(err);\n    }\n}\n\n".getBytes();

    private static final Logger log = LoggerFactory.getLogger(TryCatchJsResourceContentAnnotator.class);

    @Override
    public void before(Resource resource, OutputStream stream) throws IOException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Writing try-catch annotation (before)");
        }
        stream.write(BEFORE_CHUNK);
    }

    @Override
    public void after(Resource resource, OutputStream stream) throws IOException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Writing try-catch annotation (after)");
        }
        stream.write(AFTER_CHUNK);
    }

    @Override
    public int beforeOffset()
    {
        return 1;
    }
}
