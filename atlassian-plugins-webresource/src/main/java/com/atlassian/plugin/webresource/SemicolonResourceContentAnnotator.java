package com.atlassian.plugin.webresource;

import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Prepend and append a semicolon.
 * <p/>
 * If a JavaScript file follows another that doesn't end with a semicolon, when it's batched, it's possible for the
 * semantics of the JavaScript to change. Consider the following:
 * <pre>// FILE ONE.js
 * console.log("Loading...")
 *
 * // FILE TWO.js
 * (function () {})()</pre>
 * When batched, it's turned into:
 * <pre>console.log("Loading...")(function () {})()</pre>
 * (Notice that the result of `console.log` is being *executed* with `function () {}` passed in as an argument. This
 * means you'll get an exception like this:
 * <pre>TypeError: undefined is not a function</pre>
 * This is a stateless implementation of the {@link ResourceContentAnnotator} that will write a semicolon before and
 * after each resource.
 *
 * @since 3.1.1
 */
public class SemicolonResourceContentAnnotator implements ResourceContentAnnotator {
    @Override
    public void before(Resource resource, OutputStream stream) throws IOException
    {
        stream.write(';');
        stream.write('\n');
    }

    @Override
    public void after(Resource resource, OutputStream stream) throws IOException
    {
        stream.write(';');
    }

    @Override
    public int beforeOffset() {
        return 1;
    }
}