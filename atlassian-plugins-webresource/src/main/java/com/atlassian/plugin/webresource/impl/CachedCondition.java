package com.atlassian.plugin.webresource.impl;

import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.condition.DecoratingCondition;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Cached condition.
 *
 * @since v3.3
 */
public class CachedCondition
{
    private static final Map<String, Object> EMPTY_IMMUTABLE_CONTEXT =
        Collections.unmodifiableMap(new HashMap<String, Object>());

    private final DecoratingCondition condition;
    private final boolean isLegacy;

    public CachedCondition(DecoratingCondition condition)
    {
        this.condition = condition;
        this.isLegacy = !condition.canEncodeStateIntoUrl();
    }

    /**
     * Evaluate condition. The legacy Condition will be evaluated, the UrlReadingCondition will be asked to
     * generate parameters and evaluate it.
     * If error will be thrown it will be intercepted and condition evaluated to false.
     */
    public boolean evaluateSafely(RequestCache requestCache)
    {
        Boolean result = requestCache.getCachedConditionsEvaluation().get(this);
        if (result == null)
        {
            if (isLegacy)
            {
                try
                {
                    result = condition.shouldDisplayImmediate(EMPTY_IMMUTABLE_CONTEXT);
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("exception thrown in `shouldDisplayImmediate` during condition evaluation", e);
                    return false;
                }
            }
            else
            {
                DefaultUrlBuilder defaultUrlBuilder = new DefaultUrlBuilder();
                addToUrlSafely(requestCache, defaultUrlBuilder);
                result = evaluateSafely(requestCache, defaultUrlBuilder.buildParams());
            }
            requestCache.getCachedConditionsEvaluation().put(this, result);
        }
        return result;
    }

    /**
     * Evaluate condition against parameters. The legacy Condition will be evaluated, the UrlReadingCondition will
     * be evaluated against parameters.
     * If error will be thrown it will be intercepted and condition evaluated to false.
     */
    public boolean evaluateSafely(RequestCache requestCache, Map<String, String> params)
    {
        Boolean result = requestCache.getCachedConditionsEvaluation().get(this);
        if (result == null)
        {
            try
            {
                result = condition.shouldDisplay(QueryParams.of(params));
            }
            catch (RuntimeException e)
            {
                Support.LOGGER.warn("exception thrown in `shouldDisplay` during condition evaluation", e);
                return false;
            }
            requestCache.getCachedConditionsEvaluation().put(this, result);
        }
        return result;
    }

    /**
     * Add parameters of condition to url.
     * If error will be thrown it will be intercepted and nothing will be added to url.
     */
    public void addToUrlSafely(RequestCache requestCache, UrlBuilder urlBuilder)
    {
        if (!isLegacy)
        {
            DefaultUrlBuilder result = requestCache.getCachedConditionsParameters().get(this);
            if (result == null)
            {
                result = new DefaultUrlBuilder();
                try
                {
                    condition.addToUrl(result);
                }
                catch (RuntimeException e)
                {
                    Support.LOGGER.warn("exception thrown in `addToUrl` during condition evaluation", e);
                }
                requestCache.getCachedConditionsParameters().put(this, result);
            }
            result.applyTo(urlBuilder);
        }
    }

    public boolean isLegacy()
    {
        return isLegacy;
    }
}
