package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.WebResourceIntegration;

import java.util.HashSet;
import java.util.Map;

import static com.atlassian.plugin.servlet.AbstractFileServerServlet.PATH_SEPARATOR;
import static com.atlassian.plugin.servlet.AbstractFileServerServlet.SERVLET_PATH;

/**
 * Creates a batch of all like-typed resources that are declared as "super-batch="true"" in their plugin
 * definitions.
 * <p/>
 * The URL for batch resources is /download/superbatch/&lt;type>/batch.&lt;type. The additional type part in the path
 * is simply there to make the number of path-parts identical with other resources, so relative URLs will still work
 * in CSS files.
 */
public class SuperBatchPluginResource extends AbstractPluginResource
{
    protected SuperBatchPluginResource()
    {
        super(new HashSet<String>());
    }

    public void addBatchedWebResourceDescriptor(String key)
    {
        completeKeys.add(key);
    }

}
