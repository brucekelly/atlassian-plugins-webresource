package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.DefaultWebResourceFilter;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Builds superbatch resources
 * @since v3.0
 */
public class SuperBatchBuilder
{
    private final ResourceDependencyResolver dependencyResolver;
    private final PluginResourceLocator pluginResourceLocator;

    public SuperBatchBuilder(ResourceDependencyResolver dependencyResolver, PluginResourceLocator pluginResourceLocator)
    {
        this.dependencyResolver = dependencyResolver;
        this.pluginResourceLocator = pluginResourceLocator;
    }

    /**
     * Get all super-batch resources that match the given filter.
     *
     * @return super-batch resources in the order dictated by {@link com.atlassian.plugin.webresource.legacy.BatchResourceComparator}. They should be rendered exactly in this order otherwise dependency problems may arise.
     */
    public Iterable<PluginResource> build()
    {
        final Iterable<WebResourceModuleDescriptor> superBatchModuleKeys = dependencyResolver.getSuperBatchDependencies();
        final List<PluginResource> resources = new ArrayList<PluginResource>();

        SuperBatchPluginResource superBatchPluginResource = new SuperBatchPluginResource();
        boolean isEmpty = true;
        for (final WebResourceModuleDescriptor moduleDescriptor : superBatchModuleKeys)
        {
            for (final PluginResource pluginResource : pluginResourceLocator.getPluginResources(moduleDescriptor.getCompleteKey()))
            {
                // record the content of each super batch to enable front-end dependency tracking
                if (pluginResource instanceof BatchPluginResource)
                {
                    superBatchPluginResource.addBatchedWebResourceDescriptor(((BatchPluginResource) pluginResource).getModuleCompleteKey());
                    isEmpty &= false;
                }
            }
        }

        // here we sort the order of the batches. The unconditional batches should come first and the most conditional batches should come last.
        if (!isEmpty)
        {
            resources.add(superBatchPluginResource);
        }

        return resources;
    }
}
