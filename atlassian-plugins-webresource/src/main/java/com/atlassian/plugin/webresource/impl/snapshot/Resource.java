package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.impl.support.ContentImpl;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.sourcemap.SourceMap;
import com.google.common.base.Objects;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.support.Support.copy;

/**
 * Resource of Web Resource or Plugin.
 *
 * @since 3.3
 */
public class Resource
{
    private static final Set<String> HTTP_PARAM_NAMES_SET = new HashSet<String>(Arrays.asList(Config.HTTP_PARAM_NAMES));

    private final Bundle parent;
    private final String type;
    private final ResourceLocation resourceLocation;

    public Resource(Bundle parent, ResourceLocation resourceLocation, String type)
    {
        this.parent = parent;
        this.type = type;
        this.resourceLocation = resourceLocation;
    }

    /**
     * Get Parent - Web Resource or Plugin.
     */
    public Bundle getParent()
    {
        return parent;
    }

    public String getName()
    {
        return resourceLocation.getName();
    }

    public String getLocation()
    {
        return resourceLocation.getLocation();
    }

    public boolean isRedirect()
    {
        return "webContext".equalsIgnoreCase(resourceLocation.getParameter(Config.SOURCE_PARAM_NAME));
    }

    public String getType()
    {
        return type;
    }

    public String getContentType()
    {
        return resourceLocation.getContentType();
    }

    public boolean isBatchable()
    {
        return !isRedirect() && !"false".equalsIgnoreCase(resourceLocation.getParameter(Config.BATCH_PARAM_NAME));
    }

    public InputStream getStreamFor(String path)
    {
        return parent.getSnapshot().config.getStreamFor(this, path);
    }

    /**
     * The actual path of the resource consist of two parts - the location and the file path.
     * So, this method returns the actual path to the file.
     */
    public String getPath()
    {
        String extraPath = getFilePath();
        if ((extraPath != null) && !"".equals(extraPath.trim()) && !resourceLocation.getLocation().endsWith("/"))
        {
            extraPath = "/" + extraPath;
        }
        return resourceLocation.getLocation() + extraPath;
    }

    /**
     * Should it be included in Batch with given Parameters?
     */
    public boolean isBatchable(Map<String, String> batchParams)
    {
        if (!isBatchable())
        {
            return false;
        }
        for (final String key : Config.HTTP_PARAM_NAMES)
        {
            if (!Support.equals(batchParams.get(key), getParams().get(key)))
            {
                return false;
            }
        }
        return true;
    }

    public Map<String, String> getParams()
    {
        return resourceLocation.getParams();
    }

    /**
     * Get only those parameters that would be output to url.
     */
    public Map<String, String> getUrlParams()
    {
        return getUrlParamsStatic(getParams());
    }

    public static Map<String, String> getUrlParamsStatic(Map<String, String> params)
    {
        Map<String, String> urlParams = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (HTTP_PARAM_NAMES_SET.contains(entry.getKey()))
            {
                urlParams.put(entry.getKey(), entry.getValue());
            }
        }
        return urlParams;
    }

    public boolean isTransformable()
    {
        return parent.isTransformable();
    }

    public String getVersion()
    {
        return parent.getVersion();
    }

    public String getKey()
    {
        return parent.getKey();
    }

    public boolean isCacheable()
    {
        return isCacheableStatic(this.getUrlParams());
    }

    public static boolean isCacheableStatic(Map<String, String> params)
    {
        return !"false".equalsIgnoreCase(params.get(Config.CACHE_PARAM_NAME));
    }

    public Content getContent()
    {
        return new ContentImpl(getContentType(), false)
        {
            @Override
            public SourceMap writeTo(OutputStream out, boolean isSourceMapEnabled)
            {
                copy(getStreamFor(getPath()), out);
                return null;
            }
        };

    }

    /**
     * @deprecated since 3.3.2
     */
    @Deprecated
    public ResourceLocation getResourceLocation()
    {
        return resourceLocation;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if ((o == null) || (getClass() != o.getClass()))
        {
            return false;
        }
        Resource resource = (Resource) o;
        return parent.equals(resource.parent) && getName().equals(resource.getName());
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(parent, getName());
    }

    @Override
    public String toString()
    {
        return "{" + getName() + (!isBatchable() ? " isNotBatchable" : "") + "}";
    }

    /**
     * There's a method `getFilePath` on the resource but no the `filePath` variable itself,
     * this is done in order to not take
     * additional memory. The case when the `filePath` is different from the empty string is rare,
     * and handled by overriding this
     * method.
     * @deprecated since 3.3.2
     */
    @Deprecated
    public String getFilePath()
    {
        return "";
    }
}