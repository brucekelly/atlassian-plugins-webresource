package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;
import org.dom4j.Element;

/**
 * Transforms a downloadable resource.  Instances are created per request, though it is assumed the resource will have
 * the appropriate caching headers to ensure the same user doesn't download it twice.
 *
 * Transformers based on this interface cannot contribute to the URL, meaning they must make decisions based on request
 * state (for example, getting the current user from the incoming request). They also cannot affect the URL to force
 * browsers to refresh something when the underlying transformation changes. This has been deprecated in favour of
 * {@link com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer} which fixes these issues.
 *
 * @deprecated since 3.0. Use {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory}
 *
 * @since 2.5.0
 */
@Deprecated
public interface WebResourceTransformer 
{
    /**
     * Transforms the downloadable resource by returning a new one.
     *
     * @param configElement The element where it was used.  This is provided to allow the transformer to
     * take additional configuration in the form of custom attributes or sub-elements.
     * @param location The original resource location
     * @param filePath Extra path information.  Cannot be null, but can be an empty string if no extra path information
     * @param nextResource The original resource
     * @return The new resource representing the transformed resource
     */
    DownloadableResource transform(Element configElement, ResourceLocation location, String filePath, DownloadableResource nextResource);
}
