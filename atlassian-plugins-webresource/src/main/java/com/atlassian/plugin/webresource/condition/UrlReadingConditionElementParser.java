package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.loaders.LoaderUtils;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.baseconditions.AbstractConditionElementParser;
import com.atlassian.plugin.web.baseconditions.CompositeCondition;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;
import com.atlassian.plugin.webresource.util.PluginClassLoader;
import org.dom4j.Element;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of AbstractConditionElementParser for UrlReadingCondition.
 *
 * For backwards compatibility, this supports classes that are instances of either
 * Condition (deprecated for web-resources since 3.0) or UrlReadingCondition. If it's a legacy Condition is specified,
 * makeConditionImplementation() will return the Condition wrapped in a DecoratingLegacyCondition.
 *
 * This has some extra semantics around the class attribute:
 *
 * <ul>
 *     <li>
 *         The "class" attribute may represent a UrlReadingCondition or a legacy Condition.
 *         If a legacy Condition is specified, makeConditionImplementation() will return an instance of the
 *         Condition wrapped in a DecoratingLegacyCondition.
 *     </li>
 *     <li>
 *         If the "class2" attribute is specified, makeConditionImplementation() will create an instance of the class
 *         specified by class2.
 *         This enables plugins that target multiple versions of the plugin framework to supply a legacy
 *         Condition implementation via the "class" attribute while leveraging the new
 *         UrlReadingCondition in newer versions of the plugin framework.
 *     </li>
 * </ul>
 *
 * @since v3.0
 */
public class UrlReadingConditionElementParser extends AbstractConditionElementParser<DecoratingCondition>
{
    private final HostContainer hostContainer;

    public UrlReadingConditionElementParser(HostContainer hostContainer)
    {
        this.hostContainer = hostContainer;
    }

    @Override
    protected DecoratingCondition makeConditionImplementation(Plugin plugin, Element element) throws PluginParseException
    {
        try
        {
            String overrideConditionClassName = element.attributeValue("class2");
            String conditionClassName = null != overrideConditionClassName ? overrideConditionClassName
                    : element.attributeValue("class");
            if (conditionClassName == null)
            {
                throw new PluginParseException("Condition element must specify a class attribute");
            }
            Map<String, String> params = LoaderUtils.getParams(element);
            return create(plugin, conditionClassName, params);
        }
        catch (final ClassCastException e)
        {
            throw new PluginParseException("Configured condition class does not implement the Condition interface", e);
        }
        catch (final ConditionLoadingException cle)
        {
            throw new PluginParseException("Unable to load the module's display conditions: " + cle.getMessage(), cle);
        }
    }

    private DecoratingCondition create(Plugin plugin, String className, Map<String, String> params) throws ConditionLoadingException
    {
        Object o = createObject(plugin, className);
        if (o instanceof UrlReadingCondition)
        {
            UrlReadingCondition urlReadingCondition = (UrlReadingCondition) o;
            // Protecting against condition being able to change params.
            HashMap<String, String> paramsCopy = new HashMap<String, String>(params);
            urlReadingCondition.init(params);
            return new DecoratingUrlReadingCondition(urlReadingCondition, paramsCopy);
        }
        else
        {
            Condition condition = (Condition) o;
            condition.init(params);
            return new DecoratingLegacyCondition(condition);
        }
    }

    private <T> T createObject(Plugin plugin, String className) throws ConditionLoadingException
    {
        try
        {
            return PluginClassLoader.create(plugin, this.getClass(), hostContainer, className);
        }
        catch (ClassNotFoundException e)
        {
            throw new ConditionLoadingException("Cannot load condition class: " + className, e);
        }
    }

    @Override
    protected DecoratingCondition invert(DecoratingCondition condition)
    {
        return condition.invertCondition();
    }

    @Override
    protected CompositeCondition<DecoratingCondition> createAndCompositeCondition()
    {
        return new DecoratingAndCompositeCondition();
    }

    @Override
    protected CompositeCondition<DecoratingCondition> createOrCompositeCondition()
    {
        return new DecoratingOrCompositeCondition();
    }
}
