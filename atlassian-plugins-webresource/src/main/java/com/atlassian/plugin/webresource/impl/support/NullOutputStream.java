package com.atlassian.plugin.webresource.impl.support;

import java.io.IOException;
import java.io.OutputStream;

/**
 * OutputStream that discards all bytes
 *
 * @since 3.3
 */
public class NullOutputStream extends OutputStream
{
    @Override
    public void write(final int b) throws IOException {}

    @Override
    public void write(final byte b[], final int off, final int len) throws IOException{}
}
