package com.atlassian.plugin.webresource.condition;

import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * Wrapper for legacy conditions
 * @since v3.0
 */
class DecoratingLegacyCondition implements DecoratingCondition
{
    protected final Condition legacyCondition;
    private final boolean invert;

    public DecoratingLegacyCondition(Condition legacyCondition)
    {
        this(legacyCondition, false);
    }

    public DecoratingLegacyCondition(Condition legacyCondition, boolean invert)
    {
        this.legacyCondition = legacyCondition;
        this.invert = invert;
    }

    @Override
    public void addToUrl(UrlBuilder urlBuilder)
    {
    }

    @Override
    public boolean shouldDisplay(QueryParams params)
    {
        // If this is being called for a legacy condition, it means that the shouldDisplayImmediate() returned true
        // and the resource was included on the served page - so return true.
        return true;
    }

    @Override
    public boolean shouldDisplayImmediate(Map<String, Object> params)
    {
        boolean shouldDisplay = this.legacyCondition.shouldDisplay(params);
        return this.invert ? !shouldDisplay : shouldDisplay;
    }

    @Override
    public boolean canEncodeStateIntoUrl()
    {
        return false;
    }

    @Override
    public DecoratingCondition invertCondition()
    {
        return new DecoratingLegacyCondition(legacyCondition, !invert);
    }
}
