package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.servlet.DownloadableResource;

/**
 * DownloadableResource that performs transformations
 *
 * @since 3.1.0
 *
 * @deprecated Since 3.0.10. See {@link com.atlassian.plugin.webresource.transformer.SearchAndReplacer}
 */
@Deprecated
public class SearchAndReplaceDownloadableResource extends CharSequenceDownloadableResource
{
    private final SearchAndReplacer grep;

    public SearchAndReplaceDownloadableResource(DownloadableResource originalResource, SearchAndReplacer grep) {
        super(originalResource);
        this.grep = grep;
    }

    @Override
    public CharSequence transform(CharSequence originalContent) {
        return grep.replaceAll(originalContent);
    }
}
