package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.webresource.api.assembler.resource.PluginCssResource;
import com.atlassian.webresource.api.assembler.resource.PluginCssResourceParams;

/**
 * Implementation of PluginUrlResource
 * @since 3.0
 */
public class DefaultPluginCssResource extends DefaultPluginUrlResource<PluginCssResourceParams> implements PluginCssResource
{
    public DefaultPluginCssResource(ResourceUrl resourceUrl)
    {
        super(resourceUrl);
    }

    @Override
    public PluginCssResourceParams getParams()
    {
        return new DefaultPluginCssResourceParams(resourceUrl.getParams());
    }
}