package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;

import java.util.Date;
import java.util.List;

/**
 * Virtual Web Resource, to represent Context or Super Batch.
 */
public class WebResource extends Bundle
{
    private final TransformerParameters transformerParameters;

    public WebResource(Snapshot snapshot, String key, List<String> dependencies, Date updatedAt,
            String version, boolean isTransformable, TransformerParameters transformerParameters)
    {
        super(snapshot, key, dependencies, updatedAt, version, isTransformable);
        this.transformerParameters = transformerParameters;
    }

    @Override
    public CachedCondition getCondition()
    {
        return snapshot.webResourcesCondition.get(this);
    }

    @Override
    public CachedTransformers getTransformers()
    {
        return snapshot.webResourcesTransformations.get(this);
    }

    @Override
    public TransformerParameters getTransformerParameters()
    {
        return transformerParameters;
    }

    /**
     * Instead of storing plugin and web resource kyes and taking memory, evaluating it each time it is used.
     * It used during transformation only.
     * @deprecated since v3.3.2
     */
    @Deprecated
    public String getWebResourceKey()
    {
        return getKey().split(":")[1];
    }

    /**
     * @deprecated since v3.3.2
     */
    @Deprecated
    public String getPluginKey()
    {
        return getKey().split(":")[0];
    }
}