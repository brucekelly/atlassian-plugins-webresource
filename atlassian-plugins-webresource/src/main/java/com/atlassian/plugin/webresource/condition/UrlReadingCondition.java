package com.atlassian.plugin.webresource.condition;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.baseconditions.BaseCondition;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;

import java.util.Map;

/**
 * Condition that contributes to the URL.
 *
 * Web Resource module descriptors can use Conditions of this class or of {@link com.atlassian.plugin.web.Condition},
 * however the use of Condition for webresources is discouraged. If a webresource contains a
 * com.atlassian.plugin.web.Condition, that webresource is taken out of the batch and included as a standalone resource
 * download. This has two negative effects:
 * <ul>
 *     <li>It breaks resource dependency, as the resource is no longer sent to the browser in dependency order</li>
 *     <li>It reduces performance, as the browser must initiate more connections to download resources that would
 *     otherwise be batched</li>
 * </ul>
 *
 * UrlReadingConditions are included in batches. When an HTML page is served, the UrlReadingCondition is asked to
 * contribute to the URL in {@link #addToUrl(com.atlassian.plugin.webresource.url.UrlBuilder)}. At resource serve time,
 * the condition evaluates whether it should display based on the querystring.
 *
 * @since v3.0
 */
@ExperimentalApi
public interface UrlReadingCondition extends BaseCondition
{
    /**
     * Called after creation and autowiring.
     * @param params The optional map of parameters specified in XML.
     */
    void init(Map<String, String> params) throws PluginParseException;

    /**
     * Called when constructing the URL as the hosting HTML page is being served. Can add parameters to the query
     * string and alter the resource hash.
     * @param urlBuilder interface for contributing to the URL
     */
    void addToUrl(UrlBuilder urlBuilder);

    /**
     * Determine whether the web fragment should be displayed. This method should only read values from its config
     * and the query params.
     * @param params query params
     * @return true if the user should see the fragment, false otherwise
     */
    boolean shouldDisplay(QueryParams params);
}
