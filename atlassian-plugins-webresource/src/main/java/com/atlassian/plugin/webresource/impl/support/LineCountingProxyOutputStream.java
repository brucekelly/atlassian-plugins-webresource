package com.atlassian.plugin.webresource.impl.support;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @since 3.3
 */
public class LineCountingProxyOutputStream extends OutputStream
{
    private final OutputStream out;
    private int linesCount = 0;

    public LineCountingProxyOutputStream(OutputStream out)
    {
        this.out = out;
    }

    public void write(int b) throws IOException
    {
        if (linesCount == 0)
        {
            linesCount += 1;
        }
        if (b == '\n')
        {
            linesCount += 1;
        }
        out.write(b);
    }

    public void flush() throws IOException
    {
        out.flush();
    }

    public void close() throws IOException
    {
        out.close();
    }

    public int getLinesCount()
    {
        return linesCount;
    }
}