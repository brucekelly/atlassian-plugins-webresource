package com.atlassian.plugin.webresource.transformer;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.servlet.DownloadableResource;

/**
 * Describes a resource for transformation
 * @since v3.0
 */
@ExperimentalApi
public class TransformableResource
{
    private final ResourceLocation location;
    private final String filePath;
    private final DownloadableResource nextResource;

    public TransformableResource(ResourceLocation location, String filePath, DownloadableResource nextResource)
    {
        this.location = location;
        this.filePath = filePath;
        this.nextResource = nextResource;
    }

    /**
     * The original resource location
     */
    public ResourceLocation location()
    {
        return this.location;
    }

    /**
     * Extra path information.  Cannot be null, but can be an empty string if no extra path information
     */
    public String filePath()
    {
        return this.filePath;
    }

    /**
     * The original resource
     */
    public DownloadableResource nextResource()
    {
        return nextResource;
    }
}
