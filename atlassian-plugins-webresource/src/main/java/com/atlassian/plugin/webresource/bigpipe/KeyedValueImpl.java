package com.atlassian.plugin.webresource.bigpipe;

import com.atlassian.fugue.Either;
import com.atlassian.webresource.api.bigpipe.Key;
import com.atlassian.webresource.api.bigpipe.KeyedValue;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 3.3
 */
public class KeyedValueImpl<V> implements KeyedValue<V>
{
    private final Key key;
    private final Either<Exception, V> value;

    public static <T> KeyedValue<T> success(Key key, T value)
    {
        return new KeyedValueImpl<T>(key, Either.<Exception, T>right(value));
    }

    public static <T> KeyedValue<T> fail(Key key, Exception ex)
    {
        return new KeyedValueImpl<T>(key, Either.<Exception, T>left(ex));
    }

    public KeyedValueImpl(Key key, Either<Exception, V> value)
    {
        this.key = checkNotNull(key);
        this.value = checkNotNull(value);
    }

    public Key key()
    {
        return key;
    }

    public Either<Exception, V> value()
    {
        return value;
    }
}
