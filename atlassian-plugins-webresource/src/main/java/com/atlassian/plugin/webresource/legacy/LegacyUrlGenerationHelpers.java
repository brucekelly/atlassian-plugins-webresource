package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.helpers.UrlGenerationHelpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */
public class LegacyUrlGenerationHelpers
{
    /**
     * DTO.
     */
    public static class Resolved
    {
        public final List<UrlGenerationHelpers.ContextBatchKey> contextBatchKeys;
        public final List<String> webResourceBatchKeys;
        public final LinkedHashSet<String> excludedResolved;

        public Resolved(List<UrlGenerationHelpers.ContextBatchKey> contextBatchKeys,
            List<String> webResourceBatchKeys, LinkedHashSet<String> excludedResolved)
        {
            this.contextBatchKeys = contextBatchKeys;
            this.webResourceBatchKeys = webResourceBatchKeys;
            this.excludedResolved = excludedResolved;
        }
    }

    /**
     * Resolves given included and excluded web resources and contexts into batches.
     * @param requestCache request cache.
     * @param allIncluded included web resources and contexts.
     * @param allExcluded excluded web resources and contexts.
     * @return context batches, web resource batches and list of actually excluded web resources and contexts.
     */
    public static Resolved calculateBatches(RequestCache requestCache, LinkedHashSet<String> allIncluded,
        LinkedHashSet<String> allExcluded, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        // Converting input data to legacy format.
        String prefix = Config.CONTEXT_PREFIX + ":";
        Set<String> requiredContexts = new LinkedHashSet<String>();
        Set<String> requiredWebResources = new LinkedHashSet<String>();
        boolean isSuperBatchEnabled = false;
        for (String key : allIncluded)
        {
            if (Config.isContextKey(key))
            {
                if (Config.SUPERBATCH_KEY.equals(key))
                {
                    isSuperBatchEnabled = true;
                }
                else
                {
                    requiredContexts.add(key.replace(prefix, ""));
                }
            }
            else
            {
                requiredWebResources.add(key);
            }
        }

        Set<String> excludedContexts = new LinkedHashSet<String>();
        Set<String> excludedWebResources = new LinkedHashSet<String>();
        boolean isSuperBatchHasBeenEscluded = false;
        for (String key : allExcluded)
        {
            if (Config.SUPERBATCH_KEY.equals(key))
            {
                isSuperBatchHasBeenEscluded = true;
            }
            else
            {
                if (Config.isContextKey(key))
                {
                    excludedContexts.add(key.replace(prefix, ""));
                }
                else
                {
                    excludedWebResources.add(key);
                }
            }
        }

        // Preparing legacy interfaces.
        Config config = requestCache.getGlobals().getConfig();
        isSuperBatchEnabled |= isSuperBatchHasBeenEscluded;
        com.atlassian.plugin.webresource.legacy.ResourceDependencyResolver legacyDependencyResolver =
            new com.atlassian.plugin.webresource.legacy.DefaultResourceDependencyResolver(requestCache.getGlobals(),
                config.getIntegration(), isSuperBatchEnabled,
                config.getBatchingConfiguration().getSuperBatchModuleCompleteKeys());
        com.atlassian.plugin.webresource.legacy.PluginResourceLocator legacyResourceLocator =
            new com.atlassian.plugin.webresource.legacy.PluginResourceLocatorImpl(config.getIntegration());

        ResourceRequirer resourceRequirer = new ResourceRequirer(
            config.getIntegration(), legacyResourceLocator, legacyDependencyResolver,
            config.getBatchingConfiguration().resplitMergedContextBatchesForThisRequest(), isSuperBatchEnabled,
            includeDependenciesForFailedUrlReadingConditions);

        // Calculating dependencies.
        InclusionState inclusionState = new InclusionState(isSuperBatchHasBeenEscluded, excludedWebResources, excludedContexts);
        Collection<PluginResource> resources = resourceRequirer.includeResources(requestCache, requiredWebResources,
            requiredContexts, inclusionState);

        // Converting output data from legacy format.
        List<UrlGenerationHelpers.ContextBatchKey> contextBatchKeys = new ArrayList<UrlGenerationHelpers.ContextBatchKey>();
        List<String> webResourceBatchKeys = new ArrayList<String>();

        boolean hasSuperbatch = false;
        for (PluginResource pluginResource : resources)
        {
            if (pluginResource instanceof SuperBatchPluginResource)
            {
                List<String> included = new ArrayList<String>();
                included.add(Config.SUPERBATCH_KEY);
                contextBatchKeys.add(new UrlGenerationHelpers.ContextBatchKey(included, new LinkedHashSet<String>()));
                hasSuperbatch = true;
                break;
            }
        }
        for (PluginResource pluginResource : resources)
        {
            if (pluginResource instanceof ContextBatchPluginResource)
            {
                List<String> included = new ArrayList<String>();
                LinkedHashSet<String> excluded = new LinkedHashSet<String>();
                ContextBatchPluginResource contextBatchPluginResource = (ContextBatchPluginResource) pluginResource;
                for (String key : contextBatchPluginResource.getContexts())
                {
                    included.add(Config.CONTEXT_PREFIX + ":" + key);
                }
                if (isSuperBatchHasBeenEscluded || (hasSuperbatch && inclusionState.superbatch))
                {
                    excluded.add(Config.SUPERBATCH_KEY);
                }
                for (String key : contextBatchPluginResource.getExcludedContexts())
                {
                    excluded.add(Config.CONTEXT_PREFIX + ":" + key);
                }
                contextBatchKeys.add(new UrlGenerationHelpers.ContextBatchKey(included, excluded));
            }
            if (pluginResource instanceof BatchPluginResource)
            {
                BatchPluginResource batchPluginResource = (BatchPluginResource) pluginResource;
                webResourceBatchKeys.add(batchPluginResource.getModuleCompleteKey());
            }
        }

        LinkedHashSet<String> excludedResolved = new LinkedHashSet<String>();
        if (isSuperBatchHasBeenEscluded || (hasSuperbatch && inclusionState.superbatch))
        {
            excludedResolved.add(Config.SUPERBATCH_KEY);
        }
        for (String key : inclusionState.contexts)
        {
            if (!Config.SUPER_BATCH_CONTEXT_KEY.equals(key))
            {
                excludedResolved.add(Config.CONTEXT_PREFIX + ":" + key);
            }
        }
        for (String key : inclusionState.webresources)
        {
            excludedResolved.add(key);
        }
        return new Resolved(contextBatchKeys, webResourceBatchKeys, excludedResolved);
    }
}
