package com.atlassian.plugin.webresource.impl.support;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.helpers.UrlGenerationHelpers;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * Cache for URL Generation, or more exactly three Caches for:
 * - Batches for given included and excluded.
 * - Legacy resources for given included and excluded.
 * - Resolved excluded for given include and excluded.
 *
 * @since v3.3
 */
public interface UrlCache
{
    public static class IncludedAndExcluded extends Tuple<LinkedHashSet<String>, LinkedHashSet<String>>
    {
        public IncludedAndExcluded(LinkedHashSet<String> included, LinkedHashSet<String> excluded)
        {
            super(included, excluded);
        }

        public LinkedHashSet<String> getIncluded()
        {
            return getFirst();
        }

        public LinkedHashSet<String> getExcluded()
        {
            return getLast();
        }
    }

    public static class IncludedAndExcludedAndConditions extends Tuple<IncludedAndExcluded, List<Boolean>>
    {
        public IncludedAndExcludedAndConditions(IncludedAndExcluded includedAndExcluded, List<Boolean> conditions)
        {
            super(includedAndExcluded, conditions);
        }

        public LinkedHashSet<String> getIncluded()
        {
            return getFirst().getIncluded();
        }

        public LinkedHashSet<String> getExcluded()
        {
            return getFirst().getExcluded();
        }
    }

    public static interface ConditionsProvider
    {
        List<CachedCondition> get(IncludedAndExcluded key);
    }

    /**
     * Get list of web resources with conditions for given included and excluded.
     * @return context batches, web resource batches, resolved excluded resources.
     */
    List<CachedCondition> getConditions(IncludedAndExcluded key, ConditionsProvider provider);

    public static interface BatchesProvider
    {
        UrlGenerationHelpers.CalculatedBatches get(IncludedAndExcludedAndConditions key);
    }

    /**
     * Get batches for given included and excluded.
     * @return context batches, web resource batches, resolved excluded resources.
     */
    UrlGenerationHelpers.CalculatedBatches getBatches(IncludedAndExcludedAndConditions key, BatchesProvider provider);

    public static interface ResolvedExcludedProvider
    {
        LinkedHashSet<String> get(IncludedAndExcludedAndConditions key);
    }

    /**
     * Get resolved excluded for given include and excluded.
     * @return resolved excluded resources
     */
    LinkedHashSet<String> getResolvedExcluded(IncludedAndExcludedAndConditions key, ResolvedExcludedProvider provider);

    void clear();

    /**
     * Implementation.
     */
    public static class Impl implements UrlCache
    {
        private Cache<
            IncludedAndExcluded,
            List<CachedCondition>> cachedConditions;
        private Cache<IncludedAndExcludedAndConditions, UrlGenerationHelpers.CalculatedBatches> cachedBatches;
        private Cache<
            IncludedAndExcludedAndConditions,
            LinkedHashSet<String>> cachedResolvedExcluded;

        public Impl(int size)
        {
            cachedConditions = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedBatches = CacheBuilder.newBuilder().maximumSize(size).build();
            cachedResolvedExcluded = CacheBuilder.newBuilder().maximumSize(size).build();
        }

        @Override
        public List<CachedCondition> getConditions(final IncludedAndExcluded key, final ConditionsProvider provider)
        {
            try
            {
                return cachedConditions.get(key, new Callable<List<CachedCondition>>()
                {
                    @Override
                    public List<CachedCondition> call() throws Exception
                    {
                        return provider.get(key);
                    }
                });
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public UrlGenerationHelpers.CalculatedBatches getBatches(final IncludedAndExcludedAndConditions key, final BatchesProvider provider)
        {
            try
            {
                return cachedBatches.get(key, new Callable<UrlGenerationHelpers.CalculatedBatches>()
                {
                    @Override
                    public UrlGenerationHelpers.CalculatedBatches call() throws Exception
                    {
                        return provider.get(key);
                    }
                });
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public LinkedHashSet<String> getResolvedExcluded(final IncludedAndExcludedAndConditions key,
            final ResolvedExcludedProvider provider)
        {
            try
            {
                return cachedResolvedExcluded.get(key, new Callable<LinkedHashSet<String>>()
                {
                    @Override
                    public LinkedHashSet<String> call() throws Exception
                    {
                        return provider.get(key);
                    }
                });
            }
            catch (ExecutionException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void clear()
        {
            cachedConditions.invalidateAll();
            cachedBatches.invalidateAll();
            cachedResolvedExcluded.invalidateAll();
        }
    }

    /**
     * No caching implementation.
     */
    public static class PassThrough implements UrlCache
    {
        @Override
        public List<CachedCondition> getConditions(final IncludedAndExcluded key, final ConditionsProvider provider)
        {
            return provider.get(key);
        }

        @Override
        public UrlGenerationHelpers.CalculatedBatches getBatches(IncludedAndExcludedAndConditions key, BatchesProvider provider)
        {
            return provider.get(key);
        }

        @Override
        public LinkedHashSet<String> getResolvedExcluded
            (IncludedAndExcludedAndConditions key, ResolvedExcludedProvider provider)
        {
            return provider.get(key);
        }

        @Override
        public void clear()
        {
        }
    }
}