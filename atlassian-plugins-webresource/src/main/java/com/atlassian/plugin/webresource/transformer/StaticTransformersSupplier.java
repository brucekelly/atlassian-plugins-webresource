package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;

/**
 * Supplies static transformers
 *
 * @since v3.1.0
 */
public interface StaticTransformersSupplier
{
    /**
     * @param type type of resource (eg js / css)
     * @return static transformers to apply to the given type
     */
    public Iterable<ContentTransformerFactory> get(String type);

    /**
     * @param resourceLocation resource location
     * @return static transformers to apply to the given resourceLocation
     */
    public Iterable<ContentTransformerFactory> get(ResourceLocation resourceLocation);
}
