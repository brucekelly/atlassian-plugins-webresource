package com.atlassian.plugin.webresource.impl;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import static org.apache.commons.lang.StringUtils.join;

/**
 * State maintained during single request.
 *
 * @since 3.3
 */
public class RequestState
{
    protected final LinkedHashSet<String> included;
    protected final LinkedHashSet<String> excluded;
    protected final LinkedHashMap<String, Jsonable> includedData;
    protected final Set<String> excludedData;
    protected final RequestCache cache;

    protected final Globals globals;

    protected RequestState(Globals globals, LinkedHashSet<String> included, LinkedHashSet<String> excluded,
        LinkedHashMap<String, Jsonable> includedData, Set<String> excludedData, RequestCache cache)
    {
        this.globals = globals;
        this.included = included;
        this.excluded = excluded;
        this.includedData = includedData;
        this.excludedData = excludedData;
        this.cache = cache;
    }

    public RequestState(Globals globals)
    {
        this(globals, new LinkedHashSet<String>(), new LinkedHashSet<String>(), new LinkedHashMap<String, Jsonable>(),
            new HashSet<String>(), new RequestCache(globals));
    }

    public RequestState deepClone()
    {
        return new RequestState(globals, new LinkedHashSet<String>(included), new LinkedHashSet<String>(excluded),
            new LinkedHashMap<String, Jsonable>(includedData), new HashSet<String>(excludedData), cache);
    }

    public LinkedHashSet<String> getIncluded()
    {
        return included;
    }

    public LinkedHashMap<String, Jsonable> getIncludedData()
    {
        return includedData;
    }

    public Set<String> getExcludedData()
    {
        return excludedData;
    }

    public LinkedHashSet<String> getExcluded()
    {
        return excluded;
    }

    /**
     * Called after generating urls, to clear current state and remember already included resources.
     */
    public void clearIncludedAndUpdateExcluded(LinkedHashSet<String> excludedResolved)
    {
        included.clear();
        excluded.addAll(excludedResolved);

        excludedData.addAll(includedData.keySet());
        includedData.clear();
    }

    public Globals getGlobals()
    {
        return globals;
    }

    /**
     * Get all bundles.
     * <p/>
     * It is another layer of cache over the `globals.getBundles()` because it is used very heavily, to avoid any
     * performance drawbacks of atomic reference in the `globals.getBundles()`.
     */
    public Snapshot getSnapshot()
    {
        return getCache().getSnapshot();
    }

    public RequestCache getCache()
    {
        return cache;
    }

    public String toString()
    {
        return "[" + join(included, ", ") + "] - [" + join(excluded, ", ") + "]";
    }
}