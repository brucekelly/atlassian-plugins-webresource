package com.atlassian.plugin.webresource.assembler;

import com.atlassian.plugin.webresource.BatchResourceContentsWebFormatter;
import com.atlassian.plugin.webresource.impl.config.BatchType;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.WebResourceFormatter;
import com.atlassian.plugin.webresource.data.DataTagWriter;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;
import com.atlassian.webresource.api.assembler.WebResourceSet;
import com.atlassian.webresource.api.assembler.resource.PluginUrlResource;
import com.atlassian.webresource.api.data.PluginDataResource;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of WebResourceSet
 * @since v3.0
 */
public class DefaultWebResourceSet implements WebResourceSet
{
    private final List<Container> resources;
    private final List<PluginDataResource> data;
    private final RequestState requestState;

    public DefaultWebResourceSet(RequestState requestState, List<PluginDataResource> data, List<ResourceUrl> resourceUrls)
    {
        this.requestState = requestState;
        this.data = data;

        resources = new LinkedList<Container>();
        for (ResourceUrl resourceUrl : resourceUrls)
        {
            PluginUrlResource pluginUrlResource;
            if (BatchType.js.toString().equals(resourceUrl.getType()))
            {
                pluginUrlResource = new DefaultPluginJsResource(resourceUrl);
            }
            else if (BatchType.css.toString().equals(resourceUrl.getType()))
            {
                pluginUrlResource = new DefaultPluginCssResource(resourceUrl);
            }
            else
            {
                throw new RuntimeException("unsupported extension " + resourceUrl.getType());
            }
            resources.add(new Container(resourceUrl, pluginUrlResource));
        }
    }

    @Override
    public Iterable<WebResource> getResources()
    {
        List<WebResource> webResources = new LinkedList<WebResource>(data);
        for (Container resource : resources)
        {
            webResources.add(resource.getPluginUrlResource());
        }
        return webResources;
    }

    @Override
    public <T extends WebResource> Iterable<T> getResources(Class<T> clazz)
    {
        return (Iterable<T>) Iterables.filter(getResources(), Predicates.instanceOf(clazz));
    }

    @Override
    public void writeHtmlTags(Writer writer, UrlMode urlMode)
    {
        writeHtmlTags(writer, urlMode, Predicates.<WebResource>alwaysTrue());
    }

    @Override
    public void writeHtmlTags(Writer writer, UrlMode urlMode, final Predicate<WebResource> predicate)
    {
        writeHtmlTags(writer, urlMode, predicate, Predicates.<ResourceUrl>alwaysTrue());
    }

    public void writeHtmlTags(Writer writer, UrlMode urlMode, final Predicate<WebResource> predicate,
        final Predicate<ResourceUrl> legacyPredicate)
    {
        try
        {
            new DataTagWriter().write(writer, Iterables.filter(data, predicate));
        }
        catch (IOException ex)
        {
            Support.LOGGER.error("IOException encountered rendering data tags", ex);
        }

        List<Container> localCopyOfResources = new LinkedList<Container>();
        for (Container resource : resources)
        {
            if (predicate.apply(resource.getPluginUrlResource()) && legacyPredicate.apply(resource.getResourceUrl()))
            {
                localCopyOfResources.add(resource);
            }
        }

        for (final WebResourceFormatter formatter : WebResourceFormatter.webResourceFormatters)
        {
            for (final Iterator<Container> iter = localCopyOfResources.iterator(); iter.hasNext(); )
            {
                final Container resource = iter.next();
                if (formatter.matches(resource.getResourceUrl().getName()))
                {
                    writeResourceTag(urlMode, resource, formatter, writer);
                    iter.remove();
                }
            }
        }

        for (Container resource : localCopyOfResources)
        {
            writeContentAndSwallowErrors(writer, "<!-- Error loading resource \"",
                resource.getResourceUrl().getKey(),
                "\".  No resource formatter matches \"", resource.getResourceUrl().getName(), "\" -->\n");
        }
    }

    private void writeResourceTag(final UrlMode urlMode, final Container resource,
        final WebResourceFormatter formatter, final Writer writer)
    {
        PluginUrlResource urlResource = resource.getPluginUrlResource();
        String formattedResource = formatter.formatResource(urlResource.getStaticUrl(urlMode), urlResource.getParams().all());

        // insert the dependency information into the formatted resource
        if (requestState.getGlobals().getConfig().isBatchContentTrackingEnabled())
        {
            formattedResource = BatchResourceContentsWebFormatter.insertBatchResourceContents(resource, formattedResource);
        }

        writeContentAndSwallowErrors(writer, formattedResource);
    }

    private void writeContentAndSwallowErrors(final Writer writer, final String... contents)
    {
        try
        {
            for (final String content : contents)
            {
                writer.write(content);
            }
        }
        catch (final IOException ex)
        {
            Support.LOGGER.error("IOException encountered rendering resource", ex);
        }
    }

    /**
     * Internal data structure to pass couple of objects together.
     */
    public static class Container
    {
        private final ResourceUrl resourceUrl;
        private final PluginUrlResource pluginUrlResource;

        public Container(final ResourceUrl resourceUrl, PluginUrlResource pluginUrlResource)
        {

            this.resourceUrl = resourceUrl;
            this.pluginUrlResource = pluginUrlResource;
        }
        
        public PluginUrlResource getPluginUrlResource()
        {
            return pluginUrlResource;
        }

        public ResourceUrl getResourceUrl()
        {
            return resourceUrl;
        }
    }
}
