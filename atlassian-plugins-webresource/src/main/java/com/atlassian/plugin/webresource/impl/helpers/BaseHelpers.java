package com.atlassian.plugin.webresource.impl.helpers;

import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.snapshot.Snapshot;
import com.atlassian.plugin.webresource.impl.support.Support;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.support.Support.efficientAndPredicate;

/**
 * Stateless helper functions providing basic support for resolving dependencies for resources.
 *
 * @since v3.3
 */
public class BaseHelpers
{
    /**
     * Filter by web resource conditions evaluated against params.
     */
    public static Predicate<Bundle> isConditionsSatisfied(final RequestCache requestCache,
        final Map<String, String> params)
    {
        return new Predicate<Bundle>()
        {
            @Override
            public boolean apply(Bundle bundle)
            {
                CachedCondition condition = bundle.getCondition();
                return (condition == null) || (condition.evaluateSafely(requestCache, params));
            }
        };
    }

    /**
     * Filter by web resource conditions evaluated immediately.
     */
    public static Predicate<Bundle> isConditionsSatisfied(final RequestCache requestCache)
    {
        return new Predicate<Bundle>()
        {
            @Override
            public boolean apply(Bundle bundle)
            {
                CachedCondition condition = bundle.getCondition();
                return (condition == null) || (condition.evaluateSafely(requestCache));
            }
        };
    }

    /**
     * Filter that leaves only resources that have legacy conditions.
     */
    public static Predicate<Bundle> hasLegacyCondition()
    {
        return new Predicate<Bundle>()
        {
            @Override
            public boolean apply(Bundle bundle)
            {
                return bundle.hasLegacyConditions();
            }
        };
    }

    /**
     * Filter that leaves only resources that have legacy conditions.
     */
    public static Predicate<Bundle> hasConditions()
    {
        return new Predicate<Bundle>()
        {
            @Override
            public boolean apply(Bundle bundle)
            {
                return bundle.getCondition() != null;
            }
        };
    }

    /**
     * DSL for bundle search queries.
     */
    public static class BundleFinder
    {
        private Snapshot snapshot;
        private Collection<String> included = new ArrayList<String>();
        private Set<String> excluded = new HashSet<String>();
        private boolean resolveExcluded = true;
        private Predicate<Bundle> deep = Predicates.<Bundle>alwaysTrue();
        private List<Predicate<Bundle>> deepFilters = new ArrayList<Predicate<Bundle>>();
        private List<Predicate<Bundle>> shallowFilters = new ArrayList<Predicate<Bundle>>();

        public BundleFinder(Snapshot snapshot)
        {
            this.snapshot = snapshot;
        }
        
        public BundleFinder included(Collection<String> included)
        {
            this.included = included;
            return this;
        }

        public BundleFinder included(String key)
        {
            included = new ArrayList<String>();
            included.add(key);
            return this;
        }
        
        public BundleFinder excluded(LinkedHashSet<String> excluded)
        {
            this.excluded = excluded;
            this.resolveExcluded = true;
            return this;
        }

        public BundleFinder excludedResolved(Set<String> excluded)
        {
            this.excluded = excluded;
            this.resolveExcluded = false;
            return this;
        }

        /**
         * If it should resolve full tree of dependencies recursively, `true` by default.
         */
        public BundleFinder deep(boolean deep)
        {
            return deep(deep ? Predicates.<Bundle>alwaysTrue() : Predicates.<Bundle>alwaysFalse());
        }

        /**
         * If it should resolve full tree of dependencies recursively for the given Bundle,
         * it's `Predicates.<Bundle>alwaysTrue()` by default.
         */
        public BundleFinder deep(Predicate<Bundle> deep)
        {
            this.deep = deep;
            return this;
        }

        /**
         * Remove element and all its dependencies if filter fail.
         */
        public BundleFinder deepFilter(Predicate<Bundle> filter)
        {
            this.deepFilters.add(filter);
            return this;
        }

        /**
         * Remove element but leave its dependencies if filter fail.
         */
        public BundleFinder shallowFilter(Predicate<Bundle> filter)
        {
            this.shallowFilters.add(filter);
            return this;
        }

        public Found endAndGetResult()
        {
            return find(snapshot, included, excluded, resolveExcluded, deep,
                efficientAndPredicate(deepFilters), efficientAndPredicate(shallowFilters));
        }

        /**
         * Run query and get found Bundles.
         */
        public List<String> end()
        {
            return endAndGetResult().found;
        }

        /**
         * Search for Resources instead of Bundles. Appends additional search query for Resources to already defined
         * search query for Bundles.
         */
        public ResourceFinder resources(RequestCache requestCache)
        {
            return new ResourceFinder(this, requestCache);
        }
    }

    /**
     * DSL for resource search query.
     */
    public static class ResourceFinder
    {
        private final BundleFinder bundleFinder;
        private final RequestCache requestCache;
        private Predicate<Resource> filter;

        public ResourceFinder(BundleFinder bundleFinder, RequestCache requestCache)
        {
            this.bundleFinder = bundleFinder;
            this.requestCache = requestCache;
        }

        /**
         * Filter resources.
         */
        public ResourceFinder filter(Predicate<Resource> filter)
        {
            this.filter = filter;
            return this;
        }

        /**
         * Run query and get found resources.
         */
        public List<Resource> end()
        {
            List<String> keys = bundleFinder.end();
            List<Resource> resources = getResources(requestCache, keys);
            List<Resource> filtered = new ArrayList<Resource>();
            for (Resource resource : resources)
            {
                if (filter.apply(resource))
                {
                    filtered.add(resource);
                }
            }
            return filtered;
        }
    }

    /**
     * Find Bundles.
     */
    public static BundleFinder find(Snapshot snapshot)
    {
        return new BundleFinder(snapshot);
    }

    /**
     * DTO.
     */
    public static class Found
    {
        public final List<String> found;
        public final List<String> skipped;

        public Found(final List<String> found, final List<String> skipped)
        {
            this.found = found;
            this.skipped = skipped;
        }
    }

    /**
     * Resolve dependencies with filtering predicates for lists of included and excluded Web Resources.
     *
     * @param included keys of included Web Resources.
     * @param excluded keys of excluded Web Resources.
     * @return keys of resolved Web Resources.
     */
    protected static Found find(Snapshot snapshot, Collection<String> included,
        Set<String> excluded, boolean resolveExcluded, Predicate<Bundle> deep, final Predicate<Bundle> deepFilter,
        Predicate<Bundle> shallowFilter)
    {
        if (included.isEmpty())
        {
            return new Found(new ArrayList<String>(), new ArrayList<String>());
        }

        // Resolving excluded.
        Set<String> excludedResolved;
        if (resolveExcluded)
        {
            excludedResolved = new HashSet<String>(
            find(snapshot, excluded, new LinkedHashSet<String>(), false, Predicates.<Bundle>alwaysTrue(),
                Predicates.<Bundle>alwaysTrue(), Predicates.<Bundle>alwaysTrue()).found);
        }
        else
        {
            excludedResolved = excluded;
        }

        // Resolving included.
        LinkedHashSet<String> resolved = new LinkedHashSet<String>();
        List<String> skipped = new ArrayList<String>();
        for (String key : included)
        {
            findItChangesArguments(snapshot, key, excludedResolved, new LinkedHashSet<String>(),
                deep, deepFilter, shallowFilter, resolved, skipped);
        }
        return new Found(new ArrayList<String>(resolved), skipped);
    }

    /**
     * Resolve dependencies for Web Resource.
     */
    protected static void findItChangesArguments(Snapshot snapshot, String key,
        Set<String> excluded, LinkedHashSet<String> stack, Predicate<Bundle> deep, Predicate<Bundle> deepFilter,
        Predicate<Bundle> shallowFilter, LinkedHashSet<String> resolved, List<String> skipped)
    {
        if (resolved.contains(key))
        {
            return;
        }
        if (excluded.contains(key))
        {
            return;
        }
        if (stack.contains(key))
        {
            Support.LOGGER.warn("cyclic plugin resource dependency has been detected with: {}, stack trace: {}", key,
                stack);
            return;
        }
        stack.add(key);

        Bundle bundle = snapshot.get(key);

        if (bundle != null && deepFilter.apply(bundle))
        {
            if (deep.apply(bundle))
            {
                List<String> bundleDependencies = bundle.getDependencies();
                for (String dependencyKey : bundleDependencies)
                {
                    findItChangesArguments(snapshot, dependencyKey, excluded, stack, deep, deepFilter, shallowFilter,
                        resolved, skipped);
                }
            }
            if (shallowFilter.apply(bundle))
            {
                resolved.add(key);
            }
            else
            {
                skipped.add(key);
            }
        }
        else
        {
            skipped.add(key);
        }

        stack.remove(key);
    }

    /**
     * Get Resources for list of Web Resources.
     *
     * @param keys keys of Web Resources
     */
    public static List<Resource> getResources(RequestCache requestCache, Collection<String> keys)
    {
        List<Resource> resources = new ArrayList<Resource>();
        for (String key : keys)
        {
            Bundle bundle = requestCache.getSnapshot().get(key);
            if (bundle == null)
            {
                continue;
            }
            for (Resource resource : bundle.getResources(requestCache).values())
            {
                resources.add(resource);
            }
        }
        return resources;
    }
}
