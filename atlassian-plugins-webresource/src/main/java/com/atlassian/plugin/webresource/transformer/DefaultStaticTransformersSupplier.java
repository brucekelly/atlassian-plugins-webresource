package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerFactory;
import com.atlassian.plugin.webresource.transformer.instance.RelativeUrlTransformerMatcher;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;

/**
 * Implementation of {@link com.atlassian.plugin.webresource.transformer.StaticTransformersSupplier}
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformersSupplier implements StaticTransformersSupplier
{
    private final Iterable<DescribedTransformer> describedTransformers;

    public DefaultStaticTransformersSupplier(WebResourceIntegration webResourceIntegration, WebResourceUrlProvider urlProvider)
    {
        RelativeUrlTransformerFactory relativeUrlTransformerFactory = new RelativeUrlTransformerFactory(webResourceIntegration, urlProvider);
        RelativeUrlTransformerMatcher relativeUrlTransformerMatcher = new RelativeUrlTransformerMatcher();
        describedTransformers = Lists.newArrayList(
                new DescribedTransformer(relativeUrlTransformerMatcher, relativeUrlTransformerFactory)
        );
    }

    @Override
    public Iterable<ContentTransformerFactory> get(final String type)
    {
        return toTransformerFactories(describedTransformers, new Predicate<DescribedTransformer>()
        {
            @Override
            public boolean apply(@Nullable DescribedTransformer input)
            {
                return input.matcher.matches(type);
            }
        });
    }

    @Override
    public Iterable<ContentTransformerFactory> get(final ResourceLocation resourceLocation)
    {
        return toTransformerFactories(describedTransformers, new Predicate<DescribedTransformer>()
        {
            @Override
            public boolean apply(@Nullable DescribedTransformer input)
            {
                return input.matcher.matches(resourceLocation);
            }
        });
    }

    private static Iterable<ContentTransformerFactory> toTransformerFactories(Iterable<DescribedTransformer> describedTransformers, Predicate<DescribedTransformer> predicate)
    {
        return Iterables.transform(Iterables.filter(describedTransformers, predicate), new Function<DescribedTransformer, ContentTransformerFactory>()
        {
            @Override
            public ContentTransformerFactory apply(@Nullable DescribedTransformer input)
            {
                return input.transformerFactory;
            }
        });
    }

    private static final class DescribedTransformer
    {
        private final WebResourceTransformerMatcher matcher;
        private final ContentTransformerFactory transformerFactory;

        private DescribedTransformer(WebResourceTransformerMatcher matcher, ContentTransformerFactory transformerFactory)
        {
            this.matcher = matcher;
            this.transformerFactory = transformerFactory;
        }
    }
}
