package com.atlassian.plugin.webresource.impl.support.http;

import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Probably would be removed after the refactoring, currently needed to hide some implementation details.
 *
 * @since 3.3
 */
public class Response
{
    private final Request request;
    private final HttpServletResponse originalResponse;

    public Response(Globals globals, Request request, HttpServletResponse response)
    {
        this.request = request;
        this.originalResponse = response;
    }

    public void sendRedirect(Resource resource)
    {
        sendRedirect(resource.getLocation(), resource.getContentType());
    }

    public void sendRedirect(String location, String contentType)
    {
        setContentTypeIfNotBlank(contentType);
        try
        {
            request.getOriginalRequest().getRequestDispatcher(location).forward(request.getOriginalRequest(),
                    originalResponse);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        catch (ServletException e2)
        {
            throw new RuntimeException(e2);
        }
    }

    public HttpServletResponse getOriginalResponse()
    {
        return originalResponse;
    }

    public void sendError(int code)
    {
        try
        {
            originalResponse.sendError(code);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setContentType(String contentType)
    {
        originalResponse.setContentType(contentType);
    }

    public OutputStream getOutputStream()
    {
        try
        {
            return originalResponse.getOutputStream();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setContentTypeIfNotBlank(String contentType)
    {
        if (isNotBlank(contentType))
        {
            setContentType(contentType);
        }
    }

    public void setStatus(int status)
    {
        originalResponse.setStatus(status);
    }
}