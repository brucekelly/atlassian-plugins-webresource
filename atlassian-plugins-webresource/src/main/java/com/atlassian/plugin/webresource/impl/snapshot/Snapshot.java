package com.atlassian.plugin.webresource.impl.snapshot;

import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.helpers.BaseHelpers;
import com.atlassian.plugin.webresource.impl.helpers.Helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Snapshot of the current state of the plugin system, captures list of web resources and its transformers
 * and conditions.
 *
 * @since v3.3
 */
public class Snapshot
{
    final Config config;
    final Map<String, Bundle> cachedBundles = new HashMap<String, Bundle>();

    // Storing some attribute of Resource outside of it in a separate Set in order to consume less memory
    // because amount of such attributes should be much smaller than the amount of web resources.
    final Map<WebResource, CachedTransformers> webResourcesTransformations;
    final Map<WebResource, CachedCondition> webResourcesCondition;
    final Set<WebResource> webResourcesWithLegacyConditions;
    final Set<WebResource> webResourcesWithLegacyTransformers;
    final Set<WebResource> webResourcesWithDisabledMinification;

    public Snapshot(Config config)
    {
        this(config, new HashMap<WebResource, CachedTransformers>(), new HashMap<WebResource, CachedCondition>(),
            new HashSet<WebResource>(), new HashSet<WebResource>(), new HashSet<WebResource>());
    }

    public Snapshot(Config config, Map<WebResource, CachedTransformers> webResourcesTransformations,
        Map<WebResource, CachedCondition> webResourcesCondition, Set<WebResource> webResourcesWithLegacyConditions,
        Set<WebResource> webResourcesWithLegacyTransformers, Set<WebResource> webResourcesWithDisabledMinification)
    {
        this.webResourcesTransformations = webResourcesTransformations;
        this.webResourcesCondition = webResourcesCondition;
        this.webResourcesWithLegacyConditions = webResourcesWithLegacyConditions;
        this.webResourcesWithLegacyTransformers = webResourcesWithLegacyTransformers;
        this.webResourcesWithDisabledMinification = webResourcesWithDisabledMinification;
        this.config = config;
    }

    public List<Bundle> toBundles(Iterable<String> keys)
    {
        List<Bundle> bundles = new ArrayList<Bundle>();
        for (String key : keys)
        {
            Bundle bundle = get(key);
            if (bundle != null)
            {
                bundles.add(bundle);
            }

        }
        return bundles;
    }

    public Bundle get(final String key)
    {
        return cachedBundles.get(key);
    }

    public Bundle put(String key, Bundle bundle)
    {
        return cachedBundles.put(key, bundle);
    }

    /**
     * Find bundles.
     */
    public BaseHelpers.BundleFinder find()
    {
        return Helpers.find(this);
    }

    public Set<WebResource> getWebResourcesWithLegacyConditions()
    {
        return webResourcesWithLegacyConditions;
    }

    public Set<WebResource> getWebResourcesWithLegacyTransformers()
    {
        return webResourcesWithLegacyTransformers;
    }
}
