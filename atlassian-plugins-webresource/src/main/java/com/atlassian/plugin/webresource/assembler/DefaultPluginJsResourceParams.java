package com.atlassian.plugin.webresource.assembler;

import com.atlassian.webresource.api.assembler.resource.PluginJsResourceParams;

import java.util.Map;

/**
 * @since v3.0
 */
class DefaultPluginJsResourceParams extends DefaultPluginUrlResourceParams implements PluginJsResourceParams
{
    public DefaultPluginJsResourceParams(Map<String, String> params)
    {
        super(params);
    }
}
