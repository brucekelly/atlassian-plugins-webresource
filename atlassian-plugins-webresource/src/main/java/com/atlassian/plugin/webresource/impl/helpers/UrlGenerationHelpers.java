package com.atlassian.plugin.webresource.impl.helpers;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.ContextSubBatchResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrl;
import com.atlassian.plugin.webresource.ResourceUrlImpl;
import com.atlassian.plugin.webresource.WebResourceSubBatchUrl;
import com.atlassian.plugin.webresource.impl.config.BatchType;
import com.atlassian.plugin.webresource.impl.snapshot.Bundle;
import com.atlassian.plugin.webresource.impl.CachedCondition;
import com.atlassian.plugin.webresource.impl.CachedTransformers;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.UrlCache;
import com.atlassian.plugin.webresource.legacy.LegacyUrlGenerationHelpers;
import com.atlassian.plugin.webresource.impl.support.http.BaseRouter;
import com.atlassian.plugin.webresource.url.DefaultUrlBuilder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.plugin.webresource.impl.RequestCache.toResourceKeys;
import static com.google.common.base.Predicates.not;

/**
 * Stateless helper functions providing basic support for resource url generation.
 *
 * @since v3.3
 */
public class UrlGenerationHelpers extends BaseHelpers
{
    /**
     * Data structure representing Web Resource Batch Key.
     */
    public static class ContextBatchKey
    {
        public final List<String> included;
        public LinkedHashSet<String> excluded;

        public ContextBatchKey(List<String> included, LinkedHashSet<String> excluded)
        {
            this.included = included;
            this.excluded = excluded;
        }

        @Override
        public String toString()
        {
            StringBuilder buff = new StringBuilder();
            buff.append(StringUtils.join(included, ", "));
            if (excluded.size() > 0)
            {
                buff.append("-");
                buff.append(StringUtils.join(excluded, ", "));
            }
            return buff.toString();
        }
    }

    /**
     * Data structure representing Web Resource Batch, it is used in cache and shouldn't contain any references.
     */
    public static class WebResourceBatch
    {
        public final String key;
        public final List<SubBatch> subBatches;
        protected final Map<BatchType, RequestCache.ResourceKeysSupplier> standaloneResourceKeysByType = new
            HashMap<BatchType, RequestCache.ResourceKeysSupplier>();

        public WebResourceBatch(String key, List<SubBatch> subBatches, List<Resource> standaloneResources)
        {
            this.key = key;
            this.subBatches = subBatches;
            for (BatchType type : BatchType.values())
            {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                    new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(standaloneResources, type)));
                standaloneResourceKeysByType.put(type, resourceKeysSupplier);
            }
        }

        public List<Resource> getStandaloneResourcesOfType(RequestCache requestCache, BatchType type)
        {
            return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
        }
    }

    /**
     * Data structure representing Context Batch, it is used in cache and shouldn't contain any references.
     */
    public static class ContextBatch
    {
        public final List<String> included;
        public final LinkedHashSet<String> excluded;
        public final List<String> skippedWebResourcesWithUrlReadingConditions;
        public final List<SubBatch> subBatches;
        protected final Map<BatchType, RequestCache.ResourceKeysSupplier> standaloneResourceKeysByType =
            new HashMap<BatchType, RequestCache.ResourceKeysSupplier>();
        public final boolean isAdditionalSortingRequired;

        public ContextBatch(List<String> included, LinkedHashSet<String> excluded,
            List<String> skippedWebResourcesWithUrlReadingConditions, List<SubBatch> subBatches,
            List<Resource> standaloneResources, boolean isAdditionalSortingRequired)
        {
            this.included = included;
            this.excluded = excluded;
            this.skippedWebResourcesWithUrlReadingConditions = skippedWebResourcesWithUrlReadingConditions;
            this.subBatches = subBatches;
            for (BatchType type : BatchType.values())
            {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                    new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(standaloneResources, type)));
                standaloneResourceKeysByType.put(type, resourceKeysSupplier);
            }
            this.isAdditionalSortingRequired = isAdditionalSortingRequired;
        }

        public List<Resource> getStandaloneResourcesOfType(RequestCache requestCache, BatchType type)
        {
            return requestCache.getCachedResources(standaloneResourceKeysByType.get(type));
        }

        @Override
        public String toString()
        {
            StringBuilder buff = new StringBuilder();
            buff.append(StringUtils.join(included, ", "));
            if (excluded.size() > 0)
            {
                buff.append("-");
                buff.append(StringUtils.join(excluded, ", "));
            }
            return buff.toString();
        }
    }

    /**
     * Data structure representing Sub Batch of both Web Resource Batch and Context Batch, it is used in cache and
     * shouldn't contain any references.
     */
    public static class SubBatch
    {
        public final Map<String, String> resourcesParams;
        // Bundles can't be inferred from resourceKeys because there could be an empty bundle without any
        // resources but with condition and dependencies.
        // It is ok to have reference to Bundle because it is anyway stored in Snapshot cache.
        public final List<Bundle> bundles;
        private final Map<BatchType, RequestCache.ResourceKeysSupplier> resourceKeysByType =
            new HashMap<BatchType, RequestCache.ResourceKeysSupplier>();

        public SubBatch(Map<String, String> resourcesParams, Bundle bundle, List<Resource> resources)
        {
            this(resourcesParams, Lists.newArrayList(bundle), resources);
        }

        public SubBatch(Map<String, String> resourcesParams, List<Bundle> bundles, List<Resource> resources)
        {
            this.resourcesParams = resourcesParams;
            this.bundles = bundles;
            for (BatchType type : BatchType.values())
            {
                // Instead of storing the reference to the resources storing its keys.
                RequestCache.ResourceKeysSupplier resourceKeysSupplier =
                    new RequestCache.ResourceKeysSupplier(toResourceKeys(resourcesOfType(resources, type)));
                resourceKeysByType.put(type, resourceKeysSupplier);
            }
        }

        public List<Resource> getResourcesOfType(RequestCache requestCache, BatchType type)
        {
            return requestCache.getCachedResources(resourceKeysByType.get(type));
        }
    }

    /**
     * DTO.
     */
    public static class Resolved
    {
        public final LinkedHashMap<String, Jsonable> data;
        public final List<ResourceUrl> urls;
        public final LinkedHashSet<String> excludedResolved;

        Resolved(LinkedHashMap<String, Jsonable> data, List<ResourceUrl> urls, LinkedHashSet<String> excludedResolved)
        {
            this.data = data;
            this.urls = urls;
            this.excludedResolved = excludedResolved;
        }
    }

    /**
     * Resolve given included and excluded resources.
     * @return data resources, url resources, resolved excluded resources.
     */
    public static Resolved resolve(final RequestState requestState)
    {
        if (requestState.getIncluded().isEmpty())
        {
            return new Resolved(new LinkedHashMap<String, Jsonable>(), new ArrayList<ResourceUrl>(), requestState.getExcluded());
        }

        UrlCache.IncludedAndExcludedAndConditions cacheKey = buildIncludedAndExcludedAndConditions(
            requestState.getCache(), requestState.getIncluded(), requestState.getExcluded());

        // Calculating
        CalculatedBatches calculatedBatches = requestState.getGlobals().getUrlCache().getBatches(cacheKey, new UrlCache.BatchesProvider()
        {
            @Override
            public CalculatedBatches get(UrlCache.IncludedAndExcludedAndConditions key)
            {
                return calculateBatches(requestState.getCache(), key.getIncluded(), key.getExcluded());
            }
        });

        // Assembling data resources.
        List<String> dataResources = requestState.getSnapshot().find()
            .included(requestState.getIncluded())
            .excluded(requestState.getExcluded())
            .deepFilter(isConditionsSatisfied(requestState.getCache()))
        .end();

        // Building data resources.
        LinkedHashMap<String, Jsonable> dataJsonResources = buildDataResources(requestState, dataResources);

        // Collecting transformer parameters and assembling resource urls.
        List<ResourceUrl> resourceUrls = collectUrlStateAndBuildResourceUrls(requestState.getCache(),
            calculatedBatches.contextBatches, calculatedBatches.webResourceBatches);

        return new Resolved(dataJsonResources, resourceUrls, calculatedBatches.excludedResolved);
    }

    /**
     * Building the cache key of included, excluded and conditions.
     */
    protected static UrlCache.IncludedAndExcludedAndConditions buildIncludedAndExcludedAndConditions(
        RequestCache requestCache, LinkedHashSet<String> included, LinkedHashSet<String> excluded)
    {
        // requestState could be modified in future, creating new included and excluded sets.
        UrlCache.IncludedAndExcluded includedAndExcluded = new UrlCache.IncludedAndExcluded(
            new LinkedHashSet<String>(included), new LinkedHashSet<String>(excluded));

        List<CachedCondition> conditions = getConditionsCached(requestCache, includedAndExcluded);

        // Evaluating conditions.
        List<Boolean> evaluatedConditions = new ArrayList<Boolean>();
        for (CachedCondition condition : conditions)
        {
            evaluatedConditions.add(condition.evaluateSafely(requestCache));
        }

        // Building cache key.
        return new UrlCache.IncludedAndExcludedAndConditions(includedAndExcluded, evaluatedConditions);
    }

    /**
     * Get web resources with conditions, the subset of the resolve web resources for given set of included and excluded.
     */
    protected static List<CachedCondition> getConditionsCached(final RequestCache requestCache,
        UrlCache.IncludedAndExcluded includedAndExcluded)
    {
        // Getting list of web resources with conditions.
        return requestCache.getGlobals().getUrlCache().getConditions(
            includedAndExcluded, new UrlCache.ConditionsProvider()
        {
            @Override
            public List<CachedCondition> get(UrlCache.IncludedAndExcluded key)
            {
                List<String> webResourcesWithConditions = requestCache.getSnapshot().find()
                    .included(key.getIncluded())
                    .excluded(key.getExcluded())
                    .deepFilter(hasConditions())
                .end();
                List<CachedCondition> conditions = new ArrayList<CachedCondition>();
                for (Bundle bundle : requestCache.getSnapshot().toBundles(webResourcesWithConditions))
                {
                    CachedCondition condition = bundle.getCondition();
                    if (condition != null)
                    {
                        conditions.add(condition);
                    }
                }
                return conditions;
            }
        });
    }

    /**
     * Resolves list of excluded resources.
     * @param requestCache request cache.
     * @param allIncluded included web resources and contexts.
     * @param allExcluded excluded web resources and contexts.
     * @return list of actually excluded web resources and contexts.
     */
    public static LinkedHashSet<String> resolveExcluded(final RequestCache requestCache,
        final List<String> allIncluded, final LinkedHashSet<String> allExcluded)
    {
        UrlCache.IncludedAndExcludedAndConditions cacheKey = buildIncludedAndExcludedAndConditions(
            requestCache, new LinkedHashSet<String>(allIncluded), allExcluded);

        return requestCache.getGlobals().getUrlCache().getResolvedExcluded(cacheKey,
            new UrlCache.ResolvedExcludedProvider()
        {
            @Override
            public LinkedHashSet<String> get(UrlCache.IncludedAndExcludedAndConditions key)
            {
                return LegacyUrlGenerationHelpers.calculateBatches(requestCache, key.getIncluded(),
                    key.getExcluded(), false).excludedResolved;
            }
        });
    }

    /**
     * DTO.
     */
    public static class CalculatedBatches
    {
        public final List<Helpers.ContextBatch> contextBatches;
        public final List<Helpers.WebResourceBatch> webResourceBatches;
        public final LinkedHashSet<String> excludedResolved;

        public CalculatedBatches(List<UrlGenerationHelpers.ContextBatch> contextBatches,
            List<UrlGenerationHelpers.WebResourceBatch> webResourceBatches, LinkedHashSet<String> excludedResolved)
        {
            this.contextBatches = contextBatches;
            this.webResourceBatches = webResourceBatches;
            this.excludedResolved = excludedResolved;
        }
    }

    /**
     * Calculate batches.
     */
    protected static CalculatedBatches calculateBatches(
        RequestCache requestCache, LinkedHashSet<String> included, LinkedHashSet<String> excluded)
    {
        LegacyUrlGenerationHelpers.Resolved resolved =
            LegacyUrlGenerationHelpers.calculateBatches(requestCache, included, excluded, false);

        // Splitting list of batches into list of sub-batches.
        Tuple<List<ContextBatch>, List<WebResourceBatch>> subBatches =
            splitIntoSubBatches(requestCache, resolved.contextBatchKeys, resolved.webResourceBatchKeys);
        List<ContextBatch> contextBatches = subBatches.getFirst();
        List<WebResourceBatch> webResourceBatches = subBatches.getLast();

        return new CalculatedBatches(contextBatches, webResourceBatches, resolved.excludedResolved);
    }

    /**
     * Build data resources.
     */
    protected static LinkedHashMap<String, Jsonable> buildDataResources(RequestState requestState,
        List<String> webResourceKeys)
    {
        LinkedHashMap<String, Jsonable> dataResources = new LinkedHashMap<String, Jsonable>();

        for (Bundle bundle : requestState.getSnapshot().toBundles(webResourceKeys))
        {
            for (Map.Entry<String, Jsonable> entry : bundle.getData().entrySet())
            {
                dataResources.put(bundle.getKey() + "." + entry.getKey(), entry.getValue());
            }
        }

        // Adding explicitly required data.
        for (Map.Entry<String, Jsonable> entry : requestState.getIncludedData().entrySet())
        {
            if (!requestState.getExcludedData().contains(entry.getKey()))
            {
                dataResources.put(entry.getKey(), entry.getValue());
            }
        }
        return dataResources;
    }

    /**
     * Collect URL State for Conditions and Transformers and build Resource URLs.
     * @return list of Resource URLs.
     */
    protected static List<ResourceUrl> collectUrlStateAndBuildResourceUrls(RequestCache requestCache,
        List<ContextBatch> contextBatches, List<WebResourceBatch> webResourceBatches)
    {
        Globals globals = requestCache.getGlobals();
        List<ResourceUrl> resourceUrls = new ArrayList<ResourceUrl>();
        for (BatchType type : BatchType.values())
        {
            // Assembling resource urls for context batches.
            for (ContextBatch contextBatch : contextBatches)
            {
                List<ResourceUrl> contextBatchResourceUrls = new ArrayList<ResourceUrl>();
                for (SubBatch subBatch : contextBatch.subBatches)
                {
                    if (!subBatch.getResourcesOfType(requestCache, type).isEmpty())
                    {
                        Tuple<Boolean, DefaultUrlBuilder> taintAndUrlBuilder = encodeStateInUrlIfSupported(requestCache,
                            type, subBatch.resourcesParams, subBatch.bundles,
                            requestCache.getSnapshot().toBundles(contextBatch.skippedWebResourcesWithUrlReadingConditions));
                        boolean taint = taintAndUrlBuilder.getFirst();
                        DefaultUrlBuilder urlBuilder = taintAndUrlBuilder.getLast();
                        if (globals.getConfig().isContextBatchingEnabled())
                        {
                            // Creating resource url.
                            contextBatchResourceUrls.add(new ContextSubBatchResourceUrl(requestCache.getGlobals(),
                                contextBatch, subBatch, type, urlBuilder.buildParams(), urlBuilder.buildHash(), taint));
                        }
                        else
                        {
                            List<Resource> resources = subBatch.getResourcesOfType(requestCache, type);
                            // Handling case when context batch should be reduced to web resource batches.
                            if (globals.getConfig().isWebResourceBatchingEnabled())
                            {
                                Map<Bundle, List<Resource>> bundles = new HashMap<Bundle, List<Resource>>();
                                for (Resource resource : resources)
                                {
                                    List<Resource> bundleResources = bundles.get(resource.getParent());
                                    if (bundleResources == null)
                                    {
                                        bundleResources = new ArrayList<Resource>();
                                        bundles.put(resource.getParent(), bundleResources);
                                    }
                                    bundleResources.add(resource);
                                }

                                for (Map.Entry<Bundle, List<Resource>> entry : bundles.entrySet())
                                {
                                    SubBatch webResourceSubBatch = new SubBatch(urlBuilder.buildParams(),
                                        entry.getKey(), entry.getValue());
                                    resourceUrls.add(new WebResourceSubBatchUrl(requestCache.getGlobals(),
                                        entry.getKey().getKey(), webResourceSubBatch, type, urlBuilder.buildParams(),
                                        urlBuilder.buildHash(), taint));
                                }
                            }
                            else
                            {
                                for(Resource resource : resources)
                                {
                                    contextBatchResourceUrls.add(new ResourceUrlImpl(globals, resource, urlBuilder.buildParams(),
                                        urlBuilder.buildHash(), taint));
                                }
                            }
                        }
                    }
                }

                // Sorting batch by urls, it's not needed and exists only for backward compatibility,
                // because for some unknown reason in the previous version sub-batches where sorted by its urls.
                // It exists only to ease backward compatibility testing, after some time this code should
                // be removed.
                if (contextBatch.isAdditionalSortingRequired)
                {
                    final ParamsComparator paramsComparator = new ParamsComparator();
                    Collections.sort(contextBatchResourceUrls, new Comparator<ResourceUrl>()
                    {
                        @Override
                        public int compare(ResourceUrl a, ResourceUrl b)
                        {
                            int result = paramsComparator.compare(a.getParams(), b.getParams());
                            // Only the part of the url with parameters should be compared, because hashes could
                            // be different and change sorting.
                            String aUrl = BaseRouter.buildUrl("", a.getParams());
                            String bUrl = BaseRouter.buildUrl("", b.getParams());
                            return result == 0 ? aUrl.compareTo(bUrl) : result;
                        }
                    });
                }

                resourceUrls.addAll(contextBatchResourceUrls);

                resourceUrls.addAll(createResourceUrlsForRedirectResources(requestCache, type,
                    contextBatch.getStandaloneResourcesOfType(requestCache, type)));
            }

            // Assembling resource urls for all web resources.
            for (WebResourceBatch webResourceBatch : webResourceBatches)
            {
                for (SubBatch subBatch : webResourceBatch.subBatches)
                {
                    List<Resource> resources = subBatch.getResourcesOfType(requestCache, type);
                    if (resources.size() > 0)
                    {
                        Tuple<Boolean, DefaultUrlBuilder> taintAndUrlBuilder = encodeStateInUrlIfSupported(requestCache,
                            type, subBatch.resourcesParams, subBatch.bundles, new ArrayList<Bundle>());
                        boolean taint = taintAndUrlBuilder.getFirst();
                        DefaultUrlBuilder urlBuilder = taintAndUrlBuilder.getLast();
                        if (globals.getConfig().isWebResourceBatchingEnabled())
                        {
                            resourceUrls.add(new WebResourceSubBatchUrl(requestCache.getGlobals(), webResourceBatch.key,
                                subBatch, type, urlBuilder.buildParams(), urlBuilder.buildHash(), taint));
                        }
                        else
                        {
                            for(Resource resource : resources)
                            {
                                resourceUrls.add(new ResourceUrlImpl(globals, resource, urlBuilder.buildParams(),
                                    urlBuilder.buildHash(), taint));
                            }
                        }
                    }
                }
                resourceUrls.addAll(createResourceUrlsForRedirectResources(requestCache, type,
                    webResourceBatch.getStandaloneResourcesOfType(requestCache, type)));
            }
        }
        return resourceUrls;
    }

    /**
     * Split batches into sub batches grouped by URL parameters and set of standalone resources.
     * @param contextBatchKeys keys of context batches
     * @param webResourceBatchKeys keys of web resource batches.
     * @return context batches and web resource batches split into sub batches.
     */
    protected static Tuple<List<ContextBatch>, List<WebResourceBatch>> splitIntoSubBatches(
        RequestCache requestCache, List<ContextBatchKey> contextBatchKeys, List<String> webResourceBatchKeys)
    {
        // Processing context batches.
        List<ContextBatch> contextBatches = new ArrayList<ContextBatch>();
        for (ContextBatchKey key : contextBatchKeys)
        {
            Found found = requestCache.getSnapshot().find()
                .included(key.included)
                .excluded(key.excluded)
                .deepFilter(isConditionsSatisfied(requestCache))
                .deepFilter(not(hasLegacyCondition()))
            .endAndGetResult();

            // Only UrlReadingConditions should be in Context Batch, removing any legacy conditions.
            List<String> skippedWebResourcesWithUrlReadingConditions = new ArrayList<String>();
            for (Bundle bundle : requestCache.getSnapshot().toBundles(found.skipped))
            {
                if ((bundle.getCondition() != null) && !bundle.getCondition().isLegacy())
                {
                    skippedWebResourcesWithUrlReadingConditions.add(bundle.getKey());
                }
            }

            SplitSubBatches result = splitBatchIntoSubBatches(requestCache, found.found, true);
            contextBatches.add(new ContextBatch(key.included, key.excluded, skippedWebResourcesWithUrlReadingConditions,
                result.contextSubBatches, result.contextStandaloneResources, result.isAdditionalSortingRequired));
        }

        // Processing web resource batches.
        List<WebResourceBatch> webResourceBatches = new ArrayList<WebResourceBatch>();
        {
            // Splitting into sub batches.
            for (String key : webResourceBatchKeys)
            {
                List<String> keys = new ArrayList<String>();
                keys.add(key);
                SplitSubBatches result = splitBatchIntoSubBatches(requestCache, keys, false);
                if (result.contextSubBatches.size() > 0 && result.legacyWebResources.size() == 0)
                {
                    webResourceBatches.add(
                        new WebResourceBatch(key, result.contextSubBatches, result.contextStandaloneResources));
                }
                else if (result.contextSubBatches.size() == 0 && result.legacyWebResources.size() > 0)
                {
                    if (result.contextStandaloneResources.size() > 0)
                    {
                        throw new RuntimeException("single web resource cannot have context standalone resources!");
                    }
                    if (result.legacyWebResources.size() > 1)
                    {
                        throw new RuntimeException("single web resource cannot split into multiple web resources!");
                    }
                    webResourceBatches.add(result.legacyWebResources.get(0));
                }
                else if (contextBatchKeys.size() > 0 && result.legacyWebResources.size() > 0)
                {
                    throw new RuntimeException("single web resource batch could be either legacy or not, "
                        + "but not both at the same time!");
                }
            }
        }

        return new Tuple<List<ContextBatch>, List<WebResourceBatch>>(contextBatches, webResourceBatches);
    }

    /**
     * Temporary data structure used during splitting batches into sub batches.
     */
    protected static class SplitSubBatches
    {
        List<SubBatch> contextSubBatches;
        List<Resource> contextStandaloneResources;
        List<WebResourceBatch> legacyWebResources;
        boolean isAdditionalSortingRequired;
    }

    /**
     * Split batch into sub batches and standalone resources, same code used to split both context and resource batches.
     * @param completeKeys list of complete keys that the batch contains.
     * @param doSorting some legacy stuff, in some cases it sorted in some not.
     * @return batch split into sub batches and
     */
    protected static SplitSubBatches splitBatchIntoSubBatches(RequestCache requestCache, Iterable<String> completeKeys,
        boolean doSorting)
    {
        final String IS_STANDALONE = "_isStandalone";
        List<Bundle> bundles = requestCache.getSnapshot().toBundles(completeKeys);

        // During splitting into sub-batches the order of standalone resources would be lost,
        // this comparator needed to restore the right order.
        final Map<Resource, Integer> allResourcesOrdered = new HashMap<Resource, Integer>();
        int i = 0;
        Comparator<Resource> RESOURCE_COMPARATOR = new Comparator<Resource>()
        {
            @Override
            public int compare(final Resource a, final Resource b)
            {
                return allResourcesOrdered.get(a) - allResourcesOrdered.get(b);
            }
        };

        // Getting list of unique params combinations, each combination would produce one sub-batch.
        Map<Map<String, String>, List<Resource>> uniqueParams = new LinkedHashMap<Map<String, String>, List<Resource>>();
        LinkedHashMap<Bundle, Map<Map<String, String>, List<Resource>>> legacyUniqueParams =
            new LinkedHashMap<Bundle, Map<Map<String, String>, List<Resource>>>();
        for (Bundle bundle : bundles)
        {
            for (Resource resource : bundle.getResources(requestCache).values())
            {
                allResourcesOrdered.put(resource, i);
                i += 1;

                Map<String, String> params = resource.getUrlParams();
                if (!resource.isBatchable())
                {
                    params.put(IS_STANDALONE, "true");
                }

                // Checking only for the legacy conditions and ignoring legacy transformers.
                if (!resource.getParent().hasLegacyConditions())
                {
                    List<Resource> resources = uniqueParams.get(params);
                    if (resources == null)
                    {
                        resources = new ArrayList<Resource>();
                        uniqueParams.put(params, resources);
                    }
                    resources.add(resource);
                }
                else
                {
                    Map<Map<String, String>, List<Resource>> webResourceUniqueParams =
                        legacyUniqueParams.get(resource.getParent());
                    if (webResourceUniqueParams == null)
                    {
                        webResourceUniqueParams = new LinkedHashMap<Map<String, String>, List<Resource>>();
                        legacyUniqueParams.put(resource.getParent(), webResourceUniqueParams);
                    }
                    List<Resource> resources = webResourceUniqueParams.get(params);
                    if (resources == null)
                    {
                        resources = new ArrayList<Resource>();
                        webResourceUniqueParams.put(params, resources);
                    }
                    resources.add(resource);
                }
            }
        }

        // Sorting list of unique params combination, the sub-batches would be sorted in the same way.
        SplitSubBatches result = new SplitSubBatches();
        List<Map<String, String>> uniqueParamsSorted = new ArrayList<Map<String, String>>(uniqueParams.keySet());
        ParamsComparator paramsComparator = new ParamsComparator();
        if (doSorting)
        {
            Collections.sort(uniqueParamsSorted, paramsComparator);
        }
        result.isAdditionalSortingRequired = paramsComparator.isAdditionalSortingRequired();

        // Sorting list of unique params combination for legacy web resources, the sub-batches would be sorted in the
        // same way.
        Map<Bundle, List<Map<String, String>>> legacyUniqueParamsSorted = new LinkedHashMap<Bundle, List<Map<String, String>>>();
        for (Map.Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet())
        {
            List<Map<String, String>> webResourceUniqueParamsSorted = new ArrayList<Map<String,
                String>>(entry.getValue().keySet());
            // According to the previous implementation the sub batches for web resources should be never sorted.
            // if (doSorting)
            // {
            //     Collections.sort(webResourceUniqueParamsSorted, PARAMS_COMPARATOR);
            // }
            legacyUniqueParamsSorted.put(entry.getKey(), webResourceUniqueParamsSorted);
        }

        // Assembling sub batches.
        result.contextSubBatches = new ArrayList<SubBatch>();
        result.contextStandaloneResources = new ArrayList<Resource>();
        for (Map<String, String> params : uniqueParamsSorted)
        {
            if (params.containsKey(IS_STANDALONE))
            {
                result.contextStandaloneResources.addAll(uniqueParams.get(params));
            }
            else
            {
                result.contextSubBatches.add(new SubBatch(params, bundles, uniqueParams.get(params)));
            }
        }
        Collections.sort(result.contextStandaloneResources, RESOURCE_COMPARATOR);

        // Assembling legacy sub batches, it should be last and should be re-grouped into web resource batches.
        result.legacyWebResources = new ArrayList<WebResourceBatch>();
        for (Map.Entry<Bundle, Map<Map<String, String>, List<Resource>>> entry : legacyUniqueParams.entrySet())
        {
            List<Resource> webResourceStandaloneResources = new ArrayList<Resource>();
            List<SubBatch> webResourceSubBatches = new ArrayList<SubBatch>();
            for (Map<String, String> params: legacyUniqueParamsSorted.get(entry.getKey()))
            {
                if (params.containsKey(IS_STANDALONE))
                {
                    webResourceStandaloneResources.addAll(uniqueParams.get(params));
                }
                else
                {
                    List<Resource> resources = legacyUniqueParams.get(entry.getKey()).get(params);
                    webResourceSubBatches.add(new SubBatch(params, entry.getKey(), resources));
                }
            }
            Collections.sort(webResourceStandaloneResources, RESOURCE_COMPARATOR);
            result.legacyWebResources.add(
                new WebResourceBatch(entry.getKey().getKey(), webResourceSubBatches, webResourceStandaloneResources));
        }

        return result;
    }

    private static final HashSet<String> PARAMS_SORT_ORDER_SET = new HashSet<String>(Config.PARAMS_SORT_ORDER);

    /**
     * Comparator for sorting sub batches.
     */
    public static final class ParamsComparator implements Comparator<Map<String, String>>
    {
        // The only reason this exists it to preserve backward compatibility when sub-batches where sorted
        // by its urls.
        private boolean isAdditionalSortingRequired = false;

        @Override
        public int compare(Map<String, String> a, Map<String, String> b)
        {
            // the fewer conditions, the high priority
            Set<String> aKeys = Sets.intersection(a.keySet(), PARAMS_SORT_ORDER_SET);
            Set<String> bKeys = Sets.intersection(b.keySet(), PARAMS_SORT_ORDER_SET);

            // if only one condition, media comes first and ieonly comes last.
            if ((aKeys.size() == 1) && (bKeys.size() == 1))
            {
                String aKey = Iterables.getOnlyElement(aKeys);
                String bKey = Iterables.getOnlyElement(bKeys);

                int aIndex = Config.PARAMS_SORT_ORDER.indexOf(aKey);
                int bIndex = Config.PARAMS_SORT_ORDER.indexOf(bKey);

                if (aIndex != bIndex)
                {
                    return aIndex - bIndex;
                }
            }

            if (aKeys.size() == bKeys.size())
            {
                // In old implementation sub batches where sorted by its urls, technically it should be
                // safe to sort it by query string only, as written below, but in order to ease
                // backward compatibility testing temporarily it is disabled and 100% backward compatibility
                // maintained, after some time this code should be uncommented and `isAdditionalSortingRequired`
                // stuff should be removed.
                //
                // String aQueryString = BaseRouter.buildUrl("", Resource.getUrlParamsStatic(a));
                // String bQueryString = BaseRouter.buildUrl("", Resource.getUrlParamsStatic(b));
                // return aQueryString.compareTo(bQueryString);
                //
                isAdditionalSortingRequired |= true;
                return 0;
            }

            return aKeys.size() - bKeys.size();
        }

        public boolean isAdditionalSortingRequired()
        {
            return isAdditionalSortingRequired;
        }
    }

    /**
     * Get resources of given type.
     */
    protected static List<Resource> resourcesOfType(Collection<Resource> resources, BatchType type)
    {
        List<Resource> result = new ArrayList<Resource>();
        for (Resource resource : resources)
        {
            if (type.toString().equals(resource.getType()))
            {
                result.add(resource);
            }
        }
        return result;
    }

    /**
     * Collect and encode URL state for given Resources, it collects resource params, condition params and
     * transformer params.
     */
    protected static Tuple<Boolean, DefaultUrlBuilder> encodeStateInUrlIfSupported(RequestCache requestCache,
        BatchType type, Map<String, String> params, List<Bundle> bundles, List<Bundle> skipped)

    {
        // Adding resource params.
        DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
        }

        // The url parameters from the excluded UrlReadingConditions also should be added.
        List<Bundle> allBundles = new ArrayList<Bundle>();
        allBundles.addAll(bundles);
        allBundles.addAll(skipped);

        // Adding condition and transformer params.
        boolean taint = false;
        for (Bundle bundle : allBundles)
        {
            CachedCondition condition = bundle.getCondition();
            if (condition != null)
            {
                condition.addToUrlSafely(requestCache, urlBuilder);
                taint |= condition.isLegacy();
            }
            CachedTransformers transformers = bundle.getTransformers();
            if (transformers != null)
            {
                taint |= transformers.addToUrlSafely(urlBuilder, type.toString(), requestCache.getGlobals().getConfig()
                    .getTransformerCache(), bundle.getTransformerParameters());
            }
            requestCache.getGlobals().getConfig().getStaticTransformers().addToUrl(type.toString(), bundle.getTransformerParameters(), urlBuilder);
        }
        return new Tuple<Boolean, DefaultUrlBuilder>(taint, urlBuilder);
    }

    /**
     * Creates Resource URLs for redirect resources.
     */
    protected static List<ResourceUrl> createResourceUrlsForRedirectResources(RequestCache requestCache, BatchType type,
        List<Resource> resources)
    {
        List<ResourceUrl> resourceUrls = new ArrayList<ResourceUrl>();
        for (Resource resource : resources)
        {
            // Calculating params and hash.
            DefaultUrlBuilder urlBuilder = new DefaultUrlBuilder();
            for (Map.Entry<String, String> entry : resource.getParams().entrySet())
            {
                urlBuilder.addToQueryString(entry.getKey(), entry.getValue());
            }

            boolean taint = false;
            Bundle webResource = resource.getParent();
            CachedCondition condition = webResource.getCondition();
            if (condition != null)
            {
                condition.addToUrlSafely(requestCache, urlBuilder);
                taint |= condition.isLegacy();
            }
            CachedTransformers transformers = webResource.getTransformers();
            if (transformers != null)
            {
                taint |= transformers.addToUrlSafely(urlBuilder, type.toString(), requestCache.getGlobals().getConfig().getTransformerCache(), webResource.getTransformerParameters());
            }
            requestCache.getGlobals().getConfig().getStaticTransformers().addToUrl(type.toString(), webResource.getTransformerParameters(), urlBuilder);

            resourceUrls.add(new ResourceUrlImpl(requestCache.getGlobals(), resource, urlBuilder.buildParams(),
                urlBuilder.buildHash(), taint));
        }
        return resourceUrls;
    }
}