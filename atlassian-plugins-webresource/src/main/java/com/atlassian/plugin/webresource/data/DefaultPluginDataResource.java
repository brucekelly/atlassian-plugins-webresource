package com.atlassian.plugin.webresource.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.PluginDataResource;

/**
 * Default PluginDataResource
 * @since v3.0
 */
public class DefaultPluginDataResource implements PluginDataResource
{
    private final String key;
    private final Jsonable jsonable;

    public DefaultPluginDataResource(String key, Jsonable jsonable)
    {
        this.key = key;
        this.jsonable = jsonable;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public Jsonable getJsonable()
    {
        return jsonable;
    }


}
