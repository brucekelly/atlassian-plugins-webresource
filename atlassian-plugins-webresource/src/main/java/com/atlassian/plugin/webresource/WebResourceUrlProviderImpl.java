package com.atlassian.plugin.webresource;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.webresource.impl.http.Router;

import java.util.HashMap;

import static com.atlassian.plugin.webresource.impl.support.http.BaseRouter.joinWithSlashWithoutEmpty;

/**
 * The default implementation of {@link WebResourceUrlProvider}.
 *
 * @since 2.9.0
 */
public class WebResourceUrlProviderImpl implements WebResourceUrlProvider
{
    static final String STATIC_RESOURCE_PREFIX = "s";
    static final String STATIC_RESOURCE_SUFFIX = "_";

    private final WebResourceIntegration webResourceIntegration;

    public WebResourceUrlProviderImpl(WebResourceIntegration webResourceIntegration)
    {
        this.webResourceIntegration = webResourceIntegration;
    }

    @Override
    public String getStaticResourcePrefix(UrlMode urlMode)
    {
        // "{base url}/s/{lang?}/{build num}/{system counter}/_"
        // {lang} is optional
        return joinWithSlashWithoutEmpty(
            webResourceIntegration.getBaseUrl(urlMode),
            STATIC_RESOURCE_PREFIX,
            webResourceIntegration.getStaticResourceLocale(),
            webResourceIntegration.getSystemBuildNumber(),
            webResourceIntegration.getSystemCounter(),
            STATIC_RESOURCE_SUFFIX
        );
    }

    @Override
    public String getStaticResourcePrefix(String resourceCounter, UrlMode urlMode)
    {
        // "{base url}/s/{lang?}/{build num}/{system counter}/{resource counter}/_"
        // {lang} is optional
       return joinWithSlashWithoutEmpty(
           webResourceIntegration.getBaseUrl(urlMode),
           STATIC_RESOURCE_PREFIX,
           webResourceIntegration.getStaticResourceLocale(),
           webResourceIntegration.getSystemBuildNumber(),
           webResourceIntegration.getSystemCounter(),
           resourceCounter,
           STATIC_RESOURCE_SUFFIX
       );
    }

    @Override
    public String getStaticResourcePrefix(String contributedHash, String resourceCounter, UrlMode urlMode)
    {
        // "{base url}/s/{contributedHash}/{lang?}/{build num}/{system counter}/{resource counter}/_"
        // {lang} is optional
        return joinWithSlashWithoutEmpty(
            webResourceIntegration.getBaseUrl(urlMode),
            STATIC_RESOURCE_PREFIX,
            contributedHash,
            webResourceIntegration.getStaticResourceLocale(),
            webResourceIntegration.getSystemBuildNumber(),
            webResourceIntegration.getSystemCounter(),
            resourceCounter,
            STATIC_RESOURCE_SUFFIX
        );
    }

    @Override
    public String getStaticPluginResourceUrl(final String moduleCompleteKey, final String resourceName, final UrlMode urlMode)
    {
        final ModuleDescriptor<?> moduleDescriptor = webResourceIntegration.getPluginAccessor().getEnabledPluginModule(moduleCompleteKey);
        if (moduleDescriptor == null)
        {
            return null;
        }
        return getStaticPluginResourceUrl(moduleDescriptor, resourceName, urlMode);
    }

    @Override
    public String getStaticPluginResourceUrl(ModuleDescriptor moduleDescriptor, String resourceName, UrlMode urlMode)
    {
        PluginInformation pluginInfo = moduleDescriptor.getPlugin().getPluginInformation();
        String pluginVersion = pluginInfo != null ? pluginInfo.getVersion() : "unknown";

        // "{base url}/s/{build num}/{system counter}/{plugin version}/_"
        final String staticUrlPrefix = getStaticResourcePrefix(pluginVersion, urlMode);
        // "/download/resources/plugin.key:module.key/resource.name"
        return staticUrlPrefix + getResourceUrl(moduleDescriptor.getCompleteKey(), resourceName);
    }

    @Override
    public String getResourceUrl(final String moduleCompleteKey, final String resourceName)
    {
        return "/" + AbstractFileServerServlet.SERVLET_PATH + Router.resourceUrlAsStaticMethod(moduleCompleteKey, resourceName, new HashMap<String, String>());
    }

    @Override
    public String getBaseUrl()
    {
        return webResourceIntegration.getBaseUrl();
    }

    @Override
    public String getBaseUrl(UrlMode urlMode)
    {
        return webResourceIntegration.getBaseUrl(urlMode);
    }
}
