package com.atlassian.plugin.webresource.transformer.instance;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.CssWebResource;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerMatcher;

/**
 * {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerMatcher} for relative url resources
 *
 * @since v3.1.0
 */
public class RelativeUrlTransformerMatcher implements WebResourceTransformerMatcher
{

    public static final String LESS_EXTENSION = "less";

    @Override
    public boolean matches(String type)
    {
        if (type != null)
        {
            final String fileType = type.toLowerCase();
            return fileType.equals("css") || fileType.equals(LESS_EXTENSION);
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean matches(ResourceLocation resourceLocation)
    {
        if (resourceLocation.getName() != null)
        {
            return CssWebResource.FORMATTER.matches(resourceLocation.getName())
                    || resourceLocation.getName().endsWith(LESS_EXTENSION);
        }

        return false;
    }
}