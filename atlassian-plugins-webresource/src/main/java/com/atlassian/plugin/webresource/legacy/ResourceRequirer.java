package com.atlassian.plugin.webresource.legacy;

import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Iterables.transform;

/**
 * TODO: Document this class / interface here
 *
 * @since v6.3
 */
public class ResourceRequirer
{
    public final WebResourceIntegration webResourceIntegration;
    public final PluginResourceLocator pluginResourceLocator;
    private final ResourceDependencyResolver dependencyResolver;
    private static final Logger log = LoggerFactory.getLogger(ResourceRequirer.class);
    private final boolean isSuperBatchingEnabled;
    private final boolean resplitMergedContextBatchesForThisRequest;
    private final boolean includeDependenciesForFailedUrlReadingConditions;

    public ResourceRequirer(WebResourceIntegration
        webResourceIntegration, PluginResourceLocator pluginResourceLocator,
        ResourceDependencyResolver dependencyResolver, boolean resplitMergedContextBatchesForThisRequest,
        boolean isSuperBatchingEnabled, boolean includeDependenciesForFailedUrlReadingConditions)
    {
        this.webResourceIntegration = webResourceIntegration;
        this.pluginResourceLocator = pluginResourceLocator;
        this.dependencyResolver = dependencyResolver;
        this.resplitMergedContextBatchesForThisRequest = resplitMergedContextBatchesForThisRequest;
        this.isSuperBatchingEnabled = isSuperBatchingEnabled;
        this.includeDependenciesForFailedUrlReadingConditions = includeDependenciesForFailedUrlReadingConditions;
    }

    public Collection<PluginResource> includeResources(RequestCache requestCache, Set<String> requiredWebResources,
        Set<String> requiredContexts,
        InclusionState inclusion)
    {
        final List<PluginResource> resourcesToInclude = Lists.newLinkedList();

        // Add superbatch
        addSuperBatchResources(resourcesToInclude, inclusion);

        // Add contexts
        addContextBatchDependencies(requestCache, resourcesToInclude, requiredWebResources, requiredContexts, inclusion);

        // Add webresources
        Iterable<String> dependencyModuleKeys = getAllModuleKeysDependencies(requestCache, requiredWebResources);
        addModuleResources(resourcesToInclude, dependencyModuleKeys, inclusion.webresources);

        // Add contexts, webresources & data to inclusion state
        Iterables.addAll(inclusion.contexts, requiredContexts);
        Iterables.addAll(inclusion.webresources, dependencyModuleKeys);

        return resourcesToInclude;
    }

    private void addSuperBatchResources(List<PluginResource> resourcesToInclude, InclusionState inclusion)
    {
        if (inclusion.superbatch || !isSuperBatchingEnabled)
        {
            return;
        }
        inclusion.superbatch = true;
        Iterables.addAll(resourcesToInclude, new SuperBatchBuilder(dependencyResolver, pluginResourceLocator).build());
    }

    private void addContextBatchDependencies(RequestCache requestCache, List<PluginResource> resourcesToInclude,
         Set<String> requiredWebResources, Set<String> requiredContexts, InclusionState inclusion)
    {
        final ContextBatchBuilder builder = new ContextBatchBuilder(dependencyResolver,
            resplitMergedContextBatchesForThisRequest, isSuperBatchingEnabled, includeDependenciesForFailedUrlReadingConditions);

        Iterable<PluginResource> contextResources = builder.buildBatched(requestCache, new ArrayList<String>(requiredContexts), inclusion.contexts);

        // Check if any context resource has been previously included as single webresources. If so, deliver all
        // contexts as individual webresources without the previously-included webresources.
        boolean isAnyContextAlreadyIncluded = false;
        for (String moduleKey : builder.getAllIncludedResources())
        {
            if (inclusion.webresources.contains(moduleKey))
            {
                isAnyContextAlreadyIncluded = true;
                log.debug("The module '{}' has already been included in the Assembler. Inclusion contexts = {}, required contexts = {}", new Object[] { moduleKey, inclusion.contexts, requiredContexts});
            }
        }
        if (isAnyContextAlreadyIncluded)
        {
            Iterables.addAll(requiredWebResources, builder.getAllIncludedResources());
        }
        else
        {
            Iterables.addAll(resourcesToInclude, contextResources);
            Iterables.addAll(inclusion.webresources, builder.getAllIncludedResources());
        }
        Iterables.addAll(requiredWebResources, builder.getSkippedResources());
    }

    private Iterable<String> getAllModuleKeysDependencies(RequestCache requestCache, Iterable<String> moduleCompleteKeys)
    {
        final Set<String> dependencyModuleCompleteKeys = Sets.newLinkedHashSet();
        for (final String moduleCompleteKey : moduleCompleteKeys)
        {
            final Iterable<String> dependencies = toModuleKeys(dependencyResolver.getDependencies(requestCache, moduleCompleteKey, isSuperBatchingEnabled, includeDependenciesForFailedUrlReadingConditions));
            Iterables.addAll(dependencyModuleCompleteKeys, dependencies);
        }
        return dependencyModuleCompleteKeys;
    }

    private void addModuleResources(final List<PluginResource> resourcesToInclude,
        final Iterable<String> dependencyModuleCompleteKeys, final Set<String> excludeModuleKeys)
    {
        for (final String moduleKey : dependencyModuleCompleteKeys)
        {
            if (excludeModuleKeys.contains(moduleKey))
            {
                // skip this resource if it is already visited
                continue;
            }

            for (final PluginResource moduleResource : pluginResourceLocator.getPluginResources(moduleKey))
            {
                resourcesToInclude.add(moduleResource);
            }
        }
    }

    private Iterable<String> toModuleKeys(final Iterable<WebResourceModuleDescriptor> descriptors)
    {
        return transform(descriptors, new TransformDescriptorToKey());
    }
}