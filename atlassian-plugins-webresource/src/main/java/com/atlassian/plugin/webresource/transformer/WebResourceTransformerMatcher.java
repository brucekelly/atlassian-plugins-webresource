package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;

/**
 * Describes whether a resource transformer matches a type or resource location
 *
 * @since v3.1.0
 */
public interface WebResourceTransformerMatcher
{
    /**
     * Called at url generation time
     * @param type resource type (eg js / css)
     * @return true if the transformer should be applied to the given type
     */
    public boolean matches(String type);

    /**
     * Called at resource fetch time
     * @param resourceLocation resource location
     * @return true if the transformer should be applied to the given type
     */
    public boolean matches(ResourceLocation resourceLocation);
}
