package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.elements.ResourceLocation;
import com.atlassian.plugin.webresource.impl.support.Content;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.url.UrlBuilder;

/**
 * {@link com.atlassian.plugin.webresource.transformer.StaticTransformers} implementation.
 *
 * @since v3.1.0
 */
public class DefaultStaticTransformers implements StaticTransformers
{
    private final StaticTransformersSupplier staticTransformersSupplier;

    public DefaultStaticTransformers(StaticTransformersSupplier staticTransformersSupplier)
    {
        this.staticTransformersSupplier = staticTransformersSupplier;
    }

    @Override
    public void addToUrl(String type, TransformerParameters transformerParameters, UrlBuilder urlBuilder)
    {
        for (ContentTransformerFactory transformerFactory : transformersForType(type))
        {
            transformerFactory.makeUrlBuilder(transformerParameters).addToUrl(urlBuilder);
        }
    }

    @Override
    public Content transform(Content content, TransformerParameters transformerParameters,
            ResourceLocation resourceLocation, QueryParams queryParams, String sourceUrl)
    {
        for (ContentTransformerFactory transformerFactory : transformersForLocation(resourceLocation))
        {
            content = transformerFactory.makeResourceTransformer(transformerParameters).transform(content,
                    resourceLocation, queryParams, sourceUrl);
        }
        return content;
    }

    private Iterable<ContentTransformerFactory> transformersForType(final String type)
    {
        return staticTransformersSupplier.get(type);
    }

    private Iterable<ContentTransformerFactory> transformersForLocation(final ResourceLocation location)
    {
        return staticTransformersSupplier.get(location);
    }
}
