package com.atlassian.plugin.webresource.data;

import org.apache.commons.lang.Validate;
import org.dom4j.Element;


/**
 * Container for data providers
 * @since v3.0
 */
class KeyedDataProvider
{
    private final String key;
    private final String className;

    KeyedDataProvider(Element e)
    {
        Validate.notNull(e.attribute("key"));
        Validate.notNull(e.attribute("class"));

        this.key = e.attributeValue("key");
        this.className = e.attributeValue("class");
    }

    String getKey()
    {
        return key;
    }

    String getClassName()
    {
        return className;
    }
}
