package com.atlassian.plugin.webresource.transformer;

import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

/**
 * Transformer parameters to be passed to
 * {@link com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory}'s methods
 *
 * @since v3.0.5
 */
public class TransformerParameters
{
    private final String pluginKey;
    private final String moduleKey;

    /**
     * @param webResourceModuleDescriptor web-resource descriptor
     * @return TransformerParameters for the given web-resource and transformer config element
     */
    public static TransformerParameters of(WebResourceModuleDescriptor webResourceModuleDescriptor)
    {
        return new TransformerParameters(webResourceModuleDescriptor.getPluginKey(), webResourceModuleDescriptor.getKey());
    }

    public TransformerParameters(String pluginKey, String moduleKey)
    {
        this.pluginKey = pluginKey;
        this.moduleKey = moduleKey;
    }

    /**
     * @return the key of the plugin containing resources to be transformed
     */
    public String getPluginKey()
    {
        return pluginKey;
    }

    /**
     * @return the key of the web-resource module containing resources to be transformed
     */
    public String getModuleKey()
    {
        return moduleKey;
    }
}
