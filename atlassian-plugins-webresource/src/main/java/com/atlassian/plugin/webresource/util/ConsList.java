package com.atlassian.plugin.webresource.util;

import static com.atlassian.fugue.Option.none;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Iterator;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.annotations.Internal;
import com.atlassian.fugue.Option;
import com.atlassian.fugue.Pair;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * @since 3.3
 */
@Internal
public abstract class ConsList<A> implements Iterable<A>
{
    //
    // static factory methods
    //

    public static <A> ConsList<A> empty()
    {
        return new Empty<A>();
    }

    public static <A> ConsList<A> of(A... as)
    {
        ConsList<A> result = empty();
        for (A a : as)
        {
            result = result.prepend(a);
        }
        return result.reverse();
    }

    //
    // interface
    //

    public abstract Option<A> head();

    public abstract Option<ConsList<A>> tail();

    public abstract boolean isEmpty();

    public ConsList<A> prepend(A a)
    {
        return new Node<A>(a, this.asOption());
    }

    public <B> B foldLeft(B zero, Function<Pair<B, A>, B> append)
    {
        B result = zero;
        for (A a : this)
        {
            result = append.apply(Pair.pair(result, a));
        }
        return result;
    }

    public ConsList<A> remove(final A a)
    {
        final Predicate<A> isRemove = new Predicate<A>()
        {
            @Override
            public boolean apply(A input)
            {
                return input == a;
            }
        };

        ConsList<A> result = new Empty<A>();
        Option<ConsList<A>> next = this.asOption();
        while (next.isDefined())
        {
            ConsList<A> cell = next.get();
            Option<A> head = cell.head();
            if (!head.exists(isRemove))
            {
                result = result.prepend(head.get());
            }
            next = cell.tail();
        }
        return result.reverse();
    }

    @Override
    public Iterator<A> iterator()
    {
        return new Iterator<A>()
        {
            private Option<ConsList<A>> next = asOption();

            @Override
            public boolean hasNext()
            {
                return next.isDefined();
            }

            @Override
            public A next()
            {
                ConsList<A> n = next.get();
                try
                {
                    return n.head().get();
                }
                finally
                {
                    next = n.tail();
                }
            }

            @Override
            public void remove()
            {
                throw new UnsupportedOperationException();
            }
        };
    }

    //
    // internal
    //

    abstract Option<ConsList<A>> asOption();

    ConsList<A> reverse()
    {
        return foldLeft(ConsList.<A> empty(), new Function<Pair<ConsList<A>, A>, ConsList<A>>()
        {
            @Override
            public ConsList<A> apply(Pair<ConsList<A>, A> input)
            {
                return input.left().prepend(input.right());
            }
        });
    }

    //
    // util
    //

    @Override
    public String toString()
    {
        return Iterables.toString(this);
    }

    static final class Node<A> extends ConsList<A>
    {
        private final A head;
        private Option<ConsList<A>> tail;

        Node(A head, Option<ConsList<A>> tail)
        {
            this.head = checkNotNull(head);
            this.tail = tail;
        }

        public final Option<A> head()
        {
            return Option.some(head);
        }

        public final Option<ConsList<A>> tail()
        {
            return tail;
        }

        @Override
        public boolean isEmpty()
        {
            return false;
        }

        @Override
        Option<ConsList<A>> asOption()
        {
            return Option.<ConsList<A>> some(this);
        }
    }

    static final class Empty<A> extends ConsList<A>
    {

        @Override
        public Option<A> head()
        {
            return none();
        }

        @Override
        public Option<ConsList<A>> tail()
        {
            return none();
        }

        @Override
        public ConsList<A> remove(A a)
        {
            return this;
        }

        @Override
        public boolean isEmpty()
        {
            return true;
        }

        @Override
        Option<ConsList<A>> asOption()
        {
            return Option.none();
        }
    }
}
