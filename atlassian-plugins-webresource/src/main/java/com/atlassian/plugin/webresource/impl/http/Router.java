package com.atlassian.plugin.webresource.impl.http;

import com.atlassian.plugin.webresource.ResourceUtils;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.impl.RequestCache;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.snapshot.Resource;
import com.atlassian.plugin.webresource.impl.support.Tuple;
import com.atlassian.plugin.webresource.impl.support.http.BaseRouter;
import com.atlassian.plugin.webresource.impl.support.http.Request;
import com.atlassian.plugin.webresource.impl.support.http.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import static com.atlassian.plugin.webresource.impl.helpers.BaseHelpers.isConditionsSatisfied;
import static com.atlassian.plugin.webresource.impl.helpers.ResourceServingHelpers.shouldBeIncludedInBatch;

/**
 * Generates urls and route requests to proper handlers.
 *
 * @since 3.3
 */
public class Router extends BaseRouter<Controller>
{
    public Router(final Globals globals)
    {
        super(globals);

        // Routes.

        addRoute("/resources/:completeKey/*resourceName.map", new Handler()
        {
            public void apply(Controller controller, String completeKey, String resourceName)
            {
                controller.serveResourceSourceMap(completeKey, resourceName);
            }
        });

        addRoute("/resources/:completeKey/*resourceName", new Handler()
        {
            public void apply(Controller controller, String completeKey, String resourceName)
            {
                controller.serveResource(completeKey, resourceName);
            }
        });

        addRoute("/sources/:completeKey/*resourceName", new Handler()
        {
            public void apply(Controller controller, String completeKey, String resourceName)
            {
                controller.serveSource(completeKey, resourceName);
            }
        });

        addRoute("/contextbatch/:type/:encodedContexts/*batchPostfixOrResourceName.map", new Handler()
        {
            public void apply(Controller controller, String type, String encodedContexts,
                    String batchPostfixOrResourceName)
            {
                Tuple<Collection<String>, LinkedHashSet<String>> includedAndExcludedWebResources =
                        decodeContextsAsWebResources(encodedContexts);
                if (batchPostfixOrResourceName.equals("batch." + type))
                {
                    controller.serveBatchSourceMap(includedAndExcludedWebResources.getFirst(),
                            includedAndExcludedWebResources.getLast(), type, true, false);
                }
                else
                {
                    controller.serveResourceRelativeToBatchSourceMap(includedAndExcludedWebResources.getFirst(),
                            includedAndExcludedWebResources.getLast(), batchPostfixOrResourceName, true, false);
                }
            }
        });

        addRoute("/contextbatch/:type/:encodedContexts/*batchPostfixOrResourceName", new Handler()
        {
            public void apply(Controller controller, String type, String encodedContexts,
                    String batchPostfixOrResourceName)
            {
                Tuple<Collection<String>, LinkedHashSet<String>> includedAndExcludedWebResources =
                        decodeContextsAsWebResources(encodedContexts);
                if (batchPostfixOrResourceName.equals("batch." + type))
                {
                    controller.serveBatch(includedAndExcludedWebResources.getFirst(),
                            includedAndExcludedWebResources.getLast(), type, true, false);
                }
                else
                {
                    controller.serveResourceRelativeToBatch(includedAndExcludedWebResources.getFirst(),
                            includedAndExcludedWebResources.getLast(), batchPostfixOrResourceName, true, false);
                }
            }
        });

        addRoute("/batch/:completeKey/*batchPostfixOrResourceName.map", new Handler()
        {
            public void apply(Controller controller, String completeKey, String batchPostfixOrResourceName)
            {
                List<String> included = Arrays.asList(completeKey);
                LinkedHashSet<String> excluded = new LinkedHashSet<String>();
                if (completeKey.equals(ResourceUtils.getBasename(batchPostfixOrResourceName)))
                {
                    controller.serveBatchSourceMap(included, excluded,
                            ResourceUtils.getType(batchPostfixOrResourceName), false, true);
                }
                else
                {
                    controller.serveResourceRelativeToBatchSourceMap(included, excluded, batchPostfixOrResourceName,
                            false, true);
                }
            }
        });

        addRoute("/batch/:completeKey/*batchPostfixOrResourceName", new Handler()
        {
            public void apply(Request request, Response response, Controller controller, String[] arguments)
            {
                String completeKey = arguments[0];
                String batchPostfixOrResourceName = arguments[1];
                List<String> included = Arrays.asList(completeKey);
                LinkedHashSet<String> excluded = new LinkedHashSet<String>();
                String type = ResourceUtils.getType(batchPostfixOrResourceName);

                if (completeKey.equals(ResourceUtils.getBasename(batchPostfixOrResourceName)))
                {
                    controller.serveBatch(included, excluded, type, false, true);
                }
                else
                {
                    // It is impossible to detect if resource is batch or single relative to batch by url only. Because
                    // in previous implementation urls like `/batch/app:page/:anything.css` could be treated as both
                    // batch and
                    // single resource.
                    // So, the only way to detect what it is is to try to build batch and see if it's empty or not.
                    //
                    // TODO it seems that it doesn't work properly, because relative resources still served as
                    // batched (see test case for it shouldServeSingleResourceRealtiveToBatchWithTheSameType).
                    // so, maybe we can remove it and always use only `/batch/:completeKey/:completeKey.:type` for
                    // batch?
                    RequestCache requestCache = new RequestCache(globals);

                    List<Resource> resources = globals.getSnapshot().find()
                        .included(included)
                        .excluded(excluded)
                        .deep(false)
                        .deepFilter(isConditionsSatisfied(requestCache, request.getParams()))
                        .resources(requestCache)
                        .filter(shouldBeIncludedInBatch(type, request.getParams()))
                    .end();

                    if (!resources.isEmpty())
                    {
                        // The already found batch resource not reused here because it's very rare legacy case,
                        // no need to optimize it.
                        controller.serveBatch(included, excluded, type, false, true);
                    }
                    else
                    {
                        controller.serveResourceRelativeToBatch(included, excluded, batchPostfixOrResourceName, false,
                            true);
                    }
                }
            }
        });
    }

    /**
     * Url helpers.
     */

    public String contextBatchUrl(String key, String type, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/contextbatch/:type/:key/batch.:type", type, key, type), params,
            isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String contextBatchSourceMapUrl(String key, String type, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/contextbatch/:type/:key/batch.:type.map", type, key, type), params,
            isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String resourceUrlRelativeToContextBatch(String key, String type, String resourceName, Map<String,
            String> params, boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash,
            String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/contextbatch/:type/:key/:resourceName", type, key, resourceName),
            params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    // Needed to support legacy API.
    public static String resourceUrlAsStaticMethod(String completeKey, String resourceName, Map<String, String> params)
    {
        return buildUrl(interpolate("/resources/:completeKey/:resourceName", completeKey, resourceName), params);
    }

    public String resourceUrl(String completeKey, String resourceName, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/resources/:completeKey/:resourceName", completeKey, resourceName),
            params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String resourceSourceMapUrl(String completeKey, String resourceName, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/resources/:completeKey/:resourceName.map", completeKey,
            resourceName), params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String pluginResourceUrl(String pluginKey, String resourceName, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/resources/:pluginKey/:resourceName", pluginKey, resourceName),
            params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    // TODO currently sources not cached, because otherwise it would be hard to use it - the hash goes first in
    // the url and it makes sources in Chrome Inspector look like a bunch of folders with random number names.
    // When we change url scheme in such a way that the hash would be a parameter, then it would be possible to add hash
    // to source urls and cache it the same way as other files.
    public String resourceUrlWithoutHash(Resource resource, Map<String, String> params)
    {
        return buildUrl(globals.getConfig().getBaseUrl() + interpolate("/resources/:completeKey/:resourceName",
            resource.getKey(), resource.getName()), params);
    }

    public String sourceUrl(String completeKey, String resourceName, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        // TODO currently sources not cached, because otherwise it would be hard to use it - the hash goes first in
        // the url and it makes sources in Chrome Inspector look like a bunch of folders with random number names.
        // When we change url scheme in such a way that the hash would be a parameter,
        // then it would be possible to add hash
        // to source urls and cache it the same way as other files.
        return buildUrl(globals.getConfig().getBaseUrl() + interpolate("/sources/:completeKey/:resourceName",
            completeKey, resourceName), params);
    }

    public String sourceUrl(Resource resource)
    {
        return sourceUrl(resource.getKey(), resource.getName(), new HashMap<String, String>(), true, true, "",
            resource.getVersion());
    }

    /**
     * It is important for the url structure to be: 1. the same number of sectioned paths as the SinglePluginResource 2.
     * include the module complete key in the path before the resource name This is due to css resources referencing
     * other resources such as images in relative path forms.
     */
    public String webResourceBatchUrl(String completeKey, String type, Map<String, String> params,
            boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash, String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/batch/:completeKey/:completeKey.:type", completeKey, completeKey,
            type), params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String resourceUrlRelativeToWebResourceBatch(String completeKey, String resourceName, Map<String,
            String> params, boolean isResourceSupportCache, boolean isResourceSupportCdn, String resourceHash,
            String resourceVersion)
    {
        return buildUrlWithPrefix(interpolate("/batch/:completeKey/:resourceName", completeKey, resourceName),
            params, isResourceSupportCache, isResourceSupportCdn, resourceHash, resourceVersion);
    }

    public String sourceMapUrl(String resourceUrl, Map<String, String> generatedParams)
    {
        return buildUrl(resourceUrl + ".map", generatedParams);
    }

    /**
     * Temporary, should be removed after refactoring when resource id would be used as a cache key instead of resource
     * url.
     */
    public String resourcePathFromSourceMapPath(String path)
    {
        return path.replaceAll("\\.map$", "");
    }

    /**
     * Needed to support legacy API.
     */
    protected Router(Globals globals, List<Route> routes, boolean useAbsoluteUrl)
    {
        super(globals, routes, useAbsoluteUrl);
    }

    /**
     * Needed to support legacy API.
     */
    public Router cloneWithNewUrlMode(boolean useAbsoluteUrl)
    {
        return new Router(globals, routes, useAbsoluteUrl);
    }

    /**
     * Adds prefix to relative url.
     */
    public String buildUrlWithPrefix(String url, Map<String, String> params, boolean isResourceSupportCache,
            boolean isResourceSupportCdn, String hash, String version)
    {
        Object urlWithParams = buildUrl(url, params);
        Config config = globals.getConfig();
        if (isResourceSupportCache)
        {
            String hashWithCdnMark = hash + (isResourceSupportCdn ? "-CDN" : "-T");
            if (isResourceSupportCdn && config.isCdnEnabled())
            {
                String prefix = config.getResourceUrlPrefix(hashWithCdnMark, version, false);
                return config.getResourceCdnPrefix(prefix + urlWithParams);
            }
            else
            {
                String prefix = config.getResourceUrlPrefix(hashWithCdnMark, version, useAbsoluteUrl);
                return prefix + urlWithParams;
            }
        }
        else
        {
            return config.getBaseUrl(useAbsoluteUrl) + urlWithParams;
        }
    }

    /**
     * Encodes lists of included and excluded contexts into single key like `batch1,batch2,-excludedBatch3,
     * -excludedBatch4`.
     */
    public static String encodeContexts(Collection<String> includedContexts, Iterable<String> excludedContexts)
    {
        String prefix = Config.CONTEXT_PREFIX + ":";
        StringBuilder buff = new StringBuilder();
        for (String context : includedContexts)
        {
            buff.append(context.replace(prefix, "")).append(",");
        }
        for (String context : excludedContexts)
        {
            buff.append("-").append(context.replace(prefix, "")).append(",");
        }
        buff.deleteCharAt(buff.length() - 1);
        return buff.toString();
    }

    /**
     * Decodes context key like `batch1,batch2,-excludedBatch3,-excludedBatch4` into lists of included and excluded
     * contexts.
     */
    public static Tuple<Collection<String>, Collection<String>> decodeContexts(String encodedContexts)
    {
        List<String> includedContexts = new ArrayList<String>();
        List<String> excludedContexts = new ArrayList<String>();
        String[] tokens = encodedContexts.split(",");
        for (String token : tokens)
        {
            if (token.startsWith("-"))
            {
                excludedContexts.add(token.substring(1));
            }
            else
            {
                includedContexts.add(token);
            }
        }
        return new Tuple<Collection<String>, Collection<String>>(includedContexts, excludedContexts);
    }

    /**
     * Decodes list of Contexts Keys into Virtual Resources Keys.
     */
    public static Tuple<Collection<String>, LinkedHashSet<String>> decodeContextsAsWebResources(String encodedContexts)
    {
        Tuple<Collection<String>, Collection<String>> includedAndExcludedContexts = decodeContexts(encodedContexts);
        return new Tuple<Collection<String>, LinkedHashSet<String>>(
            turnContextsIntoVirtualResources(includedAndExcludedContexts.getFirst()),
            new LinkedHashSet<String>(turnContextsIntoVirtualResources(includedAndExcludedContexts.getLast()))
        );
    }

    /**
     * Decodes list of Contexts Keys into Virtual Resources Keys.
     */
    protected static Collection<String> turnContextsIntoVirtualResources(Collection<String> contexts)
    {
        List<String> virtualContextResources = new ArrayList<String>();
        for (String context : contexts)
        {
            virtualContextResources.add(Config.CONTEXT_PREFIX + ":" + context);
        }
        return virtualContextResources;
    }

    @Override
    protected Controller createController(Globals globals, Request request, Response response)
    {
        return new Controller(globals, request, response);
    }

    /**
     * If the request is source map.
     */
    public static boolean isSourceMap(Request request)
    {
        return "map".equals(request.getType());
    }
}
