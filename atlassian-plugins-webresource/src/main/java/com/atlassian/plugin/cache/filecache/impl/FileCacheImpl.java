package com.atlassian.plugin.cache.filecache.impl;

import com.atlassian.plugin.cache.filecache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Implements a thread-safe FileCache.
 * <p/>
 * This implementation uses a computing map LRU map to create missing entries, and an eviction listener to identify
 * files that need to be deleted.
 * <p/>
 * Concurrent control to the contents of each cache entry is implemented in {@link StreamsCache}
 *
 * @since v2.13
 */
public class FileCacheImpl implements Cache
{

    private static final String ONE_STREAM_KEY_PREFIX = "onestreamkeyprefix:";
    private static final String TWO_STREAMS_KEY_PREFIX = "twostreamskeyprefix:";
    protected static final String EXT = ".cachedfile";
    private final com.google.common.cache.LoadingCache<String, StreamsCache> cache;
    private final File tmpDir;

    /**
     * This field is static to allow multiple instances of cache in the system to use the same tmp dir, without
     * overwriting each other's files.
     */
    private static final AtomicLong filenameCounter = new AtomicLong(0);

    /**
     * @param tmpDir directory to store cache contents. Files in this dir will be deleted by constructor
     * @param maxSize size of the LRU cache. zero means evict-immediately
     */
    public FileCacheImpl(File tmpDir, int maxSize) throws IOException
    {
        if (maxSize < 0)
        {
            throw new IllegalArgumentException("Max files can not be less than zero");
        }
        initDirectory(tmpDir);

        this.tmpDir = tmpDir;

        cache = CacheBuilder.newBuilder().maximumSize(maxSize).removalListener(new RemovalListener<String,
                StreamsCache>()
        {
            @Override
            public void onRemoval(RemovalNotification<String, StreamsCache> notification)
            {
                onEviction(notification.getValue());
            }
        }).build(new CacheLoader<String, StreamsCache>()
        {
            @Override
            public StreamsCache load(String key) throws Exception
            {
                return newCachedFile(key);
            }
        });
    }

    private static void initDirectory(File tmpDir) throws IOException
    {
        tmpDir.mkdirs();
        File[] files = tmpDir.listFiles();
        if (files != null)
        {
            for (File f : files)
            {
                if (f.getName().toLowerCase().endsWith(EXT) && !f.delete())
                {
                    throw new IOException("Could not delete existing cache file " + f);
                }
            }
        }

        if (!tmpDir.isDirectory())
        {
            throw new IOException("Could not create tmp directory " + tmpDir);
        }
    }

    @Override
    public void cache(String bucket, String key, OutputStream out, StreamProvider provider)
    {
        final StreamsCache cachedFile = cache.getUnchecked(ONE_STREAM_KEY_PREFIX + bucket + ":" + key);
        // Call stream() ASAP, even though `cachedFile` may already be evicted by the time we call it.
        // That's okay, though, it still streams (just not from the cache).
        cachedFile.stream(out, provider);
    }

    @Override
    public void cacheTwo(String bucket, String key, OutputStream out1, OutputStream out2, TwoStreamProvider provider)
    {
        final StreamsCache cachedFile = cache.getUnchecked(TWO_STREAMS_KEY_PREFIX + bucket + ":" + key);
        // Call stream() ASAP, even though `cachedFile` may already be evicted by the time we call it.
        // That's okay, though, it still streams (just not from the cache).
        // One of streams could be null.
        cachedFile.streamTwo(ensureNotNull(out1), ensureNotNull(out2), provider);
    }

    @Override
    public void clear()
    {
        // between the call to .values() and .clear(), further cache entries may be added.
        // In that case, we will never call .delete() on those files, and they will be orhpaned.
        // That's not so bad.

        Collection<StreamsCache> cachedFiles = new ArrayList<StreamsCache>(cache.asMap().values());
        cache.invalidateAll();

        for (StreamsCache cachedFile : cachedFiles)
        {
            cachedFile.deleteWhenPossible();
        }
    }

    private StreamsCache newCachedFile(String key)
    {
        if (key.startsWith(ONE_STREAM_KEY_PREFIX))
        {
            File file = new File(tmpDir, generateNextFileName());
            return new OneStreamCache(file);
        }
        else if (key.startsWith(TWO_STREAMS_KEY_PREFIX))
        {
            File file1 = new File(tmpDir, generateNextFileName());
            File file2 = new File(tmpDir, generateNextFileName());
            return new TwoStreamsCache(file1, file2);
        }
        else
        {
            throw new RuntimeException("internal error, invalid cache key '" + key + "' prefix!");
        }
    }

    private void onEviction(StreamsCache node)
    {
        node.deleteWhenPossible();
    }

    public static OutputStream ensureNotNull(OutputStream stream)
    {
        return (stream != null) ? stream : new OutputStream()
        {
            public void write(int b) throws IOException
            {

            }
        };
    }

    /**
     * Atomically generates next file name for the cache file.
     */
    protected String generateNextFileName()
    {
        return filenameCounter.incrementAndGet() + EXT;
    }
}
