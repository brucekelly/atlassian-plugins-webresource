package com.atlassian.plugin.cache.filecache;

import com.atlassian.plugin.servlet.DownloadException;

import java.io.OutputStream;

/**
 * Represents a file cache.
 * @since 2.13
 */
public interface Cache
{

    /**
     * Interface used by the file caching system. Items wishing to participate in file caching will need to
     * implement this interface. This interface gives the file cache a means to get hold of the contents that will be cached.
     * @since v2.13
     */
    public interface StreamProvider {
        /**
         * Produce the complete stream and write to the designated output stream. Classes implementing this method should not
         * close the output stream.
         *
         * @param out designated output stream.
         * @throws DownloadException If something goes awry while writing the file.
         */
        void write(OutputStream out);
    }

    /**
     * Stream the contents identified by the key to the destination stream. Should the contents not exist in the cache
     * a new entry should be created if the implementation is a caching implementation.
     * @param bucket bucket to group keys
     * @param key can not be null
     * @param out where to write the cached item to
     * @param provider provides the underlying item on a cache-miss
     * @throws DownloadException if there was an error writing to dest, or reading from input, or reading from the cache
     */
    void cache(String bucket, String key, OutputStream out, StreamProvider provider);

    /**
     * Interface used by the file caching system. Items wishing to participate in file caching will need to
     * implement this interface. This interface gives the file cache a means to get hold of two contents that will be cached.
     * @since v3.3
     */
    public interface TwoStreamProvider {
        /**
         * Produce the complete stream and write to the designated output stream. Classes implementing this method should not
         * close the output stream.
         *
         * @param out1 first designated output stream.
         * @param out2 second designated output stream.
         * @throws DownloadException If something goes awry while writing the file.
         */
        void write(OutputStream out1, OutputStream out2);
    }

    /**
     * Stream two contents identified by the key to the destination stream. Should the contents not exist in the cache
     * a new entry should be created if the implementation is a caching implementation.
     * @param bucket bucket to group keys
     * @param key can not be null
     * @param out1 where to write the first cached item to
     * @param out2 where to write the second cached item to
     * @param provider provides the underlying item on a cache-miss
     * @throws DownloadException if there was an error writing to dest, or reading from input, or reading from the cache
     *
     * @since 3.3
     */
    void cacheTwo(String bucket, String key, OutputStream out1, OutputStream out2, TwoStreamProvider provider);

    /**
     * Remove all entries in the cache.
     */
    void clear();
}
