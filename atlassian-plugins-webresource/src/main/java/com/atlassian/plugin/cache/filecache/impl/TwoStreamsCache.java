package com.atlassian.plugin.cache.filecache.impl;

import com.atlassian.plugin.cache.filecache.Cache;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Caches the contents of two streams, and deletes the cache when asked to do so.
 * This class encapsulates the lifecycle of two cached files, from as-yet-uncached, to cached, to needs-deletion, to deleted.
 * <p/>
 * <p>Calls to stream() always succeed, even if the cache was deleted.
 * The initial call to stream() will block other callers to stream() and deleteWhenPossible().
 * <p/>
 * Subsequent calls to stream() don't block other calls.
 * <p/>
 * Subsequent calls to deleteWhenPossible() don't block.
 * <p/>
 * <p>A call to deleteWhenPossible() always results in the file being (eventually) deleted.
 * <p/>
 * <p>Both stream() and deleteWhenPossible() can be called multiple times, in any order, with any
 * level of concurrency.
 *
 * @since v3.3
 */
public class TwoStreamsCache extends StreamsCache
{
    protected final File tmpFile1;
    protected final File tmpFile2;

    public TwoStreamsCache(File tmpFile1, File tmpFile2)
    {
        this.tmpFile1 = tmpFile1;
        this.tmpFile2 = tmpFile2;
    }

    public void streamTwo(OutputStream out1, OutputStream out2, final Cache.TwoStreamProvider provider)
    {
        boolean fromCache = doEnter(
            new Callback()
            {
                public void apply()
                {
                    try
                    {
                        streamToCache(provider);
                    } catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            },
            new CallbackWithBooleanReturn()
            {
                public boolean apply()
                {
                    return tmpFile1.isFile() && tmpFile2.isFile();
                }
            }
        );
        try
        {
            if (fromCache)
            {
                if (out1 != null)
                {
                    streamFromFile(tmpFile1, out1);
                }
                if (out2 != null)
                {
                    streamFromFile(tmpFile2, out2);
                }
            }
            else
            {
                provider.write(out1, out2);
            }
        }
        finally
        {
            doExit(new Callback()
            {
                public void apply()
                {
                    tmpFile1.delete();
                    tmpFile2.delete();
                }
            });
        }
    }

    public void stream(OutputStream out, Cache.StreamProvider provider)
    {
        throw new RuntimeException("two streams cache doesn't support stream method!");
    }

    public void deleteWhenPossible()
    {
        deleteWhenPossible(new Callback()
        {
            public void apply()
            {
                tmpFile1.delete();
                tmpFile2.delete();
            }
        });
    }

    private void streamToCache(Cache.TwoStreamProvider provider) throws IOException
    {
        OutputStream cacheout1 = null;
        OutputStream cacheout2 = null;
        try
        {
            cacheout1 = new BufferedOutputStream(createWriteStream(tmpFile1));
            cacheout2 = new BufferedOutputStream(createWriteStream(tmpFile2));
            provider.write(cacheout1, cacheout2);
        }
        finally
        {
            if (cacheout1 != null)
            {
                cacheout1.close(); // NB: if this fails, we don't want to silently ignore it
            }
            if (cacheout2 != null)
            {
                cacheout2.close(); // NB: if this fails, we don't want to silently ignore it
            }
        }
    }
}