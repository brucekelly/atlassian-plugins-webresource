package com.atlassian.webresource.plugin.i18n;

import org.apache.commons.lang.StringUtils;

import java.util.Locale;

/**
 * Utilities for serializing / deserializing locales.
 *
 * Note that they are NOT designed to handle locales according to any standard eg BCP 47 - the only goal of this class
 * is its serialization and deserialization match = ie that that myLocale.equals(deserialize(serialize(myLocale)));
 * and vice versa.
 *
 * This will be unnecessary once we move to Java 1.7, and should be replaced by
 * {@link java.util.Locale#forLanguageTag(String)} / {@link java.util.Locale#toLanguageTag()}.
 *
 * @since v3.0.8
 */
public class LocaleUtils
{
    public static String serialize(Locale locale)
    {
        StringBuilder str = new StringBuilder(locale.getLanguage());

        String country = locale.getCountry();
        if (StringUtils.isBlank(country))
        {
            return str.toString();
        }
        str.append("-").append(country);

        String variant = locale.getVariant();
        if (StringUtils.isBlank(variant))
        {
            return str.toString();
        }
        str.append("-").append(variant);

        return str.toString();
    }

    public static Locale deserialize(String str)
    {
        String[] split = str.split("-");
        switch (split.length)
        {
            case 1:
                return new Locale(split[0]);
            case 2:
                return new Locale(split[0], split[1]);
            case 3:
                return new Locale(split[0], split[1], split[2]);
            default:
                throw new IllegalArgumentException("Cannot parse locale: " + str);
        }
    }
}
