package com.atlassian.webresource.plugin.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

/**
 * Provides the product's context path to applications
 * @since v3.1
 */
public class ContextPathProvider implements WebResourceDataProvider
{
    private final WebResourceIntegration webResourceIntegration;

    public ContextPathProvider(WebResourceIntegration webResourceIntegration)
    {
        this.webResourceIntegration = webResourceIntegration;
    }

    @Override
    public Jsonable get()
    {
        return new JsonableString(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE));
    }
}
