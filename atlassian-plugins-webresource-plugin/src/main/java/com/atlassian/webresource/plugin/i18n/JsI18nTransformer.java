package com.atlassian.webresource.plugin.i18n;

import com.atlassian.html.encode.JavascriptEncoder;
import com.atlassian.plugin.servlet.DownloadableResource;
import com.atlassian.plugin.webresource.QueryParams;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.transformer.SearchAndReplaceDownloadableResource;
import com.atlassian.plugin.webresource.transformer.SearchAndReplacer;
import com.atlassian.plugin.webresource.transformer.TransformableResource;
import com.atlassian.plugin.webresource.transformer.TransformerParameters;
import com.atlassian.plugin.webresource.transformer.TransformerUrlBuilder;
import com.atlassian.plugin.webresource.transformer.UrlReadingWebResourceTransformer;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerFactory;
import com.atlassian.plugin.webresource.url.UrlBuilder;
import com.google.common.base.Function;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Web resource transformer to translate i18n methods in JavaScript to the literal strings.
 * 
 * This transforms the content by pattern matching on the text AJS.I18n.getText("key")
 * where key can only contain letters, numbers or dots and hyphens. It replaces this syntax with the literal
 * string translation before serving the resource.
 *
 * If a comma is found after the key instead of a closing brace ')', it will translate the string but also
 * wrap it in an AJS.format("translation", args) syntax so that the message can be formatted.
 *
 * @since 3.1.0
 */
public class JsI18nTransformer implements WebResourceTransformerFactory
{
    private static final Logger logger = LoggerFactory.getLogger(JsI18nTransformer.class);

    // Not a very sophisticated matcher. Doesn't check
    // for variables after the key if a comma has been detected.
    private static final Pattern PATTERN = Pattern.compile(
        "(\\w+)\\.I18n\\.getText" +
        "\\(\\s*" + // start paren
        "(['\"])([\\w.-]+)\\2" + // single or double quoted word
        "\\s*([\\),])" // end paren, or start-of-args
    );

    private static final String QUERY_KEY = "locale";
    private static final String HASH_KEY = "locale-hash";

    private final WebResourceIntegration webResourceIntegration;

    public JsI18nTransformer(WebResourceIntegration webResourceIntegration)
    {
        this.webResourceIntegration = webResourceIntegration;
    }

    @Override
    public TransformerUrlBuilder makeUrlBuilder(TransformerParameters params)
    {
        return new JsI18nTransformerUrlBuilder();
    }

    private final class JsI18nTransformerUrlBuilder implements TransformerUrlBuilder
    {
        @Override
        public void addToUrl(UrlBuilder urlBuilder)
        {
            urlBuilder.addToQueryString(QUERY_KEY, LocaleUtils.serialize(webResourceIntegration.getLocale()));
            // We add webResourceIntegration.getStaticResourceLocale() to the key;
            // this value varies whenever any plugins that contribute to i18n (eg language
            // packs) are changed.
            urlBuilder.addToHash(HASH_KEY, webResourceIntegration.getStaticResourceLocale());
        }
    }

    @Override
    public UrlReadingWebResourceTransformer makeResourceTransformer(TransformerParameters params)
    {
        return new JsI18nUrlReadingWebResourceTransformer();
    }

    private final class JsI18nUrlReadingWebResourceTransformer implements UrlReadingWebResourceTransformer
    {
        @Override
        public DownloadableResource transform(TransformableResource transformableResource, QueryParams params)
        {
            final Locale locale = getLocaleFromQueryParams(params);

            Function<Matcher, CharSequence> replacer = new Function<Matcher, CharSequence>() {
                public CharSequence apply(Matcher matcher) {
                    return doReplace(locale, matcher);
                }
            };
            // Note that this uses the deprecated SearchAndReplacer transformer util - when that class is removed,
            // we will make it protected and local.
            SearchAndReplacer grep = SearchAndReplacer.create(PATTERN, replacer);
            return new SearchAndReplaceDownloadableResource(transformableResource.nextResource(), grep);
        }

        /**
         * Returns the locale from query params
         * @param params query parameters
         * @return the locale discovered in the query params, or the application's default locale if none is found
         */
        private Locale getLocaleFromQueryParams(QueryParams params)
        {
            String localeKey = params.get(QUERY_KEY);
            if (!StringUtils.isBlank(localeKey))
            {
                return LocaleUtils.deserialize(localeKey);
            }
            else
            {
                return Locale.US;
            }
        }

        private String doReplace(Locale locale, Matcher matcher)
        {
            String namespace = matcher.group(1);
            String key = matcher.group(3);
            boolean format = ",".equals(matcher.group(4));

            StringBuilder result = new StringBuilder();
            if (format)
            {
                result.append(namespace + ".format(");
                String translation = webResourceIntegration.getI18nRawText(locale, key);
                if (null == translation)
                {
                    translation = key;
                }
                result.append("\"").append(jsEncode(translation)).append("\"");
                result.append(",");
            }
            else
            {
                // no need to call AJS.format, format it ourselves by calling getText() instead of getRawText()
                String translation = webResourceIntegration.getI18nText(locale, key);
                if (null == translation)
                {
                    translation = key;
                }
                result.append("\"").append(jsEncode(translation)).append("\"");
            }

            return result.toString();
        }

        private String jsEncode(String str)
        {
            try
            {
                StringWriter writer = new StringWriter();
                JavascriptEncoder.escape(writer, str);
                return writer.toString();
            }
            catch (IOException e)
            {
                // this should never ever happen while writing to a StringWriter
                logger.error("Error during javascript encoding", e);
                return "";
            }
        }
    }
}
