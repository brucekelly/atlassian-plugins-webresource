(function() {
    if (!window.WRM) {
        window.WRM = {};
    }

    function parseJsonData(unparsedData) {
        if (!unparsedData) {
            return unparsedData;
        }
        return JSON.parse(unparsedData);
    }

    // This is extremely defensive, but we use Object.prototype.hasOwnProperty as developers could include
    // a piece of data with key "hasOwnProperty"
    function _hasOwnProperty(o, key) {
        return Object.prototype.hasOwnProperty.call(o, key);
    }

    WRM.data = {
        /**
         * Claims the data returned from the server under the given key.
         *
         * Data for a specific key is retrieved only once. Subsequent requests for the same key will return no data.
         * If the data is required by multiple callers, the suggested pattern here is to wrap an API eg
         * AJS.contextPath around the WRM.data.claim method and cache inside that external API - see context-path.js
         * in this directory for an example.
         *
         * Callers must ensure that any JS requesting data appears *after* the data is inserted into the page - this
         * is achieved via the caller expressing a web-resource <dependency> on the resource serving the data.
         *
         * @param key data key.
         * @returns {*} JSON data.
         */
        claim: function(key) {
            // Lazily parses JSON data from WRM._unparsedData
            if (WRM._unparsedData && _hasOwnProperty(WRM._unparsedData, key)) {
                var unparsedData = WRM._unparsedData[key];
                try {
                    var data = parseJsonData(unparsedData);
                    delete WRM._unparsedData[key]; // purge from unparsed data map
                    return data;
                }
                catch (e) {
                    console && console.log && console.log("JSON Error parsing data with key " + key + ": " + e);
                }
            }
        }
    };

    /**
     * Registers a listener for the arrival of data. This listener will be called immediately (either now or in a timeout)
     * for all past data,
     * and later for any future data.
     *
     * @param key (optional) the key to listen for. If absent, then the listener will be called for all keys
     * @param cb callback function(data){...} called when data arrives
     */
    WRM.onData = function(key, cb) {
        if (!cb) {
            cb = key;
            key = false;
        }
        ///...

    }
}());