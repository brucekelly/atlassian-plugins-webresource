(function() {
    var contextPath = null;

    (window.AJS || (window.AJS = {}));

    AJS.contextPath = function() {
        if (contextPath === null) {
            contextPath = WRM.data.claim('com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path.context-path');
        }
        return contextPath;
    };

    // TODO: hack, this only exists for unit testing. Should refactor into a factory here
    AJS.contextPath._reset = function() {
        contextPath = null;
    };
}());