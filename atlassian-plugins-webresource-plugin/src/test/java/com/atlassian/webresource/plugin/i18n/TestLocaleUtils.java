package com.atlassian.webresource.plugin.i18n;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;

public class TestLocaleUtils
{
    @Test
    public void testSerialize()
    {
        assertEquals("en", LocaleUtils.serialize(new Locale("en")));
        assertEquals("fr", LocaleUtils.serialize(new Locale("fr")));
        assertEquals("en-US", LocaleUtils.serialize(new Locale("en", "us")));
        assertEquals("en-GB", LocaleUtils.serialize(new Locale("en", "gb")));
        assertEquals("en-US-posix", LocaleUtils.serialize(new Locale("en", "us", "posix")));
        assertEquals("en-US-POSIX", LocaleUtils.serialize(new Locale("en", "us", "POSIX")));
    }

    @Test
    public void testDeserialize()
    {
        assertEquals(new Locale("en"), LocaleUtils.deserialize("en"));
        assertEquals(new Locale("en"), LocaleUtils.deserialize("EN"));

        assertEquals(new Locale("fr"), LocaleUtils.deserialize("fr"));
        assertEquals(new Locale("fr"), LocaleUtils.deserialize("FR"));

        assertEquals(new Locale("en", "us"), LocaleUtils.deserialize("en-US"));
        assertEquals(new Locale("en", "us"), LocaleUtils.deserialize("en-us"));
        assertEquals(new Locale("en", "us"), LocaleUtils.deserialize("EN-US"));
        assertEquals(new Locale("en", "us"), LocaleUtils.deserialize("EN-us"));

        assertEquals(new Locale("en", "gb"), LocaleUtils.deserialize("en-GB"));
        assertEquals(new Locale("en", "gb"), LocaleUtils.deserialize("en-gb"));

        assertEquals(new Locale("en", "us", "posix"), LocaleUtils.deserialize("en-us-posix"));
        assertEquals(new Locale("en", "us", "posix"), LocaleUtils.deserialize("en-US-posix"));
        assertEquals(new Locale("en", "us", "POSIX"), LocaleUtils.deserialize("EN-US-POSIX"));
        assertEquals(new Locale("en", "us", "POSIX"), LocaleUtils.deserialize("en-us-POSIX"));
    }

    @Test
    public void testRoundTrip()
    {
        for (Locale locale : Locale.getAvailableLocales())
        {
            if (!locale.toString().contains("#"))
            {
                // '#' means the locale has a script element, which I don't believe exists in Java 1.6
                assertEquals(locale, LocaleUtils.deserialize(LocaleUtils.serialize(locale)));
            }
        }
    }
}
