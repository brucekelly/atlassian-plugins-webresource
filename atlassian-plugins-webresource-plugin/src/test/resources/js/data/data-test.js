module("data", {
    addData: function(key, jsonValue) {
        if (!WRM._unparsedData) {
            WRM._unparsedData = {};
        }
        WRM._unparsedData[key] = jsonValue;
    },
    teardown: function() {
        delete WRM._unparsedData;
    }
});

test("Does not fail when WRM._unparsedData does not exist", function() {
    ok(!WRM.data.claim("On a steel horse I ride"));
});

test("Returns basic data", function() {
    this.addData("bon", JSON.stringify("jovi"));

    equal(WRM.data.claim("bon"), "jovi");
});

test("Only the first data call is respected", function() {
    this.addData("bon", JSON.stringify("doge-vi"));

    equal(WRM.data.claim("bon"), "doge-vi");
    ok(!WRM.data.claim("bon"));
});

test("Returns JSON data", function() {
    this.addData("bon-jovi", JSON.stringify({ shot: "through the heart" }));

    deepEqual(WRM.data.claim("bon-jovi"), {
        shot: "through the heart"
    });
});

test("Returns data with pathologically bad keys", function() {
    this.addData("hasOwnProperty", JSON.stringify("mwahhahahah"));

    equal(WRM.data.claim("hasOwnProperty"), "mwahhahahah");
});

test("Handles malformed data", function() {
    var consoleLogSpy = sinon.spy(console, "log");
    this.addData("malformed", "{");

    ok(!WRM.data.claim("malformed"));
    // Test that an error message is logged to the console
    ok(consoleLogSpy.calledOnce);

    // Test that error message is logged every time
    WRM.data.claim("malformed");
    ok(consoleLogSpy.calledTwice);
});
