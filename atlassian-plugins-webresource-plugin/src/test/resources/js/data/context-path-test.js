module("context-path", {
    setContextPath: function(jsonValue) {
        if (!WRM._unparsedData) {
            WRM._unparsedData = {};
        }
        WRM._unparsedData["com.atlassian.plugins.atlassian-plugins-webresource-plugin:context-path.context-path"] = jsonValue;
    },
    setup: function() {
        AJS.contextPath._reset();
    },
    teardown: function() {
        delete WRM._unparsedData;
    }
});

test("Delivers context path", function() {
    this.setContextPath(JSON.stringify("/j-doge"));

    equal(AJS.contextPath(), "/j-doge");
});

test("Delivers empty context path successfully", function() {
    this.setContextPath(JSON.stringify(""));

    strictEqual(AJS.contextPath(), "");
    strictEqual(AJS.contextPath(), "");
});
