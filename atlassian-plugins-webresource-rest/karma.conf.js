module.exports = function(config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: 'src',


        // frameworks to use
        frameworks: ['qunit', 'sinon'],


        // list of files / patterns to load in the browser
        files: [
            // deps
            'test/resources/js/wrm/fixture/lib/jquery.js',
            'test/resources/js/wrm/fixture/ajs-mock.js',
            'test/resources/js/wrm/fixture/wrm-data-mock.js',
            // curl
            'main/resources/js/wrm/curl-config.js',
            'main/resources/js/wrm/lib/curl-0.7.3.js',
            'main/resources/js/wrm/lib/curl-css-0.7.3.js',
            'main/resources/js/wrm/lib/curl-js-0.7.3.js',
            // wrm
            'main/resources/js/wrm/jquery.js',
            'main/resources/js/wrm/underscore.js',
            'main/resources/js/wrm/builder.js',
            'main/resources/js/wrm/conditional-comment-filter.js',
            'main/resources/js/wrm/ie-only-filter.js',
            'main/resources/js/wrm/resource-base-url-pattern.js',
            'main/resources/js/wrm/require-handler.js',
            'main/resources/js/wrm/index.js',
            // tests
            'test/resources/js/wrm/fixture/test-fixture.js',
            'test/resources/js/**/*-test.js'
        ],


        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: 'target/surefire-reports/karma-results.xml',
            suite: ''
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        //browsers: ['Chrome', 'Safari', 'Firefox', 'Opera', 'IE11 - Win7', 'IE10 - Win7', 'IE9 - Win7'],
        browsers: ['PhantomJS'],


        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,


        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};
