WRM.curl(["wrm/require-handler"], function(RequireHandler) {

    var requireHandler;

    WRM.require = function(resources, cb) {
        if (!requireHandler) {
            requireHandler = new RequireHandler();
        }
        return requireHandler.require(resources, cb);
    };
});