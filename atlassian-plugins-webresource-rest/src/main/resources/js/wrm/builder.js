
WRM.define("wrm/builder", function() {
    /**
     * @param {string} baseUrlPattern A regular expression that matches the base URL for static resources. The pattern
     *   should match up to but not including /superbatch/, /contextbatch/, /batch/, /resources/.
     */
    var Builder = function(baseUrlPattern) {
        var optionalQueryString = "(?:$|\\?.*)";
        var prefix = "^" + baseUrlPattern;

        this.patterns = {
            js: {
                superBatch: new RegExp(prefix + "/superbatch/js/batch.js" + optionalQueryString),
                contextBatch: new RegExp(prefix + "/contextbatch/js/(.*?)/batch.js" + optionalQueryString),
                resourceBatch: new RegExp(prefix + "/batch/(.*?)/(.*).js" + optionalQueryString),
                noBatch: new RegExp(prefix + "/resources/(.*?)/(.*).js" + optionalQueryString)
            },
            css: {
                superBatch: new RegExp(prefix + "/superbatch/css/batch.css" + optionalQueryString),
                contextBatch: new RegExp(prefix + "/contextbatch/css/(.*?)/batch.css" + optionalQueryString),
                resourceBatch: new RegExp(prefix + "/batch/(.*?)/(.*).css" + optionalQueryString),
                noBatch: new RegExp(prefix + "/resources/(.*?)/(.*).css" + optionalQueryString)
            }
        };
    };
    Builder.prototype = {
        // Builds a list of loaded modules and contexts on the page
        initialize: function(container) {
            var i,
                scripts = container.getElementsByTagName("script"),
                links = container.getElementsByTagName("link"),
                loadedResources = {
                    superBatch: false,
                    modules: [],
                    contexts: []
                };

            for (i = 0; i < scripts.length; ++i) {
                var script = scripts[i];
                if (script.getAttribute("src")) {
                    this._inspectMatches(script.getAttribute("src"), this.patterns.js, loadedResources);
                }
            }

            for (i = 0; i < links.length; ++i) {
                var link = links[i];
                if (link.getAttribute("href")) {
                    this._inspectMatches(link.getAttribute("href"), this.patterns.css, loadedResources);
                }
            }

            return loadedResources;
        },
        getModulesAndContextsFromScriptUrl: function(url) {
            var loadedResources = {
                superBatch: false,
                modules: [],
                contexts: []
            };
            this._inspectMatches(url, this.patterns.js, loadedResources);
            return loadedResources;
        },
        _inspectMatches: function(url, patterns, loadedResources) {
            var match;
            match = url.match(patterns.superBatch);
            if (match) {
                loadedResources.superBatch = true;
                return;
            }
            match = url.match(patterns.contextBatch);
            if (match) {
                var matches = match[1].split(",");
                for (var i = 0; i < matches.length; ++i) {
                    this._addToLoadedList(loadedResources.contexts, matches[i]);
                }
                return;
            }
            match = url.match(patterns.resourceBatch);
            if (match) {
                this._addToLoadedList(loadedResources.modules, match[1]);
                return;
            }
            match = url.match(patterns.noBatch);
            if (match) {
                this._addToLoadedList(loadedResources.modules, match[1]);
                return;
            }
        },
        _addToLoadedList: function(loadedList, key) {
            // indexOf doesn't exist in IE8 and we can't assume underscore will be on the page, so do it the dirty way
            for (var i = 0; i < loadedList.length; ++i) {
                if (loadedList[i] === key) {
                    return;
                }
            }
            loadedList.push(key);
        }
    };

    return Builder;
});

