WRM.define("wrm/ie-only-filter", ["wrm/jquery", "wrm/_"], function($, _) {

    function filter(resources) {
        var browserMsie = $.browser.msie;
        return _.filter(resources, function(resource) {
            return resource.ieOnly ? browserMsie : true;
        });
    }

    return filter;
});

