WRM.define("wrm/conditional-comment-filter", ["wrm/jquery", "wrm/_"], function($, _) {

    function filter(resources) {
        var browserMsie = $.browser.msie;
        var browserVersion = $.browser.version;
        return _.filter(resources, function(resource) {
            var conditionalComment = resource.conditionalComment;
            if (!conditionalComment) {
                return true;
            }
            // This is pretty much the worst thing ever.
            // We could write a regex parser but given the number of actual cases it's probably easier doing it
            // this way. Also the number of cases supported by atlassian-plugins is pretty small:
            // - We don't support the !IE conditionalComment
            // - We only support IE8 & 9
            // - If anyone uses & or | they can either change it to use lt / lte / gt / gte or they can gtfo srsly
            if (!browserMsie || browserVersion > 9) {
                return false;
            }
            conditionalComment = conditionalComment.toLowerCase();
            switch (conditionalComment) {
                case "ie":          return true;
                case "lt ie 9":     return browserVersion <  9;
                case "lte ie 9":    return browserVersion <= 9;
                case "ie 9":        return browserVersion == 9;
                case "gt ie 9":     return browserVersion >  9;
                case "gte ie 9":    return browserVersion >= 9;
                case "lt ie 8":     return browserVersion <  8;
                case "lte ie 8":    return browserVersion <= 8;
                case "ie 8":        return browserVersion == 8;
                case "gt ie 8":     return browserVersion >  8;
                case "gte ie 8":    return browserVersion >= 8;
                default:
                    AJS.log("Cannot parse conditional comment " + conditionalComment);
                    return false;
            }
        });
    }

    return filter;
});

