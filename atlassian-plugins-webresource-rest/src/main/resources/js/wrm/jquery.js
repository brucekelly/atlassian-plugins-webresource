// Wrapper for AJS.$ to include jquery as a module
WRM.define("wrm/jquery", function() {
    return AJS.$;
});