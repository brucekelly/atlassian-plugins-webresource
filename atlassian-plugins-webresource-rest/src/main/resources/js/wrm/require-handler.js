WRM.define("wrm/require-handler",
    ["wrm/jquery", "wrm/_", "wrm/builder", "wrm/conditional-comment-filter", "wrm/ie-only-filter", "wrm/resource-base-url-pattern"],
    function($, _, Builder, conditionalCommentFilter, ieOnlyFilter, resourceBaseUrlPattern) {

    var WEBRESOURCE_PATTERN = /^wr!(.*)$/,
        CONTEXT_PATTERN = /^wrc!(.*)$/;

    var RequireHandler = function() {
        this._requireCache = {};
    };
    RequireHandler.prototype = {
        /**
         * Requires resources on the page.
         * @param resources list of resources (eg webresources or webresource contexts)
         * @param cb Callback that is executed when the actual JS / CSS resources representing the given
         * resources have been included on the page
         * @return a Promise that is resolved when all JS / CSS resources have been included on the page. Can be used
         * as an alternative to callback.
         */
        require: function(resources, cb) {
            if (!_.isArray(resources)) {
                resources = [resources];
            }
            // Requests are cached in this._requireCache so that if a client makes multiple requests for the same
            // set of resources, we can piggyback off the promise for the first request.
            // In other words, if a client calls require([a, b]), then does some work, then calls require([a, b])
            // again, the second call should resolve immediately (or after the first call has resolved).
            if (!this._requireCache.hasOwnProperty(resources)) {
                this._requireCache[resources] = this._resolveAsync(resources);
            }
            var promise = this._requireCache[resources];
            if (cb) {
                promise.done(function() {
                    cb.apply(this, arguments);
                });
            }
            return promise;
        },
        /**
         * Given a list of resources, translates those resources to actual CSS / JS files and includes them on the page
         * @param resources a list of webresources and webresource contexts to include
         * @return a Promise that is resolved only after all resources have been included on the page
         * @private
         */
        _resolveAsync: function(resources) {
            var deferred = $.Deferred();
            var that = this;
            this._getScriptsForResources(resources).done(function(resourceResponse) {
                if (resourceResponse.unparsedData) {
                    window.WRM._unparsedData || (window.WRM._unparsedData = {});
                    _.each(resourceResponse.unparsedData, function(val, key) {
                        window.WRM._unparsedData[key] = val;
                    });
                }
                var curlResources = [];
                var cssMediaResources = [];
                var filteredResourceUrlInfos = that._filter(resourceResponse.resources);
                for (var i = 0; i < filteredResourceUrlInfos.length; ++i) {
                    var resource = filteredResourceUrlInfos[i];
                    var url = resource.url;
                    if (resource.resourceType === "JAVASCRIPT") {
                        if (!that._isJSInInitLoad(url)) {
                            curlResources.push("js!" + url + "!order");
                        }
                    }
                    else if (resource.resourceType === "CSS") {
                        if (!that._isCSSInInitLoad(url)) {
                            if (resource.media && "all" !== resource.media) {
                                // HACK: this can't be loaded by curl.js. The solution is to the DOM immediately
                                // using a <link> tag. This means two things:
                                // - the callback may be called before the CSS has been loaded, resulting in a flash
                                //   of unstyled content.
                                // - it's easier to blow the IE9 stylesheet cap (curl.js works around this).
                                cssMediaResources.push(resource);
                            }
                            else {
                                curlResources.push("css!" + url);
                            }
                        }
                    }
                    else {
                        AJS.log("Unknown resource type required: " + url);
                    }
                }
                AJS.log("Downloading resources:\n" + curlResources.join("\n"));
                WRM.curl(curlResources, function() {
                    // Add all css media resources. This is done after curl resources to ensure ordering is consistent
                    // with the way resources are delivered on the server.
                    _.each(cssMediaResources, function(resource) {
                        that._loadCssImmediate(resource);
                    });
                    var callbackArgs = _.map(resources, function() { return window; });
                    deferred.resolveWith(that, callbackArgs);
                }, function() {
                    deferred.rejectWith(that, arguments);
                });
            });
            return deferred.promise();
        },
        /**
         * Loads
         * @param resource
         * @private
         */
        _loadCssImmediate: function(resource) {
            AJS.log('WARN: asynchronously loading a CSS resource containing a media query: ' + resource.url);
            var tag = '<link rel="stylesheet" type="text/css" href="' + resource.url + '" media="' + resource.media + '" />';
            $(tag).appendTo('head');
        },
        /**
         * Makes an AJAX request to retrieve the actual JS and CSS files required to represent a resource
         * @param resources list of resources (eg webresource, webresource context)
         * @return a Promise for the AJAX request
         * @private
         */
        _getScriptsForResources: function(resources) {
            if (!this._builder) {
                this._builder = new Builder(resourceBaseUrlPattern);
                this._initLoadResources = this._builder.initialize(document);
            }
            var webResources = [];
            var contexts = [];
            _.each(resources, function(resource) {
                var match;
                if (match = resource.match(WEBRESOURCE_PATTERN)) {
                    webResources.push(match[1]);
                }
                else if (match = resource.match(CONTEXT_PATTERN)) {
                    contexts.push(match[1]);
                }
            });
            return $.ajax({
                url: AJS.contextPath() + "/rest/webResources/1.0/resources",
                type: "POST",
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    r: webResources,
                    c: contexts,
                    xc: this._initLoadResources.contexts,
                    xr: this._initLoadResources.modules
                })
            });
        },
        /**
         * Checks if a script element whose src is the given url exists on the page
         * @param url url
         * @return {boolean} True if the script is on the page, otherwise false
         * @private
         */
        _isJSInInitLoad: function(url) {
            return $("script[src='" + url + "']").length > 0;
        },
        /**
         * Checks if a link element whose href is the given url exists on the page
         * @param url url
         * @return {boolean} True if the link is on the page, otherwise false
         * @private
         */
        _isCSSInInitLoad: function(url) {
            return $("link[href='" + url + "']").length > 0;
        },
        /**
         * Filters resources that should not be included from the given list
         * @param resourceUrlInfos list
         * @return list of filters resourceUrlInfo objects.
         * @private
         */
        _filter: function(resourceUrlInfos) {
            if (!this._filters) {
                this._filters = [
                    conditionalCommentFilter,
                    ieOnlyFilter
                ];
            }
            var filteredResourceUrlInfos = resourceUrlInfos;
            _.each(this._filters, function(filter) {
                filteredResourceUrlInfos = filter(filteredResourceUrlInfos);
            });
            return filteredResourceUrlInfos;
        }
    };

    return RequireHandler;
});
