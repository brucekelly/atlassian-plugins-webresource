package com.atlassian.webresource.plugin.async;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Resolves web resources into a format suitable for delivery via REST
 *
 * @since v3.0
 */
public interface AsyncWebResourceLoader
{
    public static enum ResourceType
    {
        JAVASCRIPT,
        CSS
    }

    @XmlRootElement
    public static class Resource
    {
        @XmlAttribute
        public final String url;
        @XmlAttribute
        public final ResourceType resourceType;
        @XmlAttribute
        public final String conditionalComment;
        @XmlAttribute
        public final boolean ieOnly;
        @XmlAttribute
        public final String media;

        public Resource(String url, ResourceType resourceType, String conditionalComment, boolean ieOnly, String media)
        {
            this.url = url;
            this.resourceType = resourceType;
            this.conditionalComment = conditionalComment;
            this.ieOnly = ieOnly;
            this.media = media;
        }
    }

    @XmlRootElement
    public static class ResourcesAndData
    {
        @XmlAttribute
        public final Iterable<Resource> resources;

        @XmlAttribute
        public final Map<String, String> unparsedData;

        public ResourcesAndData(Iterable<Resource> resources, Map<String, String> data) {
            this.resources = resources;
            this.unparsedData = data;
        }
    }

    public ResourcesAndData resolve(Set<String> webResources, Set<String> contexts, Set<String> excludeResources,
                                  Set<String> excludeContexts) throws IOException;
}
