package com.atlassian.webresource.plugin.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.Writer;
import java.util.regex.Pattern;

/**
 * Provide a JavaScript regular expression that matches the base URL for static resources (i.e. everything leading up
 * to /contextbatch/, /superbatch/, etc).
 * @since v3.1
 */
public class ResourceBaseUrlPatternProvider implements WebResourceDataProvider {
    private final WebResourceIntegration webResourceIntegration;
    private static final Pattern SPECIAL_CHARACTERS = Pattern.compile("[\\-\\[\\]\\{\\}\\(\\)\\*\\+\\?\\.\\\\\\^\\$\\|]");

    public ResourceBaseUrlPatternProvider(WebResourceIntegration webResourceIntegration)
    {
        this.webResourceIntegration = webResourceIntegration;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException
            {
                StringBuilder builder = new StringBuilder();
                String contextPath = webResourceIntegration.getBaseUrl(UrlMode.RELATIVE);
                builder.append("(?:");

                // e.g. /context-path/download
                // e.g. /context-path/s/.../_/download
                builder.append(quote(contextPath));
                builder.append("(?:/s/.*?/_)?/download");

                CDNStrategy cdnStrategy = webResourceIntegration.getCDNStrategy();
                if (cdnStrategy != null && cdnStrategy.supportsCdn())
                {
                    builder.append("|");

                    // e.g. //xyz.cdn.com/jira-instance-domain/context-path/download
                    // e.g. //xyz.cdn.com/jira-instance-domain/context-path/s/.../_/download
                    builder.append(quote(cdnStrategy.transformRelativeUrl(contextPath)));
                    builder.append("(?:/s/.*?/_)?/download");
                }

                builder.append(")");
                writeJson(writer, builder.toString());
            }
        };
    }

    private void writeJson(Writer writer, String value) throws IOException
    {
        JsonFactory jsonFactory = new JsonFactory();
        // Prevent the ObjectMapper from automatically closing the writer.
        jsonFactory.disable(JsonGenerator.Feature.AUTO_CLOSE_TARGET);
        ObjectMapper mapper = new ObjectMapper(jsonFactory);
        mapper.writeValue(writer, value);
    }

    /**
     * Escape all characters that have special meaning in JavaScript regular expressions.
     *
     * It's not appropriate to use {@link java.util.regex.Pattern#quote} here, as it uses \Q \E which is not supported
     * by JavaScript's regular expressions.
     *
     * @param text Text to escape special characters.
     * @return a version of text that has all special characters (in the context of regex) escaped.
     */
    private String quote(String text)
    {
        // e.g. Replace '?' with '\?'.
        return SPECIAL_CHARACTERS.matcher(text).replaceAll("\\\\$0");
    }
}
