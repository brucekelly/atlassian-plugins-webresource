package com.atlassian.webresource.plugin.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import com.google.common.collect.Sets;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * REST endpoint for retrieving list of js and css resources
 * @since v3.0
 */
@AnonymousAllowed
@Path("resources")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class ListOfResources
{
    private final AsyncWebResourceLoader asyncWebResourceLoader;

    public ListOfResources(AsyncWebResourceLoader asyncWebResourceLoader)
    {
        this.asyncWebResourceLoader = asyncWebResourceLoader;
    }

    /**
     * HTTP GET version of {@link #post} allowing parameters to be supplied via the URL.
     *
     * Avoid this in favour of the POST form when you risk exceeding the maximum URL length supported by browsers. For
     * example when you expect to have a large number of requires or exclusions (e.g. in development mode where
     * resources are not batched).
     *
     * @param webResources Comma separated list of IDs of resources to require.
     * @param contexts Comma separated list of IDs of contexts to require.
     * @param excludeResources Comma separated list of IDs of resources to exclude.
     * @param excludeContexts Comma separated list of IDs of contexts to exclude.
     * @throws IOException
     */
    @GET
    public AsyncWebResourceLoader.ResourcesAndData get(
            @QueryParam("r") String webResources,
            @QueryParam("c") String contexts,
            @QueryParam("xr") String excludeResources,
            @QueryParam("xc") String excludeContexts) throws IOException {
        return asyncWebResourceLoader.resolve(asSet(webResources), asSet(contexts), asSet(excludeResources),
                asSet(excludeContexts));
    }

    /**
     * Calculate the missing JS, CSS and extra data required by the client, taking into consider exclusions (which
     * are what the client already has).
     *
     * @param request The resources and contexts to include and exclude.
     * @throws IOException
     */
    @POST
    public AsyncWebResourceLoader.ResourcesAndData post(Request request) throws IOException
    {
        return asyncWebResourceLoader.resolve(asSet(request.resources), asSet(request.contexts),
                asSet(request.excludeResources), asSet(request.excludeContexts));
    }

    private Set<String> asSet(String str)
    {
        if (null == str || "".equals(str))
        {
            return Collections.emptySet();
        }
        else
        {
            return Sets.newLinkedHashSet(Arrays.asList(str.split(",")));
        }
    }

    private Set<String> asSet(List<String> list)
    {
        return Sets.newLinkedHashSet(list);
    }

    @XmlRootElement
    static class Request {
        @XmlAttribute(name="r")
        public List<String> resources;
        @XmlAttribute(name="c")
        public List<String> contexts;
        @XmlAttribute(name="xr")
        public List<String> excludeResources;
        @XmlAttribute(name="xc")
        public List<String> excludeContexts;

        public Request() {}

        public Request(List<String> resources, List<String> contexts, List<String> excludeResources,
                       List<String> excludeContexts)
        {
            this.resources = resources;
            this.contexts = contexts;
            this.excludeResources = excludeResources;
            this.excludeContexts = excludeContexts;
        }
    }
}
