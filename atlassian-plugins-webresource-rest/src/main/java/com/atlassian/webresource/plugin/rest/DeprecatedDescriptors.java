package com.atlassian.webresource.plugin.rest;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.impl.config.Config;
import com.atlassian.plugin.webresource.impl.Globals;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugin.webresource.impl.RequestState;
import com.atlassian.plugin.webresource.ResourceBatchingConfiguration;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.plugin.webresource.transformer.TransformerCache;
import com.atlassian.plugin.webresource.transformer.WebResourceTransformerModuleDescriptor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * REST endpoint for retrieving list of deprecated transform1 and condition1's.
 * Provided to allow
 * @since v3.1.0
 */
@AnonymousAllowed
@Path("deprecatedDescriptors")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class DeprecatedDescriptors
{
    private final PluginAccessor pluginAccessor;
    private final TransformerCache transformerCache;
    private final Globals globals;

    public DeprecatedDescriptors(
            PluginAccessor pluginAccessor,
            PluginResourceLocator pluginResourceLocator,
            WebResourceIntegration wri,
            ResourceBatchingConfiguration batchingConfiguration)
    {
        this.pluginAccessor = pluginAccessor;
        this.transformerCache = pluginResourceLocator.getTransformerCache();
        this.globals = pluginResourceLocator.temporaryWayToGetGlobalsDoNotUseIt();
    }

    @GET
    public Result getInfo()
    {
        return new Result(condition1s(), transform1s(), listWebresourceDescriptors());
    }
    
    private Iterable<WebresourceDescriptor> listWebresourceDescriptors()
    {
        RequestState requestState = new RequestState(globals);
        List<String> superbatch = requestState.getSnapshot().find()
            .included(Config.SUPERBATCH_KEY)
        .end();
        LinkedHashSet<String> superbatchAsSet = new LinkedHashSet<String>(superbatch);

        // find which contexts each webresource is *transitively* a part of.
        Multimap<String, String> wr2contexts = LinkedListMultimap.create();
        for (WebResourceModuleDescriptor descriptor : pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
        {
            String key = descriptor.getCompleteKey();
            Set<String> ctx = new HashSet<String>(descriptor.getContexts());
            ctx.remove(key); // remove each webresource's implicit context
            wr2contexts.putAll(key, ctx);

            List<String> dependencies = requestState.getSnapshot().find()
                .included(key)
                .excludedResolved(superbatchAsSet)
            .end();
            for (String depKey : dependencies)
            {
                wr2contexts.putAll(depKey, ctx);
            }
        }

        // for each webresource that is tainted, return that resource, its contexts, and what tainted it
        List<WebresourceDescriptor> descriptors = Lists.newArrayList();
        for (WebResourceModuleDescriptor descriptor : pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
        {
            Set<String> conditions = descriptor.getDeprecatedConditionKeys();
            Set<String> transforms = descriptor.getDeprecatedTransformKeys(transformerCache);
            if (!(conditions.isEmpty() && transforms.isEmpty()))
            {
                String completeKey = descriptor.getCompleteKey();
                WebresourceDescriptor d = new WebresourceDescriptor(completeKey);
                d.condition1keys = nullIfEmpty(conditions);
                d.transform1keys = nullIfEmpty(transforms);
                Collection<String> contexts = wr2contexts.get(completeKey);
                if (contexts != null && !contexts.isEmpty())
                {
                    d.context = new ArrayList<String>(new HashSet<String>(contexts));
                }
                if (superbatch.contains(completeKey)) {
                    d.superbatch = true;
                }

                descriptors.add(d);
            }
        }

        return descriptors;
    }

    private Iterable<String> transform1s()
    {
        return Iterables.transform(pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceTransformerModuleDescriptor.class), new Function<WebResourceTransformerModuleDescriptor, String>()
        {
            @Override
            public String apply(WebResourceTransformerModuleDescriptor moduleDescriptor)
            {
                return moduleDescriptor.getCompleteKey();
            }
        });
    }

    private Iterable<String> condition1s()
    {
        // Condition1's are used for web fragments as well as web resources, so find only those that are used in
        // web-resource module descriptors
        Set<String> conditionKeys = Sets.newTreeSet();
        for (WebResourceModuleDescriptor wrmd : pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class))
        {
            conditionKeys.addAll(wrmd.getDeprecatedConditionKeys());
        }
        return conditionKeys;
    }

    private static ArrayList<String> nullIfEmpty(Set<String> conditions) {
        return conditions.isEmpty() ? null : new ArrayList<String>(conditions);
    }

    public static class WebresourceDescriptor
    {
        @XmlAttribute
        public final String moduleKey;
        @XmlAttribute
        public List<String> condition1keys;
        @XmlAttribute
        public List<String> transform1keys;
        @XmlAttribute
        public List<String> context;
        @XmlAttribute
        public Boolean superbatch;

        public WebresourceDescriptor(String moduleKey)
        {
            this.moduleKey = moduleKey;
        }
    }

    @XmlRootElement
    private static class Result {
        @XmlAttribute
        public final List<String> condition1s;
        @XmlAttribute
        public final List<String> transform1s;
        @XmlAttribute
        public final Iterable<WebresourceDescriptor> webresources;

        public Result(Iterable<String> condition1s, Iterable<String> transform1s,
                      Iterable<WebresourceDescriptor> webresources) {
            this.condition1s = Lists.newArrayList(condition1s);
            this.transform1s = Lists.newArrayList(transform1s);
            this.webresources = webresources;
        }
    }
}
