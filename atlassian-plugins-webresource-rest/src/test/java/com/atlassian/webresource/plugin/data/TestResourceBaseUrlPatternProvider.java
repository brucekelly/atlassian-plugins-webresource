package com.atlassian.webresource.plugin.data;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;
import com.atlassian.plugin.webresource.cdn.CDNStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.StringWriter;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

public class TestResourceBaseUrlPatternProvider {
    @Mock
    private WebResourceIntegration webResourceIntegration;
    @Mock
    private CDNStrategy cdnStrategy;

    @Before
    public void injectMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void cdnBaseUrlIsIncludedWhenCdnIsEnabled() throws IOException
    {
        String contextPath = "/jeera";
        String cdnBaseUrl = "//something.cloudfront.net/customer.atlassian.net" + contextPath;
        when(cdnStrategy.supportsCdn()).thenReturn(true);
        when(cdnStrategy.transformRelativeUrl(contextPath)).thenReturn(cdnBaseUrl);
        when(webResourceIntegration.getCDNStrategy()).thenReturn(cdnStrategy);
        when(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn(contextPath);

        ResourceBaseUrlPatternProvider provider = new ResourceBaseUrlPatternProvider(webResourceIntegration);
        StringWriter writer = new StringWriter();
        provider.get().write(writer);
        String pattern = writer.getBuffer().toString();

        assertEquals("\"(?:/jeera(?:/s/.*?/_)?/download|//something\\\\.cloudfront\\\\.net/customer\\\\.atlassian\\\\.net/jeera(?:/s/.*?/_)?/download)\"", pattern);
    }

    @Test
    public void cdnBaseUrlIsExcludedWhenCdnIsDisabledViaSupportsCdnApi() throws IOException
    {
        String contextPath = "/jeera";
        when(cdnStrategy.supportsCdn()).thenReturn(false);
        when(webResourceIntegration.getCDNStrategy()).thenReturn(cdnStrategy);
        when(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn(contextPath);

        ResourceBaseUrlPatternProvider provider = new ResourceBaseUrlPatternProvider(webResourceIntegration);
        StringWriter writer = new StringWriter();
        provider.get().write(writer);
        String pattern = writer.getBuffer().toString();

        assertEquals("\"(?:/jeera(?:/s/.*?/_)?/download)\"", pattern);
    }

    @Test
    public void cdnBaseUrlIsExcludedWhenCdnIsDisabledViaNoCdnStrategy() throws IOException
    {
        String contextPath = "/jeera";
        when(webResourceIntegration.getCDNStrategy()).thenReturn(null);
        when(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn(contextPath);

        ResourceBaseUrlPatternProvider provider = new ResourceBaseUrlPatternProvider(webResourceIntegration);
        StringWriter writer = new StringWriter();
        provider.get().write(writer);
        String pattern = writer.getBuffer().toString();

        assertEquals("\"(?:/jeera(?:/s/.*?/_)?/download)\"", pattern);
    }

    @Test
    public void providerDoesNotCloseOutputStream() throws IOException
    {
        when(webResourceIntegration.getCDNStrategy()).thenReturn(null);
        when(webResourceIntegration.getBaseUrl(UrlMode.RELATIVE)).thenReturn("/jeera");

        ResourceBaseUrlPatternProvider provider = new ResourceBaseUrlPatternProvider(webResourceIntegration);
        VisiblyClosedStringWriter writer = new VisiblyClosedStringWriter();
        provider.get().write(writer);
        assertFalse(writer.isClosed);
    }

    private class VisiblyClosedStringWriter extends StringWriter {
        public boolean isClosed = false;

        @Override
        public void close() throws IOException
        {
            super.close();
            isClosed = true;
        }
    }
}
