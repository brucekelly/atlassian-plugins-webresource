package com.atlassian.webresource.plugin.rest;

import com.atlassian.webresource.plugin.async.AsyncWebResourceLoader;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.collections.Sets;

import static org.mockito.Mockito.verify;

public class TestListOfResources {
    @Mock
    private AsyncWebResourceLoader asyncWebResourceLoader;

    @Before
    public void injectMocks()
    {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getDecodesCommaSeparatedValues() throws Exception
    {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("wr1,wr2", "c1,c2", "xr1,xr2", "xc1,xc2");
        verify(asyncWebResourceLoader).resolve(Sets.newSet("wr1", "wr2"), Sets.newSet("c1", "c2"),
                Sets.newSet("xr1", "xr2"), Sets.newSet("xc1", "xc2"));
    }

    @Test
    public void getDecodesSingleValues() throws Exception
    {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("wr1", "c1", "xr1", "xc1");
        verify(asyncWebResourceLoader).resolve(Sets.newSet("wr1"), Sets.newSet("c1"),
                Sets.newSet("xr1"), Sets.newSet("xc1"));
    }

    @Test
    public void getDecodesEmptyValues() throws Exception
    {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.get("", "", "", "");
        verify(asyncWebResourceLoader).resolve(Sets.<String>newSet(), Sets.<String>newSet(),
                Sets.<String>newSet(), Sets.<String>newSet());
    }

    @Test
    public void postPassesRequestArgumentsThroughToAsyncWebResourceLoader() throws Exception
    {
        ListOfResources listOfResources = new ListOfResources(asyncWebResourceLoader);
        listOfResources.post(new ListOfResources.Request(
                Lists.newArrayList("wr1", "wr2"),
                Lists.newArrayList("c1", "c2"),
                Lists.newArrayList("xr1", "xr2"),
                Lists.newArrayList("xc1", "xc2")));
        verify(asyncWebResourceLoader).resolve(Sets.newSet("wr1", "wr2"), Sets.newSet("c1", "c2"),
                Sets.newSet("xr1", "xr2"), Sets.newSet("xc1", "xc2"));
    }

    @Test
    public void requestCanBeInstantiatedWithoutArgumentsAsRequiredByJersey()
    {
        new ListOfResources.Request();
    }
}
