WRM.curl(["wrm/_", "wrm/builder"], function(_, Builder) {
    module("Web Resource Manager Builder", {
        setup: function() {
            this.el = document.createElement("div");
        },
        addScript: function(urls) {
            if (!_.isArray(urls)) {
                urls = [urls];
            }
            _.each(urls, _.bind(function(url) {
                var script = document.createElement("script");
                script.src = url;
                this.el.appendChild(script);
            }, this));
        },
        addStylesheet: function(urls) {
            if (!_.isArray(urls)) {
                urls = [urls];
            }
            _.each(urls, _.bind(function(url) {
                var link = document.createElement("link");
                link.rel = "stylesheet";
                link.href = url;
                this.el.appendChild(link);
            }, this));
        },
        getSuperBatchJs: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/e8875559670682df0292c55a2c212d99/_/download/superbatch/js/batch.js?locale=en-US";
        },
        getSuperBatchCss: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/e8875559670682df0292c55a2c212d99/_/download/superbatch/css/batch.css";
        },
        getContextBatchJs: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/e8875559670682df0292c55a2c212d99/_/download/contextbatch/js/jira.general/batch.js?locale=en-US";
        },
        getContextBatchCss: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/e8875559670682df0292c55a2c212d99/_/download/contextbatch/css/jira.general/batch.css";
        },
        getContextBatchMultiJs: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/8a25a3496ac214f7e4163b2fe8ade38d/_/download/contextbatch/js/greenhopper-rapid-non-gadget,atl.general/batch.js?locale=en-US";
        },
        getContextBatchMultiCss: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/8a25a3496ac214f7e4163b2fe8ade38d/_/download/contextbatch/css/greenhopper-rapid-non-gadget,atl.general/batch.css";
        },
        getResourceBatchJs: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/6.0-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-header-plugin:jira-header/com.atlassian.jira.jira-header-plugin:jira-header.js?locale=en-US";
        },
        getResourceBatchCss: function(contextPath) {
            return contextPath + "/s/en_US-um72o-418945332/6010/1191/6.0-SNAPSHOT/_/download/batch/com.atlassian.jira.jira-header-plugin:jira-header/com.atlassian.jira.jira-header-plugin:jira-header.css";
        },
        getNoBatchJs: function(contextPath) {
            return [
                contextPath + "/s/en_US-s9ga4r-418945332/812/3/1.1.29/_/download/resources/com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources/subdir/whats-new-template.js",
                contextPath + "/s/en_US-s9ga4r-418945332/812/3/1.1.29/_/download/resources/com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources/whats-new.js"
            ];
        },
        getNoBatchCss: function(contextPath) {
            return [
                contextPath + "/s/en_US-s9ga4r-418945332/812/3/1.1.29/_/download/resources/com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources/subdir/whats-new-template.css",
                contextPath + "/s/en_US-s9ga4r-418945332/812/3/1.1.29/_/download/resources/com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources/whats-new.css"
            ];
        },
        assertModulesAndContexts: function(actual, expectedSuperBatch, expectedModules, expectedContexts) {
            var expected = {
                superBatch: expectedSuperBatch || false,
                modules: expectedModules || [],
                contexts: expectedContexts || []
            };
            deepEqual(actual, expected);
        }
    });

    test("JS loading", function() {
        this.addScript(this.getSuperBatchJs(""));
        this.addScript(this.getContextBatchJs(""));
        this.addScript(this.getContextBatchMultiJs(""));
        this.addScript(this.getResourceBatchJs(""));
        this.addScript(this.getNoBatchJs(""));

        var loaded = new Builder("/s/.*?/_/download").initialize(this.el);

        ok(loaded.superBatch, "Expected superbatch");
        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("CSS loading", function() {
        this.addStylesheet(this.getSuperBatchCss(""));
        this.addStylesheet(this.getContextBatchCss(""));
        this.addStylesheet(this.getContextBatchMultiCss(""));
        this.addStylesheet(this.getResourceBatchCss(""));
        this.addStylesheet(this.getNoBatchCss(""));

        var loaded = new Builder("/s/.*?/_/download").initialize(this.el);

        ok(loaded.superBatch, "Expected superbatch");
        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("JS + CSS loading", function() {
        this.addStylesheet(this.getSuperBatchCss(""));
        this.addStylesheet(this.getContextBatchCss(""));
        this.addStylesheet(this.getContextBatchMultiCss(""));
        this.addStylesheet(this.getResourceBatchCss(""));
        this.addStylesheet(this.getNoBatchCss(""));
        this.addScript(this.getSuperBatchJs(""));
        this.addScript(this.getContextBatchJs(""));
        this.addScript(this.getContextBatchMultiJs(""));
        this.addScript(this.getResourceBatchJs(""));
        this.addScript(this.getNoBatchJs(""));

        var loaded = new Builder("/s/.*?/_/download").initialize(this.el);

        ok(loaded.superBatch, "Expected superbatch");
        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("JS + CSS loading with contextPath", function() {
        this.addStylesheet(this.getSuperBatchCss("/jira"));
        this.addStylesheet(this.getContextBatchCss("/jira"));
        this.addStylesheet(this.getContextBatchMultiCss("/jira"));
        this.addStylesheet(this.getResourceBatchCss("/jira"));
        this.addStylesheet(this.getNoBatchCss("/jira"));
        this.addScript(this.getSuperBatchJs("/jira"));
        this.addScript(this.getContextBatchJs("/jira"));
        this.addScript(this.getContextBatchMultiJs("/jira"));
        this.addScript(this.getResourceBatchJs("/jira"));
        this.addScript(this.getNoBatchJs("/jira"));

        var loaded = new Builder("/jira/s/.*?/_/download").initialize(this.el);

        ok(loaded.superBatch, "Expected superbatch");
        equal(loaded.contexts.length, 3, "Expected 1 loaded contexts");
        equal(loaded.contexts[0], "jira.general", "Expected context batch");
        equal(loaded.contexts[1], "greenhopper-rapid-non-gadget", "Expected context batch");
        equal(loaded.contexts[2], "atl.general", "Expected context batch");
        equal(loaded.modules.length, 2, "Expected 2 loaded contexts");
        equal(loaded.modules[0], "com.atlassian.jira.jira-header-plugin:jira-header", "Expected web resource batch");
        equal(loaded.modules[1], "com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources", "Expected unbatched");
    });

    test("builder.getModulesAndContextsFromScriptUrl", function() {
        var builder = new Builder("/s/.*?/_/download");
        var assertMAC = _.bind(function(url, expectedModules, expectedContexts) {
            this.assertModulesAndContexts(builder.getModulesAndContextsFromScriptUrl(url), false, expectedModules, expectedContexts);
        }, this);

        assertMAC(this.getContextBatchJs(""), [], ["jira.general"]);
        assertMAC(this.getContextBatchMultiJs(""), [], ["greenhopper-rapid-non-gadget", "atl.general"]);
        assertMAC(this.getResourceBatchJs(""), ["com.atlassian.jira.jira-header-plugin:jira-header"], []);
        assertMAC(this.getNoBatchJs("")[0], ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"], []);
        assertMAC(this.getNoBatchJs("")[1], ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"], []);
    });

    test("builder.getModulesAndContextsFromScriptUrl with context", function() {
        var builder = new Builder("/jira/s/.*?/_/download");
        var assertMAC = _.bind(function(url, expectedModules, expectedContexts) {
            this.assertModulesAndContexts(builder.getModulesAndContextsFromScriptUrl(url), false, expectedModules, expectedContexts);
        }, this);

        assertMAC(this.getContextBatchJs("/jira"), [], ["jira.general"]);
        assertMAC(this.getContextBatchMultiJs("/jira"), [], ["greenhopper-rapid-non-gadget", "atl.general"]);
        assertMAC(this.getResourceBatchJs("/jira"), ["com.atlassian.jira.jira-header-plugin:jira-header"], []);
        assertMAC(this.getNoBatchJs("/jira")[0], ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"], []);
        assertMAC(this.getNoBatchJs("/jira")[1], ["com.atlassian.jira.welcome.jira-welcome-plugin:whats-new-resources"], []);
    });
});
