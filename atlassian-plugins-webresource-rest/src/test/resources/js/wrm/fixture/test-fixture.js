WRM.define("wrm/fixture/test-fixture", ["wrm/jquery"], function($) {

    return {
        stubIE8: function() {
            this.stubBrowserMsie(true);
            this.stubBrowserVersion("8");
        },
        stubIE9: function() {
            this.stubBrowserMsie(true);
            this.stubBrowserVersion("9");
        },
        stubIE10: function() {
            this.stubBrowserMsie(true);
            this.stubBrowserVersion("10");
        },
        stubNonIE: function() {
            this.stubBrowserMsie(false);
            this.stubBrowserVersion("0");
        },
        stubBrowserMsie: function(isMsie) {
            if (!this._originalBrowserMsie) {
                this._originalBrowserMsie = $.browser.msie;
            }
            $.browser.msie = isMsie;
        },
        stubBrowserVersion: function(version) {
            if (!this._originalBrowserVersion) {
                this._originalBrowserVersion = $.browser.version;
            }
            $.browser.version = version;
        },
        restore: function() {
            if (this._originalBrowserMsie) {
                $.browser.msie = this._originalBrowserMsie;
            }
            if (this._originalBrowserVersion) {
                $.browser.version = this._originalBrowserVersion;
            }
        }
    };
});
