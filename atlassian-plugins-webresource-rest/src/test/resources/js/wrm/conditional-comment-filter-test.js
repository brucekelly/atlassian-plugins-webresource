WRM.curl(["wrm/conditional-comment-filter", "wrm/fixture/test-fixture"], function(conditionalCommentFilter, testFixture) {
    module("Web Resource Manager Conditional Comment Filter", {
        teardown: function() {
            testFixture.restore();
        },
        assertResults: function(resources, expectedResources) {
            var filteredResources = conditionalCommentFilter(resources);
            deepEqual(filteredResources, expectedResources);
        },
        createTestConditionals: function() {
            return {
                all1: this.createNonConditional("Aa"),
                all2: this.createNonConditional("Ab"),

                ltIe8: this.createConditionalLtIE8("8a"),
                lteIe8: this.createConditionalLteIE8("8b"),
                ie8: this.createConditionalIE8("8c"),
                gtIe8: this.createConditionalGtIE8("8d"),
                gteIe8: this.createConditionalGteIE8("8e"),

                ltIe9: this.createConditionalLtIE9("9a"),
                lteIe9: this.createConditionalLteIE9("9b"),
                ie9: this.createConditionalIE9("9c"),
                gtIe9: this.createConditionalGtIE9("9d"),
                gteIe9: this.createConditionalGteIE9("9e")
            };
        },
        createNonConditional: function(url) {
            return { url: url };
        },
        createConditionalLtIE8: function(url) {
            return { url: url, conditionalComment: "lt IE 8" };
        },
        createConditionalLteIE8: function(url) {
            return { url: url, conditionalComment: "lte IE 8" };
        },
        createConditionalIE8: function(url) {
            return { url: url, conditionalComment: "IE 8" };
        },
        createConditionalGtIE8: function(url) {
            return { url: url, conditionalComment: "gt IE 8" };
        },
        createConditionalGteIE8: function(url) {
            return { url: url, conditionalComment: "gte IE 8" };
        },
        createConditionalLtIE9: function(url) {
            return { url: url, conditionalComment: "lt IE 9" };
        },
        createConditionalLteIE9: function(url) {
            return { url: url, conditionalComment: "lte IE 9" };
        },
        createConditionalIE9: function(url) {
            return { url: url, conditionalComment: "IE 9" };
        },
        createConditionalGtIE9: function(url) {
            return { url: url, conditionalComment: "gt IE 9" };
        },
        createConditionalGteIE9: function(url) {
            return { url: url, conditionalComment: "gte IE 9" };
        }
    });

    test("Test non-IE", function() {
        testFixture.stubNonIE();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ltIe8], []);
        this.assertResults([c.lteIe8], []);
        this.assertResults([c.ie8], []);
        this.assertResults([c.gtIe8], []);
        this.assertResults([c.gteIe8], []);

        this.assertResults([c.ltIe9], []);
        this.assertResults([c.lteIe9], []);
        this.assertResults([c.ie9], []);
        this.assertResults([c.gtIe9], []);
        this.assertResults([c.gteIe9], []);
    });

    test("Test IE 10", function() {
        testFixture.stubIE10();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ltIe8], []);
        this.assertResults([c.lteIe8], []);
        this.assertResults([c.ie8], []);
        this.assertResults([c.gtIe8], []);
        this.assertResults([c.gteIe8], []);

        this.assertResults([c.ltIe9], []);
        this.assertResults([c.lteIe9], []);
        this.assertResults([c.ie9], []);
        this.assertResults([c.gtIe9], []);
        this.assertResults([c.gteIe9], []);
    });

    test("Test IE 9", function() {
        testFixture.stubIE9();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ltIe8], []);
        this.assertResults([c.lteIe8], []);
        this.assertResults([c.ie8], []);
        this.assertResults([c.gtIe8], [c.gtIe8]);
        this.assertResults([c.gteIe8], [c.gteIe8]);

        this.assertResults([c.ltIe9], []);
        this.assertResults([c.lteIe9], [c.lteIe9]);
        this.assertResults([c.ie9], [c.ie9]);
        this.assertResults([c.gtIe9], []);
        this.assertResults([c.gteIe9], [c.gteIe9]);
    });

    test("Test IE 8", function() {
        testFixture.stubIE8();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ltIe8], []);
        this.assertResults([c.lteIe8], [c.lteIe8]);
        this.assertResults([c.ie8], [c.ie8]);
        this.assertResults([c.gtIe8], []);
        this.assertResults([c.gteIe8], [c.gteIe8]);

        this.assertResults([c.ltIe9], [c.ltIe9]);
        this.assertResults([c.lteIe9], [c.lteIe9]);
        this.assertResults([c.ie9], []);
        this.assertResults([c.gtIe9], []);
        this.assertResults([c.gteIe9], []);
    });
});

