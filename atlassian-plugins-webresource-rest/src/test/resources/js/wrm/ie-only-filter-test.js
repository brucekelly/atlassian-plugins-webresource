WRM.curl(["wrm/ie-only-filter", "wrm/fixture/test-fixture"], function(ieOnlyFilter, testFixture) {
    module("Web Resource Manager IEOnly Filter", {
        teardown: function() {
            testFixture.restore();
        },
        assertResults: function(resources, expectedResources) {
            var filteredResources = ieOnlyFilter(resources);
            deepEqual(filteredResources, expectedResources);
        },
        createTestConditionals: function() {
            return {
                all1: this.createNonConditional("Aa"),
                all2: this.createNonConditional("Ab"),

                ieOnly: this.createConditionalIeOnly("ieOnly"),
                ieOnlyFalse: this.createConditionalIeOnlyFalse("ieOnlyFalse")
            };
        },
        createNonConditional: function(url) {
            return { url: url };
        },
        createConditionalIeOnly: function(url) {
            return { url: url, ieOnly: true };
        },
        createConditionalIeOnlyFalse: function(url) {
            return { url: url, ieOnly: false };
        }
    });

    test("Test non-IE", function() {
        testFixture.stubNonIE();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ieOnly], []);
        this.assertResults([c.ieOnlyFalse], [c.ieOnlyFalse]);
    });

    test("Test IE", function() {
        testFixture.stubIE9();

        var c = this.createTestConditionals();

        this.assertResults([], []);
        this.assertResults([c.all1, c.all2], [c.all1, c.all2]);

        this.assertResults([c.ieOnly], [c.ieOnly]);
        this.assertResults([c.ieOnlyFalse], [c.ieOnlyFalse]);
    });
});

