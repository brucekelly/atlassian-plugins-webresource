WRM.curl(["wrm/_", "wrm/require-handler", "wrm/fixture/test-fixture"], function(_, RequireHandler, testFixture) {
    module('WRM RequireHandler', {
        setup: function() {
            this.server = sinon.fakeServer.create();
            window.AJS.log = sinon.spy();
            this.previousData = WRM.data;
        },
        teardown: function() {
            this.server.restore();
            testFixture.restore();
            this.wrmMock && this.wrmMock.restore();
            this.findTestLinks().remove();
            WRM.data = this.previousData;
        },
        findTestLinks: function() {
            return AJS.$("head link[href^=test-]");
        },
        stubRequireXHR: function(resources, data, condition) {
            var that = this;
            data = data || {};
            condition = condition || function () { return true; };

            // Setup a collection of XHR handlers, and hook it up to the fake server.
            if (!this.requireXHRHandlers) {
                this.requireXHRHandlers = [];
                this.server.respondWith("POST", /.*webResources.*/, function (xhr) {
                    that.requireXHRHandlers.some(function (handler) {
                        handler(xhr);
                        // break if the handler responded to the XHR
                        return xhr.status == undefined;
                    });
                });
            }

            this.requireXHRHandlers.push(function (xhr) {
                if (condition(xhr)) {
                    xhr.respond(200, { "Content-Type": "application/json" }, JSON.stringify({
                        resources: resources,
                        unparsedData: data
                    }));
                }
            });
        },
        expectCurlInclude: function() {
            if (!this.wrmMock) {
                this.wrmMock = sinon.mock(WRM);
            }
            return this.curlMock = this.wrmMock.expects("curl");
        },
        invokeCurlHander: function() {
            var handler = this.curlMock.lastCall.args[1];
            handler && handler();
        },
        createJs: function(url, conditionalComment, ieOnly) {
            var resource = {
                url: url,
                resourceType: "JAVASCRIPT"
            };
            if (!_.isUndefined(conditionalComment)) {
                resource.conditionalComment = conditionalComment;
            }
            if (!_.isUndefined(ieOnly)) {
                resource.ieOnly = ieOnly;
            }
            return resource;
        },
        createCss: function(url, conditionalComment, ieOnly, media) {
            var resource = {
                url: url,
                resourceType: "CSS"
            };
            if (!_.isUndefined(conditionalComment) && !_.isNull(conditionalComment)) {
                resource.conditionalComment = conditionalComment;
            }
            if (!_.isUndefined(ieOnly) && !_.isNull(ieOnly)) {
                resource.ieOnly = ieOnly;
            }
            if (!_.isUndefined(media) && !_.isNull(media)) {
                resource.media = media;
            }
            return resource;
        }
    });

    test("Base js", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().once().withArgs(["js!blah.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("Base css", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createCss("blah.css")]);
        this.expectCurlInclude().once().withArgs(["css!blah.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("JS + CSS", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js"), this.createCss("blah.css"),
            this.createJs("blah1.js"), this.createCss("blah1.css")]);
        this.expectCurlInclude().once().withArgs(["js!blah.js!order", "css!blah.css", "js!blah1.js!order", "css!blah1.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("ConditionalComment filtered", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js", "IE")]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("IEOnly filtered", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js", null, true)]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
    });

    test("Passed callback is executed", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x", expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        expectRequireSuccess.verify();
    });

    test("Deferred callback is executed", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();
        requireHandler.require("x").done(expectRequireSuccess);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations before first request completes", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("x").done(expectRequireSuccess);
        requireHandler.require("x").done(expectRequireSuccess);
        this.server.respond();

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Caches resource requests for multiple invocations (array version)", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArg(1);
        var expectRequireSuccess = sinon.mock().twice().withExactArgs(window, window, window);
        var requireHandler = new RequireHandler();

        requireHandler.require(["x", "y", "z"]).done(expectRequireSuccess);
        this.server.respond();
        requireHandler.require(["x", "y", "z"]).done(expectRequireSuccess);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireSuccess.verify();
    });

    test("Does not do resource request cache collisions between unrelated objects", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah1.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "xxx";
        });
        this.stubRequireXHR([this.createJs("blah2.js")], null, function (xhr) {
            var resources = JSON.parse(xhr.requestBody).r;
            return resources.length === 1 && resources[0] === "yyy";
        });
        this.expectCurlInclude().twice().callsArg(1);
        var expectRequireSuccess1 = sinon.mock().once().withExactArgs(window);
        var expectRequireSuccess2 = sinon.mock().once().withExactArgs(window);
        var requireHandler = new RequireHandler();

        requireHandler.require("wr!xxx").done(expectRequireSuccess1);
        requireHandler.require("wr!yyy").done(expectRequireSuccess2);
        this.server.respond();

        this.curlMock.verify();
        equal(this.server.requests.length, 2);
        expectRequireSuccess1.verify();
        expectRequireSuccess2.verify();
    });

    test("Fires failure callback if curljs request fails", function() {
        expect(0);
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArgWith(2, "arg1", "arg2");
        var expectRequireFailure = sinon.mock().once().withExactArgs("arg1", "arg2");
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();

        this.curlMock.verify();
        expectRequireFailure.verify();
    });

    test("Failure callback is cached", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")]);
        this.expectCurlInclude().callsArgWith(2, "arg1", "arg2");
        var expectRequireFailure = sinon.mock().twice().withExactArgs("arg1", "arg2");
        var requireHandler = new RequireHandler();

        requireHandler.require("x").fail(expectRequireFailure);
        this.server.respond();
        requireHandler.require("x").fail(expectRequireFailure);

        this.curlMock.verify();
        equal(this.server.requests.length, 1);
        expectRequireFailure.verify();
    });

    test("CSS with media queries are not added via curl.js", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createCss("test-blah.css", null, null, "print")]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();

        var c = AJS.log.callCount;
        this.invokeCurlHander();
        equal(AJS.log.callCount, c + 1);

        var resourceMediaQuery = null;
        AJS.$("head link").each(function() {
            if ($(this).attr("href") === "test-blah.css") {
                resourceMediaQuery = $(this).attr("media");
            }
        });
        equal(resourceMediaQuery, "print");
    });

    test("CSS with media queries are not added via curl.js", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createCss("test-blah.css", null, null, "print")]);
        this.expectCurlInclude().once().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();

        var c = AJS.log.callCount;
        this.invokeCurlHander();
        equal(AJS.log.callCount, c + 1);

        var links = this.findTestLinks();
        equal(links.length, 1);
        equal(links.attr("href"), "test-blah.css");
        equal(links.attr("media"), "print");
    });

    test("CSS with media queries are discovered on subsequent calls", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createCss("test-blah.css", null, null, "print")]);
        this.expectCurlInclude().twice().withArgs([]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();
        this.invokeCurlHander();
        requireHandler.require("y");
        this.server.respond();
        this.invokeCurlHander();

        this.curlMock.verify();

        var links = this.findTestLinks();
        equal(links.length, 1);
        equal(links.attr("href"), "test-blah.css");
        equal(links.attr("media"), "print");
    });

    test("CSS media query check respects media='all'", function() {
        testFixture.stubNonIE();
        this.stubRequireXHR([this.createCss("test-blah.css", null, null, "all")]);
        this.expectCurlInclude().once().withArgs(["css!test-blah.css"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
        equal(this.findTestLinks().length, 0);
    });

    test("JS that has provided data", function() {
        // This case depends on `web-resource/data` module, but we don't have it here, stubbing it instead.
        WRM.data = {
            claim: function(key) { return JSON.parse(WRM._unparsedData[key]) }
        };

        testFixture.stubNonIE();
        this.stubRequireXHR([this.createJs("blah.js")], {"com.foo.x": JSON.stringify({x:"y"})});
        this.expectCurlInclude().once().withArgs(["js!blah.js!order"]);
        var requireHandler = new RequireHandler();

        requireHandler.require("x");
        this.server.respond();

        this.curlMock.verify();
        var claim = WRM.data.claim("com.foo.x");
        equal(claim.x, "y");
    });
});
