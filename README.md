# Atlassian Webresources

## Description

A framework for Atlassian plugins to manage Javascript and CSS resources provided by plugins. The
framework allows for plugins to declare web resources, bind them to contexts in applications, and
set up dependencies so that requiring one resource will also pull in all its dependencies.

Batching of related resources into a single download is also provided.

## JavaScript unit testing

The `atlassian-plugins-webresource-plugin` submodule uses [karma](http://karma-runner.github.io/) for javascript unit testing.
This requires [npm](https://www.npmjs.org/).

The maven build shells out to invoke `npm install` and `npm test` to run the tests. You must have npm installed to do this.

To run tests in watch mode while developing:

- ensure you have invoked: `npm install`,
- then run karma: `./node_modules/.bin/karma start`

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

## Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/PLUGWEBR).

## Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLUGWEB).

## Documentation

- [JIRA Web Resources](https://developer.atlassian.com/display/JIRADEV/Web+Resource+Plugin+Module)
- [Confluence Web Resources](https://developer.atlassian.com/display/CONFDEV/Web+Resource+Module)
