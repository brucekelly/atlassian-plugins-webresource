package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Service for working with the page being rendered by the current HTTP request.
 * <p/>
 * Note, {@link WebResourceAssembler} is currently an experimental API and may break between versions of web resources.
 * It is advised that you use {@link com.atlassian.plugin.webresource.WebResourceManager} in the mean time if you
 * require a stable API.
 *
 * @since v3.0
 */
@ExperimentalApi
public interface PageBuilderService
{
    /**
     * Retrieves the request-local WebResourceAssembler. If none is found, a new instance is constructed and stored in the
     * request-local cached.
     */
    public WebResourceAssembler assembler();

    /**
     * Seeds the request-local WebResourceAssembler. This can be done to allow specific configuration of the
     * request-local assembler - for example, to not include superbatch resources.
     *
     * @throws java.lang.IllegalStateException if the request-local WebResourceAssembler has already been initialised
     * via a call to {@link #seed(WebResourceAssembler)} or to {@link #assembler()}.
     *
     * @deprecated since 3.3.2
     */
    public void seed(WebResourceAssembler webResourceAssembler);
}