package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.webresource.api.UrlMode;
import com.google.common.base.Predicate;

import java.io.Writer;

/**
 * Set of assembled web resources and web resources contexts.
 * @since v3.0
 */
@ExperimentalApi
public interface WebResourceSet
{
    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     * @param writer writer to output to
     * @urlMode url formatting mode
     */
    public void writeHtmlTags(Writer writer, UrlMode urlMode);

    /**
     * Write the HTML tags for this object's resources to the writer. Writing will be done in order of
     * webResourceFormatters so that all CSS resources will be output before Javascript.
     * @param writer writer to output to
     * @urlMode url formatting mode
     * @param predicate only resources matching this predicate will be written
     */
    public void writeHtmlTags(Writer writer, UrlMode urlMode, Predicate<WebResource> predicate);

    /**
     * Returns a list of included WebResources
     * @return included resources
     */
    public Iterable<WebResource> getResources();

    /**
     * Returns a filtered list of included WebResources matching the given class
     * @param clazz class of webresource to return
     * @return included resources
     */
    public <T extends WebResource> Iterable<T> getResources(Class<T> clazz);
}
