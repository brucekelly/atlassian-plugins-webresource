package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Marker interface for resources returned from the web resource system. This includes javascript, css and data
 * resources.
 * @since v3.0
 */
@ExperimentalApi
public interface WebResource
{
}
