package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Map;

/**
 * Parameters that affect a resource that occur outside its querystring - eg conditional comment, media query,
 * and other attributes on an HTML tag eg charset
 * @since v3.0
 */
@ExperimentalApi
public interface PluginUrlResourceParams
{
    /**
     * @return this resource's IE conditionalComment param, otherwise null
     */
    public String conditionalComment();

    /**
     * @return true if this resource is for IE only, otherwise false
     */
    public boolean ieOnly();

    /**
     * @return Any other params (not conditionalComment, ieOnly etc) to be inserted on the HTML tag
     */
    public Map<String, String> other();

    /**
     * @return All params defined for this resource
     */
    public Map<String, String> all();
}
