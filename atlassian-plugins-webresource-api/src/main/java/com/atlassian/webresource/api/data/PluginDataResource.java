package com.atlassian.webresource.api.data;

import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.assembler.WebResource;

/**
 * Container for a data key and value
 * @since v3.0
 */
public interface PluginDataResource extends WebResource
{
    public String getKey();
    public Jsonable getJsonable();
}
