package com.atlassian.webresource.api.bigpipe;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.fugue.Either;

/**
 * Represents a successful or failed invocation of an an asynchronous task.
 * @param <V> type of successful values
 * @since 3.3
 */
@ExperimentalApi
public interface KeyedValue<V>
{
    public Key key();

    public Either<Exception, V> value();
}
