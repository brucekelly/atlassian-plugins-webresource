package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.json.marshal.Jsonable;

import java.util.Set;

/**
 * Interface for clients to require web resources.
 * <p/>
 * Note, {@link RequiredResources} is currently an experimental API and may break between versions of web resources.
 * It is advised that you use {@link com.atlassian.plugin.webresource.WebResourceManager} in the mean time if you
 * require a stable API.
 *
 * @since v3.0
 * @see WebResourceAssembler
 * @see PageBuilderService
 */
@ExperimentalApi
public interface RequiredResources
{
    /**
     * Specifies that resources for the given module should be included on the next call to drainIncludedResources().
     * @param moduleCompleteKey key of webresource module
     * @return this, to support method chaining
     */
    public RequiredResources requireWebResource(String moduleCompleteKey);

    /**
     * Specifies that resources for the given webresource context should be included on the next call to
     * drainIncludedResources().
     * @param context webresource context to include
     * @return this, to support method chaining
     */
    public RequiredResources requireContext(String context);

    /**
     * Specifies that the given resources should be excluded from all future calls to drainIncludedResources().
     * This is equivalent to requiring the given webresources and contexts then calling drain().
     * <p>
     * Any currently un-drained resources/contexts will be remain and be included in the next drain.
     * </p>
     * <p>
     * If this WebResourceAssembler includes implicit resources (e.g. "superbatch"), this call will also exclude all
     * such implicit resources from future calls to drainIncludedResources().
     * </p>
     *
     * @param webResources webresource keys to exclude.
     * @param contexts contexts to exclude.
     * @return this, to support method chaining
     */
    public RequiredResources exclude(Set<String> webResources, Set<String> contexts);
}
