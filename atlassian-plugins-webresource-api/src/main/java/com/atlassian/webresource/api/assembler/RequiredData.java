package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.json.marshal.Jsonable;

import java.util.concurrent.Future;

/**
 * Interface for clients to require data blobs.
 * @since v3.0
 */
@ExperimentalApi
public interface RequiredData
{
   /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     * @param key data key
     * @param content JSON data
     * @return this, to support method chaining
    * @see {{WRM.data()}} JavaScript API
     */
    public RequiredData requireData(String key, Jsonable content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     * @param key data key
     * @param content content to render
     * @return this, to support method chaining
     * @see {{WRM.data()}} JavaScript API
     */
    public RequiredData requireData(String key, Number content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     * @param key data key
     * @param content content to render
     * @return this, to support method chaining
     * @see {{WRM.data()}} JavaScript API
     */
    public RequiredData requireData(String key, String content);

    /**
     * Specifies that JSON data with the given key should be included on the next call to drainIncludedResources().
     * @param key data key
     * @param content content to render
     * @return this, to support method chaining
     * @see {{WRM.data()}} JavaScript API
     */
    public RequiredData requireData(String key, Boolean content);

    /**
     * Specifies that JSON data with the given key should be avaialbe to the client-side webpage at some point
     * in the future. This may be at a subsequent call to drainIncludedResources() if the Future is done, otherwise
     * it will be available asyncrhonously.
     * <p>
     *    The Web Resource mechanism will not keep track of the Future for an indefinite period of time.
     *    If it is not retrieved from the client side, it may be discared due to user logout or due to
     *    a server-configured timeout. Callers of this method should attempt to complete their Futures
     *    within 1-2 mintues.
     * </p>
     *
     * @param key
     * @param content
     * @return
     * @see {{WRM.onData()}} JavaScript API
     */
    public RequiredData requireData(String key, Future<Jsonable> content);

}
