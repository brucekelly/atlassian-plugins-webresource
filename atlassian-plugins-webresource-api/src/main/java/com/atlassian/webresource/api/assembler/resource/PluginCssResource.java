package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

/**
 * A CSS webresource
 * @since v3.0
 */
@ExperimentalApi
public interface PluginCssResource extends PluginUrlResource<PluginCssResourceParams>
{
}
