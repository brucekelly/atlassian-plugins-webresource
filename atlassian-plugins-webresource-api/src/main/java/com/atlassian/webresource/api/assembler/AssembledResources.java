package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Interface for clients to include web resources that have been required via {@link RequiredResources}.
 * <p/>
 * Note, {@link WebResourceAssembler} is currently an experimental API and may break between versions of web resources.
 * It is advised that you use {@link com.atlassian.plugin.webresource.WebResourceManager} in the mean time if you
 * require a stable API.
 *
 * @since v3.0
 * @see WebResourceAssembler
 * @see PageBuilderService
 */
@ExperimentalApi
public interface AssembledResources
{
    /**
     * Calculates dependencies, returns requested resources, then clears the list of webresources + contexts that have
     * been included since the last call to {@link #drainIncludedResources()}.
     * <p />
     * This method ensures that no individual webresource will be emitted twice, either as part of a superbatch,
     * context batch or individual include.
     * <p />
     * The order of resources in the returned list is as follows:
     * <ol>
     *     <li>
     *         If this is the first call and superbatch resources were requested in the factory that constructed this
     *         request, superbatch resources are included
     *     </li>
     *     <li>
     *         Then all required contexts are included
     *     </li>
     *     <li>
     *         Then required webresources are included
     *     </li>
     * </ol>.
     * @return list of plugin resources to emit.
     */
    public WebResourceSet drainIncludedResources();

    /**
     * Returns the currently requested resources as per {@link #drainIncludedResources()}, however does not clear
     * the internal list. Intended to be used to debugging or introspecting the current state.
     */
    public WebResourceSet peek();
}
