package com.atlassian.webresource.api.bigpipe;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.util.concurrent.Promise;

import java.util.concurrent.TimeUnit;

/**
 * Interface for pushing data into and pulling data from the Big Pipe.
 *
 * @since v3.3
 */
@ExperimentalApi
public interface BigPipeManager
{
    /**
     * @param requestId current request id
     * @return an interface for pushing data onto the Big Pipe
     */
    BigPipe getBigPipe(String requestId);

    /**
     * @param requestId request id to consume content from
     * @return an interface for pulling data from the Big Pipe
     */
    ConsumableBigPipe getConsumableBigPipe(String requestId);

    /**
     * Interface for pushing data onto the Big Pipe
     */
    public interface BigPipe
    {
        /**
         * Pushes data onto the big pipe
         * @param key key of content being pushed. Multiple futures with the same key can be pushed;
         *            in this case they will be returned to the client in the order they are completed,
         *            not the order that they are pushed.
         * @param promise content to push
         * @return this BigPipe instance for chaining
         */
        BigPipe push(String key, Promise<Jsonable> promise);

        void cancelRemaining();
    }

    /**
     * Interface for retrieving data from the Big Pipe
     */
    public interface ConsumableBigPipe
    {
        /**
         * @return all available content without blocking. If no content is available, the empty list is returned.
         */
        Iterable<KeyedValue<Jsonable>> getAvailableContent();

        /**
         * Waits for content.
         * <p />
         * If any content is available, this method returns immediately. If content is not available,
         * it blocks until any content becomes available.
         * @param timeout how long to wait before giving up, in units of unit
         * @param unit a TimeUnit determining how to interpret the timeout parameter
         * @return content from the Big Pipe. If no content becomes available before the timeout, the empty
         * list is returned.
         * @throws InterruptedException if interrupted while executing
         */
        Iterable<KeyedValue<Jsonable>> waitForContent(long timeout, TimeUnit unit) throws InterruptedException;

        boolean isComplete();
    }
}
