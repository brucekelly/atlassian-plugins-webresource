package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Stateful component that handles inclusion of css and javascript resources. This may be used as a request-local
 * for rendering the current page but it can also be constructed as a standalone instance for other use cases requiring
 * sets of resources.
 * <p/>
 * Within a single instance of this class, previously drained resources are tracked such that any subsequent calls to
 * {@link #assembled()}.drainIncludedResources() will not return the resources that were previously 'drained' -
 * regardless of any require calls made.
 * <p/>
 * WebResourceAssemblers are constructed by calling {@link WebResourceAssemblerFactory#create()}.
 * <p/>
 * The {@link WebResourceAssembler}, for rendering the current page in the current HTTP request is found by calling
 * {@link PageBuilderService#assembler()}.
 * <p/>
 * Note, {@link WebResourceAssembler} is currently an experimental API and may break between versions of web resources.
 * It is advised that you use {@link com.atlassian.plugin.webresource.WebResourceManager} in the mean time if you
 * require a stable API.
 *
 * @since v3.0
 * @see PageBuilderService
 */
@ExperimentalApi
public interface WebResourceAssembler
{
    /**
     * @return AssembledResources for rendering included resources onto the page
     */
    public AssembledResources assembled();

    /**
     * @return RequiredResources for requesting resources to be included or excluded
     */
    public RequiredResources resources();

    /**
     * @return RequiredData for requesting data to be included or excluded
     */
    public RequiredData data();

    /**
     * @return a deep clone of this WebResourceAssembler's current state, including the list of previously drained
     * resources and resources that have been required by not yet drained. Calls to any methods in the cloned instance
     * will not affect this instance.
     */
    public WebResourceAssembler copy();

}

