package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

/**
* Parameters for a Javascript webresource
* @since v3.0
*/
@ExperimentalApi
public interface PluginJsResourceParams extends PluginUrlResourceParams {}
