package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.webresource.api.UrlMode;
import com.atlassian.webresource.api.assembler.WebResource;

/**
 * Representation of a resource accessible via a URL
 * @since v3.0
 */
@ExperimentalApi
public interface PluginUrlResource<T extends PluginUrlResourceParams> extends WebResource
{
    /**
     * The resource's url.
     * @param urlMode url mode, either absolute or relative
     * @return the url to this resource, prefixed by a static resource. If caching has been disabled for this resource,
     * the raw url without the static prefix is returned.
     */
    public String getStaticUrl(UrlMode urlMode);

    /**
     * Returns params that go outside this url's querystring. Examples are conditional comments, ie only, media queries
     * and attributes that may be added to an HTML tag
     * @return non-querystring params
     */
    public T getParams();
}
