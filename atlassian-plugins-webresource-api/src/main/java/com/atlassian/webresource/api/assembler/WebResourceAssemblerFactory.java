package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Factory for creating web resource builders.
 *
 * @since v3.0
 */
@ExperimentalApi
public interface WebResourceAssemblerFactory
{
    /**
     * Creates a new WebResourceAssemblerParams for constructing a builder.
     * @return builder instance
     */
    public WebResourceAssemblerBuilder create();

    /**
     * Clears the web resource cache maintained by this factory.
     *
     * @deprecated since v3.3.2
     */
    @Deprecated
    public void clearCache();
}
