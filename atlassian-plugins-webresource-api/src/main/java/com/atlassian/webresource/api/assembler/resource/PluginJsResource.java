package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

/**
 * A Javascript webresource
 * @since v3.0
 */
@ExperimentalApi
public interface PluginJsResource extends PluginUrlResource<PluginJsResourceParams>
{
}
