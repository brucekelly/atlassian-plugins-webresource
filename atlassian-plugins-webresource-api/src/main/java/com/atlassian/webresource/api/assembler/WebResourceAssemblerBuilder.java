package com.atlassian.webresource.api.assembler;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Builder for constructing WebResourceAssembler
 * @since v3.0
 */
@ExperimentalApi
public interface WebResourceAssemblerBuilder
{
    /**
     * In addition to any resources explicitly added to the WebResourceAssembler, always implicitly include
     * resources from the superbatch.
     * When disabled, superbatch resources are not implicitly included, but may be individually (and explicitly) included
     * by requiring them with the WebResourceAssembler.
     */
    public WebResourceAssemblerBuilder includeSuperbatchResources(boolean include);
    /**
     * Constructs a WebResourceAssembler
     * @return a WebResourceAssembler
     */
    public WebResourceAssembler build();

}
