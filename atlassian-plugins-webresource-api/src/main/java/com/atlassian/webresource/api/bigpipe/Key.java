package com.atlassian.webresource.api.bigpipe;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Typed key
 * @since 3.3
 */
@ExperimentalApi
public interface Key extends Comparable<Key>
{
    Comparable key();
}
