package com.atlassian.webresource.api.data;

import com.atlassian.json.marshal.Jsonable;
import com.google.common.base.Supplier;

/**
 * Interface for providing JSON data to a client.
 *
 * Instances of this class are constructed at plugin enablement time.
 *
 * @since v3.0
 */
public interface WebResourceDataProvider extends Supplier<Jsonable>
{
}
