package com.atlassian.webresource.api.assembler.resource;

import com.atlassian.annotations.ExperimentalApi;

/**
* Parameters for a CSS webresource
* @since v3.0
*/
@ExperimentalApi
public interface PluginCssResourceParams extends PluginUrlResourceParams
{
    /**
     * @return this resource's media param, otherwise null
     */
    public String media();
}
